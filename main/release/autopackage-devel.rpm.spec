##
# Build Package Variables
##
%define name 		autopackage-devel
%define version 	%AutopackageVersion%
%define release 	1


##
# Build Environment Variables
##
# use autoconf macros as defined: _prefix _exec_prefix _bindir _datadir
# reference: /usr/lib/rpm/macros
# %_prefix                /usr
# %_exec_prefix           %{_prefix}
# %_bindir                %{_exec_prefix}/bin
# %_datadir               %{_prefix}/share

%define _apkgprefix 	%_AutopackagePrefix%
%define _apkgbindir 	%{_apkgprefix}/bin
%define _apkgdatadir 	%{_apkgprefix}/share
# fixup libexec which on MDK10 wants to go to /lib
%define _libexecdir 	%{_apkgprefix}/libexec
%define _prefix 	/usr/local
%define _sysconfdir 	/etc
%define _tmppath 	%Home%/rpm/tmp
%define _topdir 	%Home%/rpm


##
# Package Data
##
Name: 			%{name}
Summary: 		Development tools for autopackage distribution neutral packaging framework.
Version: 		%{version}
Release: 		%{release}
Source: 		http://ftp.sunsite.dk/projects/autopackage/%{version}/%{name}.tar.bz2
URL: 			http://autopackage.org/
Group: 			System/Configuration/Packaging
Prefix: 		%{_prefix}
BuildRoot: 	%{_tmppath}/%{name}-buildroot/
BuildArch: 	i386
License: 		LGPL
Requires: 		autopackage = %AutopackageVersion%
AutoReqProv: 	no

Vendor: 		autopackage.org
Packager: 		Curtis L. Knight <knighcl@gmail.com>


%description
Autopackage enables developers to build packages that
will install on many different Linux distributions.

Autopackage enables users to install packages on many different Linux
distributions.

Autopackage supports multiple languages (build and installation) and
automatically verifies/resolves dependencies independent of their
installation method.


%prep


%setup -q -n autopackage


%build
%{__mkdir_p} %{buildroot}/%{_apkgdatadir}/autopackage
%{__mkdir_p} %{buildroot}/%{_apkgbindir}

%{__mv} share/*.template share/skeletons	./
%{__rm} -rf share

# remove extra source tarball files
%{__rm} -f Makefile README

%{__cp} -r *					%{buildroot}/%{_apkgdatadir}/autopackage/


%install
%{__rm} -rf $RPM_BUILD_ROOT

cd apbuild
make PREFIX=$RPM_BUILD_ROOT%{_prefix} install
cd ..
%{__rm} -rf apbuild

%{__mkdir_p} $RPM_BUILD_ROOT/%{_prefix}/binreloc
%{__cp}  -r binreloc/*			$RPM_BUILD_ROOT/%{_prefix}/binreloc
%{__rm} -rf binreloc

cd gtk-headers
make PREFIX=$RPM_BUILD_ROOT%{_prefix} install
cd ..
%{__rm} -rf gtk-headers

cd lzma
make
make PREFIX=$RPM_BUILD_ROOT%{_prefix} install
cd ..
%{__rm} -rf lzma

%{__mkdir_p} $RPM_BUILD_ROOT/%{_apkgdatadir}/autopackage
%{__mkdir_p} $RPM_BUILD_ROOT/%{_apkgbindir}

%{__mkdir_p} $RPM_BUILD_ROOT/%{_libexecdir}/autopackage
%{__cp} libexec/*				$RPM_BUILD_ROOT/%{_libexecdir}/autopackage/
%{__rm} -rf libexec

%{__cp} -r *					$RPM_BUILD_ROOT/%{_apkgdatadir}/autopackage

# move makeinstaller to the autopackage bindir
%{__mv} $RPM_BUILD_ROOT/%{_apkgdatadir}/autopackage/makepackage		$RPM_BUILD_ROOT/%{_apkgbindir}/

# Generate file list from build root tree
find $RPM_BUILD_ROOT -type f \( -name \* \) -print \
    | %{__sed} "s#^$RPM_BUILD_ROOT/*#/#" > filelisting


%clean
%{__rm} -rf $RPM_BUILD_ROOT


%files -f filelisting
%defattr(-,root,root)


%changelog
* Tue Jul 10 2007  Curtis L. Knight <knighcl@gmail.com>  1.2.5-1
- Rebuild for autopackage 1.2.5 release.

* Sat Aug 04 2007  Curtis L. Knight <knighcl@gmail.com>  1.2.4-2
- Rebuild because makepackage had some commented code that did not allow
  sealed packages to be created.

* Fri May 24 2007  Curtis L. Knight <knighcl@gmail.com>  1.2.4-1
- Rebuild for autopackage 1.2.4 release.

* Sun Mar 25 2007  Curtis L. Knight <knighcl@fastmail.fm>  1.2.3-1
- Rebuild for autopackage 1.2.3 release.

* Sun Nov 19 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.2.2-1
- Rebuild for autopackage 1.2.2 release.

* Fri Nov 03 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.2.1-1
- Rebuild for autopackage 1.2.1 release.

* Wed Aug 09 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.2-1
- Rebuild for autopackage 1.2 release.

* Thu Apr 13 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.1.4-1
- Rebuild for autopackage 1.1.4 release.

* Sat Dec 17 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.2-1
- Rebuild for autopackage 1.2 release.

* Sun Nov 13 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.3-1
- Rebuild for autopackage 1.1.3 release.

* Thu Oct 27 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.2-1
- Rebuild for autopackage 1.1.2 release.

* Sun Aug 21 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.1-1
- Rebuild for autopackage 1.1.1 release.

* Wed Jun 01 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.0-1
- Rebuild for autopackage 1.1.0 release.
- Default installed file to root user.

* Sun Apr 10 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0.2-1
- Rebuild for autopackage 1.0.2 release.

* Sat Apr 02 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0.1-1
- Rebuild for autopackage 1.0.1 release.

* Sat Mar 19 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-5
- Rebuild for next autopackage RC release.

* Mon Mar 07 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-3
- Regenerated for libuau.so.3 which was rebuilt against libcurl.so.2 .
- Pickup 1.0RC2 release.

* Sun Feb 13 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-1
- Move objects from source share directory to root install directory.
- Remove collision of files with autopackage rpm.

* Sun Jan  9 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-1
- Updated for source developer archive file name to be without a version.
- No longer has collisions with autopackage rpm.

* Sat Nov 13 2004  Curtis L. Knight <knighcl@fastmail.fm>  0.7-1
- Initial rpm package release to coincide with 0.7 release.
