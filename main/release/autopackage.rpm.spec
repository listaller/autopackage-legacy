##
# Build Package Variables
##
%define name 		autopackage
%define version 	%AutopackageVersion%
%define release 	1


##
# Build Environment Variables
##
# use autoconf macros as defined: _prefix _exec_prefix _bindir _libexecdir _datadir _sysconfdir
# reference: /usr/lib/rpm/macros
# %_prefix                /usr
# %_exec_prefix           %{_prefix}
# %_bindir                %{_exec_prefix}/bin
# %_libexecdir            %{_exec_prefix}/libexec
# %_datadir               %{_prefix}/share
# %_sysconfdir            %{_prefix}/etc

# fixup libexec which on MDK10 wants to go to /lib
%define _libexecdir 	%{_exec_prefix}/libexec
%define _prefix 		%_AutopackagePrefix%
%define _sysconfdir 	/etc
%define _tmppath 		%Home%/rpm/tmp
%define _topdir 		%Home%/rpm


##
# Package Data
##
Name: 			%{name}
Summary: 		Distribution neutral packaging framework for Linux systems.
Version: 		%{version}
Release: 		%{release}
Source: 		http://ftp.sunsite.dk/projects/autopackage/%{version}/%{name}.tar.bz2
URL: 			http://autopackage.org/
Group: 			System/Configuration/Packaging
Prefix: 		%{_prefix}
BuildRoot: 	%{_tmppath}/%{name}-buildroot/
BuildArch: 	i386
License: 		LGPL
Requires: 		bash >= 2.0 grep sed gawk bzip2 tar make libglib-2.0.so.0 libz.so.1 libxml2.so.2 libc.so.6(GLIBC_2.0) libc.so.6(GLIBC_2.1) libc.so.6(GLIBC_2.1.2) libc.so.6(GLIBC_2.1.3) libc.so.6(GLIBC_2.2) libc.so.6(GLIBC_2.2.4)
AutoReqProv: 	no

Vendor: 		autopackage.org
Packager: 		Curtis L. Knight <knighcl@gmail.com>


%description
Autopackage enables developers to build packages that
will install on many different Linux distributions.

Autopackage enables users to install packages on many different Linux
distributions.

Autopackage supports multiple languages (build and installation) and
automatically verifies/resolves dependencies independent of their
installation method.


%prep


%setup -q -n %{name}


%build
%{__mkdir_p} %{buildroot}/%{_bindir}
%{__mkdir_p} %{buildroot}/%{_sysconfdir}/%{name}
%{__mkdir_p} %{buildroot}/%{_libexecdir}/%{name}
%{__mkdir_p} %{buildroot}/%{_datadir}/%{name}

%{__cp} package					%{buildroot}/%{_bindir}/package
%{__cp} etc/*					%{buildroot}/%{_sysconfdir}/%{name}/
%{__cp} libexec/*				%{buildroot}/%{_libexecdir}/%{name}/
%{__cp} -r share/*				%{buildroot}/%{_datadir}/%{name}/
%{__mv} %{buildroot}/%{_datadir}/%{name}/locale	%{buildroot}/%{_datadir}/locale


%install
%{__rm} -rf $RPM_BUILD_ROOT

%{__mkdir_p} $RPM_BUILD_ROOT/%{_bindir}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_sysconfdir}/%{name}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_libexecdir}/%{name}
%{__mkdir_p} $RPM_BUILD_ROOT/%{_datadir}/%{name}

%{__cp} package					$RPM_BUILD_ROOT/%{_bindir}/package
%{__cp} etc/*					$RPM_BUILD_ROOT/%{_sysconfdir}/%{name}/
%{__cp} libexec/*				$RPM_BUILD_ROOT/%{_libexecdir}/%{name}/
%{__cp} -r share/locale			$RPM_BUILD_ROOT/%{_datadir}
%{__rm} -rf share/locale
%{__cp} share/*					$RPM_BUILD_ROOT/%{_datadir}/%{name}/

# remove script defines installation from install script
%{__rm} $RPM_BUILD_ROOT/%{_datadir}/%{name}/remove

%{__ln_s} libcurl.so.2.0.2			$RPM_BUILD_ROOT/%{_libexecdir}/%{name}/libcurl.so.2
%{__ln_s} libuau.so.3.0.0			$RPM_BUILD_ROOT/%{_libexecdir}/%{name}/libuau.so.3

# fixup installation prefix in config - since it is RPM hardcoded
# we can do it here instead of in the %post directive
data=`%{__sed} "s|%AutopackagePrefix%|%{_exec_prefix}|g" "$RPM_BUILD_ROOT/%{_sysconfdir}/%{name}/config"`
echo "$data" > "$RPM_BUILD_ROOT/%{_sysconfdir}/%{name}/config"


%clean
%{__rm} -rf $RPM_BUILD_ROOT


%files
%defattr(0644,root,root,0755)
%{_datadir}/%{name}/apkg-bashlib
%{_datadir}/%{name}/apkg-db
%{_datadir}/%{name}/apkg-defs
%{_datadir}/%{name}/apkg-dep
%{_datadir}/%{name}/apkg-failsafelib
%{_datadir}/%{name}/apkg-funclib
%{_datadir}/%{name}/apkg-io
%{_datadir}/%{name}/apkg-native
%{_datadir}/%{name}/apkg-script-utils
%{_datadir}/locale/*/LC_MESSAGES/*
%attr(0755,root,root) %{_bindir}/*
%attr(0755,root,root) %{_datadir}/%{name}/apkg-ttyfe
%attr(0755,root,root) %{_libexecdir}/%{name}/*
%config %{_sysconfdir}/%{name}/*


%changelog
* Tue Jul 10 2007  Curtis L. Knight <knighcl@gmail.com>  1.2.5-1
- Rebuild for autopackage 1.2.5 release.

* Fri May 24 2007  Curtis L. Knight <knighcl@gmail.com>  1.2.4-1
- Rebuild for autopackage 1.2.4 release.

* Sun Mar 25 2007  Curtis L. Knight <knighcl@fastmail.fm>  1.2.3-1
- Rebuild for autopackage 1.2.3 release.

* Sun Nov 19 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.2.2-1
- Rebuild for autopackage 1.2.2 release.

* Fri Nov 03 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.2.1-1
- Rebuild for autopackage 1.2.1 release.

* Wed Aug 09 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.2-1
- Rebuild for autopackage 1.2 release.

* Thu Apr 13 2006  Curtis L. Knight <knighcl@fastmail.fm>  1.1.4-1
- Rebuild for autopackage 1.1.4 release.

* Sat Dec 17 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.2-1
- Rebuild for autopackage 1.2 release.

* Sun Nov 13 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.3-1
- Rebuild for autopackage 1.1.3 release.

* Thu Oct 27 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.2-1
- Rebuild for autopackage 1.1.2 release.

* Sun Aug 21 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.1-1
- Rebuild for autopackage 1.1.1 release.

* Wed Jun 01 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.1.0-1
- Rebuild for autopackage 1.1.0 release.

* Sun Apr 10 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0.2-1
- Rebuild for autopackage 1.0.2 release.

* Sat Apr 02 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0.1-1
- Rebuild for autopackage 1.0.1 release.

* Sat Mar 19 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-5
- Rebuild for next autopackage RC release.

* Mon Mar 07 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-3
- Regenerated for libuau.so.3 which was rebuilt against libcurl.so.2 .
- Pickup 1.0RC2 release.

* Sat Feb 12 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-1
- Updated for gettext machine objects in locale directory.

* Sun Jan  9 2005  Curtis L. Knight <knighcl@fastmail.fm>  1.0-1
- Updated for source support code archive file name to be without a version.
- No longer has collisions with autopackage-devel rpm.

* Sat Nov  6 2004  Curtis L. Knight <knighcl@fastmail.fm>  0.7-1
- Updated to include translation files. Place holder in files directive.

* Sat Apr 10 2004  Curtis L. Knight <knighcl@fastmail.fm>  0.5-1
- Initial rpm package release to coincide with 0.5 release.
