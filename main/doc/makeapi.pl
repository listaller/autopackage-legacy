#!/usr/bin/env perl
use warnings;
use strict;

use FindBin;
use XML::DOM;


# %items is an associate array that contains information about
# all the functions and variables. The key is the name, and the
# value is the associated information.
# The values are associative arrays too, with the following keys:
#   $name:     Name.
# The keys below only apply to functions:
#   $syntax:   Syntax.
#   \%params:  An reference array of parameters. The key is the parameter
#              name, the data is the description for that parameter.
#   $desc:     A description for this function.
#   $outputs:  The function's output.
#   $returns:  The function's return value.
#   $example:  An example on how to use this function.
our %items = ();
# %docs contains information about categories and which functions
# belong to which categories.
our %docs = ();
our $verbose = 0;
our $warning = 0;
our @files = ();


# Parse parameters
usage (1) if (@ARGV < 1);
for (my $i = 0; $i < @ARGV; $i++) {
	if ($ARGV[$i] eq '--help' || $ARGV[$i] eq '-h') {
		usage (0);
	} elsif ($ARGV[$i] eq '--version' || $ARGV[$i] eq '-v') {
		print ("DocExtract(r) FX 2.0 Professional(tm) Service Pack 3 Update 4.1 Gold\n");
		print ("Copyright (c) 2003 Hongli Lai.\n");
		exit (0);
	} elsif ($ARGV[$i] eq '--warning' || $ARGV[$i] eq '-w') {
		$warning = 1;
	} elsif ($ARGV[$i] eq '--verbose' || $ARGV[$i] eq '-V') {
		$verbose = 1;
	} else {
		push (@files, $ARGV[$i]);
	}
}


if (! -f "categories.xml") {
	print "Error: categories.xml not found! Aborting...\n";
	exit 1;
}


foreach my $filename (@files) {
	extractDoc($filename);
}

mergeWithCategories();
outputHTML();



######## FUNCTIONS BEGIN HERE ########



sub usage {
	my ($exitCode) = @_;

	print ("Usage: makedoc.pl [OPTIONS] FILE1 FILE2 FILEn...\n");
	print ("Generate documentation from comments in source files.\n\n");
	print ("Options:\n");
	print ("  --help     Display this help text.\n");
	print ("  --version  Display version number.\n");
	print ("  --warning  Enable warnings.\n");
	print ("  --verbose  Be verbose.\n");
	exit ($exitCode);
}


# extractDoc(FILENAME)
# Extract documentation from comments in files.
sub extractDoc {
	my ($filename, $extracting, @docText);
	$filename = $_[0];
	$extracting = 0;
	@docText = ();

	print ("Extracting documentation from $filename...") if ($verbose == 1);

	open (F, "<$filename");
	while (my $line = <F>) {
		$line =~ s/\n//;

		# '##' indicates the start of documentation.
		# If these two characters are detected, keep reading lines
		# until a line that doesn't start with '#' is encountered.
		if ($extracting == 0 && $line =~ /^[\t ]*##$/)
		{
			$extracting = 1;
			next;
		} elsif ($extracting == 1)
		{
			if ($line =~ /^[\t ]*#/) {
				push (@docText, "$line");
			} else {
				# The end of the comment is reached.
				# Pass the entire string to parseDoc.

				&parseDoc (\@docText, $filename);
				@docText = ();
				$extracting = 0;
				next;
			}
		}
	}
	close (F);
	
	# Parse the last document section too if we missed that one
	if ($#docText >= 0)
	{
		&parseDoc (\@docText, $filename);
	}

	print (" done\n") if ($verbose == 1);
}


# parseDoc(\@TEXT, $filename)
# Parse the documentation.
sub parseDoc {
	my (@docText, $filename);
	@docText = @{$_[0]};
	$filename = $_[1];
	my $lineNum = 0;

	my ($state, $name, $syntax, %params, $outputs, $returns, $since, $deprecated, $desc, $example);
	$state = 0;
	%params = ();
	$outputs = '';
	$returns = '';
	$since = '';
	$deprecated = '';
	$desc = '';
	$example = '';

	foreach my $line (@docText)
	{
		$line =~ s/[\t ]*#//;
		$line =~ s/^ *// unless ($state == 2);
		$lineNum++;

		if (!$name)
		{
			# Get function name and syntax
			$name = $line;
			$name =~ s/^([a-z0-9_]+)[\t ]*.*$/$1/i;
			$syntax = $line;
		} elsif ($state == 0)
		{
			# Get parameters and return value
			# An empty line indicates the end of the parameters section.
			if ($line =~ /^[\t ]*$/)
			{
				$state = 1;
				next;
			}

			my ($param, $paramDesc) = split (/:/, $line, 2);
			if (!$param || !$paramDesc)
			{
				print (STDERR "WARNING: line $lineNum in $filename is not a valid parameter description.\n");
				$param = '';
				$paramDesc = '';
			} else
			{
				$param =~ s/(^[\t ]*|[\t ]*$)//g; # strip leading and trailing whitespace
				$paramDesc =~ s/(^[\t ]*|[\t ]*$)//g;
			}

			if ($param =~ /returns/i)
			{
				$returns = $paramDesc;
			} elsif ($param =~ /outputs/i) {
				$outputs = $paramDesc;
			} elsif ($param =~ /since/i) {
				$since = $paramDesc;
			} elsif ($param =~ /deprecated/i) {
				$deprecated = $paramDesc;
			} else {
				$params{$param} = $paramDesc;
			}
		} elsif ($state == 1)
		{
			if ($line =~ /example:/i)
			{
				$state = 2;
				next;
			}

			# Get the main description
			$desc = "$desc\n$line";
		} elsif ($state == 2)
		{
			if ($example eq '')
			{
				$example = "$line";
			} else
			{
				$example = "$example\n$line";
			}
		}
	}

	# Strip whitespaces from the beginning and end of the
	# description.
	$desc =~ s/^\s+//;
	$desc =~ s/\s+$//;
	# Strip only the first leading whitespace from example
	$example =~ s/^ //gm;

	# set defaults for since and deprecated values
	if ($since eq '')
	{
		$since = "1.0"
	}
	if ($deprecated eq '')
	{
		$deprecated = "N/A"
	}

	# The parsing is done. Now put all this stuff in a nice associative
	# array
	my %values = (
		"name",    $name,
		"syntax",  $syntax,
		"params",  \%params,
		"desc",    $desc,
		"outputs", $outputs,
		"returns", $returns,
		"since", $since,
		"deprecated", $deprecated,
		"example", $example
	);
	$items{$name} = \%values;
}


sub
mergeWithCategories
{
	my $parser = new XML::DOM::Parser;
	my $xml = $parser->parsefile ("categories.xml")->getDocumentElement;
	my $i;

	foreach my $category ($xml->getElementsByTagName('category', 0))
	{
		my %subDocs = ();
		&processCategory ($category, \%subDocs);
		$docs{$category->getAttribute('name')} = \%subDocs;
	}

	$xml->dispose ();
	undef $parser;
	undef $xml;
}

sub processCategory {
	my ($category, $doc) = @_;
	my ($c, @cItems, %subCategories);

	$doc->{'name'} = $category->getAttribute('name');
	$c = $category->getElementsByTagName ('description', 0)->item(0);
	$doc->{'desc'} = $c->getFirstChild->getNodeValue if ($c);
	$c = $category->getElementsByTagName ('shortdesc', 0)->item(0);
	$doc->{'shortDesc'} = $c->getFirstChild->getNodeValue if ($c);


	# Process items
	@cItems = ();
	foreach my $node ($category->getElementsByTagName ('item', 0))
	{
		my ($name, $ref, %info);

		$name = $node->getFirstChild()->getNodeValue;
		$ref = $items{$name};
		if ($ref)
		{
			push (@cItems, $ref);
		} else {
			print ("Warning: no documentation found for function $name\n") if ($warning == 1);
		}
	}
	$doc->{'items'} = \@cItems;


	# Process subcategories
	%subCategories = ();
	foreach my $subCategory ($category->getElementsByTagName ('category', 0)) {
		my %subDocs;

		&processCategory ($subCategory, \%subDocs);
		$subCategories{$subCategory->getAttribute('name')} = \%subDocs;
	}
	$doc->{'subCategories'} = \%subCategories;
}


sub replace ($$$)
{
	my ($str, $from, $to) = @_;

	$from =~ s/([\/\\\*\(\)\+\[\]\^\|\$\.\?])/\\$1/g;
	$to = '' if (!$to);
	$str =~ s/${from}/${to}/gs;
	return $str;
}


# Generate a filename from a category name
sub
genFileName
{
	my ($category) = @_;
	$category =~ s/  //g;
	$category =~ s/ /\-/g;
	$category =~ s/\//\-/g;
	return lc ("${category}.html")
}


sub docToCategories {
	my ($docsRef, $str) = @_;
	my (@docs, $output, $hasItems);

	@docs = keys %{$docsRef};
	$output = "$str<ul class=\"subcategories\">\n";
	$hasItems = 0;

	foreach my $doc (@docs) {
		my $desc = '';

		# Add an entry in index.html
		$hasItems = 1;
		$desc = " - $docsRef->{$doc}{'shortDesc'}" if ($docsRef->{$doc}{'shortDesc'});
		$output = "$output$str<li><a href=\"" . &genFileName ($doc) .
			"\">$doc</a>$desc";

		my $subCategories = $docsRef->{$doc}{'subCategories'};
		my $subItems = &docToCategories ($subCategories, "$str   ");
		if ($subItems eq '') {
			$output = "$output</li>\n";
		} else {
			$output = "$output\n" . $subItems . "$str</li>\n";
		}
	}

	if ($hasItems == 0) {
		return '';
	} else {
		$output = "$output$str</ul>\n";
		return $output;
	}
}


# Associate functions with a location in their .html file.
sub
associatePages ($)
{
	my $docsRef = $_[0];

	foreach my $doc (keys %{$docsRef})
	{
		my $subCategories = $docsRef->{$doc}{'subCategories'};
		my $filename = &genFileName ($doc);
		my $subItems = &associatePages ($subCategories);


		# Associate variables
		foreach my $item (@{$docsRef->{$doc}{'items'}})
		{
			my $name = $item->{'name'};
			next if (!($name =~ /^\$/)); # Must be a variable
			$name =~ s/^\$//;
			$item->{'link'} = "$filename#var-$name";
		}


		# Associate functions
		foreach my $item (@{$docsRef->{$doc}{'items'}})
		{
			next if ($item->{'name'} =~ /^\$/); # Must be a function
			$item->{'link'} = "$filename#func-" . $item->{'name'};
		}
	}
}


# Process a description text. Put hyperlinks to other functions/variables
# when necessary.
sub
processDesc ($)
{
	my $desc = $_[0];

	sub
	mkFuncLink ($)
	{
		my $name = $_[0];

		if (!defined ($items{$name}) || !defined ($items{$name}{'link'}))
		{
			print ("Warning: cannot link to $name - function is not documented.\n") if ($warning == 1);
			return $name;
		}
		return '<A HREF="' . $items{$name}{'link'} . "\">$name()</A>";
	}

	sub
	mkVarLink ($)
	{
		my $name = $_[0];
		return $name if (!defined ($items{$name}));
		return '<A HREF="' . $items{$name}{'link'} . "\">$name</A>";
	}

	$desc =~ s/ $/<\/P><P>/gm;
	$desc =~ s/^$/<\/P><P>/gm;
	$desc =~ s/([_a-z]+)\(\)/&mkFuncLink($1)/sige;
	$desc =~ s/\$([_a-z_]+)/&mkVarLink("\$$1")/sige;
	return $desc;
}


sub
docToPages {
	my ($docsRef, $location) = @_;

	foreach my $doc (keys %{$docsRef})
	{
		my $subCategories = $docsRef->{$doc}{'subCategories'};
		my $filename = genFileName($doc);
		my $subItems = docToPages($subCategories,
			"$location\n\t<li><a href=\"$filename\">$doc</a></li>\n");

		print ("Generating api/" . &genFileName ($doc) . "...") if ($verbose == 1);
		my $content = '';
		open (F, "< skel/page.html");
		while (my $line = <F>) {
			$content = "$content$line";
		}
		close (F);


		# Generate name, description and generation date & location
		$content = replace($content, '@NAME@', $doc);
		$content = replace($content, '@LOCATION@', "$location\n\t<li><b>$doc</b></li>");
		if (!$docsRef->{$doc}{'desc'} || ($docsRef->{$doc}{'desc'} eq '')) {
			if ($docsRef->{$doc}{shortDesc}) {
				$content = replace ($content, '@DESCRIPTION@',
					$docsRef->{$doc}{shortDesc});
			} else {
				$content = replace ($content, '@DESCRIPTION@', '');
			}
		} else {
			$content = replace($content, '@DESCRIPTION@', $docsRef->{$doc}{desc});
		}
		my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday) = gmtime(time);
		$year = 1900 + $year;
		$mon++;
		$content = replace($content, '@GENERATED@', "$year-$mon-$mday");


		# Generate subcategories index
		my $subCategoriesContent = docToCategories($subCategories, '');
		$subCategoriesContent = "<p><a name=\"subcategories\"></a>\n" .
			"<h3>Subcategories</h3>\n" .
			"$subCategoriesContent"
			if (!$subCategoriesContent eq '');
		$content = &replace ($content, '@SUBCATEGORIES@', $subCategoriesContent);


		# Generate variable list
		my ($varTable, $varTable2, $hasItems, $i);
		$hasItems = 0;
		$i = 0;
		$varTable = "<p><a name=\"vars\"></a>\n" .
			"<h3>Variables in this category</h3>\n" .
			"<TABLE BORDER=\"0\" WIDTH=\"100%\" CLASS=\"VARS\">\n";
		$varTable2 = '';

		foreach my $item (@{$docsRef->{$doc}{items}})	{
			$i++;
			next if ($item->{name} !~ /^\$/); # Must be a variable

			$hasItems = 1;
			$item->{'documented'} = 1;
			my $name = $item->{'name'};
			$name =~ s/\$//g;
			$varTable = "$varTable<TR>" .
				"<TD><A HREF=\"$filename#var-$name\">" .
					$item->{'name'} . "</A></TD>" .
				"</TR>\n";
			#$item->{'link'} = "$filename#var-$name";

			$varTable2 = "$varTable2<A NAME=\"var-$name\"></A><B>" . $item->{'name'} . "</B>";
			$varTable2 = "$varTable2<BR>\n" . $item->{'desc'} if ($item->{'desc'});
			$varTable2 = "$varTable2\n<P><HR>\n\n";
		}
		if ($hasItems == 1)
		{
			$varTable = "$varTable</TABLE>\n";
			$content = replace($content, '@VARLIST@', $varTable);
			$content = replace($content, '@VARLIST_DETAIL@', $varTable2);
		} else {
			$content = replace($content, '@VARLIST@', '');
			$content = replace($content, '@VARLIST_DETAIL@', '');
		}


		# Generate function list
		my ($funcTable, $funcTable2);
		$hasItems = 0;
		$funcTable = "<p><a name=\"funcs\"></a>\n" .
			"<h2>Functions in this category</h2>\n" .
			"<table class=\"functionIndex\">\n" .
			"<tr><th>Name</th><th>Parameters</th></tr>\n";
		$funcTable2 = '';

		$i = 0;
		foreach my $item (@{$docsRef->{$doc}{items}}) {
			$i++;
			next if ($item->{name} =~ /^\$/); # Must be a function

			$hasItems = 1;
			$item->{'documented'} = 1;
			# Generate index
			my $params = $item->{'syntax'};
			$params =~ s/^[a-z_0-9]+ ?//i;
			$params =~ s/</&lt;/g;
			$params =~ s/>/&gt;/g;
			$funcTable .= "<tr onclick=\"location.href='$filename#func-$item->{name}';\">\n" .
				"	<td class=\"func\"><a href=\"$filename#func-$item->{name}\">" .
					$item->{'name'} . "</a></td>\n" .
				"	<td class=\"decl\">" . $params . "</td>\n" .
				"</tr>\n";


			# Generate detailed information
			my $example = $item->{'example'} if ($item->{'example'});
			if ($example) {
				$example =~ s/</&lt;/g;
				$example =~ s/>/&gt;/g;
				$example =~ s/(#.*)/<SPAN CLASS="COMMENT">$1<\/SPAN>/g;
				#$example =~ s/\n/<BR>\n/sg;
			}

			$funcTable2 .= "<a name=\"func-$item->{name}\"></a>" .
				"<p><table width=\"100%\">\n" .
				"<TR><TD CLASS=\"FUNC-NAME\"><B>" . $item->{'name'} . "</B></TD></TR>\n" .
				"<TR><TD CLASS=\"FUNC-SYNTAX\"><B>Syntax:</B> <TT>" . $item->{'name'} . " $params</TT></TD></TR>\n";

			my $hasParams = 0;
			my $paramTable = "<TR><TD><TABLE BORDER=\"0\" CLASS=\"FUNC-PARAMS\" WIDTH=\"100%\">\n";
			foreach my $param (keys %{$item->{'params'}}) {
				$hasParams = 1;
				$paramTable = "$paramTable	<TR><TD NOWRAP ALIGN=\"Right\"><TT>$param</TT>: " .
					"</TD><TD WIDTH=\"100%\">" . $item->{'params'}->{$param} . "</TD></TR>\n";
			}
			# default values where set for since and deprecated so always show param table
			# $hasParams = 1 if ($item->{'outputs'} ne '' || $item->{'returns'} ne '');
			$hasParams = 1;

			$paramTable = "$paramTable	<TR><TD NOWRAP ALIGN=\"Right\">Outputs: </TD><TD WIDTH=\"100%\">" .
				$item->{'outputs'} . "</TD></TR>\n" if ($item->{'outputs'} ne '');
			$paramTable = "$paramTable	<TR><TD NOWRAP ALIGN=\"Right\">Returns: </TD><TD WIDTH=\"100%\">" .
				$item->{'returns'} . "</TD></TR>\n" if ($item->{'returns'} ne '');
			$paramTable = "$paramTable	<TR><TD NOWRAP ALIGN=\"Right\">Since: </TD><TD WIDTH=\"100%\">" .
				$item->{'since'} . "</TD></TR>\n" if ($item->{'since'} ne '');
			$paramTable = "$paramTable	<TR><TD NOWRAP ALIGN=\"Right\">Deprecated: </TD><TD WIDTH=\"100%\">" .
				$item->{'deprecated'} . "</TD></TR>\n" if ($item->{'deprecated'} ne '');
			$funcTable2 .= "$paramTable</TABLE>\n" if ($hasParams == 1);

			$funcTable2 .= "<TR><TD CLASS=\"FUNC-DESC\"><P>" .
				processDesc ($item->{'desc'}) . "</P></TD></TR>\n" if ($item->{desc});
			
			$funcTable2 .= "<TR><TD CLASS=\"FUNC-EXAMPLE\"><B>Example</B>:\n<BLOCKQUOTE><PRE>" .
				"$example</PRE></BLOCKQUOTE></TR>\n" if ($example);
			$funcTable2 .= "</table>\n";
			$funcTable2 .= "<P><HR>\n\n" unless ($i == @{$docsRef->{$doc}{items}});
		}
		if ($hasItems == 1)
		{
			$funcTable = "$funcTable</TABLE>\n";
			$content = replace($content, '@FUNCLIST@', $funcTable);
			$content = replace($content, '@FUNCLIST_DETAIL@', $funcTable2);
		} else {
			$content = replace($content, '@FUNCLIST@', '');
			$content = replace($content, '@FUNCLIST_DETAIL@', '');
		}


		if (open (F, "> api/$filename"))
		{
			print (F $content);
			close (F);
			print (" done\n") if ($verbose == 1);
		} else
		{
			print (" cannot write to api/$filename\n");
		}
	}
}


sub
docToIndex
{
	my ($docsRef) = @_;

	my $content = '';
	open (F, "skel/alphabetical.html");
	while (my $line = <F>) {$content = "$content$line"};
	close (F);

	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday) = gmtime(time);
	$year = 1900 + $year;
	$mon++;
	$content = &replace ($content, '@GENERATED@', "$year-$mon-$mday");


	my $vars = "<table class=\"functionIndex\">\n";
	foreach my $item (sort (keys %items))
	{
		next if (!($items{$item}{'name'} =~ /^\$/)); # Must be a variable

		if (!($items{$item}{'link'}))
		{
		    printf ("Warning: %s is not listed in categories.xml\n", $items{$item}{'name'}) if ($warning == 1);
		    next;
		}
		$vars .= "<tr onclick=\"location.href='$items{$item}{link}';\">\n" .
			"	<td class=\"func\"><a href=\"$items{$item}{link}\">$items{$item}{name}</a></td>\n" .
			"</tr>\n";
	}
	$content = replace($content, '@VARIABLES@', "$vars</table>\n");


	my $funcs = "<table class=\"functionIndex\">\n";
	foreach my $item (sort (keys %items))
	{
		next if ($items{$item}{'name'} =~ /^\$/); # Must be a variable
		if (!$items{$item}{'documented'}) {
		        next if ( $items{$item}{'name'} =~ /^\_/ );
			printf ("Warning: function %s is not listed in category.xml\n",	$items{$item}{'name'}) if ($warning == 1);
			next;
		}
		my $params = "$items{$item}{syntax}";
		$params =~ s/^[a-z_0-9]+ ?//i;
		$params =~ s/</&lt;/g;
		$params =~ s/>/&gt;/g;
		$funcs .= "<tr onclick=\"location.href='$items{$item}{link}';\">\n" .
			"	<td class=\"func\"><a href=\"$items{$item}{link}\">$items{$item}{name}</a></td><td>$params</td>\n" .
			"</tr>\n";
	}
	$content = replace($content, '@FUNCTIONS@', "$funcs</table>\n");


	if (open (F, "> api/alphabetical.html"))
	{
		print (F $content);
		close (F);
	} else
	{
		print (STDERR "Error: unable to write to api/alphabetical.html\n");
	}
}


sub
outputHTML
{
	my ($content, $categories);

	print ("Generating api/index.html...") if ($verbose == 1);
	open (F, "skel/index.html");
	while (my $line = <F>) {$content = "$content$line"};
	close (F);

	$categories = &docToCategories (\%docs, '');
	$content = &replace ($content, '@CATEGORIES@', $categories);
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday) = gmtime(time);
	$year = 1900 + $year;
	$mon++;
	$content = &replace ($content, '@GENERATED@', "$year-$mon-$mday");

	if (open (F, "> api/index.html"))
	{
		print (F "$content");
		close (F);
		print (" done\n") if ($verbose == 1);
	} else
	{
		print (" cannot write to api/index.html\n");
	}

	associatePages(\%docs);
	docToPages(\%docs, "<li><a href=\"index.html\">Table of Contents</a></li>");
	docToIndex(\%docs);
}
