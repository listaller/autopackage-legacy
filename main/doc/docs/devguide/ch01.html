<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            Chapter&nbsp;1.&nbsp;Introduction to the framework
        </title>
        <link rel="stylesheet" href="autopackage.css" type="text/css" />
        <meta name="generator" content="DocBook XSL Stylesheets V1.75.2" />
        <link rel="home" href="index.html" title="Autopackage Packagers Guide" />
        <link rel="up" href="index.html" title="Autopackage Packagers Guide" />
        <link rel="prev" href="index.html" title="Autopackage Packagers Guide" />
        <link rel="next" href="ch02.html" title="Chapter&nbsp;2.&nbsp;Creating a package" />
    </head>
    <body>
        <div class="navheader">
            <table width="100%" summary="Navigation header">
                <tr>
                    <th colspan="3" align="center">
                        Chapter&nbsp;1.&nbsp;Introduction to the framework
                    </th>
                </tr>
                <tr>
                    <td width="20%" align="left">
                        <a accesskey="p" href="index.html">Prev</a>&nbsp;
                    </td>
                    <th width="60%" align="center">
                        &nbsp;
                    </th>
                    <td width="20%" align="right">
                        &nbsp;<a accesskey="n" href="ch02.html">Next</a>
                    </td>
                </tr>
            </table>
            <hr />
        </div>
        <div class="chapter" title="Chapter&nbsp;1.&nbsp;Introduction to the framework">
            <div class="titlepage">
                <div>
                    <div>
                        <h2 class="title">
                            <a id="intro" name="intro"></a>Chapter&nbsp;1.&nbsp;Introduction to the
                            framework
                        </h2>
                    </div>
                </div>
            </div>
            <div class="toc">
                <p>
                    <b>Table of Contents</b>
                </p>
                <dl>
                    <dt>
                        <span class="section"><a href="ch01.html#id310093">Basic
                        concepts</a></span>
                    </dt>
                    <dd>
                        <dl>
                            <dt>
                                <span class="section"><a href="ch01.html#id311354">Interface
                                versions</a></span>
                            </dt>
                            <dt>
                                <span class="section"><a href="ch01.html#id310282">Skeleton
                                files</a></span>
                            </dt>
                            <dt>
                                <span class="section"><a href="ch01.html#id310309">Names and
                                naming</a></span>
                            </dt>
                        </dl>
                    </dd>
                </dl>
            </div>
            <p>
                Autopackage is a framework for developers that lets them build easy to use
                installers for their software. It provides an API which allows delegation many
                things to the framework - for instance, installing menu entries, copying files,
                uninstalling the software, and providing feedback to the users while it is
                installing.
            </p>
            <p>
                As well as providing some useful utility functions for your installers, autopackage
                acts as a <span class="emphasis"><em>distribution abstraction</em></span> .
                Packages built with autopackage are designed to be distribution neutral. The
                underlying framework and tools take care of the differences between the systems
                your software is being installed on.
            </p>
            <p>
                An autopackage is typically a self extracting shell script with a <code class=
                "filename">.package</code> extension. It will first check that the autopackage
                tools are present. If not, then it will automatically fetch them, install them, and
                then proceed with the installation. It will also download the graphical frontend
                that best matches the users desktop.
            </p>
            <div class="section" title="Basic concepts">
                <div class="titlepage">
                    <div>
                        <div>
                            <h2 class="title" style="clear: both">
                                <a id="id310093" name="id310093"></a>Basic concepts
                            </h2>
                        </div>
                    </div>
                </div>
                <p>
                    Autopackage deals with dependencies by checking the system directly for the
                    components needed. Rather than depending on a particular version of a
                    particular piece of software, autopackages depend on an implementation of a
                    particular interface being present. Interfaces are abstract things - a shared
                    library exports an interface (or more commonly, many) but an interface can also
                    be for instance the presence of certain files in certain locations, command
                    line arguments given, protocols between system components and so on.
                </p>
                <p>
                    The first thing to realize then is the need to tell autopackage which
                    interfaces your package needs, and then the system tries to figure out the
                    "best" package that can satisfy that interface, typically the most recent
                    compatible version of that package (but an interface doesn't have to correspond
                    one to one with a particular piece of software).
                </p>
                <div class="section" title="Interface versions">
                    <div class="titlepage">
                        <div>
                            <div>
                                <h3 class="title">
                                    <a id="id311354" name="id311354"></a>Interface versions
                                </h3>
                            </div>
                        </div>
                    </div>
                    <p>
                        Autopackage abstracts interfaces behind <a class="glossterm" href=
                        "go01.html#id359719"><em class="glossterm">interface version</em></a>
                        numbers. These take the form of two integers written as A.B, and they work
                        similar to the libtool versions many of us are used to. The A number is the
                        major number and is incremented when a breaking change is made to an
                        interface (a function is removed, renamed etc). When A is incremented, B is
                        set back to zero. B is the minor number and is incremented when an
                        interface is added. This is in contrast to the <a class="glossterm" href=
                        "go01.html#id359736"><em class="glossterm">software version</em></a> ,
                        which is pretty much freeform and is usually set to the version number
                        chosen by the software authors.
                    </p>
                </div>
                <p>
                    Note that interface versions don't need to bear any resemblance to the version
                    numbers software authors assign their creations (though they can do, if that's
                    convenient). WhizzBang 2003 may well have an interface version of 0.35, or 4.1
                    or whatever. On the other hand, <a class="ulink" href="http://gtk.org/" target=
                    "_top">GTK+</a> for instance follows the libtool versioning style - every
                    release in the 2.x series is backwards compatible. Because it also uses kernel
                    versioning though, each release increments the minor number by two, not by one.
                    In this case, the interface version can be equal to the software version - the
                    meaning is still close enough that everything will work OK.
                </p>
                <div class="section" title="Skeleton files">
                    <div class="titlepage">
                        <div>
                            <div>
                                <h3 class="title">
                                    <a id="id310282" name="id310282"></a>Skeleton files
                                </h3>
                            </div>
                        </div>
                    </div>
                    <p>
                        The mapping from the state of the system to an interface version (or set of
                        interface versions) is determined by the <a class="glossterm" href=
                        "go01.html#id359753"><em class="glossterm">skeleton file</em></a> which
                        encapsulates that dependency. Skeleton files contain a small amount of
                        metadata about a dependency, and they are used by packages to make
                        dependency detection and resolution automatic. Typically for each
                        dependency a package has, there must be a corresponding skeleton file - if
                        one doesn't exist, it should be created and then let the maintainers of the
                        software it represents know of its existence. Don't worry though, creating
                        skeleton files is very easy.
                    </p>
                    <p>
                        Autopackage comes with a collection of useful skeleton files for common
                        dependencies that can be used immediately in your own packages. If a
                        skeleton is created, please forward the skeleton to us so that it can be in
                        the next release.
                    </p>
                </div>
                <div class="section" title="Names and naming">
                    <div class="titlepage">
                        <div>
                            <div>
                                <h3 class="title">
                                    <a id="id310309" name="id310309"></a>Names and naming
                                </h3>
                            </div>
                        </div>
                    </div>
                    <p>
                        Autopackage is designed to be a decentralized system. For this reason, it
                        leverages off of the DNS network to uniquely identify things. The basic
                        unit of currency is called a <a class="glossterm" href=
                        "go01.html#id359770"><em class="glossterm">root name</em></a> , and looks a
                        bit like this: <code class="literal">@foobar.org/frob:2000:3</code> . Root
                        names have several components:
                    </p>
                    <div class="itemizedlist">
                        <ul class="itemizedlist" type="disc">
                            <li class="listitem">The path, this consists of the @foobar.org/frob
                            part
                            </li>
                            <li class="listitem">The software version number, which comes after the
                            first colon (2000 in this case)
                            </li>
                            <li class="listitem">The package release number, which comes after the
                            second colon.
                            </li>
                        </ul>
                    </div>
                    <div class="warning" title="Warning" style=
                    "margin-left: 0.5in; margin-right: 0.5in;">
                        <h3 class="title">
                            Warning
                        </h3>A root name should not be chosen based on where the package will be
                        hosted, who built it, or anything <span class=
                        "strong"><strong>other</strong></span> than whoever owns the software. Root
                        names are designed to help reduce name conflicts without the need for yet
                        another name "ownership" registry. Typically, if the packager is not the
                        maintainer of the software that is being packaged, the true maintainer
                        should be at the very least emailed to notify them that this root name has
                        been allocated for them. Ideally of course the packager is the enlightened
                        maintainer and wishes to build binary packages so this is not an issue.
                    </div>
                    <p>
                        Packages (but not skeletons) can optionally have a <a class="glossterm"
                        href="go01.html#id359702"><em class="glossterm">short name</em></a> . Short
                        names are more convenient to use than root names, and are more familiar to
                        Linux users. They look like "frozen-bubble", "libxml" or "gnome-panel".
                        Typically, these are the names users will use from the command line. Try
                        and keep them unique if possible, but it's not a disaster if there are
                        conflicts.
                    </p>
                    <p>
                        Finally, packages and skeletons should be given a <a class="glossterm"
                        href="go01.html#id359786"><em class="glossterm">display name</em></a> .
                        This is a human readable string, that can be optionally localized. It will
                        be used in user interfaces where possible, and should take the form of
                        "Product-Name Purpose", for instance "Sound Juicer CD Ripper", "Mozilla Web
                        Browser", or "GNU Emacs Text Editor".
                    </p>
                </div>
            </div>
        </div>
        <div class="navfooter">
            <hr />
            <table width="100%" summary="Navigation footer">
                <tr>
                    <td width="40%" align="left">
                        <a accesskey="p" href="index.html">Prev</a>&nbsp;
                    </td>
                    <td width="20%" align="center">
                        &nbsp;
                    </td>
                    <td width="40%" align="right">
                        &nbsp;<a accesskey="n" href="ch02.html">Next</a>
                    </td>
                </tr>
                <tr>
                    <td width="40%" align="left" valign="top">
                        Autopackage Packagers Guide&nbsp;
                    </td>
                    <td width="20%" align="center">
                        <a accesskey="h" href="index.html">Home</a>
                    </td>
                    <td width="40%" align="right" valign="top">
                        &nbsp;Chapter&nbsp;2.&nbsp;Creating a package
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
