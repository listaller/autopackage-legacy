<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            Versioning
        </title>
        <link rel="stylesheet" href="autopackage.css" type="text/css" />
        <meta name="generator" content="DocBook XSL Stylesheets V1.75.2" />
        <link rel="home" href="index.html" title="Autopackage Packagers Guide" />
        <link rel="up" href="ch06.html" title=
        "Chapter&nbsp;6.&nbsp;Autopackage Root Names &amp; Versioning" />
        <link rel="prev" href="ch06s02.html" title="What is a root name?" />
        <link rel="next" href="ch06s04.html" title="How to integrate this with skeletons" />
    </head>
    <body>
        <div class="navheader">
            <table width="100%" summary="Navigation header">
                <tr>
                    <th colspan="3" align="center">
                        Versioning
                    </th>
                </tr>
                <tr>
                    <td width="20%" align="left">
                        <a accesskey="p" href="ch06s02.html">Prev</a>&nbsp;
                    </td>
                    <th width="60%" align="center">
                        Chapter&nbsp;6.&nbsp;Autopackage Root Names &amp; Versioning
                    </th>
                    <td width="20%" align="right">
                        &nbsp;<a accesskey="n" href="ch06s04.html">Next</a>
                    </td>
                </tr>
            </table>
            <hr />
        </div>
        <div class="section" title="Versioning">
            <div class="titlepage">
                <div>
                    <div>
                        <h2 class="title" style="clear: both">
                            <a id="id357481" name="id357481"></a>Versioning
                        </h2>
                    </div>
                </div>
            </div>
            <p>
                One reason why root names contain a version number is to make it possible to
                install two different versions of the same software in parallel.
            </p>
            <p>
                However, version numbers are also used for dependency handling. Let's say Foobar is
                a GTK+ 2.0 app. Now we have a few problems:
            </p>
            <div class="itemizedlist">
                <ul class="itemizedlist" type="disc">
                    <li class="listitem">
                        <p>
                            We don't want to check for just "@gtk.org/gtk:2.0.1", we just want to
                            check for *any* 2.0.x release.
                        </p>
                    </li>
                    <li class="listitem">
                        <p>
                            At the time Foobar is written, GTK+ 2.2 didn't exist. GTK+ 2.2 is now
                            released, and turns out to be binary compatible with 2.0. How can we
                            know this?
                        </p>
                    </li>
                </ul>
            </div>
            <p>
                So, we introduce a new kind of version number: interface numbers. Interface numbers
                are not related to the software's version number: they change when the <span class=
                "emphasis"><em>interface</em></span> has changed. Interface can mean several
                things, depending on your software. For shared libraries, it usually indicates
                binary compatibility, but it can also include things like file formats and command
                line arguments.
            </p>
            <p>
                Interface numbers are used to check package compatibility.
            </p>
            <p>
                Interface numbers are made of two numbers, each separated by a dot:
            </p>
            <div class="itemizedlist">
                <ul class="itemizedlist" type="disc">
                    <li class="listitem">
                        <p>
                            Major: Every time the interface has changed (binary compatibility
                            changed), this number is increased by 1 and the minor number is reset
                            to 0.
                        </p>
                    </li>
                    <li class="listitem">
                        <p>
                            Minor: This number is increased by 1 if something has been added to the
                            interface, like a new function in a library. Apps that depend on
                            previous revision numbers will still work on this revision, but apps
                            that depend on this revision will not work on previous revisions.
                        </p>
                    </li>
                </ul>
            </div>
            <p>
                Interface numbers <span class="emphasis"><em>only</em></span> contain numbers, not
                letters or anything weird like that!
            </p>
            <div class="table">
                <a id="id357568" name="id357568"></a>
                <p class="title">
                    <b>Table&nbsp;6.1.&nbsp;Software &amp; Interface Number</b>
                </p>
                <div class="table-contents">
                    <table summary="Software &amp; Interface Number" border="1">
                        <colgroup>
                            <col />
                            <col />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>
                                    Software
                                </th>
                                <th>
                                    Interface number
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    GTK+ 1.2.0
                                </td>
                                <td>
                                    1.0
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    GTK+ 1.2.1
                                </td>
                                <td>
                                    1.0
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    GTK+ 2.0.0
                                </td>
                                <td>
                                    2.0
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    GTK+ 2.2.0
                                </td>
                                <td>
                                    2.2
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    GTK+ 2.2.1
                                </td>
                                <td>
                                    2.2
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <p>
                <br class="table-break" />
            </p>
            <p>
                GTK+ 1.2.1, 2.0.1 and 2.2.1 are bugfix releases. So their major and revision
                numbers don't change.
            </p>
            <p>
                GTK+ 2.0 is binary incompatible with GTK+ 1.2, so the major is increased by 1.
            </p>
            <p>
                GTK+ 2.2 is binary compatible with GTK+ 2.0, but new functions has been added.
                That's why the major number doesn't change, and the revision number is increased by
                1.
            </p>
            <p>
                GTK is simple, because its software version numbers happen to reflect the
                interfaces. Other software is less simple, for instance:
            </p>
            <div class="table">
                <a id="id357677" name="id357677"></a>
                <p class="title">
                    <b>Table&nbsp;6.2.&nbsp;Software &amp; Interface number - Complex</b>
                </p>
                <div class="table-contents">
                    <table summary="Software &amp; Interface number - Complex" border="1">
                        <colgroup>
                            <col />
                            <col />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>
                                    Release name
                                </th>
                                <th>
                                    Major interface number
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    libpng 1.0.x
                                </td>
                                <td>
                                    1
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    libpng 1.2.5
                                </td>
                                <td>
                                    2
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    libpng 1.2.6
                                </td>
                                <td>
                                    3
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <p>
                <br class="table-break" />
            </p>
            <p>
                The interface number is not part of the root name, and is stored separately. When
                software contains multiple interfaces, such as libraries that use symbol versions,
                the major number stays the same and the revision is incremented.
            </p>
        </div>
        <div class="navfooter">
            <hr />
            <table width="100%" summary="Navigation footer">
                <tr>
                    <td width="40%" align="left">
                        <a accesskey="p" href="ch06s02.html">Prev</a>&nbsp;
                    </td>
                    <td width="20%" align="center">
                        <a accesskey="u" href="ch06.html">Up</a>
                    </td>
                    <td width="40%" align="right">
                        &nbsp;<a accesskey="n" href="ch06s04.html">Next</a>
                    </td>
                </tr>
                <tr>
                    <td width="40%" align="left" valign="top">
                        What is a root name?&nbsp;
                    </td>
                    <td width="20%" align="center">
                        <a accesskey="h" href="index.html">Home</a>
                    </td>
                    <td width="40%" align="right" valign="top">
                        &nbsp;How to integrate this with skeletons
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
