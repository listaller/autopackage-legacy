<?xml version="1.0" encoding="UTF-8"?>
<!-- Used to process MIME files which use the Autopackage extention of the spec -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:apkg="http://autopackage.org/xdgmime-extensions"
                xmlns:fdo="http://www.freedesktop.org/standards/shared-mime-info">
  
<xsl:output method='text' version='1.0' encoding='UTF-8' indent='no'/>
    <xsl:template match="/">
        <xsl:for-each select="fdo:mime-info/fdo:mime-type">
            <xsl:variable name="type" select="@type"/>
<xsl:document href="{$type}.desktop" method="text" xmlns="http://www.freedesktop.org/standards/shared-mime-info" xmlns:apkg="http://autopackage.org/xdgmime-extensions">
[Desktop Entry]
Encoding=UTF-8
<xsl:for-each select="fdo:comment">
    <xsl:text>Comment</xsl:text>
    <xsl:if test="@xml:lang">
        <xsl:text>[</xsl:text><xsl:value-of select="@xml:lang"/><xsl:text>]</xsl:text>
    </xsl:if>
    <xsl:text>=</xsl:text><xsl:value-of select="text()"/><xsl:text>
</xsl:text>
</xsl:for-each>
Type=MimeType
MimeType=<xsl:value-of select="@type"/>
Patterns=<xsl:for-each select="fdo:glob"><xsl:value-of select="@pattern"/>;</xsl:for-each>
Icon=<xsl:value-of select="apkg:icon"/>
X-KDE-AutoEmbed=false
</xsl:document>
<xsl:value-of select="@type"/>
<xsl:text> <!--new line generated here-->
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
