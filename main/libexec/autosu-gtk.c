/*  Autopackage Superuser
 *  Copyright (C) 2004,2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include "binreloc.h"
#include "autosu.h"
#include "autosu-su.h"
#include "autosu-sudo.h"
#include "su-icon.c"

#include "auth.csource"


struct _GUI {
	guint pass;
	gchar *user;
	gchar *command;
	GtkWidget *win;
	GtkWidget *entry;
	GtkWidget *status;
	GtkWidget *why;
	GtkWidget *nopasswd;
	GtkWidget *cancel;
	GtkWidget *ok;
} GUI;

static SudoSession *sudo = NULL;


/********************************************
 * Utility functions
 ********************************************/

static void
clear_entry (GtkWidget *entry)
{
	gchar *blank;

	/* Make a pathetic stab at clearing the GtkEntry field memory */
	blank = (gchar *) gtk_entry_get_text (GTK_ENTRY (entry));
	if (blank && strlen (blank))
		memset (blank, ' ', strlen (blank));

	blank = g_strdup (blank);
	if (strlen (blank))
		memset (blank, ' ', strlen (blank));

	gtk_entry_set_text (GTK_ENTRY (entry), blank);
	gtk_entry_set_text (GTK_ENTRY (entry), "");
}

void
show_dialog (GtkMessageType type, GtkButtonsType buttons, gchar *msg)
{
	gdk_threads_enter ();
	GtkWidget *dialog = gtk_message_dialog_new ((GtkWindow *) GUI.win,
		GTK_DIALOG_MODAL,
		type,
		buttons,
		"%s", msg);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	gdk_threads_leave ();
}

void
show_error (gchar *format, ...)
{
	va_list ap;
	gchar *msg;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);

	show_dialog (GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, msg);
}


/********************************************
 * GUI code
 ********************************************/

static void
cb_no_password ()
{
	exit (EXIT_NOPASSWD);
}


static void
cb_cancel ()
{
	exit (EXIT_CANCEL);
}


static gboolean
on_correct_password (gpointer data)
{
	/* Correct password for su; run command and exit */
	gchar *password = (gchar *) data;
	gint ret;

	signal (SIGINT, SIG_DFL);
	gtk_widget_hide (GUI.win);
	gtk_widget_destroy (GUI.win);
	while (gtk_events_pending ())
		gtk_main_iteration ();

	ret = su_exec (GUI.user, password, GUI.command, NULL, NULL, NULL);
	if (ret == -1)
		exit (EXIT_ABNORMAL);

	report_exit_code (ret);
	exit (0);
	return FALSE;
}


static gboolean
on_incorrect_password (gpointer data)
{
	/* Incorrect password; exit if the user typed incorrect password 3 times */
	gchar *password = (gchar *) data;
	gchar *tmp;

	/* Try passing the password to sudo */
	if (sudo != NULL) {
		SudoAuthStatus auth;
		gint ret;

		auth = sudo_session_authenticate (sudo, password);
		safefree (password);
		password = NULL;

		switch (auth) {
		case SUDO_AUTH_SUCCESS:
			/* Authentication successfull */
			signal (SIGINT, SIG_DFL);
			gtk_widget_hide (GUI.win);
			gtk_widget_destroy (GUI.win);
			while (gtk_events_pending ())
				gtk_main_iteration ();

			ret = sudo_session_waitchild (sudo);

			sudo_session_kill (sudo);
			sudo_session_free (sudo);

			if (ret == -1)
				exit (EXIT_ABNORMAL);
			report_exit_code (ret);
			exit (0);

		case SUDO_AUTH_FAIL:
			/* Incorrect password. Keep interating until sudo
			 * is ready to ask for a password again */
			while (1) {
				SudoStatus status;

				status = sudo_session_iterate (sudo);

				switch (status) {
				case SUDO_STATUS_CONTINUE:
					break;
				case SUDO_STATUS_AUTH:
					goto incorrect;
				case SUDO_STATUS_FAIL:
					/* User entered too many wrong passwords */
					exit (EXIT_INCORRECT);
				default:
					/* Sudo exited abnormally; don't call sudo again. */
					sudo_session_free (sudo);
					sudo = NULL;
					goto incorrect;
				};
			}

		default:
			/* Sudo exited with an error, but it wasn't
			 * because of an incorrect password. Don't call
			 * sudo again. */
			sudo_session_free (sudo);
			sudo = NULL;
			goto incorrect;
		};

	} else {
		incorrect:

		if (password != NULL)
			safefree (password);
		if (GUI.pass >= 3)
			exit (EXIT_INCORRECT);

		if (GUI.pass == 2)
			tmp = g_strdup_printf ("<b>%s</b>",
				_("Password incorrect, please try again. You have one more chance."));
		else
			tmp = g_strdup_printf ("<b>%s</b>",
				_("Password incorrect, please try again."));
		gtk_label_set_markup (GTK_LABEL (GUI.status), tmp);
		g_free (tmp);

		gtk_widget_set_sensitive (GUI.entry, TRUE);
		if (GUI.nopasswd)
			gtk_widget_set_sensitive (GUI.nopasswd, TRUE);
		gtk_widget_set_sensitive (GUI.cancel, TRUE);
		gtk_widget_set_sensitive (GUI.ok, TRUE);
		gtk_widget_grab_focus (GUI.entry);
	}

	return FALSE;
}


static gpointer
start_check (gpointer data)
{
	gchar *password;
	gint result;
	GError *error = NULL;

	/* Ubuntu is the only distribution that uses sudo instead
	 * of su, so consult su first, then sudo. */
	password = data;
	result = su_check_password (GUI.user, password, &error);

	if (result == 1) {
		/* Correct password for su; run command and exit */
		gtk_idle_add (on_correct_password, password);

	} else if (result == 0) {
		/* Incorrect password */
		gtk_idle_add (on_incorrect_password, password);

	} else {
		/* An error occured */
		safefree (password);
		show_error (_("An error occured while checking the password:\n%s"),
			error->message);
		exit(EXIT_ABNORMAL);
	}

	return NULL;
}


static void
cb_ok ()
{
	gchar *tmp, *password;

	password = g_strdup (gtk_entry_get_text (GTK_ENTRY (GUI.entry)));
	clear_entry (GUI.entry);

	tmp = g_strdup_printf ("<b>%s</b>",
			       _("Checking password, please wait..."));
	gtk_label_set_markup (GTK_LABEL (GUI.status), tmp);
	g_free (tmp);

	gtk_widget_show (GUI.status);
	gtk_widget_set_sensitive (GUI.entry, FALSE);
	if (GUI.nopasswd) gtk_widget_set_sensitive (GUI.nopasswd, FALSE);
	gtk_widget_set_sensitive (GUI.cancel, FALSE);
	gtk_widget_set_sensitive (GUI.ok, FALSE);

	GUI.pass++;
	g_thread_create (start_check, password, FALSE, NULL);
}

static gboolean
why_pressed_cb (GtkWidget *widget, GdkEventButton *event, gpointer userdata)
{
	GtkDialog *dialog =
		GTK_DIALOG( gtk_dialog_new_with_buttons ("",
							 GTK_WINDOW (GUI.win),
							 GTK_DIALOG_MODAL,
							 GTK_STOCK_OK,
							 GTK_RESPONSE_ACCEPT,
							 NULL) );

	/* Translators: This message is split into 4 parts to make translation easier */
#define WHY_MESSAGE \
	g_strconcat (_("The <b>system password</b> is the secret word or phrase you enter to administer your computer.\n\n" \
	"On many systems this is the 'root password', but on some it may be your own. If " \
	"you aren't sure if your system uses a root password, you should try entering your own. " \
	"If that doesn't work you can sometimes click 'No Password' to proceed, but please read the " \
	"advice below first. Thanks."),\
	 "\n\n\n", \
	_("When programs are run on your computer, they usually do not have full control over it. " \
	"Often they will only have limited privileges: for instance, applications cannot reconfigure " \
	"your network settings."), \
	"\n\n", \
	_("In order to install this software system-wide, the setup program must have elevated " \
	"access rights to your computer. To give these to the setup program, you must enter your "\
	"password. This step is required to stop programs pretending to be you from gaining access."), \
	"\n\n",\
	_("Privilege separation can help stop the spread of viruses and spyware. By entering the " \
	"system password now, you can prevent this software from being maliciously modified later. For these " \
	"reasons it is recommended you provide the system password."), \
	NULL);
	
#define WIDTH 550
	
	GtkLabel *label = GTK_LABEL (gtk_label_new(""));
	gchar *why_message = WHY_MESSAGE;
	gtk_label_set_markup (label, why_message);
	gtk_label_set_line_wrap (label, TRUE);
	gtk_label_set_justify (label, GTK_JUSTIFY_FILL);
	gtk_label_set_selectable (label, TRUE);
	gtk_widget_show (GTK_WIDGET (label));
	gtk_widget_set_size_request (GTK_WIDGET(label), WIDTH, -1);
	g_free (why_message);
	
	GtkWidget *scroller = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scroller);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroller), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	
	GtkWidget *viewport = gtk_viewport_new (NULL, NULL);
	gtk_widget_set_size_request (GTK_WIDGET (viewport), WIDTH + 30, 250);	     
	gtk_widget_show (viewport);
	gtk_widget_modify_bg (viewport, GTK_STATE_NORMAL, &gtk_widget_get_style (viewport)->white);
	gtk_container_add (GTK_CONTAINER (scroller), viewport);

	/* grey top */
	GtkWidget *vbox = gtk_vbox_new (FALSE, 8);
	gtk_widget_show (vbox);
	GtkWidget *evbox = gtk_event_box_new ();
	gtk_widget_show (evbox);
	GdkColor color;
	gdk_color_parse ("grey", &color);
	gtk_widget_modify_bg (evbox, GTK_STATE_NORMAL, &color);
	GtkWidget *hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 0);

	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_inline (sizeof (auth_icon), auth_icon, FALSE, NULL);
	GtkWidget *lock = gtk_image_new_from_pixbuf (pixbuf);
	gtk_widget_show (lock);
	char *tmp = g_strdup_printf ("<span size='large'><b>%s</b></span>", _("Why am I asked for a password?"));
	GtkWidget *toplabel = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (toplabel), tmp);
	gtk_misc_set_alignment (GTK_MISC (toplabel), 0.0, 0.5); /* left align */
	g_free(tmp);
	gtk_widget_show(toplabel);
	
	gtk_box_pack_start (GTK_BOX (hbox), lock, FALSE, FALSE, 5);
	gtk_box_pack_start_defaults (GTK_BOX (hbox), toplabel);

	gtk_container_add (GTK_CONTAINER(evbox), GTK_WIDGET (hbox));
	gtk_box_pack_start_defaults (GTK_BOX (vbox), GTK_WIDGET (evbox));
	gtk_box_pack_start_defaults (GTK_BOX (vbox), GTK_WIDGET (label));
	gtk_container_add (GTK_CONTAINER (viewport), GTK_WIDGET (vbox));
	gtk_container_add (GTK_CONTAINER (dialog->vbox), GTK_WIDGET (scroller));

	gtk_dialog_run (dialog);

	gtk_widget_destroy (GTK_WIDGET (dialog));

	return TRUE;
}

static gint
ask_password (gchar *user, gchar *command, Options *options)
{
	GtkWidget *win;
	GtkWidget *vbox,
		*hbox,
			*image,
			*intro,
		*vbox2, // to reduce the padding for the "why" label
			*table,
				/* *label1, *commandlabel, */
				*label2, *entry,
			*eventbox, *why,
		*status,
		*hbox2,
			*btnbox1,
				*nopassword,
			*hbox3,
				*btnbox2,
					*cancel,
				*btnbox3,
					*ok;
	GtkWidget *btnalign, *btnhbox, *btnimage, *btnlabel;
	GtkTooltips *tips;
	GdkPixbuf *pixbuf;
	gboolean is_kde;
	gchar *tmp;
	gchar *title = NULL, *header = NULL, *description = NULL,
	      *ok_label = NULL, *cancel_label = NULL, *no_password_label = NULL;

	is_kde = !system ("ps ux | grep -v grep | grep -qE '(kicker|slicker|karamba)'");

	GUI.user = user;
	GUI.command = command;

	/* Mode-specific labels */
	switch (options->mode)
	{
	case APKG_INSTALL:
		title = g_strdup (_("Autopackage support code installation"));
		description = g_strdup (_("If yes, please enter it below. The autopackage support code will then be installed system-wide."));
		break;
	case MANAGE:
		title = g_strdup (_("Manage 3rd party software"));
		break;
	default:
		break;
	}

	/* Default labels */
	if (!title) {
		if (options->displayName)
			title = g_strdup_printf (_("%s - Software installation"), options->displayName);
		else
			title = g_strdup (_("Software installation"));
	}
	if (!header)
		header = _("Do you have the system password?");
	if (!description) {
		if (options->displayName)
			description = g_strdup_printf (
				_("If yes, please enter it below. \"%s\" will then be installed system-wide."),
				options->displayName);
		else
			description = g_strdup (_("If yes, please enter it below."));
	}
	if (!ok_label) ok_label = _("C_ontinue");
	if (!cancel_label) cancel_label = _("_Cancel");
	if (!no_password_label) no_password_label = _("_No Password");


	GUI.win = win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (win), 12);
	gtk_window_set_title (GTK_WINDOW (win), title);
	gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER);
//	gtk_window_set_resizable (GTK_WINDOW (win), FALSE);
	g_signal_connect (win, "delete_event", G_CALLBACK (gtk_true), NULL);
	g_free (title);

	// setup layout
	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_add (GTK_CONTAINER (win), vbox);
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	// set window icon and main image
	pixbuf = gdk_pixbuf_new_from_inline (sizeof (su_icon),
			(const guint8 *) su_icon, FALSE, NULL);
	image = gtk_image_new_from_pixbuf (pixbuf);
	gtk_window_set_icon (GTK_WINDOW (win), pixbuf);
	g_object_unref (pixbuf);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);

	// intro text
	intro = gtk_label_new ("");
	gtk_label_set_line_wrap (GTK_LABEL (intro), TRUE);
	tmp = g_strdup_printf ("<b>%s</b>          \n%s", header, description);
	gtk_label_set_markup (GTK_LABEL (intro), tmp);
	g_free (tmp);
	g_free (description);
	gtk_misc_set_alignment (GTK_MISC (intro), 0.0, 0.0);
	gtk_label_set_selectable (GTK_LABEL (intro), TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), intro, TRUE, TRUE, 0);

	vbox2 = gtk_vbox_new (FALSE, 2);
	gtk_box_pack_start (GTK_BOX (vbox), vbox2, FALSE, FALSE, 0);
	gtk_widget_show (vbox2);
	
	// more layout stuff
	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), 6);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
	gtk_box_pack_start (GTK_BOX (vbox2), table, FALSE, FALSE, 0);

/*	label1 = gtk_label_new (_("Command to run:"));
	gtk_misc_set_alignment (GTK_MISC (label1), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label1, 0, 1, 0, 1,
		GTK_FILL, 0,
		0, 0);

	commandlabel = gtk_label_new (command);
	gtk_misc_set_alignment (GTK_MISC (commandlabel), 0.0, 0.5);
	gtk_label_set_selectable (GTK_LABEL (commandlabel), TRUE);
	gtk_table_attach (GTK_TABLE (table), commandlabel, 1, 2, 0, 1,
		GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL,
		0, 0); */
	
	/* Password prompt */
	label2 = gtk_label_new (_("Password:"));
	gtk_misc_set_alignment (GTK_MISC (label2), 1.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label2, 0, 1, 1, 2,
		GTK_FILL, 0,
		0, 0);

	// password entry
	GUI.entry = entry = gtk_entry_new ();
	gtk_entry_set_invisible_char (GTK_ENTRY(entry), 9679);
	gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
	gtk_entry_set_activates_default (GTK_ENTRY (entry), TRUE);
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 1, 2,
		GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL,
		0, 0);
	gtk_widget_grab_focus (entry);

	// "Why am I asked to enter my password?"
	GUI.why = why = gtk_label_new ("");
	tmp = g_strdup_printf ("<u><span color='blue'><small>%s</small></span></u>",
			       _("Why am I asked for a password?"));
	gtk_label_set_markup (GTK_LABEL (why), tmp);
	g_free (tmp);
	gtk_label_set_selectable (GTK_LABEL (why), FALSE);
	gtk_widget_show (why);
	gtk_misc_set_alignment (GTK_MISC (why), 1.0, 0.5);
	eventbox = gtk_event_box_new (); /* we need an event box to set the cursor and receive clicks */
	gtk_widget_show (GTK_WIDGET(eventbox));
	gtk_container_add (GTK_CONTAINER(eventbox), why);
	gtk_box_pack_start (GTK_BOX (vbox2), eventbox, FALSE, FALSE, 0);
	gtk_widget_realize (eventbox);
	gdk_window_set_cursor (eventbox->window, gdk_cursor_new (GDK_HAND2));
	g_signal_connect (G_OBJECT(eventbox), "button-press-event", G_CALLBACK (why_pressed_cb), NULL);
	
	// status label
	GUI.status = status = gtk_label_new ("");
	tmp = g_strdup_printf ("<b>%s</b>",
		_("Checking password, please wait..."));
	gtk_label_set_markup (GTK_LABEL (status), tmp);
	gtk_label_set_selectable (GTK_LABEL (status), TRUE);
	gtk_box_pack_start (GTK_BOX (vbox), status, FALSE, FALSE, 0);
	g_free (tmp);

	hbox2 = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, FALSE, 0);

	// buttons
	btnbox1 = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (btnbox1), GTK_BUTTONBOX_START);
	gtk_box_pack_start (GTK_BOX (hbox2), btnbox1, TRUE, TRUE, 0);

	// add no password button
	GUI.nopasswd = nopassword = NULL;
	if (!options->rootonly) {
		GUI.nopasswd = nopassword = gtk_button_new ();
		gtk_container_add (GTK_CONTAINER (btnbox1), nopassword);

		btnalign = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
		gtk_container_add (GTK_CONTAINER (nopassword), btnalign);

		btnhbox = gtk_hbox_new (FALSE, 2);
		gtk_container_add (GTK_CONTAINER (btnalign), btnhbox);
		btnimage = gtk_image_new_from_stock (GTK_STOCK_QUIT, GTK_ICON_SIZE_BUTTON);
		gtk_box_pack_start (GTK_BOX (btnhbox), btnimage, FALSE, FALSE, 0);
		btnlabel = gtk_label_new (no_password_label);
		gtk_label_set_use_underline (GTK_LABEL (btnlabel), TRUE);
		gtk_box_pack_start (GTK_BOX (btnhbox), btnlabel, FALSE, FALSE, 0);
		g_signal_connect (nopassword, "clicked", G_CALLBACK (cb_no_password), NULL);
	}
	
	hbox3 = gtk_hbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (hbox2), hbox3, TRUE, TRUE, 0);

	btnbox2 = gtk_hbutton_box_new ();
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (btnbox2), 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (btnbox2), GTK_BUTTONBOX_END);

	// cancel button
	GUI.cancel = cancel = gtk_button_new ();
	btnalign = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	gtk_container_add (GTK_CONTAINER (cancel), btnalign);
	btnhbox = gtk_hbox_new (FALSE, 2);
	gtk_container_add (GTK_CONTAINER (btnalign), btnhbox);
	btnimage = gtk_image_new_from_stock (GTK_STOCK_CANCEL, GTK_ICON_SIZE_BUTTON);
	gtk_box_pack_start (GTK_BOX (btnhbox), btnimage, FALSE, FALSE, 0);
	btnlabel = gtk_label_new (cancel_label);
	gtk_label_set_use_underline (GTK_LABEL (btnlabel), TRUE);
	gtk_box_pack_start (GTK_BOX (btnhbox), btnlabel, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (btnbox2), cancel);
	g_signal_connect (cancel, "clicked", G_CALLBACK (cb_cancel), NULL);

	// ok button
	btnbox3 = gtk_hbutton_box_new ();
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (btnbox3), 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (btnbox3), GTK_BUTTONBOX_END);

	GUI.ok = ok = gtk_button_new ();
	btnalign = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	gtk_container_add (GTK_CONTAINER (ok), btnalign);
	btnhbox = gtk_hbox_new (FALSE, 2);
	gtk_container_add (GTK_CONTAINER (btnalign), btnhbox);
	btnimage = gtk_image_new_from_stock (GTK_STOCK_OK, GTK_ICON_SIZE_BUTTON);
	gtk_box_pack_start (GTK_BOX (btnhbox), btnimage, FALSE, FALSE, 0);
	btnlabel = gtk_label_new (ok_label);
	gtk_label_set_use_underline (GTK_LABEL (btnlabel), TRUE);
	gtk_box_pack_start (GTK_BOX (btnhbox), btnlabel, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (btnbox3), ok);
	GTK_WIDGET_SET_FLAGS (ok, GTK_CAN_DEFAULT);
	g_signal_connect (ok, "clicked", G_CALLBACK (cb_ok), NULL);

	/* Reverse button order if we're running in KDE */
	if (is_kde) {
		gtk_box_pack_start (GTK_BOX (hbox3), btnbox3, TRUE, TRUE, 0);
		gtk_box_pack_start (GTK_BOX (hbox3), btnbox2, FALSE, TRUE, 0);
	} else {
		gtk_box_pack_start (GTK_BOX (hbox3), btnbox2, TRUE, TRUE, 0);
		gtk_box_pack_start (GTK_BOX (hbox3), btnbox3, FALSE, TRUE, 0);
	}
	gtk_widget_grab_default (ok);


	tips = gtk_tooltips_new ();
	if (!options->rootonly)
		gtk_tooltips_set_tip (tips, nopassword,
				_("Click here if you do not know the system password, or if "
				"you want to install the software locally (as normal user)."),
				NULL);
	gtk_tooltips_set_tip (tips, cancel, _("Cancel the installation process."), NULL);
	gtk_tooltips_set_tip (tips, ok, _("Verify the password and continue installing the software system-wide."), NULL);


	gtk_window_set_default_size (GTK_WINDOW (win), 500, -1);
	gtk_widget_show_all (hbox);
	gtk_widget_show_all (table);
	gtk_widget_show_all (hbox2);
	gtk_widget_show (vbox);
	gtk_widget_show (win);

	gdk_threads_enter ();
	gtk_main ();
	gdk_threads_leave ();
	return 0;
}


static void
sigint_handler (int num)
{
	exit (EXIT_CANCEL);
}

static int
exit_nopasswd (const gchar *message)
{
	show_error (_("An error occured while trying to ask the user for the password: %s\n"), message);
	show_dialog (GTK_MESSAGE_INFO, GTK_BUTTONS_OK, _("Attempting to continue without asking for password...\n"));
	return EXIT_NOPASSWD;
}

int main (int argc, char *argv[])
{
	gchar *user = "root";
	gchar *command, *child_command;
	Options options;
	GError *error = NULL;
	gint ret;
	const char *sudo_args[4];

	/* Parse arguments & initialize */
	GUI.win = NULL;
	memset (&options, 0, sizeof (Options));
	command = create_command (parse_args (argc, argv, &options));

	signal (SIGINT, sigint_handler);

	if (!gbr_init(&error))
	{
		g_error_free (error);
		error = NULL;
	}
	/* Initialize gettext */
	gchar *locale_dir, *prefix;
	prefix = gbr_find_prefix ("/usr/libexec");
	locale_dir = g_build_filename(prefix, "..", "share/locale", NULL);
	bindtextdomain (GETTEXT_PACKAGE, locale_dir);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
	g_free (prefix);
	g_free (locale_dir);
	
	/* Initialize GTK */
	g_thread_init (NULL);
	gdk_threads_init ();
	gtk_init (&argc, &argv);
	memset (&GUI, 0, sizeof (GUI));

	child_command = autosu_child_command (command, &error);
	if (child_command == NULL) {
		return exit_nopasswd (error->message);
	}

	ret = su_needs_password (user, &error);
	if (ret == -1) {
		/* An error occured */
		return exit_nopasswd (error->message);

	} else if (ret == 0) {
		/* No password required for this account. */
		signal (SIGINT, SIG_DFL);
		ret = su_exec (user, NULL, child_command, NULL, NULL, &error);
		if (ret == -1)
			return EXIT_ABNORMAL;

		report_exit_code (ret);
		return 0;
	}


	/* Invalidate the sudo timestamp. We always want to ask whether the
	 * user wants to install as root. */
	system ("/usr/bin/sudo -K &>/dev/null");

	/* Try sudo */
	sudo_args[0] = "bash";
	sudo_args[1] = "-c";
	sudo_args[2] = child_command;
	sudo_args[3] = NULL;
	gboolean preserve_env = TRUE;
	sudo = sudo_session_new (user, sudo_args, NULL, preserve_env);

	if (sudo != NULL)
	while (1) {
		SudoStatus status;

		status = sudo_session_iterate (sudo);
		switch (status) {
		case SUDO_STATUS_CONTINUE:
			break;

		case SUDO_STATUS_AUTH:
			goto break_while;

		case SUDO_STATUS_SUCCESS:
			/* Sudo didn't even ask for a password. */
			signal (SIGINT, SIG_DFL);
			ret = sudo_session_waitchild (sudo);
			sudo_session_kill (sudo);
			sudo_session_free (sudo);

			if (ret == -1)
				exit (EXIT_ABNORMAL);
			report_exit_code (ret);
			exit (0);

		default:
			/* Something went wrong, we didn't even get the chance
			 * send the password! It could be possible that sudo
			 * doesn't support the -E option (preserve environment)
			 * so we should check that by trying one more time. */
			sudo_session_free (sudo);
			if (preserve_env) {
				preserve_env = FALSE;
				sudo = sudo_session_new (user, sudo_args, NULL, preserve_env);
				continue;
			}
			sudo = NULL;
			goto break_while;
		};
	}

	break_while:
	/* A password is required, so ask for it */
	ask_password (user, child_command, &options);

	g_assert_not_reached ();
	return EXIT_SYSERROR;
}
