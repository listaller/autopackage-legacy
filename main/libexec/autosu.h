/*  Autopackage Superuser - core
 *  Copyright (C) 2004,2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _AUTOSU_H_
#define _AUTOSU_H_

#include <glib.h>

/*
 * Standard gettext macros.
 */
#include <libintl.h>
#undef _
#define _(String) gettext(String)
#ifdef gettext_noop
#  define N_(String) gettext_noop (String)
#else
#  define N_(String) (String)
#endif

/* Exit codes */
#define EXIT_SUCCESS	0	/* Success */
#define EXIT_CANCEL	11	/* User clicked cancel */
#define EXIT_NOPASSWD	12	/* User doesn't have a password (autopackage will run command directly) */
#define EXIT_ABNORMAL	13	/* su exited abnormally */
#define EXIT_INCORRECT	14	/* User typed incorrect password 3 times */
#define EXIT_SYSERROR	15	/* System error */

gchar *autosu_child_command (gchar *command, GError **error);


/* Utility functions */
void safefree (gchar *password);
gchar *create_command (gchar **argv);
void report_exit_code (gint code);

extern void show_error (gchar *format, ...); /* this function is defined in autosu-gtk and autosu-tui */


/* Argument parsing */

typedef struct {
	enum {
		INSTALL,	/* Installing a package */
		APKG_INSTALL,	/* Installing autopackage support code */
		MANAGE		/* Running 3rd party software manager as root */
	} mode;
	char *displayName;
        gboolean rootonly;
} Options;

char **parse_args (int argc, char **argv, Options *options);


#endif /* _AUTOSU_H_ */
