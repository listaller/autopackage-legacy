/* Simple library for messing with pseudo terminals
 * Copyright (C) 2005  Hongli Lai
 * Copyright (C) 2001,2002 Red Hat, Inc
 *
 * Parts are directly stolen from pty.c from livte.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <stdio.h>
#include "ptyconf.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stropts.h>
#include <termios.h>
#include <signal.h>

#include "pty.h"

typedef struct {
	pid_t pid;
	Pty *pty;
	guint source_id;
} PidWatcher;

struct _PtyPrivate {
	PidWatcher *pid_watcher;
	int exit_code;
};


static GQuark error_domain = 0;

static gboolean watch_pid (PidWatcher *watch);
static void pty_reset_signal_handlers ();
static ssize_t n_read (int fd, void *buffer, size_t count);
static ssize_t n_write (int fd, const void *buffer, size_t count);


Pty *
pty_new (char **argv, PtySetupFunc func, gpointer user_data, GError **error)
{
	Pty *pty;
	int fd = -1;
	long flags;
	char *name;
	pid_t pid;

	/* Initialize GError if necessary */
	if (error_domain == 0)
		error_domain = g_quark_from_static_string ("pty-lib");


	#ifdef HAVE_GETPT
	/* Allocate pseudo terminal master */
	fd = getpt ();
	#endif

	if (fd == -1) {
		/* Try to allocate a pty by accessing the pty master multiplex. */
		fd = open ("/dev/ptmx", O_RDWR | O_NOCTTY);
		if (fd == -1 && errno == ENOENT) {
			fd = open ("/dev/ptc", O_RDWR | O_NOCTTY); /* AIX */
		}
	}

	if (fd == -1) {
		g_set_error (error, error_domain, PTY_ERROR_ALLOC,
			"Unable to allocate a pseudo terminal: %s",
			g_strerror (errno));
		return NULL;
	}

	/* Set it to blocking. */
	flags = fcntl (fd, F_GETFL);
	flags &= ~(O_NONBLOCK);
	fcntl (fd, F_SETFL, flags);

	/* Read the slave number and unlock it. */
	name = ptsname (fd);
	if (ttyname == NULL || grantpt (fd) != 0 || unlockpt (fd) != 0) {
		g_set_error (error, error_domain, PTY_ERROR_ALLOC,
			"Unable to allocate a pseudo terminal: %s",
			g_strerror (errno));
		close (fd);
		return NULL;
	}

	pty = (Pty *) g_new0 (Pty, 1);
	pty->tty = fd;
	pty->ttyname = g_strdup (name);

	if (!pty_fork (pty, argv, &pid, func, user_data, error)) {
		close (fd);
		free (pty->ttyname);
		free (pty);
		return NULL;
	}

	pty->child_pid = pid;

	pty->_priv = (PtyPrivate *) g_new0 (PtyPrivate, 1);
	pty->_priv->pid_watcher = (PidWatcher *) g_new0 (PidWatcher, 1);
	pty->_priv->pid_watcher->pid = pid;
	pty->_priv->pid_watcher->pty = pty;
	pty->_priv->pid_watcher->source_id = g_timeout_add_full (
		G_PRIORITY_LOW, 1000,
		(GSourceFunc) watch_pid, pty->_priv->pid_watcher,
		NULL);
	return pty;
}

void
pty_free (Pty *pty)
{
	int status;
	#ifdef HAVE_GETPGID
	pid_t pgrp;
	#endif

	g_return_if_fail (pty != NULL);

	if (pty->_priv->pid_watcher != NULL)
		pty->_priv->pid_watcher->pty = NULL;

	if (pty->child_pid != 0) {
		/* Kill child unless the pid watcher told us that
		 * the child has already exited. */
		#ifdef HAVE_GETPGID
		pgrp = getpgid (pty->child_pid);
		if (pgrp != -1)
			kill (-pgrp, SIGHUP);
		#endif
		kill (pty->child_pid, SIGHUP);
	}

	close (pty->tty);
	if (pty->child_pid != 0 && waitpid (pty->child_pid, &status, WNOHANG) != 0) {
		/* If the child exited; then the pid watcher is no longer needed.
		 * If not, let the pid watcher running. */
		if (pty->_priv->pid_watcher != NULL) {
			g_source_remove (pty->_priv->pid_watcher->source_id);
			g_free (pty->_priv->pid_watcher);
		}
	}

	g_free (pty->ttyname);
	g_free (pty->_priv);
	g_free (pty);
}

ssize_t
pty_read (Pty *pty, void *buffer, size_t count)
{
	g_return_val_if_fail (pty != NULL, -1);
	return read (pty->tty, buffer, count);
}

/* Read exactly count bytes from the pty. Blocks until at
 * least count bytes are received. */
ssize_t
pty_read_exact (Pty *pty, void *buffer, size_t count)
{
	g_return_val_if_fail (pty != NULL, -1);
	return n_read (pty->tty, buffer, count);
}

ssize_t
pty_write (Pty *pty, void *buffer, ssize_t count)
{
	size_t real_count;

	g_return_val_if_fail (pty != NULL, -1);

	if (count == -1)
		real_count = strlen ((char *) buffer);
	else
		real_count = (size_t) count;
	return n_write (pty->tty, buffer, real_count);
}

/** Checks whether the pseudo terminal is in ECHO mode.
 */
gboolean
pty_has_echo (Pty *pty)
{
	struct termios ios;
	g_return_val_if_fail (pty != NULL, FALSE);
	tcgetattr (pty->tty, &ios);
	return !!(ios.c_lflag & ECHO);
}

/** Wait until the child process has exited.
 */
gint
pty_waitpid (Pty *pty)
{
	int status;

	g_return_val_if_fail (pty != NULL, -1);

	if (pty->child_pid != 0) {
		g_source_remove (pty->_priv->pid_watcher->source_id);
		waitpid (pty->child_pid, &status, 0);
		g_free (pty->_priv->pid_watcher);
		pty->_priv->pid_watcher = NULL;

		pty->child_pid = 0;
		pty->_priv->exit_code = status;

	} else
		/* Child has already exited (found out by PID watcher). */
		status = pty->_priv->exit_code;

	return status;
}


gboolean
pty_fork (Pty *pty, char **argv, pid_t *return_pid, PtySetupFunc func, gpointer user_data, GError **error)
{
	int pipe1[2], pipe2[2];
	struct winsize size;
	struct termios ios;
	int fd, i;
	pid_t pid;
	char c = 0;

	if (pipe (pipe1) == -1) {
		g_set_error (error, error_domain, PTY_ERROR_PIPE,
			"Unable to allocate a pipe: %s", g_strerror (errno));
		return FALSE;
	}
	if (pipe (pipe2) == -1) {
		close (pipe1[0]);
		close (pipe1[1]);
		g_set_error (error, error_domain, PTY_ERROR_PIPE,
			"Unable to allocate a pipe: %s", g_strerror (errno));
		return FALSE;
	}

	pid = fork ();
	switch (pid) {
	case -1:
		/* Error */
		close (pipe1[0]);
		close (pipe1[1]);
		close (pipe2[0]);
		close (pipe2[1]);
		g_set_error (error, error_domain, PTY_ERROR_FORK,
			"Unable to fork a new process: %s", g_strerror (errno));
		return FALSE;

	case 0:
		if (func != NULL)
			(*func) (user_data);

		/* Child process; close the parent's ends of the pipes */
		close (pipe1[0]);
		close (pipe2[1]);

		/* Start a new session and become process-group leader. */
		setsid ();
		setpgid (0, 0);

		/* Close most descriptors. */
		int sc_open_max = sysconf (_SC_OPEN_MAX);
		for (i = 0; i < sc_open_max; i++)
			if (i != pipe2[0] && i != pipe1[1])
				close (i);

		/* Open the slave PTY, acquiring it as the controlling terminal
		 * for this process and its children. */
		fd = open (pty->ttyname, O_RDWR);
		if (fd == -1)
			_exit (1);

		#ifdef TIOCSCTTY
		ioctl (fd, TIOCSCTTY, fd);
		#endif
		setenv ("TERM", "xterm", 1);

		/* Make the slave PTY the controlling terminal */
		if (fd != STDIN_FILENO)
			dup2 (fd, STDIN_FILENO);
		if (fd != STDOUT_FILENO)
			dup2 (fd, STDOUT_FILENO);
		if (fd != STDERR_FILENO)
			dup2 (fd, STDERR_FILENO);

		/* Close the original slave descriptor, unless it's one of the stdio
		 * descriptors. */
		if (fd != STDIN_FILENO && fd != STDOUT_FILENO && fd != STDERR_FILENO)
			close (fd);

		if ((ioctl (fd, I_FIND, "ptem") == 0)
		 && (ioctl (fd, I_PUSH, "ptem") == -1)) {
			close (fd);
			_exit (1);
		}

		if ((ioctl (fd, I_FIND, "ldterm") == 0)
		 && (ioctl (fd, I_PUSH, "ldterm") == -1)) {
			close (fd);
			_exit (1);
		}

		if ((ioctl (fd, I_FIND, "ttcompat") == 0)
		 && (ioctl (fd, I_PUSH, "ttcompat") == -1)) {
			perror ("ioctl (fd, I_PUSH, \"ttcompat\")");
			close (fd);
			_exit (1);
		}

		pty_reset_signal_handlers ();

		/* Synchronize with parent */
		n_write (pipe1[1], &c, 1);
		fsync (pipe1[1]);
		n_read (pipe2[0], &c, 1);
		close (pipe1[1]);
		close (pipe2[0]);

		execvp (argv[0], argv);
		_exit (1);

		break;

	default:
		/* Parent process; close the child's end of the pipes */
		close (pipe1[1]);
		close (pipe2[0]);

		/* Synchronize with child */
		if (n_read (pipe1[0], &c, 1) <= 0) {
			/* Child failed to setup */
			close (pipe1[0]);
			close (pipe2[1]);
			g_set_error (error, error_domain, PTY_ERROR_UNKNOWN,
				"An unknown error occured.");
			return FALSE;
		}

		/* Set terminal size */
		memset (&size, 0, sizeof (size));
		size.ws_row = 24;
		size.ws_col = 80;
		ioctl (pty->tty, TIOCSWINSZ, &size);

		tcgetattr (pty->tty, &ios);
		ios.c_iflag &= ~(IGNCR | INLCR);
		ios.c_iflag |= ICRNL;
		ios.c_oflag |= ONLCR;
		tcsetattr (pty->tty, TCSANOW, &ios);

		/* Synchronize with child (again) */
		n_write (pipe2[1], &c, 1);
		close (pipe1[0]);
		close (pipe2[1]);

		if (return_pid)
			*return_pid = pid;

		break;
	};
	return TRUE;
}


/**********************
 * Utility functions
 **********************/

/* Periodically call waitpid() on a child process to make sure
 * zombie processes are cleaned up. */
static gboolean
watch_pid (PidWatcher *watch)
{
	int status;

	if (waitpid (watch->pid, &status, WNOHANG) == 0)
		return TRUE;
	else {
		/* Child exited */
		if (watch->pty != NULL) {
			watch->pty->child_pid = 0;
			watch->pty->_priv->exit_code = status;
			watch->pty->_priv->pid_watcher = NULL;
		}
		g_free (watch);
		return FALSE;
	}
}

/* Reset the handlers for all known signals to their defaults. The parent
 * (or one of the libraries it links to) may have changed one to be ignored. */
static void
pty_reset_signal_handlers ()
{
	int sigs[] = {
		SIGHUP,  SIGINT,  SIGILL,  SIGABRT, SIGFPE,  SIGKILL,
		SIGSEGV, SIGPIPE, SIGALRM, SIGTERM, SIGCHLD, SIGCONT,
		SIGSTOP, SIGTSTP, SIGTTIN, SIGTTOU,
		#ifdef SIGBUS
			SIGBUS,
		#endif
		#ifdef SIGPOLL
			SIGPOLL,
		#endif
		#ifdef SIGPROF
			SIGPROF,
		#endif
		#ifdef SIGSYS
			SIGSYS,
		#endif
		#ifdef SIGTRAP
			SIGTRAP,
		#endif
		#ifdef SIGURG
			SIGURG,
		#endif
		#ifdef SIGVTALRM
			SIGVTALRM,
		#endif
		#ifdef SIGXCPU
			SIGXCPU,
		#endif
		#ifdef SIGXFSZ
			SIGXFSZ,
		#endif
		#ifdef SIGIOT
			SIGIOT,
		#endif
		#ifdef SIGEMT
			SIGEMT,
		#endif
		#ifdef SIGSTKFLT
			SIGSTKFLT,
		#endif
		#ifdef SIGIO
			SIGIO,
		#endif
		#ifndef SIGCLD
			SIGCLD,
		#endif
		#ifdef SIGPWR
			SIGPWR,
		#endif
		#ifdef SIGINFO
			SIGINFO,
		#endif
		#ifdef SIGLOST
			SIGLOST,
		#endif
		#ifndef SIGWINCH
			SIGWINCH,
		#endif
		#ifdef SIGUNUSED
			SIGUNUSED,
		#endif
		SIGUSR1, SIGUSR2
	};
	int i;

	for (i = 0; i < sizeof (sigs) / sizeof (int); i++)
		signal (sigs[i], SIG_DFL);
}

/* Like read, but hide EINTR and EAGAIN. */
static ssize_t
n_read (int fd, void *buffer, size_t count)
{
	size_t n = 0;
	char *buf = buffer;
	int i;
	while (n < count) {
		i = read(fd, buf + n, count - n);
		switch (i) {
		case 0:
			return n;
			break;
		case -1:
			switch (errno) {
			case EINTR:
			case EAGAIN:
			#ifdef ERESTART
			case ERESTART:
			#endif
				break;
			default:
				return -1;
			}
			break;
		default:
			n += i;
			break;
		}
	}
	return n;
}

/* Like write, but hide EINTR and EAGAIN. */
static ssize_t
n_write (int fd, const void *buffer, size_t count)
{
	size_t n = 0;
	const char *buf = buffer;
	int i;
	while (n < count) {
		i = write(fd, buf + n, count - n);
		switch (i) {
		case 0:
			return n;
			break;
		case -1:
			switch (errno) {
			case EINTR:
			case EAGAIN:
			#ifdef ERESTART
			case ERESTART:
			#endif
				break;
			default:
				return -1;
			}
			break;
		default:
			n += i;
			break;
		}
	}
	return n;
}
