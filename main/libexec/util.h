/*
 * luau (Lib Update/Auto-Update): Simple Update Library
 * Copyright (C) 2003  David Eklund
 *
 * - This library is free software; you can redistribute it and/or             -
 * - modify it under the terms of the GNU Lesser General Public                -
 * - License as published by the Free Software Foundation; either              -
 * - version 2.1 of the License, or (at your option) any later version.        -
 * -                                                                           -
 * - This library is distributed in the hope that it will be useful,           -
 * - but WITHOUT ANY WARRANTY; without even the implied warranty of            -
 * - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         -
 * - Lesser General Public License for more details.                           -
 * -                                                                           -
 * - You should have received a copy of the GNU Lesser General Public          -
 * - License along with this library; if not, write to the Free Software       -
 * - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA -
 */
 
/** @file util.h
 * \brief Basic methods to make life easier
 *
 * Provides some simple convenience methods that could be applied to any project.
 */
 
#ifndef UTIL_H
#define UTIL_H

#ifdef WITH_DMALLOC
#  include <dmalloc.h>
#endif

#include <stdio.h>

#include <glib.h>


#ifdef WITH_LEAKBUG
#  include <leakbug.h>
#  define g_free(ptr) if (ptr != NULL) { free(ptr); }
#  define g_strdup strdup
#  define g_calloc calloc
#  define g_malloc malloc
#endif

/// Turn debugging on or off
#define DEBUG 0

/// Output debugging messages
#if DEBUG
#  define DBUGOUT(args...) do { printf("luau: %s: %s: ", __FILE__, __func__); printf(args); printf("\n"); } while (0)
#else
#  define DBUGOUT(args...)
#endif

/// call g_string_free on a pointer only if it's not == NULL
#define nnull_g_string_free(ptr, boole)    do {            \
	if (ptr != NULL) { g_string_free(ptr, boole); }        \
	                                         } while (0)

/// temp directory
#define TEMP_DIR "/tmp"

/* General utility functions */
/// Check if two strings are equal
inline int lutil_streq(const char *str1, const char *str2);
/// Check if two strings are equal, ignoring case
inline int lutil_strcaseeq(const char *str1, const char *str2);
/// Create a new string
char* lutil_createString(int length);
/// Append any number of strings together into one.
char* lutil_vstrcreate(const char *src1, ...);
/// Like sprintf, but allocate memory for and return the string
char* lutil_mprintf(const char *template, ...);
/// Join an array of strings together separated by the given delimiter
char* lutil_strjoin(const char *delim, const GPtrArray *strings);
/// Split a string by character delim into a GPtrArray
GPtrArray* lutil_gsplit(const char *delim, const char *str);

char* lutil_valistToString(const char *template, va_list args);

/// See if a string exists in a GPtrArray of strings (char *)
gboolean lutil_findString(const GPtrArray *array, const char *string);

/// Create a valid temporary filename
char* lutil_getTempFilename(void);
gboolean lutil_fileExists(const char *path);
gboolean lutil_isDirectory(const char *path);

/// Compare two gint pointers
gint lutil_gintCompare(gpointer p1, gpointer p2);
/// Compare two ints
int lutil_intcmp(int i1, int i2);
char* lutil_sizeToString(int size, int sigfig);

/// Free a GPtrArray and all the pointers it stores
void lutil_destroyArray(GPtrArray *array);

#endif
