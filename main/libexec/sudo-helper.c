#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <glib.h>

int
main (int argc, char *argv[])
{
	if (argc < 3) {
		printf ("This program is used internally and shouldn't be used directly.\n");
		return 2;
	}

	signal (SIGHUP, SIG_IGN);
	if (argv[1][0] != 0)
		setenv ("LC_ALL", argv[1], 1);
	printf ("SUDO_PASSED\n");
	execvp (argv[2], argv + 2);
	perror (g_strdup_printf ("%s: %s", argv[0], argv[1]));
	return 1;
}
