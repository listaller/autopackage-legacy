/*  Autopackage Superuser
 *  Copyright (C) 2004  Hongli Lai <h.lai@chello.nl>
 *                      Mike Hearn <mike@navi.cx>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include <glib.h>
#include <glib/gprintf.h>

static gboolean
exists (char *path)
{
	struct stat buf;
	if (stat(path, &buf) == -1)
		return FALSE;
	return TRUE;
}

static char *
get_xauth_path ()
{
	#define check(p) if (exists (p)) return p

	check ("/usr/X11R6/bin/xauth");
	check ("/usr/X11R7/bin/xauth");
	check ("/usr/X11/bin/xauth");
	check ("/usr/bin/xauth");
	check ("/bin/xauth");
	check ("/sbin/xauth");
	check ("/usr/sbin/xauth");
	check ("/usr/local/bin/xauth");
	check ("/usr/local/sbin/xauth");
	check ("/usr/local/X11R6/bin/xauth");
	check ("/usr/local/X11/bin/xauth");
	/* I'll commit seppuku if we haven't covered enough paths already. */

	/* fixme: what else goes here? */
	perror ("could not locate xauth");
	exit (1);

	#undef check
}

static void
setup_xauth (int shmid)
{
	/* set $XAUTHORITY to ~/.Xauthority */
	char *result = g_strdup_printf("XAUTHORITY=%s/.Xauthority", g_get_home_dir());
	putenv(result);
	free(result);

	char *commands = shmat(shmid, NULL, 0);
	if (commands == (char*)-1) {
		perror("could not attach to SHM segment");
		exit(1);
	}

	char *xauth = g_strdup_printf ("%s -f \"%s/.Xauthority\" -q", get_xauth_path (), g_get_home_dir ());
	FILE *out = popen(xauth, "w");
	fwrite(commands, strlen(commands), 1, out);
	pclose(out);
	g_free (xauth);
	shmdt(commands);
}

int
main (int argc, char *argv[])
{
	char **command;
	int err;
        int shmid = -1;

        system("echo HERE! >/tmp/out");
	if (argc < 5)
		return 15;

	err = dup (2);
	fclose (stdin);
	fclose (stdout);
	fclose (stderr);

	if (!strcmp(argv[1], "--xauth-shm-id")) {
		argv++; // shift
		shmid = atoi(argv[1]);
		argv++;
		system(g_strdup_printf("echo %d >>/tmp/out", shmid));
        } else
		system("echo FAILED >>/tmp/out");

	if (argv[1] && *(argv[1]))
		stdin = freopen (argv[1], "r", stdin);
	if (argv[2] && *(argv[2]))
		stdout = freopen (argv[2], "w", stdout);
	if (argv[3] && *(argv[3]))
		stderr = freopen (argv[3], "w", stderr);

        if (shmid != -1) setup_xauth(shmid);

	setenv ("LC_ALL", argv[4], 1);
	setenv ("LANGUAGE", argv[4], 1);
	setenv ("LANG", argv[4], 1);
	
	command = argv + 5;
	execvp (command[0], command);

	fclose (stderr);
	dup2 (err, 2);
	stderr = fdopen (2, "w");
	perror (command[0]);
	return 1;
}
