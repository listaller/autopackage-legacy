#include <curl/curl.h>

int progress_callback(void *clientp,
                      double dltotal,
                      double dlnow,
                      double ultotal,
                      double ulnow)
{
	printf("\rDownloading ... %d %% ", (int)(dlnow * 100 / dltotal));
	return 0;
}

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		printf("Usage: autopackage-curl <url> <newfile>\n");
		return 1;
	}
	if(curl_global_init(CURL_GLOBAL_ALL) != 0)
	{
		printf("Error: curl_global_init returned non-zero.\n");
		return 1;
	}
	CURL* easyhandle = curl_easy_init();
	if(easyhandle == NULL)
	{
		printf("Error: curl_easy_init returned NULL.\n");
		return 1;
	}
	FILE* file = fopen(argv[2], "wb");
	if(file == NULL)
	{
		printf("Error creating %s.\n", argv[2]);
		return 1;
	}
	curl_easy_setopt(easyhandle, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(easyhandle, CURLOPT_URL, argv[1]);
	curl_easy_setopt(easyhandle, CURLOPT_FAILONERROR, 1);
	curl_easy_setopt(easyhandle, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(easyhandle, CURLOPT_NOPROGRESS, 0);
	curl_easy_setopt(easyhandle, CURLOPT_PROGRESSFUNCTION, progress_callback);
	CURLcode success = curl_easy_perform(easyhandle);
	printf("\n");
	fclose(file);
	if(success != 0)
	{
		printf("Error downloading %s.\n", argv[1]);
		return 1;
	}
	return 0;
}
