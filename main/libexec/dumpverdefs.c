/* Dump an ELF .gnu.version_d section (version definitions)
   
   Optionally check for the given version definitions and return 0 if
   all are found, writing the ones that aren't to stdout.

   Copyright (C) 2004 Mike Hearn <mike@navi.cx>
   This file is part of autopackage: http://autopackage.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <assert.h>

#include <link.h>

/* support building for 32 bit and 64 bit platforms */
#ifndef DVD_WORDSIZE
# define DVD_WORDSIZE 32
#endif

#undef ElfW
#define ElfW(type) _ElfW (Elf, DVD_WORDSIZE, type)

#define elfcast(a, b) (ElfW(a)*)(b)

static void __attribute__((format(printf, 1, 2))) bail(char *message, ...)
{
    fprintf( stderr, "dumpverdefs: " );
    
    va_list args;
    va_start( args, message );
    vfprintf( stderr, message, args );
    va_end( args );

    exit( 1 );
}

static void help()
{
    printf( "dumpverdefs: a tool to dump the gnu verneed entries of an ELF binary\n" \
            "Usage: dumpverdefs /path/to/binary [verdef1 verdef2 ...]\n\n" \
            
            "(C) 2004 Mike Hearn <mike@plan99.net>\n\n" \
            
            "If given version definitions, dumpverdef will check they are all\n" \
            "present and return with an exit code of 0 if so. If not it will\n" \
            "write the missing symbols to stdout (one per line) and exit with a\n" \
            "code of 1.\n\n" \
            
            "If no version definitions are given, it will write all definitions\n" \
            "found to stdout.\n\n" \

            "This program is a part of the autopackage Linux installer framework\n" \
            "and is not meant to be run by users.\n\n" );

    exit( 0 );
}

/* retrieves the string from the given strtab  */
static char *resolve_string(ElfW(Ehdr) *header, int tabindex, ElfW(Word) offset)
{
    ElfW(Shdr) *section = elfcast( Shdr, (char*)header + header->e_shoff );

    section = elfcast( Shdr, (char*)section + (tabindex * sizeof(ElfW(Shdr))) );

    assert( section->sh_type == SHT_STRTAB );

    char *string = (char*)header + section->sh_offset;
    
    return string + offset;
}

int main(int argc, char *argv[])
{
    argc--; argv++; /* don't care about our own name  */
    
    if (!argc) help();

    char *name = argv[0];
    
    argc--; argv++;

    struct stat buf;
    int e = stat( name, &buf );
    if (e == -1) bail( "could not open %s: %s\n", name, strerror(errno) );

    int fd = open( name, O_RDONLY );
    if (fd < 0) bail( "could not open %s: %s\n", name, strerror(errno) );
    
    void *binary = mmap( NULL, buf.st_size, PROT_READ, MAP_SHARED, fd, 0 );
    if (binary == MAP_FAILED) bail( "could not mmap %s: %s\n", name, strerror(errno) );

    close( fd );

    ElfW(Ehdr) *header = binary;
    if (strncmp( (char *) header->e_ident, ELFMAG, SELFMAG ) != 0)
        bail( "bad ident sequence, %s not an ELF file?\n", name );

    if (((header->e_ident[EI_CLASS] == ELFCLASS32) && (DVD_WORDSIZE != 32)) ||
        ((header->e_ident[EI_CLASS] == ELFCLASS64) && (DVD_WORDSIZE != 64)))
        bail( "32/64 mismatch: elfclass is %d but compiled for %dbit machine\n", header->e_ident[EI_CLASS], DVD_WORDSIZE );

    ElfW(Shdr) *sectab = binary + header->e_shoff;
    ElfW(Shdr) *section = NULL;

    for (section = sectab; section < &sectab[header->e_shnum]; section++)
        if (section->sh_type == SHT_GNU_verdef) break;

    if (section->sh_type != SHT_GNU_verdef)
        bail( "failed to find .gnu.version_d section, not a versioned Linux DSO?\n" );

    ElfW(Verdef) *def = binary + section->sh_offset;
    while (def)
    {
        ElfW(Verdaux) *aux = elfcast( Verdaux, (char*)def + def->vd_aux );

        char *symbol = resolve_string( binary, section->sh_link, aux->vda_name );

        assert( symbol );
        
        if (!argc && !(def->vd_flags & VER_FLG_BASE)) /* only print if no arguments given  */
            printf( "%s\n", symbol );

        /* set each argument to null as we find them  */
        for (char **s = argv; s < &argv[argc]; s++)
        {
            if (*s && !strcmp( *s, symbol ))
            {
                *s = NULL;
                break;
            }
        }
        
        if (!def->vd_next) def = NULL;
        else def = elfcast( Verdef, (char*)def + def->vd_next );
    }

    if (!argc) return 0;
    
    /* if we have non-NULL arguments left, we were missing some version definitions  */
    for (char **s = argv; s < &argv[argc]; s++)
    {
        if (*s)
        {
            printf( "%s\n", *s );
            return 1;
        }
    }
    
    return 0;
}
