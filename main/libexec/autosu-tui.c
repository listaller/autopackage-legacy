/*  Autopackage Superuser
 *  Copyright (C) 2004,2005  Hongli Lai <h.lai@chello.nl>
 *                           Mike Hearn <mike@navi.cx>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <signal.h>

#include "binreloc.h"
#include "autosu.h"
#include "autosu-su.h"
#include "autosu-sudo.h"


static FILE *tty = NULL;
static int consoleWidth = 80;
static SudoSession *sudo = NULL;


void
show_error (gchar *format, ...)
{
	va_list ap;
	gchar *msg;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);
	fputs ("\e[1;31m", tty);
	fprintf (tty, _("Error: %s\n"), msg);
	fputs ("\e[0m", tty);
	fflush (tty);
}


static void
printline (char *text)
{
	int len, i;

	fputc (' ', tty);
	fputs (text, tty);
	len = strlen (text) + 1;
	if (len < consoleWidth)
		for (i = 0; i < consoleWidth - len; i++)
			fputc (' ', tty);
	fputc ('\n', tty);
}


static void
printchoices (int invalid, Options *options)
{
	int i;

	setvbuf (tty, NULL, _IOFBF, 0);

	fputs ("\e[1;37m", tty);
	for (i = 0; i < consoleWidth; i++)
		fputc ('_', tty);
	fputc ('\n', tty);
	fputs ("\e[1;44m\e[1;37m", tty);
	printline ("");

	if (!invalid)
	{
		char *display;

		if (options->mode == APKG_INSTALL)
		{
			printline ("[ autopackage support code installation ]");
			printline ("");
		} else if (options->displayName)
		{
			display = g_strdup_printf (_("[ Installing: %s ]"), options->displayName);
			printline (display);
			g_free (display);
			printline ("");
		}

		fputs ("\e[1;33m", tty);
                if (options->rootonly)
                        printline(_("You need the system password in order to install this package."));
                else
                        printline (_("Do you have the system password?"));
		fputs ("\e[1;37m", tty);

		if (options->rootonly)
			printline(_("Please enter it now."));
		else
			printline(_("If so, please enter it."));
		printline ("");
	}

	fputs ("\e[1;36m", tty);

	printline (_("1. Yes, continue with password (install system-wide)."));
	if (!options->rootonly) {
		printline (_("2. No, I don't know the system password (install as normal user)."));
		printline (_("3. Cancel installation."));
	} else
		printline (_("2. Cancel installation."));
               
	fputs ("\e[1;37m", tty);

	for (i = 0; i < consoleWidth; i++)
		fputc ('_', tty);
	fprintf (tty, "\033[0m\n");

	fputs (_("Enter the number of a choice and press ENTER: "), tty);

	fflush (tty);
	setlinebuf (tty);
}


/* The 'getpass' man page says getpass() is obsolete, so I'll just write my own... */
static char *
my_getpass (const char *prompt)
{
	struct termios ios;
	char *buf, *result;
	int fd;
	FILE *f;

	fprintf (tty, "%s", prompt);
	fflush (tty);

	/* Open controlling terminal */
	fd = open ("/dev/tty", O_RDWR);
	if (fd == -1 || (f = fdopen (fd, "r")) == NULL) {
		show_error (_("Unable to read password from terminal:\n%s"),
			g_strerror (errno));
		exit (EXIT_SYSERROR);
	}

	/* Disable echo */
	tcgetattr (fd, &ios);
	ios.c_lflag &= ~ECHO;
	tcsetattr (fd, TCSANOW, &ios);

	/* Does anyone have a password longer than 1 KB? */
	buf = g_new0 (gchar, 1024);
	result = fgets (buf, 1024, f);
	fprintf (tty, "\n");

	/* Enable echo */
	ios.c_lflag |= ECHO;
	tcsetattr (fd, TCSANOW, &ios);

	if (!result) {
		show_error (_("Unable to read password from terminal."));
		exit (EXIT_SYSERROR);
	}

	fclose (f);

	return buf;
}


static void
ask_password (char *user, char *command, Options *options)
{
	char line[64];
	int i;
	GError *error = NULL;

	/* Present menu */
	printchoices (0, options);
	fgets (line, sizeof (line), stdin);

	/* Ask user to choose a menu */
	while (line[0] != '1' && line[0] != '2' && line[0] != '3') {
		fprintf (tty, "\n");
		if (line[0] == '\n')
			show_error (_("Please enter a choice."));
		else
			show_error (_("Invalid choice '%c'."), line[0]);
		printchoices (1, options);
		fgets (line, sizeof (line), stdin);
	}

	if ((line[0] == '2') && !options->rootonly)
		exit (EXIT_NOPASSWD);
	else if ((line[0] == '2') && options->rootonly)
		exit (EXIT_CANCEL);
	else if (line[0] == '3')
		exit (EXIT_CANCEL);

	/* Ask password up to 3 times */
	for (i = 0; i < 3; i++) {
		char *password;
		gint ret;

		password = my_getpass (_("Password: "));
		ret = su_check_password (user, password, &error);

		if (ret == 1) {
			/* Password correct */
			signal (SIGINT, SIG_DFL);
			ret = su_exec (user, password, command, NULL, NULL, NULL);
			if (ret == -1)
				exit (EXIT_ABNORMAL);

			report_exit_code (ret);
			exit (0);

		} else if (ret == 0) {
			/* Wrong password */
			
			/* Try passing the password to sudo */
			if (sudo != NULL) {
				SudoAuthStatus auth;
				gint ret;

				auth = sudo_session_authenticate (sudo, password);
				safefree (password);
				password = NULL;

				switch (auth) {
				case SUDO_AUTH_SUCCESS:
					/* Authentication successfull */
					signal (SIGINT, SIG_DFL);

					ret = sudo_session_waitchild (sudo);
					sudo_session_kill (sudo);
					sudo_session_free (sudo);

					if (ret == -1)
						exit (EXIT_ABNORMAL);
					report_exit_code (ret);
					exit (0);

				case SUDO_AUTH_FAIL:
					/* Incorrect password. Keep interating until sudo
					 * is ready to ask for a password again */
					while (1) {
						SudoStatus status;

						status = sudo_session_iterate (sudo);
						switch (status) {
						case SUDO_STATUS_CONTINUE:
							break;
						case SUDO_STATUS_AUTH:
							goto incorrect;
						case SUDO_STATUS_FAIL:
							/* User entered too many wrong passwords */
							exit (EXIT_INCORRECT);
						default:
							/* Sudo exited abnormally; don't call sudo again. */
							sudo_session_free (sudo);
							sudo = NULL;
							goto incorrect;
						};
					}

				default:
					/* Sudo exited with an error, but it wasn't
					 * because of an incorrect password. Don't call
					 * sudo again. */
					sudo_session_free (sudo);
					sudo = NULL;
					goto incorrect;
				};

			} else {
				incorrect:
				safefree (password);
				if (i == 0)
					show_error (_("Password incorrect, please try again."));
				else if (i == 1)
					show_error (_("Password incorrect, please try again. You have one more chance."));
			}

		} else {
			/* An error occured */
			safefree (password);
			show_error (_("An error occured while checking the password:\n%s"),
				error->message);
			exit (EXIT_ABNORMAL);
		}
	}

	exit (EXIT_INCORRECT);
}


static void
sigint_handler (int num)
{
	exit (EXIT_CANCEL);
}

static int
exit_nopasswd (const gchar *message)
{
	fprintf (stderr, _("An error occured while trying to ask the user for the password: %s\n"), message);
	fprintf (stderr, _("Attempting to continue without asking for password...\n"));
	return EXIT_NOPASSWD;
}

int main (int argc, char *argv[])
{
	gchar *user = "root";
	gchar *command, *child_command;
	Options options;
	struct winsize size;
	gint ret;
	GError *error = NULL;
	const char *sudo_args[4];

	/* Parse arguments & initialize */
	memset (&options, 0, sizeof (Options));
	command = create_command (parse_args (argc, argv, &options));

	tty = fopen ("/dev/tty", "w");
	if (!tty)
		return EXIT_SYSERROR;

	signal (SIGINT, sigint_handler);

	if (!gbr_init(&error))
	{
		g_error_free (error);
		error = NULL;
	}
	/* Initialize gettext */
	gchar *locale_dir, *prefix;
	prefix = gbr_find_prefix ("/usr/libexec");
	locale_dir = g_build_filename(prefix, "..", "share/locale", NULL);
	bindtextdomain (GETTEXT_PACKAGE, locale_dir);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
	g_free (prefix);
	g_free (locale_dir);

	/* Initialize autosu core */
	child_command = autosu_child_command (command, &error);
	if (child_command == NULL) {
		return exit_nopasswd (error->message);
	}

	ret = su_needs_password (user, &error);
	if (ret == -1) {
		/* An error occured */
		return exit_nopasswd (error->message);

	} else if (ret == 0) {
		/* No password required for this account. */
		signal (SIGINT, SIG_DFL);

		ret = su_exec (user, NULL, child_command, NULL, NULL, &error);
		if (ret == -1)
			return EXIT_ABNORMAL;

		report_exit_code (ret);
		return 0;

	}


	/* Invalidate the sudo timestamp. We always want to ask whether the
	 * user wants to install as root. */
	system ("/usr/bin/sudo -K &>/dev/null");

	/* Try sudo */
	sudo_args[0] = "bash";
	sudo_args[1] = "-c";
	sudo_args[2] = child_command;
	sudo_args[3] = NULL;
	gboolean preserve_env = TRUE;
	sudo = sudo_session_new (user, sudo_args, NULL, preserve_env);

	if (sudo != NULL)
	while (1) {
		SudoStatus status;

		status = sudo_session_iterate (sudo);
		switch (status) {
		case SUDO_STATUS_CONTINUE:
			break;

		case SUDO_STATUS_AUTH:
			goto break_while;

		case SUDO_STATUS_SUCCESS:
			/* W00t! Sudo didn't even ask for a password! */
			signal (SIGINT, SIG_DFL);
			ret = sudo_session_waitchild (sudo);
			sudo_session_kill (sudo);
			sudo_session_free (sudo);

			if (ret == -1)
				exit (EXIT_ABNORMAL);
			report_exit_code (ret);
			exit (0);

		default:
			/* Something went wrong, we didn't even get the chance
			 * send the password! It could be possible that sudo
			 * doesn't support the -E option (preserve environment)
			 * so we should check that by trying one more time. */
			sudo_session_free (sudo);
			if (preserve_env) {
				preserve_env = FALSE;
				sudo = sudo_session_new (user, sudo_args, NULL, preserve_env);
				continue;
			}
			sudo = NULL;
			goto break_while;
		};
	}
	break_while:


	/* A password is required, so ask for it */

	/* But first, get the console width */
	if (ioctl (1, TIOCGWINSZ, &size) == 0)
		consoleWidth = size.ws_col;

	ask_password (user, child_command, &options);
	return EXIT_SYSERROR;  /* Never reached */
}
