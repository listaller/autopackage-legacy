/*
 * compareVersions
 * Compares two version numbers and returns whether version1 > version2.
 * Exit codes: 0 = yes, 1 = no
 *
 * Copyright (C) 2005 autopackage team.
 *
 * Code licensed under the LGPL.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


/* Set to 1 if you want to unit test compareVersions. */
#define DEBUG 0

/* Debugging on */
//#define trace(fmt, ...) fprintf(stderr, fmt, __VA_ARGS__)
/* Debugging off */
#define trace(...)


/**
 * Remove all occurrences of specified characters from a string.
 * str will be modified. Note that this function removes individual characters,
 * not substrings!
 *
 * @param str The string to remove from.
 * @param chars_to_remove A string containing the characters to remove.
 * @require str != NULL && chars_to_remove != NULL
 */
void
removeChars (char *str, const char *chars_to_remove)
{
	int len, i;

	len = strlen (str);
	for (i = 0; i < len; i++) {
		const char *rm;

		for (rm = chars_to_remove; *rm != '\0'; rm++) {
			if (str[i] == *rm) {
				/* Current character must be removed.
				 * Move all data after this character one position to the left,
				 * including terminating NULL.
				 */
				memmove (str + i, str + i + 1, len - i);
				len--;
				i--;
				break;
			}
		}
	}
}


/**
 * Replace all occurences of from to to in string str.
 * str will be modified.
 *
 * @param str The string to modify.
 * @param from The character to search for.
 * @param the character to replace to.
 * @require str != NULL
 */
void
replace (char *str, char from, char to)
{
	for (; *str != '\0'; str++) {
		if (*str == from)
			*str = to;
	}
}


/**
 * Convert a string to lowercase. str will be modified.
 *
 * @param str The string to convert.
 * @require str != NULL
 */
void
toLowerCase (char *str)
{
	for (; *str != '\0'; str++) {
		unsigned char c = (unsigned char) *str;
		if (isupper (c))
			*str = (char) tolower (c);
	}
}


/**
 * Checks whether str contains digits. An empty string contains no digits.
 *
 * @param str The string to check.
 * @return 1 if yes, 0 if not.
 * @require str != NULL
 */
int
containsDigits (const char *str)
{
	for (; *str != '\0'; str++) {
		if (isdigit ((unsigned char) *str))
			return 1;
	}
	return 0;
}


/**
 * Checks whether str contains only alphabetic characters (a-z and A-Z).
 * Also returns 1 for empty strings.
 *
 * @param str The string to check.
 * @return 1 if true, 0 if false.
 * @require str != NULL
 */
int
containsAlphaOnly (const char *str)
{
	for (; *str != '\0'; str++) {
		if (!isalpha ((unsigned char) *str))
			return 0;
	}
	return 1;
}


/**
 * A string array data structure.
 */
typedef struct {
	unsigned int size;
	char **str;
} StrArray;


/**
 * Split a string into an array.
 */
StrArray *
split (const char *str, char delimiter)
{
	StrArray *array;
	char *lastBegin, *tmp;
	int i, len, index;

	array = (StrArray *) malloc (sizeof (StrArray));
	array->size = 0;
	tmp = strdup (str);

	/* Determine the size of the array. */
	len = strlen (tmp);
	for (i = 0; i <= len; i++) {
		if (tmp[i] == delimiter || tmp[i] == '\0')
			array->size++;
	}

	/* Create the array. */
	array->str = malloc (sizeof (char *) * array->size);
	lastBegin = tmp;

	for (i = 0, index = 0; i <= len; i++) {
		if (tmp[i] == delimiter || tmp[i] == '\0') {
			tmp[i] = '\0';
			array->str[index] = strdup (lastBegin);
			index++;
			lastBegin = tmp + i + 1;
		}
	}

	free (tmp);
	return array;
}


/**
 * Set an element in a StrArray. The old element will be freed.
 * str is not copied.
 *
 * @param array The StrArray object to set.
 * @param index The index of the element to set.
 * @param str The string which will be put into the array.
 * @require array != NULL && index < array->size && str != NULL
 */
void
str_array_set (StrArray *array, unsigned int index, char *str)
{
	free (array->str[index]);
	array->str[index] = str;
}


/**
 * Free a StrArray and its elements.
 *
 * @param array The StrArray object to free.
 * @require array != NULL
 */
void
str_array_free (StrArray *array)
{
	int i;

	for (i = 0; i < array->size; i++) {
		free (array->str[i]);
	}
	free (array->str);
	free (array);
}


/**
 * Preprocess a version number string and turn it into an array.
 * The array contains various components of the version number string.
 *
 * @param version A version number as string.
 * @return A StrArray object, constructed by split().
 * @require version != NULL
 */
StrArray *
preprocess (const char *version)
{
	StrArray *array;
	char *processed;

	processed = strdup (version);

	/* Remove the following characters: - _ / + */
	removeChars (processed, "-_/+");
	/* Replace all '.' with whitespace. */
	replace (processed, '.', ' ');
	/* Convert to lowercase. */
	toLowerCase (processed);
	/* For example, "1.0 RC2/9" becomes "1 0 RC29". */

	/* Split the string by whitespaces, and turn it into an array. */
	array = split (processed, ' ');
	free (processed);
	return array;
}


/**
 * A string data structure. The methods for manipulating this
 * data structure automatically perform bound checking.
 */
typedef struct {
	char *str;		/** The string. */
	unsigned int len;	/** Length of the string. */
	unsigned int capacity;	/** Memory reserved for the string. Is always > len. */
} String;


/**
 * Create a new String object with a maximum capacity.
 * The object will not be able to hold strings longer than capacity - 1 bytes.
 *
 * @param capacity The maximum memory that will be reserved for the string, including terminating NULL.
 * @return A new String object.
 */
String *
string_new (unsigned int capacity)
{
	String *str;

	str = malloc (sizeof (String));
	str->str = malloc (capacity);
	str->str[0] = '\0';
	str->len = 0;
	str->capacity = capacity;
	return str;
}

/**
 * Append a string to the end of a String.
 * If the String's capacity is too small, then abort() will be called.
 *
 * @param str The String object to modify.
 * @param append The string to append.
 * @require str != NULL && append != NULL && str->len + strlen (append) < str->capacity
 */
void
string_append (String *str, const char *append)
{
	int len;

	len = strlen (append);
	if (str->len + len < str->capacity) {
		strncpy (str->str + str->len, append, len);
		str->str[str->len + len] = '\0';
		str->len += len;
	} else {
		fprintf (stderr, "String capacity (%d) too small for string of %d bytes.\n",
			 str->capacity, str->len + len);
		abort ();
	}
}

/**
 * Append a single character to the end of a String.
 */
void
string_append_c (String *str, char c)
{
	char tmp[2];

	tmp[0] = c;
	tmp[1] = '\0';
	string_append (str, tmp);
}

/**
 * Free a String object and its content.
 *
 * @param str The String object to free.
 * @require str != NULL
 */
void
string_free (String *str)
{
	free (str->str);
	free (str);
}


/**
 * Checks whether str is an integer.
 *
 * @param str The string to check.
 * @return 1 if str is an integer, 0 if not.
 * @require str != NULL
 */
int
isInteger (const char *str)
{
	int i, len;

	len = strlen (str);
	for (i = 0; i < len; i++) {
		if (!isdigit ((unsigned char) str[i]))
			return 0;
	}
	return 1;
}


/**
 * Compare two strings and return 1 if VERSION1 > VERSION2, otherwise 0.
 * Otherwise, gathers digits forward to compare full numbers.
 * Special case: point release is higher than alphabetic release.
 *
 * @todo What does return value 2 do?
 * @return 0, 1 or 2
 */
int
compareAlphaNumeric (const char *version1, const char *version2)
{
	#define CAPACITY 50
	int len1, len2, limit, counter, done, result;
	String *str1, *str2;

	len1 = strlen (version1);
	len2 = strlen (version2);
	
	/* example: REQUIRED 2 > CURRENT 2b or REQUIRED 2.1 > CURRENT 2.1.b ( 0 > b ) */
	if (len1 == 0 || version1[0] == '0') {
		if (isalpha ((unsigned char) version2[0]))
			return 1;

		/* Example: REQUIRED 2.4 > CURRENT 2.4.24-pre2 because the third decimal group
		 * is undefined from REQUIRED, set a equal to 0 so that expr can eval the decimal group
		 */
		if (len1 == 0) {
			version1 = "";
			len1 = 0;
		}
	}

	/* Get length of the longest string to index with over as $limit */
	if (len1 > len2)
		limit = len1;
	else
		limit = len2;

	counter = 1;
	done = 0;
	result = 2;
	str1 = str2 = NULL;
	while (counter < limit && !done) {
		char char1, char2, tempchar;
		String *temp;
		int counterchar;

		/* Compare character by character indexing up the string */
		char1 = version1[counter - 1];
		/*
		 * special case: point release is higher than alphabetic release
		 * example: REQUIRED 2.5-pre3 < CURRENT 2.5 or REQUIRED alpha-char < CURRENT no char
		 */
		if (!isdigit ((unsigned char) char1) && counter > len2) {
			done = 1;
			result = 0;
			break;
		} else
			char2 = version2[counter - 1];

		/* Eat up any non-digit part of the version:
		 * (rc2 => 2)  (pre134 => 134) etc. */
		counterchar = counter;
		temp = string_new (CAPACITY);
		tempchar = char1;
		
		while (tempchar != '\0' && !isdigit(tempchar)) 
			tempchar = version1[++counterchar - 1];
		
		while (isdigit ((unsigned char) tempchar)) {
			/* Grab next index to analyze */
			tempchar = version1[counterchar - 1];
			/* If next index is numeric add character to temp */
			if (isdigit ((unsigned char) tempchar))
				string_append_c (temp, tempchar);
			/* Push out temp to consistent variable char1 */
			str1 = temp;
			counterchar++;
		}

		if (!str1)
			string_free (temp);
		/* Eat up any non-digit part of the version:
		 * (rc2 => 2)  (pre134 => 134) etc. */
		counterchar = counter;
		temp = string_new (CAPACITY);
		tempchar = char2;

		while (tempchar != '\0' && !isdigit(tempchar)) 
			tempchar = version2[++counterchar - 1];

		while (isdigit ((unsigned char) tempchar)) {
			/* Grab next index to analyze */
			tempchar = version2[counterchar - 1];
			/* If next index is numeric add character to temp */
			if (isdigit ((unsigned char) tempchar))
				string_append_c (temp, tempchar);
			/* Push out temp to consistent variable char2 */
			str2 = temp;
			counterchar++;
		}
		if (!str2)
			string_free (temp);

		/* If comparing numbers do an integer compare - otherwise do a string compare */
		if ((str1 && containsDigits (str1->str)) || (str2 && containsDigits (str2->str))) {
			int i1, i2;
			
			i1 = (str1 == NULL) ? 0 : atoi (str1->str);
			i2 = (str2 == NULL) ? 0 : atoi (str2->str);

			if (str1 == NULL || str1->len == 0 || i1 > i2) {
				result = 1;
				done = 1;
				break;

			} else if (i1 < i2) {
				result = 0;
				done = 1;
				break;

			} else if (i1 == i2) {
				/* Numbers match then jump counter forward over the matched digits */
				counter = counter + counterchar - 3;
			}

		} else {
			char *s2;

			s2 = (str2 == NULL) ? "" : str2->str;

			if (str1 == NULL || str1->len == 0) {
				result = 1;
				done = 1;
				break;
			} else if (strcmp (str1->str, s2) < 0) {
				result = 0;
				done = 1;
				break;
			} else if (strcmp (str1->str, s2) > 0) {
				result = 1;
				done = 1;
				break;
			}
		}

		if (str1 != NULL)
			string_free (str1);
		if (str2 != NULL)
			string_free (str2);
		str1 = str2 = NULL;

		counter++;
	}

	/* Return to continue to process next decimal group */
	if (str1 != NULL)
		string_free (str1);
	if (str2 != NULL)
		string_free (str2);
	return result;
}


/**
 * Compares two version numbers, and checks whether current >= required.
 *
 * This function compares 2 strings - infinite level of decimal groups.
 *
 * A version number has the following format: "x.y.z", where y and z are optional.
 * You can also use 'x' as wildcard, but only for an entire decimal group like "1.6.x"
 * or "2.x", but NOT "2.5x".
 *
 * This function looks ahead in the decimal groups with alphabetic and numeric
 * identifers to match full numbers. For instance required:2-RC10f and
 * current:2-rc2d, it ends up comparing 10 to 2 and returns 0 [ FAIL ]
 * instead of 1 to 2 returning 1 [ PASS ].
 *
 * @param required Required version.
 * @param current Current version.
 * @return 1 if current >= required, 0 if not.
 * @require required != NULL && current != NULL
 *
 * Example:
 *                    Required    Current          Return Value
 *    compareVersions("1"       , "1.2"      ) --->  1 [ TRUE  ]
 *    compareVersions("1.2"     , "1"        ) --->  0 [ FALSE ]
 *    compareVersions("1.1"     , "1.2"      ) --->  1 [ TRUE  ]
 *    compareVersions("1.3"     , "1.2"      ) --->  0 [ FALSE ]
 *    compareVersions("1.2"     , "1.2"      ) --->  1 [ TRUE  ]
 *    compareVersions("1.2b"    , "1.2b"     ) --->  1 [ TRUE  ]
 *    compareVersions("2.5-pre3", "2.5"      ) --->  1 [ TRUE  ]
 *    compareVersions("2.5-pre3", "2.5-pre2" ) --->  0 [ FALSE ]
 *    compareVersions("2-RC10f" , "2-rc2d"   ) --->  0 [ FALSE ]
 *    compareVersions("3.1-RC3" , "3.1-rc12" ) --->  1 [ TRUE  ]
 *    compareVersions("1.3"     , "0.1.5"    ) --->  0 [ FALSE ]
 *    compareVersions("1.99.6"  , "2"        ) --->  1 [ TRUE  ]
 *    compareVersions("1.6.x"   , "1.6.7"    ) --->  1 [ TRUE  ]
 *    compareVersions("1.6.x"   , "1.6"      ) --->  1 [ TRUE  ]
 *    compareVersions("1.6.x"   , "1.5.7"    ) --->  0 [ FALSE ]
 *    compareVersions("1.x"     , "1.5.7"    ) --->  1 [ TRUE  ]
 */
int
compareVersions (const char *required, const char *current)
{
	int index, max, result, done;
	StrArray *x, *y;

	trace ("Comparing required %s to current %s\n", required, current);

	/* Preprocess strings and turn them into arrays. */
	x = preprocess (required);
	y = preprocess (current);

	/* Find the array with the largest number of elements. */
	if (x->size > y->size)
		max = x->size;
	else
		max = y->size;

	index = 0;
	result = 1;
	done = 0;
	while (index < max && !done) {
		int tmp1, tmp2;

		/*
		 * Process each respective decimal group...
		 * - If alphanumeric group, then extend the compare to the sub.
		 * - Else do string comparisons.
		 * Special case: catches wildcard in REQUIRED decimal group and PASS.
		 */
		if (index < x->size && strcmp (x->str[index], "x") == 0) {
			result = 1;
			done = 1;
			break;
		}

		/*
		 * Special case: with a blank REQUIRED group any alpha data in CURRENT group will FAIL
		 * Example: REQUIRED 1.2 < CURRENT 1.2.0b or REQUIRED 1.2 < CURRENT 1.2.b
		 */
		if (index < x->size && (x->str[index][0] == '0' || strlen (x->str[index]) == 0)) {
			if (index < y->size && !containsAlphaOnly(y->str[index])) {
				result = 1;
				done = 1;
				break;
			}
		}

		/* If either of the array items are blank then assume it
		 * should be 0 (1 turns into 1.0)
		 */
		if (index < x->size && strlen (x->str[index]) == 0) {
			str_array_set (x, index, strdup ("0"));
		}
		if (index < y->size && strlen (y->str[index]) == 0) {
			str_array_set (y, index, strdup ("0"));
		}

		if ((index < x->size && !isInteger (x->str[index])) || (index < y->size && !isInteger(y->str[index]))) {
			char *tmp1, *tmp2;

			if (index < x->size)
				tmp1 = x->str[index];
			else
				tmp1 = "";
			if (index < y->size)
				tmp2 = y->str[index];
			else
				tmp2 = "";

			switch (compareAlphaNumeric (tmp1, tmp2)) {
			case 0:
				result = 1;
				done = 1;
				break;
			case 1:
				result = 0;
				done = 1;
				break;
			default:
				/* Passes all sub checks and returns to continue with next decimal groups. */
				break;
			}

			if (done)
				break;

			index++;
			continue;
		}

		if (index < x->size)
			tmp1 = atoi (x->str[index]);
		else
			tmp1 = 0;
		if (index < y->size)
			tmp2 = atoi (y->str[index]);
		else
			tmp2 = 0;

		if (tmp1 < tmp2) {
			result = 1;
			break;
		} else if (tmp1 > tmp2) {
			result = 0;
			break;
		}

		index++;
	}

	str_array_free (x);
	str_array_free (y);
	return result;
}


#if DEBUG

static int errors = 0;

static void
test (const char *required, const char *current, int expected)
{
	int result;

	printf ("%-9s %-9s -> %-11s", required, current,
		expected ? "true" : "false");
	fflush (stdout);
	result = compareVersions (required, current);

	if (result != expected) {
		printf ("[FAILED] (got %s instead)\n", result ? "true" : "false");
		errors++;
	} else {
		printf ("[PASSED]\n");
	}
}

static void
startUnitTest ()
{
	printf ("%-9s %-9s    %-10s %-10s\n", "Required", "Current", "Expected", "Unit test");
	printf ("---------------------------------------------\n");
	/* test (REQUIRED, CURRENT, EXPECTED) */
	test ("1"       , "1.2"     , 1);
	test ("1.2"     , "1"       , 0);
	test ("1.1"     , "1.2"     , 1);
	test ("1.3"     , "1.2"     , 0);
	test ("1.2"     , "1.2"     , 1);
	test ("1.2b"    , "1.2b"    , 1);
	test ("2.5-pre3", "2.5"     , 1);
	test ("2.5-pre3", "2.5-pre2", 0);
	test ("2.5-pre1", "2.5-pre2", 1);
	test ("2.5-pre1", "2.5-pre13", 1);
	test ("2.5-pre11", "2.5-pre10", 0);
	test ("2-RC10f" , "2-rc2d"  , 0);
	test ("3.1-RC3" , "3.1-rc12", 1);
	test ("1.3"     , "0.1.5"   , 0);
	test ("1.99.6"  , "2"       , 1);
	test ("1.6.x"   , "1.6.7"   , 1);
	test ("1.6.x"   , "1.6"     , 1);
	test ("1.6.x"   , "1.5.7"   , 0);
	test ("1.x"     , "1.5.7"   , 1);
	printf ("---------------------------------------------\n");
	printf ("Errors: %d\n", errors);
}

#endif /* DEBUG */

int
main (int argc, char *argv[])
{
	#if DEBUG
		startUnitTest ();
		return !(errors == 0);
	#else
		if (argc < 3) {
			fprintf (stderr, "Usage: compareVersions <VERSION1> <VERSION2>\n"
				"Tests whether VERSION1 is greater than VERSION2.\n");
			return 2;
		}
		return !compareVersions (argv[1], argv[2]);
	#endif
}

/*
Curtis> FooBarWidget: ok, well overall it processes the decimal grouping like a
        numeric compare unless an alphabetic character is dtected
Curtis> if an alphabetic character is detected then it trys to read ahead and
        gather the remaining chars like finding a 'p' and continuing to find the 're' for 'pre'
* Curtis looks at the list
Curtis> the compareAlphaNumeric function is what does the read ahead stuff IIRC
*/
