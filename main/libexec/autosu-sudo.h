/*  Autopackage Superuser - sudo backend
 *  Copyright (C) 2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _AUTOSU_SUDO_H_
#define _AUTOSU_SUDO_H_

#include <glib.h>
#include "pty.h"

typedef struct {
	char **argv;
	Pty *pty;
	char *prompt;
	GIOChannel *io;

	gboolean lectured;
	gsize lecture_len;
} SudoSession;

typedef enum {
	SUDO_ERROR_NO_HELPER,	/** Helper application not found. */
	SUDO_ERROR_ARGS		/** You passed one or more invalid arguments to the function. */
} SudoError;

typedef enum {
	SUDO_STATUS_CONTINUE,	/** Sudo is not yet ready. Continue to call sudo_session_iterate() */
	SUDO_STATUS_AUTH,	/** Sudo is now asking for the password. Call sudo_session_authenticate() */
	SUDO_STATUS_SUCCESS,	/** Authentication success; sudo didn't ask for a password. */
	SUDO_STATUS_EXIT,	/** Sudo has exited. */

	/* Errors */
	SUDO_STATUS_ERROR,	/** Unknown error */
	SUDO_STATUS_NOSUDOERS,	/** User is not in the sudoers file. */
	SUDO_STATUS_FAIL	/** Authentication failed; user entered too many wrong passwords. */
} SudoStatus;

typedef enum {
	SUDO_AUTH_SUCCESS,	/** Authentication success! */
	SUDO_AUTH_FAIL,		/** Authentication failed; incorrect password. */
	SUDO_AUTH_NOSUDOERS,	/** User is not in the sudoers file. */
	SUDO_AUTH_EXIT,		/** Sudo exited unexpectedly. */
	SUDO_AUTH_ERROR,	/** Sudo returned an unknown message. */
	SUDO_AUTH_ARGS		/** Error: you specified NULL for the sudo or password parameter. */
} SudoAuthStatus;

gboolean sudo_init (GError **error);

SudoSession   *sudo_session_new          (const char *user, const char **argv, GError **error, gboolean preserve_env);
SudoStatus     sudo_session_iterate      (SudoSession *sudo);
SudoAuthStatus sudo_session_authenticate (SudoSession *sudo, const char *password);
gint           sudo_session_waitchild    (SudoSession *sudo);
void           sudo_session_free         (SudoSession *sudo);
gboolean       sudo_session_kill         (SudoSession *sudo);

#endif /* _AUTOSU_SUDO_H_ */
