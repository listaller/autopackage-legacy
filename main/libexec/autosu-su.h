/*  Autopackage Superuser - su backend
 *  Copyright (C) 2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _AUTOSU_SU_H_
#define _AUTOSU_SU_H_

#include <glib.h>

typedef enum {
	/* The 'su' binary is not found */
	SU_ERROR_NOBIN,
	/* Invalid arguments passed to function */
	SU_ERROR_ARGS,
	/* The specified user account is unavailable */
	SU_ERROR_UNAVAIL,
	/* The specified user doesn't exist */
	SU_ERROR_NOUSER,
	/* Unknown error; su messed up? system compromised? */
	SU_ERROR_UNKNOWN
} SuErrorCode;

typedef void (*SuSetupFunc) (gpointer user_data);

GQuark su_error_domain ();

/* Checks whether 'su' needs a password for user.
 * 0 = no, 1 = yes, -1 = error occured */
gint su_needs_password (const char *user, GError **error);

/* Checks whether password is correct.
 * 0 = no, 1 = yes, -1 = error occured */
gint su_check_password (const char *user, const char *password, GError **error);

/* Execute a command through su. password must be NULL if su doesn't
 * expect a password (see su_needs_password()). If password is not NULL,
 * then it must be correct (see su_check_password()).
 * Blocks until child has exited.
 * Returns the child's exit code, or -1 on error */
gint su_exec (const char *user, char *password, const char *command,
	SuSetupFunc func, gpointer user_data, GError **error);

#endif /* _AUTOSU_SU_H_ */
