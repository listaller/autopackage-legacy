/*  Autopackage Superuser - sudo backend
 *  Copyright (C) 2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <glib.h>

#include "autosu-sudo.h"

#ifndef _
	#define _(x) x
#endif

static gboolean initialized = FALSE;
static GQuark domain;
static char *sudo_helper_path;

#define SUDO_EXE "/usr/bin/sudo"


/* Compare two lines; ignore \r */
static gboolean
linecmp (const gchar *line1, const gchar *line2)
{
	gchar *copy1, *copy2;
	gchar *n1, *n2;
	gboolean result;

	copy1 = g_strdup (line1);
	copy2 = g_strdup (line2);
	n1 = strchr (copy1, '\n');
	n2 = strchr (copy2, '\n');
	if (!n1 || !n2) {
		g_free (copy1);
		g_free (copy2);
		return FALSE;
	}

	/* Strip newlines */
	*n1 = *n2 = 0;
	/* Strip carriage returns */
	n1 = strchr (copy1, '\r');
	n2 = strchr (copy2, '\r');
	if (n1)
		*n1 = 0;
	if (n2)
		*n2 = 0;

	result = strcmp (copy1, copy2) == 0;
	g_free (copy1);
	g_free (copy2);
	return result;
}

static char *
get_helper_path (GError **error)
{
	char tmp[PATH_MAX + 1], *dir, *path;

	if (sudo_helper_path != NULL)
		return sudo_helper_path;

	if (!realpath ("/proc/self/exe", tmp)) {
		g_set_error (error, 0, 0, _("Unable to read from /proc/self/exe. Your operating system might be broken."));
		return FALSE;
	}
	dir = g_path_get_dirname (tmp);
	path = g_strdup_printf ("%s/sudo-helper", dir);
	g_free (dir);

	if (!g_file_test (path, G_FILE_TEST_IS_REGULAR)) {
		g_free (path);
		g_set_error (error, domain, SUDO_ERROR_NO_HELPER,
			_("Internal sudo helper application not found."));
		return NULL;
	}

	sudo_helper_path = path;
	return path;
}

static void
setup_sudo (gpointer user_data)
{
	/* Set language to english so that we can parse the output strings */
	setenv ("LC_ALL", "C", 1);
	setenv ("LANGUAGE", "C", 1);
	setenv ("LANG", "C", 1);
}


/** Initialize this sudo communication library.
 *
 * This function initializes some variables that are internally used.
 * sudo_session_new() automatically calls this function, but you should
 * call this function manually first if your application has multiple
 * threads that use this library.
 */
gboolean
sudo_init (GError **error)
{
	if (initialized)
		return TRUE;

	domain = g_quark_from_static_string ("sudo");
	if (get_helper_path (error) == NULL)
		return FALSE;
	return TRUE;
}


/** Start a new sudo session.
 *
 * After starting a sudo session, you must call sudo_session_iterate() in a loop.
 * When you are done, you must call sudo_session_free().
 *
 * @user: tell sudo to switch to this user.
 * @argv: the arguments of the commands to
 * @error: if an error occured, it will be returned to this variable.
 */
SudoSession *
sudo_session_new (const char *user, const char **argv, GError **error, gboolean preserve_env)
{
	SudoSession *session;
	GList *list = NULL, *tmp;
	char **sudo_argv;
	char *prompt, *lang;
	int i;
	GRand *rand;
	Pty *pty;

	if (!sudo_init (error))
		return NULL;

	if (user == NULL) {
		g_set_error (error, domain, SUDO_ERROR_ARGS,
			_("sudo_session_new(): argument 'user' cannot be NULL."));
		return NULL;
	}
	if (argv == NULL) {
		g_set_error (error, domain, SUDO_ERROR_ARGS,
			_("sudo_session_new(): argument 'argv' cannot be NULL."));
		return NULL;
	}

	/* Tell sudo to use a random password prompt */
	rand = g_rand_new ();
	prompt = g_strdup_printf ("%dSUDO_PASSWORD%d\n",
			g_rand_int (rand), g_rand_int (rand));
	g_rand_free (rand);

	/* Setup pseudo terminal and run sudo */
	lang = getenv ("LC_ALL");
	if (lang == NULL)
		lang = "";

	list = g_list_append (list, SUDO_EXE);
	if (preserve_env) {
		list = g_list_append (list, "-E");
	}
	list = g_list_append (list, "-p");
	list = g_list_append (list, prompt);
	list = g_list_append (list, "-u");
	list = g_list_append (list, (char *) user);
	list = g_list_append (list, "-S");
	list = g_list_append (list, sudo_helper_path);
	list = g_list_append (list, lang);

	for (sudo_argv = (char **) argv; sudo_argv[0] != NULL; sudo_argv++)
		list = g_list_append (list, sudo_argv[0]);

	sudo_argv = g_new0 (char *, g_list_length (list) + 1);
	for (tmp = list, i = 0; tmp; tmp = tmp->next, i++)
		sudo_argv[i] = (char *) tmp->data;
	printf("\n");
	g_list_free (list);

	pty = pty_new (sudo_argv, setup_sudo, NULL, error);
	if (pty == NULL)
		return NULL;

	session = g_new0 (SudoSession, 1);
	session->argv = sudo_argv;
	session->pty = pty;
	session->prompt = prompt;
	session->io = g_io_channel_unix_new (pty->tty);
	g_io_channel_set_encoding (session->io, NULL, NULL);
	return session;
}

SudoStatus
sudo_session_iterate (SudoSession *sudo)
{
	gchar *line;
	gsize len;
	SudoStatus result;
	GIOStatus status;

	g_return_val_if_fail (sudo != NULL, SUDO_STATUS_ERROR);

	/* Read a line from sudo. */
	status = g_io_channel_read_line (sudo->io, &line, &len, NULL, NULL);
	if (status != G_IO_STATUS_NORMAL)
		return SUDO_STATUS_EXIT;

	/* Check whether we've already read the initial sudo lecture.
	 * If not, keep track of how many bytes we read. Bail out
	 * if the lecture is abnormally long. */
	if (!sudo->lectured)
		sudo->lecture_len += len;

	if (sudo->lecture_len > 1024 * 512)
		/* The lecture is more than 512 KB?! This is definitely not normal. */
		result = SUDO_STATUS_ERROR;

	else if (linecmp (line, sudo->prompt)) {
		/* Received password prompt; time to authenticate. */
		sudo->lectured = TRUE;
		result = SUDO_STATUS_AUTH;

	} else if (strstr (line, "is not in the sudoers file") != NULL) {
		/* This user is not in the sudoers file. */
		sudo->lectured = TRUE;
		result = SUDO_STATUS_NOSUDOERS;

	} else if (strstr (line, "incorrect")) {
		sudo->lectured = TRUE;
		result = SUDO_STATUS_FAIL;

	} else if (linecmp (line, "SUDO_PASSED\n")) {
		sudo->lectured = TRUE;
		result = SUDO_STATUS_SUCCESS;

	} else
		/* Ignore all other messages; this is probably text from the lecture
		 * or a useless newline. */
		result = SUDO_STATUS_CONTINUE;

	g_free (line);
	return result;
}

SudoAuthStatus
sudo_session_authenticate (SudoSession *sudo, const char *password)
{
	gchar *line;
	gsize len;
	SudoAuthStatus result;
	GIOStatus status;

	g_return_val_if_fail (sudo != NULL, SUDO_AUTH_ARGS);
	g_return_val_if_fail (password != NULL, SUDO_AUTH_ARGS);

	/* Send password */
	pty_write (sudo->pty, (void *) password, -1);
	pty_write (sudo->pty, "\n", -1);

	/* Read result */
	status = g_io_channel_read_line (sudo->io, &line, &len, NULL, NULL);

	if (status != G_IO_STATUS_NORMAL)
		return SUDO_AUTH_EXIT;
	else if (strcmp (line, "\r\n") == 0 || strcmp (line, "\n") == 0) {
		/* This is just a newline; continue reading */
		status = g_io_channel_read_line (sudo->io, &line, &len, NULL, NULL);
		if (status != G_IO_STATUS_NORMAL)
			return SUDO_AUTH_EXIT;
	}

	if (linecmp (line, "SUDO_PASSED\n"))
		result = SUDO_AUTH_SUCCESS;

	else if (strstr (line, "is not in the sudoers file") != NULL)
		/* User is not in sudoers */
		result = SUDO_AUTH_NOSUDOERS;

	else if (strstr (line, "try again") != NULL)
		/* Incorrect password */
		result = SUDO_AUTH_FAIL;

	else
		/* What is this? Sudo sent an unknown message! */
		result = SUDO_AUTH_ERROR;

	free (line);
	return result;
}

/** Wait until the child process has exited.
 *
 * @return the exit code of the child process, or -1 on error.
 */
gint
sudo_session_waitchild (SudoSession *sudo)
{
	g_return_val_if_fail (sudo != NULL, -1);
	return pty_waitpid (sudo->pty);
}

gboolean
sudo_session_kill (SudoSession *sudo)
{
	char *argv[3];
	pid_t pid;
	GError *error = NULL;
	char buf[256];
	int status;

	argv[0] = SUDO_EXE;
	argv[1] = "-K";
	argv[2] = NULL;

	if (!pty_fork (sudo->pty, &argv[0], &pid, NULL, NULL, &error)) {
		fprintf (stderr, "autosu-sudo.c: sudo_session_kill: pty_fork() failed\n");
		return FALSE;
	}

	pty_read (sudo->pty, &buf, sizeof (buf));
	waitpid (pid, &status, 0);
	return TRUE;
}

void
sudo_session_free (SudoSession *sudo)
{
	g_return_if_fail (sudo != NULL);

	g_io_channel_unref (sudo->io);
	g_free (sudo->prompt);
	pty_free (sudo->pty);
	g_free (sudo->argv);
	g_free (sudo);
}
