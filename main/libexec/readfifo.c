/*
 * Read all output from a FIFO and print it to stdout.
 * Will exit automatically if the other side hasn't connected to
 * FIFO yet within a specified interval.
 *
 * Return values:
 * 0   Success.
 * 1   Wrong arguments passed to program.
 * 2   Cannot open FIFO file. Error will be printed to stderr.
 * 3   Timeout: the other side didn't connect in time.
 * 4   Other error. Message will be printed to stderr.
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

static int timeout; /* Timeout in miliseconds. */
static int opened = 0; /* Whether the FIFO has been opened. */
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static void *
fifo_timeout (void *arg)
{
	usleep (timeout * 1000);
	pthread_mutex_lock (&lock);
	if (!opened) {
		pthread_mutex_unlock (&lock);
		exit (2);
	}
	pthread_mutex_unlock (&lock);
	return NULL;
}

int
main (int argc, char *argv[])
{
	FILE *f;
	pthread_t thread;
	pthread_attr_t attr;
	int ret;

	/* Check arguments. */
	if (argc < 3) {
		fprintf (stderr, "Usage: readfifo <FIFO> <TIMEOUT>\n"
			 "TIMEOUT is in miliseconds.\n");
		return 1;
	}
	timeout = atoi (argv[2]);

	/* Initialize thread. */
	ret = pthread_attr_init (&attr);
	if (ret != 0) {
		fprintf (stderr, "Cannot initialize thread attributes: %s\n",
			 strerror (ret));
		return 4;
	}
	pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);

	ret = pthread_create (&thread, &attr, fifo_timeout, NULL);
	if (ret != 0) {
		fprintf (stderr, "Cannot create thread: %s\n",
			 strerror (ret));
		return 4;
	}

	/* Open FIFO and read it. */
	f = fopen (argv[1], "r");
	pthread_mutex_lock (&lock);
	opened = 1;
	pthread_mutex_unlock (&lock);
	if (f == NULL) {
		fprintf (stderr, "Cannot open %s: %s\n", argv[1],
			 strerror (errno));
		return 2;
	}

	while (!feof (f)) {
		unsigned char buf[1024 * 32];
		size_t bytes_read;
		int err;

		bytes_read = fread (buf, 1, sizeof (buf), f);
		if (bytes_read == 0 && (err = ferror (f)) != 0) {
			fprintf (stderr, "Error while reading: %s\n",
				 strerror (err));
			fclose (f);
			return 4;
		}

		fwrite (buf, 1, bytes_read, stdout);
		fflush (stdout);
	}
	fclose (f);
	return 0;
}
