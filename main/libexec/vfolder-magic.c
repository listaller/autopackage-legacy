/*
 *  vfolder-magic.c
 *
 *  Program that adds or removes a desktop entry in
 *  ~/.gnome2/vfolders/applications.vfolder-info. This seems to be
 *  necessary with gnome 2.8, at least.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Copyright (C) 2004 Johan Ersvik <jersvik@fastmail.fm>
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

/*
 * Functions for determining category according to boolean category definitions
 * in /etc/gnome-vfs-2.0/vfolders/applications-all-users.vfolder-info
 */
int check_query(xmlNodePtr);
int check_and(xmlNodePtr);
int check_or(xmlNodePtr);
int check_not(xmlNodePtr);
int check_keyword(xmlNodePtr);

int ncategories = 0;
char** categories = NULL;

// For the OnlyUnallocated vfolder tag
const xmlChar* pending_folders[100];
int npending = 0;

// GNOME sysconfdir
char* sysconfdir = NULL;

void usage()
{
	printf("usage: vfolder-magic <install|uninstall> <vfolder-info>\n");
	printf("                     <desktop path> <sysconfdir> [Categories]\n");
	printf("Categories only needed for 'install'\n");
	exit(-1);
}

/*
 * Check if 'cat' is among the categories
 */
int category_listed(const xmlChar* cat)
{
	int i;
	
	for (i = 0; i < ncategories; i++)
	{
		if (xmlStrcmp(categories[i], cat) == 0)
		{
			return 1;
		}
	}

	return 0;
}

/*
 * Searches for folder named <folder_name> in app_root and attaches
 * a menu entry referencing <desktop_file>
 */
void attach_app_node(xmlNodePtr app_root, const xmlChar* desktop_file, const xmlChar* folder_name)
{
	xmlNodePtr curnode;
	xmlBufferPtr buf;
	xmlNodePtr entry;

	for (curnode = app_root->xmlChildrenNode; curnode; curnode = curnode->next)
	{
		xmlNodePtr attach_to = NULL;
		if (xmlStrcmp(curnode->name, (const xmlChar*)"Folder") == 0)
		{
			xmlNodePtr node = curnode->xmlChildrenNode;
			for (; node; node = node->next)
			{
				if (xmlStrcmp(node->name, (const xmlChar*)"Name") == 0)
				{
					xmlChar* key = xmlNodeGetContent(node);
					if (xmlStrcmp(key, folder_name) == 0)
					{
						// Attach to this folder
						attach_to = curnode;
						break;
					}
				}
			}
		}

		if (attach_to)
		{
			entry = xmlNewNode(NULL, "Include");
			xmlNodeAddContent(entry, desktop_file);
			xmlAddChild(attach_to, entry);

			return;
		}
	}
	
	// A Folder named folder_name was not found
	// Create entry
	entry = xmlNewNode(NULL, "Include");
	xmlNodeAddContent(entry, desktop_file);

	// Create new folder
	curnode = xmlNewChild(app_root, NULL, "Folder", NULL);

	// Assemble name strings
	buf = xmlBufferCreate();
	xmlBufferCCat(buf, "applications-all-users:///");
	xmlBufferCat(buf, folder_name);

	// Add info to new folder
	xmlNewChild(curnode, NULL, "Name", folder_name);
	xmlNewChild(curnode, NULL, "Parent", xmlBufferContent(buf));
	
	// Add entry to new folder
	xmlAddChild(curnode, entry);

	xmlBufferFree(buf);
}

/*
 * remove_nodes: remove all folders referencing <desktop_file>
 * if prune_folders is set empty folders are removed.
 * Called before installing and when uninstalling.
 */
void remove_nodes(xmlNodePtr app_root, const xmlChar* desktop_file, int prune_folders)
{
	xmlNodePtr folder_node = app_root->xmlChildrenNode;

	if(!folder_node)
	{
		return;
	}

	while (folder_node)
	{
		xmlNodePtr tmpnode = folder_node;
		folder_node = folder_node->next;

		if (xmlStrcmp(tmpnode->name, "Folder") == 0)
		{
			xmlNodePtr node = tmpnode->xmlChildrenNode;
			// Remove identical entries in folder
			while (node)
			{
				// Backup since node will possibly be deleted
				xmlNodePtr tmp = node;
				node = node->next;

				if (xmlStrcmp(tmp->name, (const xmlChar*)"Include") == 0)
				{
					if (xmlStrcmp(xmlNodeGetContent(tmp), (const xmlChar*)desktop_file) == 0)
					{
						xmlUnlinkNode(tmp);
						xmlFreeNode(tmp);
					}
				}
			}

			if (prune_folders)
			{
				int empty_folder = 1;
				node = tmpnode->xmlChildrenNode;

				for (; node; node = node->next)
				{
					if (xmlStrcmp(node->name, "Name") != 0 &&
						xmlStrcmp(node->name, "Parent") != 0)
					{
						empty_folder = 0;
						break;
					}
				}

				if (empty_folder)
				{
					xmlUnlinkNode(tmpnode);
					xmlFreeNode(tmpnode);
				}
			}
		}
	}
}

/*
 * Category matching
 */
int check_query(xmlNodePtr query)
{
	xmlNodePtr bool_node = query->xmlChildrenNode;

	for (; bool_node; bool_node = bool_node->next)
	{
		if (xmlStrcmp(bool_node->name, "And") == 0 && (!check_and(bool_node)))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Or") == 0 && (!check_or(bool_node)))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Not") == 0 && (!check_not(bool_node)))
		{
			return 0;
		}
	}

	return 1;
}

int check_and(xmlNodePtr query)
{
	xmlNodePtr bool_node = query->xmlChildrenNode;

	for (; bool_node; bool_node = bool_node->next)
	{
		if (xmlStrcmp(bool_node->name, "And") == 0 && (!check_and(bool_node)))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Or") == 0 && (!check_or(bool_node)))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Not") == 0 && (!check_not(bool_node)))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Keyword") == 0 && !check_keyword(bool_node))
		{
			return 0;
		}

	}

	return 1;
}

int check_not(xmlNodePtr query)
{
	xmlNodePtr bool_node = query->xmlChildrenNode;

	for (; bool_node; bool_node = bool_node->next)
	{
		if (xmlStrcmp(bool_node->name, "And") == 0 && check_and(bool_node))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Or") == 0 && check_or(bool_node))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Not") == 0 && check_not(bool_node))
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "All") == 0)
		{
			return 0;
		}
		else if (xmlStrcmp(bool_node->name, "Keyword") == 0 && check_keyword(bool_node))
		{
			return 0;
		}
	}

	return 1;
}

int check_or(xmlNodePtr query)
{
	xmlNodePtr bool_node = query->xmlChildrenNode;

	for (; bool_node; bool_node = bool_node->next)
	{
		if (xmlStrcmp(bool_node->name, "And") == 0 && check_and(bool_node))
		{
			return 1;
		}
		else if (xmlStrcmp(bool_node->name, "Or") == 0 && check_or(bool_node))
		{
			return 1;
		}
		else if (xmlStrcmp(bool_node->name, "Not") == 0 && check_not(bool_node))
		{
			return 1;
		}
		else if (xmlStrcmp(bool_node->name, "All") == 0)
		{
			return 1;
		}
		else if (xmlStrcmp(bool_node->name, "Keyword") == 0 && check_keyword(bool_node))
		{
			return 1;
		}
	}

	return 0;
}

int check_keyword(xmlNodePtr node)
{
	xmlChar* key = xmlNodeGetContent(node);

	if (category_listed(key))
	{
		return 1;
	}

	return 0;
}

/**
 * Check if the categories match the folder info node 'app_folder_desc'.
 * If so, attach to 'app_root' node.
 */
int check_desktop_file(const char* desktop_file, xmlNodePtr app_root,
		xmlNodePtr app_folder_desc)
{
	xmlNodePtr curnode = app_folder_desc->xmlChildrenNode;
	const xmlChar* name = NULL;
	int match = 0;
	int only_unallocated = 0;
	int attached = 0;

	// Check categories
	for (; curnode; curnode = curnode->next)
	{
		if (xmlStrcmp(curnode->name, (const xmlChar*)"Name") == 0)
		{
			name = xmlNodeGetContent(curnode);
		}
		else if (xmlStrcmp(curnode->name, (const xmlChar*)"Query") == 0)
		{
			match = 0;
			if (check_query(curnode))
			{
				match = 1;
			}
		}
		// would only match otherwise unmatched apps.
		else if (xmlStrcmp(curnode->name, (const xmlChar*)"OnlyUnallocated") == 0)
		{
			only_unallocated = 1;
		}
	}

	if (match && name && !only_unallocated)
	{
		attach_app_node(app_root, desktop_file, name);
		attached = 1;
	}
	else if (only_unallocated && name)
	{
		// This folder can contain the desktop entry if no other
		// folder is found.
		pending_folders[npending] = name;
		npending++;
	}

	return attached;
}

static int file_exists(const char *filename)
{
	struct stat buf;
	return stat(filename, &buf) == 0;
}

/*
 * add desktop file entry to all folder matching categories;
 * determined by conf file in /etc
 */
void add_entries(const char* apps_vfolder, const char* desktop_file)
{
	xmlDocPtr descdoc = NULL;
	xmlNodePtr desc_root = NULL;
	xmlDocPtr menudoc = NULL;
	xmlNodePtr app_root = NULL;
	xmlNodePtr menuroot = NULL;
	xmlNodePtr curnode = NULL;
	xmlBufferPtr sysdesc = xmlBufferCreate();

	xmlBufferCCat(sysdesc, sysconfdir);
	xmlBufferCCat(sysdesc, "/gnome-vfs-2.0/vfolders/applications-all-users.vfolder-info");
	if (!file_exists(xmlBufferContent(sysdesc)))
		return;

	descdoc = xmlParseFile(xmlBufferContent(sysdesc));
	if (!descdoc)
	{
		//fprintf(stderr, "Couldn't parse category file: %s\n",
		//		xmlBufferContent(sysdesc));
		return;
	}
	desc_root = xmlDocGetRootElement(descdoc);
	
	menudoc = xmlParseFile(apps_vfolder);
	if (menudoc)
	{
		menuroot = xmlDocGetRootElement(menudoc);
	}
	else
	{
		// Create empty menu structure
		xmlNodePtr writedir, app_folder, name, parent;
		xmlBufferPtr writedir_string;

		menudoc = xmlNewDoc("1.0");
		menuroot = xmlNewDocNode(menudoc, NULL, "VFolderInfo", NULL);
		xmlDocSetRootElement(menudoc, menuroot);
		writedir_string = xmlBufferCreate();
		xmlBufferCat(writedir_string, getenv("HOME"));
		xmlBufferCat(writedir_string, "/.gnome2/vfolders/applications");
		writedir = xmlNewChild(menuroot, NULL, "WriteDir",
				xmlBufferContent(writedir_string));
		xmlBufferFree(writedir_string);

		xmlNodeAddContent(writedir, xmlBufferContent(writedir_string));
		app_folder = xmlNewChild(menuroot, NULL, "Folder", NULL);
		name = xmlNewChild(app_folder, NULL, "Name", "Root");
		parent = xmlNewChild(app_folder, NULL, "Parent", "applications-all-users:///");

		// No need to look for app_root below
		app_root = app_folder;
	}	


	// Find application menu
	curnode = menuroot->xmlChildrenNode;
	while (curnode && !app_root)
	{
		// Check all Folder tags. We are looking for a folder
		// named Root with "application-all-users:///" as Parent.
		if (xmlStrcmp(curnode->name, (const xmlChar*)"Folder") == 0)
		{
			xmlNodePtr tmpnode = curnode->xmlChildrenNode;
			int found_parent = 0;
			int found_name = 0;

			// Look for corrent 'Name' and 'Parent' tags
			while (tmpnode && !(found_name && found_parent))
			{
				if (xmlStrcmp(tmpnode->name, (const xmlChar*)"Name") == 0)
				{
					xmlChar* key = xmlNodeGetContent(tmpnode);
					if (xmlStrcmp(key, (const xmlChar*)"Root") == 0)
					{
						found_name = 1;
					}

					xmlFree(key);
				}
				else if (xmlStrcmp(tmpnode->name, (const xmlChar*)"Parent") == 0)
				{
					xmlChar* key = xmlNodeGetContent(tmpnode);
//					xmlChar* key = xmlNodeListGetString(menudoc, tmpnode->xmlChildrenNode, 1);
					if (xmlStrcmp(key, (const xmlChar*)"applications-all-users:///") == 0)
					{
						found_parent = 1;
					}
					xmlFree(key);
				}

				tmpnode = tmpnode->next;
			}

			if (found_name && found_parent)
			{
				// This is it!
				app_root = curnode;
				break;
			}
		}

		curnode = curnode->next;
	}

	// Add root menu if not found
	if (!app_root)
	{
		app_root = xmlNewChild(menuroot, NULL, (const xmlChar*)"Folder", NULL);
		xmlNewChild(app_root, NULL, (const xmlChar*)"Name", "Root");
		xmlNewChild(app_root, NULL, (const xmlChar*)"Parent", "applications-all-users:///");
	}
	else
	{
		// Make sure any old, identical desktop entries are removed first
		remove_nodes(app_root, desktop_file, 0);
	}

	for (curnode = desc_root->xmlChildrenNode; curnode; curnode = curnode->next)
	{
		if (xmlStrcmp(curnode->name, (const xmlChar*)"Folder") == 0)
		{
			xmlNodePtr tmp = curnode->xmlChildrenNode;
			int application_node = 0;

			for (; tmp; tmp = tmp->next)
			{
				if (xmlStrcmp(tmp->name, (const xmlChar*)"Name") == 0)
				{
					xmlChar* key = xmlNodeListGetString(descdoc, tmp->xmlChildrenNode, 1);
					if (xmlStrcmp(key, "Applications") == 0)
					{
						application_node = 1;
					}
					else
					{
						break;
					}
				}
			}

			if (!application_node)
			{
				continue;
			}
			else
			{
				// Found application folder.. search in sub folders
				xmlNodePtr tmp = curnode->xmlChildrenNode;
				int added = 0;

				for (; tmp; tmp = tmp->next)
				{
					if (xmlStrcmp(tmp->name, (const xmlChar*)"Folder") == 0)
					{
						if (check_desktop_file(desktop_file, app_root, tmp))
							added = 1;
					}
				}

				if (!added && npending)
				{
					while (npending)
					{
						npending--;
						attach_app_node(app_root, desktop_file,
							pending_folders[npending]);
					}
				}

				break;
			}
		}
	}

	xmlSaveFile(apps_vfolder, menudoc);
}

int main(int argc, char** argv)
{
	const char *action = argv[1];
	char *apps_vfolder = argv[2];
	const char *desktop_path = argv[3];

	if (argc <= 4)
	{
		usage();
	}

	ncategories = argc - 5;
	categories = argv + 5;
	sysconfdir = argv[4];

	if (strcmp(action, "install") == 0)
	{
		if (argc <= 5)
			usage();

		add_entries(apps_vfolder, desktop_path);
	}
	else if (strcmp(action, "uninstall") == 0)
	{
		if (argc != 5)
			usage();

		xmlNodePtr curnode, app_root = 0;

		if (!file_exists(apps_vfolder))
		{
			return -1;
		}

		xmlDocPtr doc = xmlParseFile((const xmlChar*)apps_vfolder);
		if (!doc)
		{
			return -1;
		}

		// Locate the application folder
		for (curnode = doc->xmlRootNode->xmlChildrenNode; curnode && (!app_root); curnode = curnode->next)
		{
			if (xmlStrcmp(curnode->name, (const xmlChar*)"Folder") == 0)
			{
				xmlNodePtr folditer = curnode->xmlChildrenNode;

				for (; folditer; folditer = folditer->next)
				{
					if (xmlStrcmp(folditer->name, (const xmlChar*)"Parent") == 0 &&
							xmlStrcmp(xmlNodeGetContent(folditer), "applications-all-users:///") == 0)
					{
						app_root = curnode;
						break;
					}
				}
			}
		}

		// Remove entry if found
		if (app_root)
		{
			remove_nodes(app_root, desktop_path, 1);
			xmlSaveFile(apps_vfolder, doc);
		}

		xmlFreeDoc(doc);
	}
	else
	{
		usage();
	}

	return 0;
}

