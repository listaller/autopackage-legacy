/* Recursively check if a directory is empty.

   Copyright (C) 2004 Hongli Lai <h.lai@chello.nl>
   This file is part of autopackage: http://autopackage.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>

#ifndef NULL
	#define NULL ((void *) 0)
#endif

typedef struct _DList DList;

struct _DList {
	char *str;
	DList *next;
	DList *prev;
};

static int no_recurse = 0;


static DList *
d_list_new (const char *dirname)
{
	DList *list = NULL, *liststart = NULL;
	DIR *dir;
	struct dirent *ent;

	if (!dirname) return NULL;

	dir = opendir (dirname);
	if (!dir) return NULL;

	while ((ent = readdir (dir))) {
		DList *item;

		/* Do not read '.' and '..' */
		if (!strcmp (ent->d_name, ".") || !strcmp (ent->d_name, ".."))
			continue;

		item = calloc (sizeof (DList), 1);
		item->str = strdup (ent->d_name);
		item->next = NULL;
		if (list) {
			item->prev = list;
			list->next = item;
		} else {
			item->prev = NULL;
			liststart = item;
		}
		list = item;
	}
	closedir (dir);

	return liststart;
}

static void
d_list_free (DList *list)
{
	DList *l, *last;

	if (!list) return;
	for (l = list; l; l = l->next) {
		free (l->str);
		if (l->prev)
			free (l->prev);
		last = l;
	}
	free (last);
}

static int
dir_is_empty (const char *dirname)
{
	DList *list, *l;
	char *cwd;

	cwd = getcwd (NULL, 0);
	if (!cwd) return 0;
	if (chdir (dirname) != 0) {
		free (cwd);
		return 1;
	}


	list = d_list_new (".");
	if (!list) {
		chdir (cwd);
		free (cwd);
		return 1;
	}

	for (l = list; l; l = l->next) {
		struct stat buf;

		if (lstat (l->str, &buf) != 0) {
			chdir (cwd);
			free (cwd);
			return 0;
		} else if (S_ISDIR (buf.st_mode)) {
			if (no_recurse || !dir_is_empty (l->str)) {
				chdir (cwd);
				free (cwd);
				return 0;
			}
		} else {
			chdir (cwd);
			free (cwd);
			return 0;
		}
	}

	d_list_free (list);


	chdir (cwd);
	free (cwd);
	return 1;
}


int
main (int argc, char *argv[])
{
	if (argc <= 0)
		return 1;
	else if (argc >= 3)
		no_recurse = strcmp (argv[2], "--no-recurse") == 0;
	return !dir_is_empty(argv[1]);
}
