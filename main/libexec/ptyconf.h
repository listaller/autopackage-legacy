/* Simple library for messing with pseudo terminals
 * Copyright (C) 2005  Hongli Lai
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _PTYCONF_H_
#define _PTYCONF_H_

/* This file defines macros which deals with cross-platform stuff. */

#ifdef __GLIBC__
	#ifndef __USE_GNU
	#	define __USE_GNU
	#endif
	#ifndef __USE_XOPEN
	#	define __USE_XOPEN
	#endif

	#define HAVE_GETPT
	#define HAVE_GETPID
#endif

#endif /* _PTYCONF_H_ */
