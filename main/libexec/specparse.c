#include <glib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/* Possible exit codes from the program. */
enum {
	EXIT_PARSE_SUCCESSFUL = 0,
	EXIT_COULDNT_OPEN_FILE,
	EXIT_PARSE_ERROR,
	EXIT_INVALID_OPTION,
	EXIT_GENERAL_ERROR,
};

void usage(void);

gint warninglevel = 0;
gint verboselevel = 0;
gboolean exit_on_error = FALSE;
gchar *progname;
gint lineno = 0;

#define trace(...) G_STMT_START { if (verboselevel > 1) { g_print ("TRACE: "); g_print (__VA_ARGS__); } } G_STMT_END
#define debug(...) G_STMT_START { if (verboselevel > 0) { g_print ("DEBUG: "); g_print (__VA_ARGS__); } } G_STMT_END
/* Used to print out warnings and errors. Currently, _error is printed regardless of warninglevel (-w switch) but _warning
 * only gets printed if warninglevel is 1 or above */
#define parser_warning(...) G_STMT_START { if (warninglevel) { g_printerr ("Warning: [Line %d]: ", lineno); g_printerr (__VA_ARGS__); } } G_STMT_END
#define parser_error(...)  G_STMT_START { g_printerr ("Error: [Line %d]: ", lineno); g_printerr (__VA_ARGS__); } G_STMT_END

#define POINTER_DIFF(larger,smaller) (GPOINTER_TO_INT(larger) - GPOINTER_TO_INT(smaller))

/* Some patterns used to determine the line type. Global so that they can be free'd nicely when the program exits */
GPatternSpec *pspec_heading = NULL;
GPatternSpec *pspec_heading_i18n = NULL;
GPatternSpec *pspec_assignment = NULL;
GPatternSpec *pspec_assignment_i18n = NULL;

const gchar *known_headings[] = { "Meta", "BuildPrepare", "BuildUnprepare", "Description", "Globals",
				  "Imports", "Prepare", "Install", "Uninstall" };

/* Checks if the language tag is correctly formatted */
gboolean validate_language (const gchar *lang)
{
	gboolean valid = TRUE;

	if (!g_ascii_isalpha(lang[0]) || !g_ascii_isalpha (lang[1]))
		valid = FALSE;
	if (strlen (lang) > 2)
	{
		if (lang[2] != '_' && !g_ascii_isalpha(lang[3] && !g_ascii_isalpha(lang[4])))
			valid = FALSE;
	}

	if (!valid)
		parser_warning ("Suspicious language tag found: %s\n", lang);

	return valid;
}

/* Validates a section heading (e.g. [Foo]) */
gboolean validate_heading (const gchar *heading, gboolean is_localized)
{
	gboolean rv = TRUE;

	g_return_val_if_fail (is_localized == FALSE || strchr (heading, ':') != NULL, FALSE);
	
	if (g_ascii_isspace (*heading)) {
	    parser_warning ("Heading begins with whitespace\n");
	    if (warninglevel > 0)
		    rv = FALSE;
	}
	while (g_ascii_isspace (*heading))
		heading++;
	heading++;
	gchar *headingname = g_strndup (heading, POINTER_DIFF(strrchr(heading, ']'), heading));
	if (is_localized)
	{
		validate_language (strrchr(headingname, ':') + 1);
		*(strrchr(headingname, ':')) = '\0';
	}
	if (strlen (headingname) == 0)
	{
		parser_error ("Empty section name\n");
		rv = FALSE;
	}
	else
	{
		/* Check the name agains all known (valid) section names */
		gint i;
		gboolean is_known = FALSE;
		for (i = 0; i < G_N_ELEMENTS(known_headings); i++)
		{
			if (g_str_equal (headingname, known_headings[i])) {
				is_known = TRUE;
				break;
			}
		}
		if (!is_known) {
			parser_warning ("Heading `%s' is not known, possible type-o?\n", headingname);
			if (warninglevel > 0)
				rv = FALSE;
		}
	}
	g_free (headingname);
	return rv;
}

gboolean validate_numeric(const gchar *value)
{
	while (g_ascii_isspace (*value))
		value++;

	if (!g_ascii_isdigit (*value))
		return FALSE;

	/* The following would only allow values containing
	 * either a variable ($MyValue) or digits separated by dots.
	 * It's a bit too strict so for now, just allow anything that begins with
	 * a digit. */
/* 	while (*value) */
/* 	{ */
/* 		if (*value == '$') */
/* 		{ */
/* 			while (*value && !g_ascii_isspace (*value)) */
/* 				value++; */
/* 		} */
/* 		else if (g_ascii_isdigit(*value) || *value == '.') */
/* 			value++; */
/* 		else */
/* 		{ */
/* 			return FALSE; */
/* 		} */
/* 	} */
	return TRUE;
}

/* Validates a value of type boolean. */
gboolean validate_boolean (const gchar *value)
{
	while (g_ascii_isspace (*value))
		value++;
	/* Should 'True' and 'False' be valid too? */ 
	if (g_strncasecmp (value, "yes", strlen("yes")) == 0 ||
	    g_strncasecmp (value, "no", strlen("no")) == 0)
		return TRUE;

	return FALSE;
}

/* The different value types a variable in the spec-file can have. */
typedef enum {
	TYPE_STRING,
	TYPE_BOOLEAN,
	TYPE_NUMERIC
} VariableType;

typedef struct 
{
	gchar *name;
	VariableType type;
	gboolean translatable;
} KnownVariable;

/* All known variables. Format is "name", "type" and "can be translated". */
/* Keep this list sorted... */
const KnownVariable known_variables[] =
	{ { "AutopackageTarget", TYPE_NUMERIC, FALSE}, { "Compression", TYPE_STRING, FALSE } ,
	  { "CPUArchitectures", TYPE_STRING, FALSE} , {"DisplayName", TYPE_STRING, TRUE },
	  { "InterfaceVersion", TYPE_NUMERIC, FALSE} , { "License", TYPE_STRING, FALSE},
	  { "Maintainer", TYPE_STRING, FALSE}, { "PackageDesktop",TYPE_STRING, FALSE },
	  { "Packager",TYPE_STRING, FALSE }, { "PackageVersion",TYPE_NUMERIC, FALSE },
	  { "Repository", TYPE_STRING, FALSE },
	  { "RootInstallOnly",TYPE_BOOLEAN, FALSE },{ "RootName",TYPE_STRING, FALSE },
	  { "ShortName",TYPE_STRING, TRUE }, { "SoftwareVersion", TYPE_NUMERIC, FALSE },
	  { "Summary", TYPE_STRING, TRUE }, { "Type", TYPE_STRING, FALSE },
	  { "URL", TYPE_STRING }, {"PackageFileName", TYPE_STRING, FALSE } };

/* Validates an assignment (e.g. ShortName: foo) */
gboolean validate_assignment (const gchar *line, gboolean is_localized)
{
	gboolean rv = TRUE;
	gchar **arr = g_strsplit (line, ":", 2);
	gchar *variable = arr[0];
	gchar *value = arr[1];

	g_return_val_if_fail (is_localized == FALSE || strchr (variable, '[') != NULL, FALSE);
	
	if (g_ascii_isspace (*variable)) {
	    parser_warning ("Assignment variable `%s' begins with whitespace\n", variable);
	    if (warninglevel > 0)
		    rv = FALSE;
	    g_strstrip (variable);
	}
	if (is_localized)
	{
		gchar *lang = g_strndup (strrchr(variable, '[') + 1, POINTER_DIFF(strrchr(variable, ']'), strrchr(variable,'[') + 1));
		if (!validate_language(lang) && warninglevel > 1)
			rv = FALSE;
		g_free (lang);
		/* Truncate language info */
		*(strrchr(variable, '[')) = '\0';
	}
	gint i;
	gboolean is_known = FALSE;
	VariableType type = TYPE_STRING;
	/* Check the variable name against known variables. */
	for (i = 0; i < G_N_ELEMENTS (known_variables); i++)
	{
		trace ("Comparing %s to %s...\n", variable, known_variables[i].name);
		if (g_str_equal (variable, known_variables[i].name)) {
			type = known_variables[i].type;
			is_known = TRUE;
			if (is_localized && known_variables[i].translatable == FALSE)
			{
				parser_warning ("Translation supplied for variable `%s' which isn't translatable!\n", variable);
				if (warninglevel > 0)
					rv = FALSE;
			}
			break;
		}
	}
	if (!is_known) {
		parser_warning ("Variable `%s' is not known, possible type-o?\n", variable);
		if (warninglevel > 0)
			rv = FALSE;
	}
	else
	{
		switch (type)
		{
			case TYPE_NUMERIC:
				if (!validate_numeric(value))
				{
					parser_warning ("Value for variable `%s' is of type numeric. Provided value: %s\n", variable, value);
					if (warninglevel > 0)
						rv = FALSE;
				}
				break;

			case TYPE_BOOLEAN:
				if (!validate_boolean(value))
				{
					parser_warning ("Value for variable `%s' is of type boolean. Provided value: %s\n", variable, value);
					if (warninglevel > 0)
						rv = FALSE;
				}
				break;

			default:
				/* Strings are always ok... */
				break;
		}
	}

	g_strfreev (arr);
	return rv;
}

/* Called for all files specified on the command line */
void parse_specfile (gchar *fname, gpointer user_data)
{
	GError *err = NULL;
	gchar *contents;
	gint errcount = 0;
	trace ("Parsing %s...\n", fname);
	
	if (!g_file_get_contents (fname, &contents, NULL, &err))
	{
		g_printerr ("Couldn't open file `%s': %s\n", fname, err->message);
		g_error_free (err);
		err = NULL;
		if (exit_on_error)
			exit (EXIT_COULDNT_OPEN_FILE);
		return;
	}
	const gchar *endmarker;
	if (!g_utf8_validate (contents, -1, &endmarker)) {
		parser_warning ("File is not valid UTF-8 format, this may not work. Invalid data begins at offset %d\n",
				GPOINTER_TO_INT(endmarker) - GPOINTER_TO_INT(contents));
		errcount++;
	}
	gchar **lines = g_strsplit (contents, "\n", -1);
	g_free (contents);
	for (lineno = 0; lines[lineno] != NULL; lineno++)
	{
		gchar *line = lines[lineno];
		if (strrchr(line, '#'))
			*strrchr(line, '#') = '\0';
		gchar *reversed = g_utf8_strreverse (line, -1);
		gint len = strlen (line);

		/* Check the line against known patterns. Start with i18n patterns since they are a subset of
		 * the other patterns. */
		if (g_pattern_match (pspec_heading_i18n, len, line, reversed))
		{
			debug ("Found translated section heading: %s\n", line);
			if (!validate_heading(line, TRUE))
			{
				errcount++;
				if (exit_on_error)
					exit (EXIT_PARSE_ERROR);
			}
		}
		else if (g_pattern_match (pspec_assignment_i18n, len, line, reversed))
		{
			debug ("Found translated assignment: %s\n", line);
			if (!validate_assignment (line, TRUE))
			{
				errcount++;
				if (exit_on_error)
					exit (EXIT_PARSE_ERROR);
			}
		}
		else if (g_pattern_match (pspec_heading, len, line, reversed))
		{
			debug ("Found heading: %s\n", line);
			if (!validate_heading(line, FALSE))
			{
				errcount++;
				if (exit_on_error)
					exit (EXIT_PARSE_ERROR);
			}
		}
		else if (g_pattern_match (pspec_assignment, len, line, reversed))
		{
			debug ("Found assignment: %s\n", line);
			if (!validate_assignment(line, FALSE))
			{
				errcount++;
				if (exit_on_error)
					exit (EXIT_PARSE_ERROR);
			}
		}
		    
	}
	if (errcount)
		g_print ("File %s contained %d error(s)\n", fname, errcount);
	else
		g_print ("File %s successfully parsed\n", fname);
	g_strfreev (lines);
}


gint main (gint argc, gchar *argv[])
{
	GList *files = NULL;

	progname = g_path_get_basename (argv[0]);
/* 	if (argc < 2) { */
/* 		g_print ("Usage: %s [-v] [-h] SPECFILE [SPECFILE2 ...]\n", */
/* 			 progname); */
/* 		g_print ("Use '%s -h' for more details\n", progname); */
/* 		return EXIT_INVALID_OPTION; */
/* 	} */
	
	if (g_getenv("SPECPARSER_DEBUGLEVEL") != NULL)
		verboselevel = atoi (g_getenv("SPECPARSER_DEBUGLEVEL"));
	
	gint i;
	for (i = 1; i < argc; i++)
	{
		if (*argv[i] != '-')
			files = g_list_append (files, argv[i]);
		else
		{
			switch (argv[i][1])
			{
				case 'w':
				case 'W':
					warninglevel++;
					break;

				case 'h':
				case 'H':
					usage();
					break;

				case 'e':
				case 'E':
					exit_on_error = FALSE;
					break;

				default:
					g_print ("Unknown option switch `%c'. Try -h for help\n", argv[i][1]);
					return EXIT_INVALID_OPTION;
			}
		}
	}
	if (files == NULL) {
		g_print ("Usage: %s [-v] [-h] SPECFILE [SPECFILE2 ...]\n",
			 progname);
		g_print ("Use '%s -h' for more details\n", progname);
		return EXIT_INVALID_OPTION;
	}
	debug ("Warninglevel level: %d\n", warninglevel);
	debug ("Verbosity level: %d\n", verboselevel);
	debug ("Exit on errors: %s\n", exit_on_error ? "Yes" : "No");
	
	pspec_heading = g_pattern_spec_new ("[*]");
	pspec_assignment = g_pattern_spec_new ("*:*");
	pspec_heading_i18n = g_pattern_spec_new ("[*:*]");
	pspec_assignment_i18n = g_pattern_spec_new ("*[*]:*");

	/* Parse all files specified on the command line */
	g_list_foreach (files, (GFunc) parse_specfile, NULL);
	g_list_free (files);
	g_pattern_spec_free (pspec_heading);
	g_pattern_spec_free (pspec_heading_i18n);
	g_pattern_spec_free (pspec_assignment);
	g_pattern_spec_free (pspec_assignment_i18n);
	
	g_free (progname);
	return EXIT_PARSE_SUCCESSFUL;
}

void usage(void)
{
	g_print ("Usage: %s [-v] [-h] SPECFILE [SPECFILE2 ...]\n", progname);
	g_print ("Options:\n");
	g_print (" -w		Increase warninglevel (use multiple times to increase it more\n");
	g_print (" -e		Abort parsing if any errors occurrs.");
	g_print (" -h		Show this help\n");
	g_print ("\n");
	g_print (" Debugging information can be controlled through the SPECPARSER_DEBUGLEVEL environment\n"
		 " variable. (Numeric value, higher == more verbosity)\n"); 
	exit (EXIT_PARSE_SUCCESSFUL);
}
