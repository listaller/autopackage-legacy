// monotester - Print version information about mono and mono assemblies
// Copyright (C) The Autopackage Project
//
// Released under the terms of the GNU GPL.
//
// Author(s):
//  Isak Savo <isak.savo@gmail.com>
//
// ==============================================
//
// Parts of this program taken from the Mono Project, copyright below:
// Mono.Tools.GacUtil
//
// Author(s):
//  Todd Berman <tberman@sevenl.net>
//  Jackson Harper <jackson@ximian.com>
//
// Copyright 2003, 2004 Todd Berman 
// Copyright 2004 Novell, Inc (http://www.novell.com)
//
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#region Assembly Attributes
// Information about this assembly is defined by the following
// attributes.
//
// change them to the information which is associated with the assembly
// you compile.

[assembly: AssemblyTitle("MonoTester")]
[assembly: AssemblyDescription("Checks for mono runtime and assemblies version information")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The Autopackage Project")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has following format :
//
// Major.Minor.Build.Revision
//
// You can specify all values by your own or you can build default build and revision
// numbers with the '*' character (the default):

[assembly: AssemblyVersion("1.0.*")]

// The following attributes specify the key for the sign of your assembly. See the
// .NET Framework documentation for more information about signing.
// This is not required, if you don't want signing let these attributes like they're.
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
#endregion


namespace monotester
{
	public class MonoTester
	{
		private enum ExitStatus : int
		{
			Success = 0,
			AssemblyNotFound,
			InvalidArguments,
			Failure,
		}
		
		private static void PrintUsage()
		{
			Console.WriteLine("Usage: {0} [option] [lib-to-check]", "monotester");
			Console.WriteLine("If no arguments are given, the tool will print the version of the mono runtime.");
			Console.WriteLine("If lib-to-check is specified, the version of that lib will be printed, or nothing if the lib was not found");
		}
		
		public static int Main(string[] args)
		{
			string libToCheck = null;
			bool onlyCheckExistance = false;
			
			if (args.Length == 0)
			{
				Console.WriteLine("{0}.{1}", Environment.Version.Major, Environment.Version.Minor);
				return (int) ExitStatus.Success;
			}
			foreach (string arg in args)
			{
				if (arg == "--help" || arg == "-h")
				{
					PrintUsage();
					return (int) ExitStatus.Success;
				}
				else if (arg == "--exists")
					onlyCheckExistance = true;
				else
				{
					if (libToCheck != null)
					{
						
						Console.Error.WriteLine("Invalid argument count. Only one library can be checked at the time");
						Console.Error.WriteLine("Try --help to see command syntax");
						return (int)  ExitStatus.InvalidArguments;
					}
					libToCheck = arg;
				}
			}
			try 
			{
				Version [] vList = FindAssemblies (libToCheck, GetGacDir());
				if (onlyCheckExistance == false)
				{
					foreach (Version v in vList)
						Console.Write ("{0}.{1} ", v.Major, v.Minor);
					if (vList.Length > 0) // Terminate the line
						Console.WriteLine("");
				}
				return (int)  (vList.Length > 0 ? ExitStatus.Success : ExitStatus.AssemblyNotFound);
				
			} 
			catch (Exception ex)
			{
				Console.Error.WriteLine(ex);
				return (int) ExitStatus.Failure;
			}
		}
		
		/// <summary>
		/// Locates the specified assemblies in the GAC and returns an array containing version
		/// information about the located assemblies. If the assembly could not be found, an 
		/// empty array is returned
		/// </summary>
		/// <param name="name">The name of the assembly, without file extension (.dll). Example: "Mono.Cairo"</param>
		/// <param name="gacdir">The global assembly cache directory.</param>
		/// <returns>
		/// An array of version objects for all the located versions of the assembly.
		/// </returns>
		private static Version[] FindAssemblies (string name, string gacdir)
		{
			ArrayList lst = new ArrayList();
			string asmdir = Path.Combine (gacdir, name);
			if (!Directory.Exists (asmdir)) {
				return new Version[0];
			}
			foreach (string dir in Directory.GetDirectories(asmdir)) {
				// The directory contains all the version information
				string asmb = Path.Combine (dir, name + ".dll");
				if (File.Exists (asmb)) {
					string versionString = new DirectoryInfo (dir).Name;
					lst.Add(GetVersionFromVersionString(versionString));
				}
			}
			return (Version[]) lst.ToArray(typeof(Version));
		}

		/// <summary>
		/// Converts a GAC directory name into a version object.
		/// </summary>
		/// <param name="versionString">The GAC directory name for the assembly</param>
		/// <returns>
		/// A version object containing version information. 
		/// If there is no version information for the assembly, an empty Version object is returned (all members set to zero)
		/// </returns>
		private static Version GetVersionFromVersionString (string versionString)
		{
			string [] pieces = versionString.Split ('_');
			if (pieces.Length == 0 || pieces[0].Length == 0)
				// All zeros to indicate version information missing (or version neutral)
				return new Version();
			else
				return new Version(pieces[0]);
		}

		/// <summary>
		/// Gets the directory containing the global assembly cache.
		/// </summary>
		private static string GetGacDir () {
			PropertyInfo gac = typeof (System.Environment).GetProperty ("GacPath",
					BindingFlags.Static|BindingFlags.NonPublic);
			if (gac == null) {
				Console.WriteLine ("ERROR: Mono runtime not detected, please use " +
						"the mono runtime for gacutil.exe");
				Environment.Exit (1);
			}
			MethodInfo get_gac = gac.GetGetMethod (true);
			return (string) get_gac.Invoke (null, null);
		}
	}
}

