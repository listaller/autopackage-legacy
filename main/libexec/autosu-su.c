/*  Autopackage Superuser - su backend
 *  Copyright (C) 2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _AUTOSU_SU_C_
#define _AUTOSU_SU_C_

#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "autosu-su.h"
#include "pty.h"


static GQuark error_domain = 0;
/* Filename to the 'su' binary */
static gchar *su_bin = NULL;


static gboolean
su_init (GError **error)
{
	if (su_bin != NULL)
		return TRUE;

	su_error_domain ();
	if (g_file_test ("/bin/su", G_FILE_TEST_EXISTS))
		su_bin = "/bin/su";
	else if (g_file_test ("/usr/bin/su", G_FILE_TEST_EXISTS))
		su_bin = "/usr/bin/su";
	else {
		g_set_error (error, error_domain, SU_ERROR_NOBIN,
			"The required system program 'su' is not found.");
		return FALSE;
	}
	return TRUE;
}

static void
setup_su (gpointer user_data)
{
	/* Set language to english so that we can parse the output strings */
	setenv ("LC_ALL", "C", 1);
	setenv ("LANGUAGE", "C", 1);
	setenv ("LANG", "C", 1);
	if (user_data) {
		gpointer *data = (gpointer *) user_data;
		SuSetupFunc func = (SuSetupFunc) data[0];
		if (func)
			(*func) (data[1]);
	}
}

static gint
handle_output (char *output, const char *user, GError **error)
{
	if (strcmp (output, "This account is currently not available.\n") == 0)
		/* Account not available */
		g_set_error (error, error_domain, SU_ERROR_UNAVAIL,
			"User account '%s' is currently not available.", user);
	else if (strstr (output, "does not exist") != NULL)
		/* User doesn't exist */
		g_set_error (error, error_domain, SU_ERROR_NOUSER,
			"User '%s' doesn't exist.", user);
	#if 0
	else if (strstr (output, "Authentication failure") != NULL)
		g_set_error (error, error_domain, SU_ERROR_FAIL,
			"The root account has been disabled. Please use sudo.");
	#endif
	else
		/* ?????? */
		g_set_error (error, error_domain, SU_ERROR_UNKNOWN,
			"'su' returned unknown data.");
	return -1;
}

static void
safefree (char *password)
{
	if (password != NULL) {
		memset (password, 0, strlen (password));
		g_free (password);
	}
}


GQuark
su_error_domain ()
{
	if (error_domain == 0)
		error_domain = g_quark_from_static_string ("su-backend");
	return error_domain;
}

gint
su_needs_password (const char *user, GError **error)
{
	Pty *pty;
	char *argv[5], buf[100];
	ssize_t r;
	gint result;

	/* Sanity checks */
	if (user == NULL) {
		g_set_error (error, error_domain, SU_ERROR_ARGS,
			"Argument 'user' in 'su_needs_password()' cannot be NULL.");
		return -1;
	}

	if (!su_init (error))
		return -1;


	/* Create a pseudo terminal and run su */
	argv[0] = "su";
	argv[1] = (char *) user;
	argv[2] = "-c";
	argv[3] = "true";
	argv[4] = NULL;
	pty = pty_new (argv, setup_su, NULL, error);
	if (pty == NULL)
		return -1;

	/*
		show_error (_("An error has occured while trying to gain "
			"superuser privileges. Your system may be low on memory."
			"Please close a few applications to free some memory.\n\n"
			"Technical details: fork() failed: %s"),
			g_strerror (errno));
		exit (EXIT_SYSERROR);
	*/

	/* Attempt to read the 'Password:' prompt */
	memset (buf, 0, sizeof (buf));
	r = pty_read (pty, buf, sizeof (buf) - 1);

	if (r == -1)
		/* No password required */
		result = 0;
	else if (strncmp (buf, "Password:", 9) == 0)
		/* Password required */
		result = 1;
	else
		result = handle_output (buf, user, error);

	pty_free (pty);
	return result;
}

gint
su_check_password (const char *user, const char *password, GError **error)
{
	Pty *pty;
	char *argv[5], buf[100];
	gint result;

	/* Sanity checks */
	if (user == NULL) {
		g_set_error (error, error_domain, SU_ERROR_ARGS,
			"Argument 'user' in 'su_check_password()' cannot be NULL.");
		return -1;
	}
	if (password == NULL) {
		g_set_error (error, error_domain, SU_ERROR_ARGS,
			"Argument 'password' in 'su_check_password()' cannot be NULL.");
		return -1;
	}

	if (!su_init (error))
		return -1;


	/* Create a pseudo terminal and run su */
	argv[0] = "su";
	argv[1] = (char *) user;
	argv[2] = "-c";
	argv[3] = "echo -n yes";
	argv[4] = NULL;
	pty = pty_new (argv, setup_su, NULL, error);
	if (pty == NULL)
		return -1;

	/* Attempt to read the 'Password:' prompt */
	memset (buf, 0, sizeof (buf));
	pty_read (pty, buf, sizeof (buf) - 1);

	if (strncmp (buf, "yes", 3) == 0)
		/* No password required */
		result = 1;

	else if (strncmp (buf, "Password:", 9) == 0) {
		/* Password required; wait until su is ready to read the password */
		while (pty_has_echo (pty))
			usleep (10000);

		/* Write password */
		pty_write (pty, (void *) password, -1);
		pty_write (pty, "\n", -1);

		/* Read result */
		memset (buf, 0, sizeof (buf));
		pty_read (pty, buf, sizeof (buf) - 1);

		if (buf[0] == 13 || buf[0] == 10) {
			/* This is just a newline; continue reading */
			memset (buf, 0, sizeof (buf));
			pty_read (pty, buf, sizeof (buf) - 1);
		}

		result = (strncmp (buf, "yes", 3) == 0);

	} else
		result = handle_output (buf, user, error);

	pty_free (pty);
	return result;
}

gint
su_exec (const char *user, char *password, const char *command,
	SuSetupFunc func, gpointer user_data, GError **error)
{
	Pty *pty;
	char *argv[5], buf[100];
	gint result;
	int status;

	/* Sanity checks */
	if (user == NULL) {
		g_set_error (error, error_domain, SU_ERROR_ARGS,
			"Argument 'user' in 'su_check_password()' cannot be NULL.");
		safefree (password);
		return -1;
	}
	if (command == NULL) {
		g_set_error (error, error_domain, SU_ERROR_ARGS,
			"Argument 'command' in 'su_check_password()' cannot be NULL.");
		safefree (password);
		return -1;
	}

	if (!su_init (error)) {
		safefree (password);
		return -1;
	}


	/* Create a pseudo terminal and run su */
	argv[0] = "su";
	argv[1] = (char *) user;
	argv[2] = "-c";
	argv[3] = (char *) command;
	argv[4] = NULL;

	pty = pty_new (argv, setup_su, user_data, error);
	if (pty == NULL) {
		safefree (password);
		return FALSE;
	}

	if (password != NULL) {
		/* This user needs a password. Attempt to read the 'Password:' prompt */
		memset (buf, 0, sizeof (buf));
		pty_read (pty, buf, sizeof (buf) - 1);

		if (strncmp (buf, "Password:", 9) == 0) {
			/* Wait until su is ready to read the password */
			while (pty_has_echo (pty))
				usleep (10000);

			/* Write password */
			pty_write (pty, (void *) password, -1);
			safefree (password);
			pty_write (pty, "\n", -1);

			/* Read newline so su doesn't lock up */
			memset (buf, 0, sizeof (buf));
			pty_read (pty, buf, sizeof (buf) - 1);

			/* Wait for child and return exit code */
			waitpid (pty->child_pid, &status, 0);
			if (!WIFEXITED (status)) {
				/* Child terminated abnormally */
				if (WIFSIGNALED (status))
					result = 128 + WTERMSIG (status);
				else {
					g_set_error (error, error_domain, SU_ERROR_UNKNOWN,
						"The root privilege provider (su) terminated unexpectedly.");
					result = -1;
				}
			} else
				result = WEXITSTATUS (status);

		} else {
			/* Not a password prompt? Something is messed up. */
			safefree (password);
			handle_output (buf, user, error);
			result = -1;
		}

	} else {
		/* No password required; just wait for the child... */
		waitpid (pty->child_pid, &status, 0);

		if (!WIFEXITED (status)) {
			/* Child terminated abnormally */
			if (WIFSIGNALED (status))
				result = 128 + WTERMSIG (status);
			else {
				g_set_error (error, error_domain, SU_ERROR_UNKNOWN,
						"The root privilege provider (su) terminated unexpectedly.");
				result = -1;
			}
		} else
			result = WEXITSTATUS (status);
	}

	pty_free (pty);
	return result;
}

#endif /* _AUTOSU_SU_C_ */
