/*  Autopackage Superuser - core
 *  Copyright (C) 2004,2005  Hongli Lai <h.lai@chello.nl>
 *                           Mike Hearn <mike@navi.cx>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>

#include <errno.h>

#include "autosu.h"

static char *helper_exe[2];
static int xauth_id = -1;

static int load_xauth (gboolean *error);


static gboolean
autosu_prepare (GError **error)
{
	char tmp[PATH_MAX + 1], *tmp2;

	/* Locate the helper application */
	if (!realpath ("/proc/self/exe", tmp)) {
		g_set_error (error, 0, 0, g_strdup_printf("Unable to read from /proc/self/exe. Your operating system might be broken (%d)", errno));
		return FALSE;
	}

	tmp2 = g_path_get_dirname (tmp);
	helper_exe[0] = g_strdup_printf ("%s/autosu-helper", tmp2);
	helper_exe[1] = NULL;
	g_free (tmp2);

	if (!g_file_test (helper_exe[0], G_FILE_TEST_EXISTS)) {
		g_set_error (error, 0, 0, _("Unable to locate autosu-helper. Your autopackage installation may be damaged."));
		return FALSE;
	}

	if (system ("grep -q pam_xauth /etc/pam.d/su 2>/dev/null") != 0) {
		/* Su doesn't use the pam_xauth module; setup xauth stuff */
		gboolean error_occurred = FALSE;
		xauth_id = load_xauth (&error_occurred);
		if (error_occurred) {
			g_set_error (error, 0, 0, _("Unable to load xauth."));
			return FALSE;
		}
	}

	return TRUE;
}


/* We use a helper application to work around X authentication bugs in certain
 * versions of su, and to redirect the pseudo terminal to the real terminal. */
gchar *
autosu_child_command (gchar *command, GError **error)
{
	gchar *child_command;
	char *in_tty[2], *out_tty[2], *err_tty[2], *lang[2];

	if (!autosu_prepare (error))
		return NULL;

	/* Prepare variables */
	lang[0] = getenv ("LC_ALL");
	if (lang[0])
		lang[0] = strdup (lang[0]);
	else
		lang[0] = "";
	lang[1] = NULL;

	in_tty[0] = ttyname (0);
	if (!in_tty[0])
		in_tty[0] = "";
	out_tty[0] = ttyname (1);
	if (!out_tty[0])
		out_tty[0] = "";
	err_tty[0] = ttyname (2);
	if (!err_tty[0])
		err_tty[0] = "";

	in_tty[1] = NULL;
	out_tty[1] = NULL;
	err_tty[1] = NULL;

	/* Create the command */
	child_command = g_strdup_printf ("%s %s %s %s %s %s %s",
		create_command (helper_exe),
		(xauth_id == -1) ? "" : g_strdup_printf ("--xauth-shm-id %d", xauth_id),
		create_command (in_tty),
		create_command (out_tty),
		create_command (err_tty),
		create_command (lang),
		command);

	return child_command;
}


/*************************
 * Utility functions
 *************************/


/* Report child's exit code back to autopackage */
void
report_exit_code (gint code)
{
	gchar *str;

	str = g_strdup_printf ("%d", code);
	write (5, str, strlen (str));
}


void
safefree (gchar *password)
{
	if (!password) return;
	memset (password, 0, strlen (password));
	g_free (password);
}


static void
replace_all (gchar **str, gchar *from, gchar *to)
{
	GString *newstr;
	gchar *found;

	g_return_if_fail (str != NULL);
	g_return_if_fail (from != NULL);
	g_return_if_fail (to != NULL);

	newstr = g_string_new (*str);
	found = strstr (newstr->str, from);
	while (found != NULL) {
		gint pos;

		pos = GPOINTER_TO_INT (found) - GPOINTER_TO_INT (newstr->str);
		g_string_erase (newstr, pos, strlen (from));
		g_string_insert (newstr, pos, to);
		found = GINT_TO_POINTER (GPOINTER_TO_INT (found) + strlen (to));
		found = strstr (found, from);
	}

	g_free (*str);
	*str = newstr->str;
	g_string_free (newstr, FALSE);
}


/* Changes an argument vector into a commandline.
   Escape special characters. */
gchar *
create_command (gchar **argv)
{
	GString *result;
	gchar *str;
	guint i;

	result = g_string_new ("");
	i = 0;
	while (argv[i] != NULL) {
		gchar *tmp;

		tmp = g_strdup (argv[i]);
		replace_all (&tmp, "\"", "\\\"");
		replace_all (&tmp, ">", "\\>");
		replace_all (&tmp, "<", "\\<");
		replace_all (&tmp, " ", "\\ ");
		replace_all (&tmp, "!", "\\!");
		replace_all (&tmp, "$", "\\$");
		replace_all (&tmp, "&", "\\&");
		if (!*tmp)
			g_string_append (result, "\"\" ");
		else
			g_string_append_printf (result, "%s ", tmp);
		g_free (tmp);
		i++;
	}

	str = result->str;
	str[strlen (str) - 1] = 0; /* Get rid of trailing whitespace */
	g_string_free (result, FALSE);
	return str;
}


static gboolean
exists (char *path)
{
	struct stat buf;
	if (stat (path, &buf) == -1)
		return FALSE;
	return TRUE;
}

static char *
get_xauth_path ()
{
	#define check(p) if (exists (p)) return p

	check ("/usr/X11R6/bin/xauth");
	check ("/usr/X11R7/bin/xauth");
	check ("/usr/X11/bin/xauth");
	check ("/usr/bin/xauth");
	check ("/bin/xauth");
	check ("/sbin/xauth");
	check ("/usr/sbin/xauth");
	check ("/usr/local/bin/xauth");
	check ("/usr/local/sbin/xauth");
	check ("/usr/local/X11R6/bin/xauth");
	check ("/usr/local/X11/bin/xauth");
	/* I'll commit seppuku if we haven't covered enough paths already. */

	/* fixme: what else goes here? */
	perror ("could not locate xauth");
	exit (1);

	#undef check
}

/* Return a shmid for a segment containing the X authentication tokens for this system */
static int
load_xauth (gboolean *error)
{
	char *command, *deststr;
	FILE *in;
	#define BUFSIZE 1024 * 5
	void *buffer;
	GString *str;
	int shmid = -1; /* If an error occurs return -1 */

	/* Run 'xauth list' and get it's output */
	command = g_strdup_printf ("%s -n list", get_xauth_path ());
	in = popen (command, "r");
	g_free (command);

	if (in == NULL) {
		perror ("could not run xauth");
		*error = TRUE;
		return -1;
	}

	buffer = malloc (BUFSIZE);	/* unlikely a token line will go >5k chars */
	str = g_string_new ("");

	while (!feof (in) && fgets (buffer, BUFSIZE, in)) {
		g_string_append (str, "add ");
		g_string_append (str, buffer);
	}

	if (pclose (in) != 0) {
		perror ("could not run xauth, pclose != 0");
		*error = TRUE;
	}
	else {
		/* Now we need to move the buffer to a shared memory segment */
		shmid = shmget (IPC_PRIVATE, str->len + 1, IPC_CREAT | IPC_EXCL | 0666);
		if (shmid == -1) {
			perror ("could not generate shared memory segment for X authentication tokens");
			*error = TRUE;
		}
		else {
			deststr = shmat (shmid, NULL, 0);
			if (deststr == (char *) - 1) {
				perror ("could not attach to shared memory segment for X authentication tokens");
				*error = TRUE;
			}
			else {
				strcpy (deststr, str->str);
			}
		}
	} /* Do not return before we haven't freed str */

	g_string_free (str, TRUE);
	return shmid;
}


/************************
 * Argument parsing
 ************************/

static void
usage (char *exe)
{
	fprintf (stderr, _("Usage: %s [--root-only] [-m apkg-install|install|manage] COMMAND [ARGS...]\n"), exe);
	exit (EXIT_SYSERROR);
}


char **
parse_args (int argc, char **argv, Options *options)
{
	char **command;
	int i;

        options->displayName = getenv ("AUTOSU_DISPLAYNAME");
        
	if (argc <= 1)
		usage (argv[0]);

	/* Parse arguments */
	for (i = 1; i < argc; i++) {
		if (strcmp (argv[i], "-m") == 0) {
			if (!argv[i + 1])
				usage (argv[0]);
			if (strcmp (argv[i + 1], "apkg-install") == 0)
				options->mode = APKG_INSTALL;
			else if (strcmp (argv[i + 1], "install") == 0)
				options->mode = INSTALL;
			else if (strcmp (argv[i + 1], "manage") == 0)
				options->mode = MANAGE;
			else
				usage (argv[0]);
			i++;

		} else if (strcmp (argv[i], "--root-only") == 0) {
			if (!argv[i + 1])
				usage (argv[0]);
			options->rootonly = TRUE;

		} else
			break;
	}

	/* Return a pointer to the first non-autosu argument */
	command = argv + i;
	if (!command[0])
		usage(argv[0]);

	return command;
}
