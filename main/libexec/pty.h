/* Simple library for messing with pseudo terminals
 * Copyright (C) 2005  Hongli Lai
 *
 * Parts are directly stolen from pty.c from livte.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef _PTY_H_
#define _PTY_H_

#include <sys/types.h>
#include <glib.h>
#include <unistd.h>

/* Mangle symbol names to avoid symbol conflicts */
#define PTY_NAMESPACE(symbol) autosu_ ## symbol
#define pty_new		PTY_NAMESPACE(pty_new)
#define pty_free	PTY_NAMESPACE(pty_free)
#define pty_read	PTY_NAMESPACE(pty_read)
#define pty_read_exact	PTY_NAMESPACE(pty_read_exact)
#define pty_write	PTY_NAMESPACE(pty_write)
#define pty_has_echo	PTY_NAMESPACE(pty_has_echo)
#define pty_waitpid	PTY_NAMESPACE(pty_waitpid)


typedef struct _PtyPrivate PtyPrivate;

typedef struct {
	int tty;
	char *ttyname;
	pid_t child_pid;
	PtyPrivate *_priv;
} Pty;

typedef enum {
	/* Unable to allocate a pseudo terminal */
	PTY_ERROR_ALLOC,
	/* Unable to allocate a pipe */
	PTY_ERROR_PIPE,
	/* Unable to fork a new process */
	PTY_ERROR_FORK,
	/* Unknown error (child failed to setup) */
	PTY_ERROR_UNKNOWN
} PtyErrorCode;

typedef void (*PtySetupFunc) (gpointer user_data);


Pty *pty_new (char **argv, PtySetupFunc func, gpointer user_data, GError **error);
void pty_free (Pty *pty);

ssize_t pty_read (Pty *pty, void *buffer, size_t count);
ssize_t pty_read_exact (Pty *pty, void *buffer, size_t count);
ssize_t pty_write (Pty *pty, void *buffer, ssize_t count);

gboolean pty_has_echo (Pty *pty);

gboolean pty_fork (Pty *pty, char **argv, pid_t *return_pid, PtySetupFunc func, gpointer user_data, GError **error);
gint pty_waitpid (Pty *pty);

#endif /* _PTY_H_ */
