/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "wizard.h"
#include <qapplication.h>
#include <qtextcodec.h>
#include <unistd.h>
#include <stdlib.h>

#include <libintl.h>
#include <locale.h>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	
	QTextCodec *codec = QTextCodec::codecForName("utf8");
	QTextCodec::setCodecForCStrings(codec);
	
	setlocale (LC_ALL, "");
	bindtextdomain ("autopackage-qt", qApp->applicationDirPath()+"/../share/locale");
	bind_textdomain_codeset ("autopackage-qt", "UTF-8");
	textdomain ("autopackage-qt");
	
	if (QString(app.argv()[1]) == "--help")
	{
		printf("Manage 3rd party software\n");
		printf("Usage: autopackage-manager-qt <package name>\n");
		printf("       Where <package name> is the name of the package you want to remove\n");
		exit(0);
	}
	
	Wizard* w = new Wizard(NULL);
	w->show();
	
	app.setMainWidget(w);
	return app.exec();
}

