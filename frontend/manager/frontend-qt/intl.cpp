#include <locale.h>
#include <libintl.h>

#include <qstring.h>

// Wrappers around gettext that return QStrings

QString gtr(const char* id)
{
	return QString(gettext(id));
}

QString ngtr(const char* singularId, const char* pluralId, int n)
{
	return QString(ngettext(singularId, pluralId, n));
}