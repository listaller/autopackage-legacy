/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef WIZARD_H
#define WIZARD_H

#include <qevent.h>
#include <qpixmap.h>
#include <qptrqueue.h> 
#include <qlistview.h>
#include <qprocess.h>
#include <qtimer.h>

#include "wizardbase.h"
#include "../managercore/frontend-interface.h"

class AppFoundEvent : public QCustomEvent
{
public:
	AppFoundEvent(struct ApplicationInfo* info)
	  : QCustomEvent(65536), m_info(info) {}
	
	struct ApplicationInfo* m_info;
};

class AppSearchDoneEvent : public QCustomEvent
{
public:
	AppSearchDoneEvent()
	  : QCustomEvent(65538) {}
};

class UninstallDoneEvent : public QCustomEvent
{
public:
	UninstallDoneEvent(int e)
	  : QCustomEvent(65537), m_error(e) {}
	
	int m_error;
};


class DesktopListItem : public QListViewItem
{
public:
	DesktopListItem(QListView* parent)
	  : QListViewItem(parent), m_info(NULL) {}
	
	struct ApplicationInfo* info() {return m_info;}
	void setInfo(struct ApplicationInfo* i) {m_info = i; repaint();}
	
	void paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align);
	int width(const QFontMetrics& fm, const QListView* lv, int c) const;
	
private:
	QString niceSize();
	QString niceDate();
	QString niceDateCache;
	mutable struct ApplicationInfo* m_info;
};


class Wizard : public WizardBase
{
	Q_OBJECT

public:
	Wizard(QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
	~Wizard();

public slots:
	void cancelPressed();
	void nextPressed();
	void customEvent(QCustomEvent* e);
	void selectionChanged();
	void allDone();
	void incProgressBar();

private:
	void initKdeSettings();
	QString runCommand(const QString& command, const QString& args);
	QString kdeConfigValue(const QString& section, const QString& name, const QString& def);
	QString findIconPath(const QString& name);
	void uninstallNextPackage();
	void prepareToUninstall();
	void beginRootUninstall();
	
	QPixmap packageIcon32;
	QPixmap packageIcon48;
	QString iconTheme;
	QStringList kdeSearchPaths;
	QStringList possibleIconPaths;
	
	QPtrQueue<struct ApplicationInfo> uninstallQueue;
	QPtrQueue<struct ApplicationInfo> rootUninstallQueue;
	struct ApplicationInfo* currentlyRemoving;
	int failed;
	int succeeded;
	int rootCount;
	
	QTimer* timer;
	
	QProcess* proc;
};

#endif

