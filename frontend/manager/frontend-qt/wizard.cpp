/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qapplication.h>
#include <qlabel.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qregexp.h>
#include <qlistview.h>
#include <qheader.h>
#include <qmessagebox.h>
#include <qprogressbar.h>
#include <qpushbutton.h>
#include <qwidgetstack.h>
#include <qpainter.h>
#include <qtimer.h>
#include <qstyle.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

#include "wizard.h"
#include "kpackage.h"
#include "intl.h"

// Callbacks
Wizard* wiz;
void appFound(struct ApplicationInfo* info)
{
	if (info == NULL)
		QApplication::postEvent(wiz, new AppSearchDoneEvent());
	else
		QApplication::postEvent(wiz, new AppFoundEvent(info));
}

void uninstallDone(int error)
{
	QApplication::postEvent(wiz, new UninstallDoneEvent(error));
}



void DesktopListItem::paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align)
{
	// Paints our nice list view items
	QListViewItem::paintCell(p, cg, column, width, align);
	
	int top = (height() - listView()->fontMetrics().height()*2) / 2 + listView()->fontMetrics().ascent();
	
	QFont boldFont = listView()->font();
	boldFont.setBold(true);
	p->setFont(boldFont);
	p->drawText(pixmap(column)->width() + 5, top, info()->name);
	
	p->setFont(listView()->font());
	if (!isSelected())  // So the text goes white when you select it
		p->setPen(listView()->colorGroup().dark());
	p->drawText(pixmap(column)->width() + 5, top + listView()->fontMetrics().height(), niceSize());
	int offset = listView()->fontMetrics().width("Size: 999.99 bytes") + 40;
	p->drawText(pixmap(column)->width() + offset, top + listView()->fontMetrics().height(), niceDate());
}

int DesktopListItem::width(const QFontMetrics& fm, const QListView* lv, int c) const
{
	return listView()->width() - qApp->style().pixelMetric(QStyle::PM_ScrollBarExtent) - 2;
}

QString DesktopListItem::niceSize()
{
	QString unit=gtr("Bytes");
	float fSize = (float)(info()->size);
	
	if ((info()->size < 1024*1024) && (info()->size > 1023) ) {
		fSize = (float)(info()->size) / 1024;
		unit=gtr("KiB");
	}
	else if ( (info()->size > 1024 * 1024 - 1) && (info()->size < (1024*1024*1024)) ) {
		fSize = (float)(info()->size) / (1024 * 1024);
		unit=gtr("MiB");
	}
	else {
		fSize = (float)(info()->size) / (1024 * 1024 * 1024);
		unit=gtr("GiB");
	}
	
	// Translators: %1 will be expanded to a size (real/double) and %2 will be a unit size such as KiB, MiB, etc
	return QString(gtr("Size: %1 %2")).arg(fSize,0,'f',2).arg(unit);
}

QString DesktopListItem::niceDate()
{
	if (!niceDateCache.isEmpty())
		return niceDateCache;
	
	// Because regular expressions are fun
	QRegExp re("(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2})");
	
	if (re.search(info()->timestamp) == -1)
		return QString::null;
	
	int timeNow_t = QDateTime::currentDateTime().toTime_t();
	QDateTime packageTime;
	packageTime.setDate(QDate(re.cap(1).toInt(), re.cap(2).toInt(), re.cap(3).toInt()));
	packageTime.setTime(QTime(re.cap(4).toInt(), re.cap(5).toInt(), re.cap(6).toInt()));
	int packageTime_t = packageTime.toTime_t();
	
	long diff = timeNow_t - packageTime_t;
	
	if (diff < 0)
		niceDateCache = gtr("Installed: The Future");
	else if (diff < 60)
		niceDateCache = ngtr("Installed: %1 second ago","Installed: %1 seconds ago",(int)diff).arg(diff,0,'f',0);
	else if (diff < 60*60)
		niceDateCache = ngtr("Installed: %1 minute ago","Installed: %1 minutes ago",(int)diff/60).arg(diff/60,0,'f',0);
	else if (diff < 60*60*24)
		niceDateCache = ngtr("Installed: %1 hour ago","Installed: %1 hours ago",(int)diff/(60*60)).arg(diff/(60*60),0,'f',0);
	else
		niceDateCache = ngtr("Installed: %1 day ago","Installed: %2 days ago",(int)diff/(60*60*24)).arg(diff/(60*60*24),0,'f',0);
	
	return niceDateCache;
}


Wizard::Wizard(QWidget* parent, const char* name, bool modal, WFlags fl)
: WizardBase(parent,name, modal,fl),
  succeeded(0),
  failed(0),
  rootCount(0)
{
	// Load the package icons
	packageIcon32.convertFromImage(qembed_findImage("kpackage32"));
	packageIcon48.convertFromImage(qembed_findImage("kpackage48"));
	appIcon->setPixmap(packageIcon32);
	
	// So the (global) callbacks can find us
	wiz = this;
	
	// Set up some parts of the UI
	packageList->header()->hide();
	adminLabel->hide();
	nextButton->setEnabled(false);
	noAppsLabel->setText(noAppsLabel->text().replace("DARK", colorGroup().dark().name()));
	
	// This QProcess is used to launch kdesu
	proc = new QProcess(this);
	connect(proc, SIGNAL(processExited()), SLOT(allDone()));
	
	// If we want to remove packages specified on the command line, we skip the
	// first page of the widget stack, and get on with it.
	if (qApp->argc() > 1)
	{
		stack->raiseWidget(1);
		progressBar->hide();
		progressLabel->setText(gtr("Please wait..."));
		nextButton->setEnabled(false);
	}
	else {
		appNameLabel->setText("<b>"+gtr("Searching...")+"</b>");
		progressLabel->setText(gtr("Searching for applications..."));
	}
	
	initKdeSettings();
	findApplications(&appFound);
	stack->raiseWidget(1);
	
	progressBar->show();
	timer = new QTimer(this);
	progressBar->setTotalSteps(0);
	connect(timer,SIGNAL(timeout()),this,SLOT(incProgressBar()));
	timer->start(10);
}

Wizard::~Wizard()
{
	cleanup();
}

void Wizard::incProgressBar() {
	int currentPos = progressBar->progress();
	progressBar->setProgress(++currentPos);
}


void Wizard::customEvent(QCustomEvent* e)
{
	// Since the callbacks are run in another thread, they communicate with the main
	// thread through Qt events.
	if (e->type() == 65536) // AppFoundEvent
	{
		AppFoundEvent* event = (AppFoundEvent*)e;

		if (event->m_info->type != APP_TYPE_DESKTOP)
			return;
		// Find an icon
		QString iconPath = findIconPath(event->m_info->icon);
		QPixmap iconPix;
		if (iconPath.isNull())
			iconPix = packageIcon48;
		else
			iconPix = QPixmap(iconPath);
		iconPix.convertFromImage(iconPix.convertToImage().smoothScale(48, 48, QImage::ScaleMin));
		
		// Add it to the list
		DesktopListItem* item = new DesktopListItem(packageList);
		item->setPixmap(0, iconPix);
		item->setInfo(event->m_info);
		
		// If it's an automatic removal, and it's one of the ones we have been asked
		// to remove, automatically select it
		if (qApp->argc() > 1)
		{
			for (int i=0 ; i<qApp->argc() ; i++)
			{
				if (QString(qApp->argv()[i]) != event->m_info->packageName)
					continue;
				packageList->setSelected(item, true);
			}
		}
	}
	if (e->type() == 65537) // UninstallDoneEvent
	{
		UninstallDoneEvent* event = (UninstallDoneEvent*)e;
		QString errorText;
		switch (event->m_error)
		{
		case -1:
			errorText = gtr("The uninstaller backend could not be started");
			break;
		case -2:
			errorText = gtr("There was an error reading the application data");
			break;
		case 1:
			errorText = gtr("The application could not be found");
			break;
		case 2:
			errorText = gtr("The application was removed, but errors were encountered");
			break;
		case 3:
			errorText = gtr("Other applications require this package to function properly.  It was not removed.");
			break;
		case 4:
			errorText = gtr("Something went very wrong");
			break;
		case 5:
			errorText = gtr("You need to be root to remove this application");
			break;
		case 6:
			errorText = gtr("You need to be a normal user to remove this application");
			break;
		}
		
		if (event->m_error != 0)
		{
			failed++;
			QMessageBox::critical(this, QString(gtr("Error removing ")) + currentlyRemoving->name, errorText, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		}
		else
			succeeded++;
		
		progressBar->setProgress(progressBar->progress() + 1);
		if (uninstallQueue.count() == 0)
		{
			if (rootUninstallQueue.count() != 0)
				beginRootUninstall();
			else
				allDone();
		}
		else
			uninstallNextPackage();
	}
	if (e->type() == 65538) // AppSearchDoneEvent
	{
		appNameLabel->setText("<b>"+gtr("Manage 3rd party software")+"</b>");
		packageList->setCurrentItem(packageList->firstChild());
		stack->raiseWidget(0);
		progressLabel->setText(gtr("Please wait while the application you selected is removed..."));
		
		if (qApp->argc() > 1) {
			// if we've been asked to uninstall, then don't confuse the user
			// by showing the applist on startup...just keep the progress bar
			// moving while we prepareToUninstall().
			prepareToUninstall();
			stack->raiseWidget(1);
		}
		
		if (qApp->argc() < 2)
			timer->stop();
		
		if (packageList->childCount() == 0)
			packageListStack->raiseWidget(1);
	}
}

void Wizard::allDone()
{
	if (qApp->argc() > 1)
	{
		accept();
		return;
	}
	// The process will never return 0.
	// 0 will only be returned if the password is not entered in kdesu
	if (proc->exitStatus() == 0)
	{
		succeeded -= rootCount;
		failed += rootCount;
	}
	nextButton->setEnabled(true);
	nextButton->setText(gtr("Finish"));
	appNameLabel->setText(gtr("All tasks complete"));
	QString summaryText;
	if (succeeded > 0)
		summaryText += ngtr("%1 application removed successfully.","%1 applications removed successfully.",succeeded).arg(succeeded);
	
	if (failed > 0)
		summaryText += "<br><b>Warning:</b> " + ngtr("%1 application could not be removed.","%1 applications could not be removed.",failed).arg(failed);
	summaryText += "<br>"+gtr("Click Finish to exit");
	progressLabel->setText(summaryText);
}


void Wizard::cancelPressed()
{
	reject();
}

void Wizard::selectionChanged()
{
	// The listbox's selection changed.
	// Do any of the selected packages need the root password?
	// Have any packages been selected at all?
	// Disable/enable things appropriately
	bool rootNeeded = false;
	bool itemsSelected = false;
	QListViewItemIterator it( packageList );
	while ( it.current() )
	{
		DesktopListItem* item = (DesktopListItem*) it.current();
		if (item->isSelected())
		{
			if ((item->info()->root == 1) && (getuid() != 0))
				rootNeeded = true;
			itemsSelected = true;
		}
		++it;
	}
	if (rootNeeded)
		adminLabel->show();
	else
		adminLabel->hide();
	nextButton->setEnabled(itemsSelected);
}

void Wizard::nextPressed()
{
	int currentId = stack->id(stack->visibleWidget());
	
	switch (currentId)
	{
	case 0:
	{
		prepareToUninstall();
		break;
	}
	case 1:
		accept();
		return;
	}
	
	stack->raiseWidget(++currentId);
}

void Wizard::prepareToUninstall()
{
	uninstallQueue.clear();
	rootUninstallQueue.clear();
	QListViewItemIterator it( packageList );
	while ( it.current() )
	{
		DesktopListItem* item = (DesktopListItem*) it.current();
		if (item->isSelected())
		{
			if ((item->info()->root == 1) && (getuid() != 0))
				rootUninstallQueue.enqueue(item->info());
			else
				uninstallQueue.enqueue(item->info());
		}
		++it;
	}
	
	if (uninstallQueue.count() > 1)
	{
		progressLabel->setText(gtr("Please wait while the applications you selected are removed..."));
		progressBar->setTotalSteps(uninstallQueue.count());
		progressBar->setProgress(0);
	}
	else
		progressBar->hide();
	
	nextButton->setEnabled(false);
	
	if (uninstallQueue.count() <= 0)
		beginRootUninstall();
	else
		uninstallNextPackage();
}

void Wizard::uninstallNextPackage()
{
	currentlyRemoving = uninstallQueue.dequeue();
	// Translators: %1 will be expanded to the (translated) name of the package being removed. Example: "Removing Firefox Web Browser"
	appNameLabel->setText("<b>"+gtr("Removing %1").arg(QString(currentlyRemoving->name)) + "</b>");
	
	uninstallPackage(currentlyRemoving, &uninstallDone);
}

void Wizard::beginRootUninstall()
{
	appNameLabel->setText("<b>"+gtr("Removing applications as root user...")+"</b>");
	proc->clearArguments();
	proc->addArgument("kdesu");
	proc->addArgument("-t");
	proc->addArgument("-c");
	QString arg = qApp->applicationFilePath();
	struct ApplicationInfo* info;
	int c = rootUninstallQueue.count();
	while ((info = rootUninstallQueue.dequeue()))
		arg += " " + QString(info->packageName);
	arg += " && exit 1";
	proc->addArgument(arg);
	
	if (!proc->start())
	{
		QMessageBox::warning(this, gtr("Warning"), gtr("kdesu could not be started.  Check that it is in your PATH."), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		failed += c;
		return;
	}
	
	succeeded += c;
	rootCount = c;
}



// What follows is just a big ugly hack to find icons
// This will all go away when we become a KDE frontend.

void Wizard::initKdeSettings()
{
	kdeSearchPaths.clear();
	
	QString kdeHome = getenv("KDEHOME");
	QString kdeDirs = getenv("KDEDIRS");
	QString kdeDir = getenv("KDEDIR");
	
	if (!kdeHome.isEmpty())
		kdeSearchPaths.append(kdeHome);
	kdeSearchPaths.append(runCommand("kde-config", "--localprefix"));
	
	if (!kdeDirs.isEmpty())
		kdeSearchPaths += QStringList::split(':', kdeDirs);
	if (!kdeDir.isEmpty())
		kdeSearchPaths.append(kdeDir);
	kdeSearchPaths.append(runCommand("kde-config", "--prefix"));
	
	kdeSearchPaths.append("/usr");
	kdeSearchPaths.append("/usr/local");
	kdeSearchPaths.append("/opt/kde3");
	
	iconTheme = kdeConfigValue("Icons", "Theme", "crystal");
	
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/apps/%1");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/apps/%1.png");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/apps/%1.xpm");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/actions/%1");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/actions/%1.png");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/actions/%1.xpm");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/devices/%1");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/devices/%1.png");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/devices/%1.xpm");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/filesystems/%1");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/filesystems/%1.png");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/filesystems/%1.xpm");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/mimetypes/%1");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/mimetypes/%1.png");
	possibleIconPaths.append("/share/icons/" + iconTheme + "/48x48/mimetypes/%1.xpm");
	possibleIconPaths.append("/share/icons/%1");
	possibleIconPaths.append("/share/icons/%1.png");
	possibleIconPaths.append("/share/icons/%1.xcf");
}

QString Wizard::runCommand(const QString& command, const QString& args)
{
	FILE* p = popen(QString(command + " " + args).latin1(), "r");
	if ((p == NULL) || (p < 0))
		return QString::null;
	
	QString ret;
	while (!feof(p))
	{
		char buffer[256];
		int n = fread(buffer, 1, 255, p);
		buffer[n] = '\0';
		ret += buffer;
	}
	pclose(p);
	
	if (ret.isEmpty() && !command.startsWith("/"))
	{
		// Try alternate paths
		ret = runCommand("/usr/bin/" + command, args);
		if (!ret.isEmpty())
			return ret;
		ret = runCommand("/usr/local/bin/" + command, args);
		if (!ret.isEmpty())
			return ret;
		ret = runCommand("/opt/kde3/bin/" + command, args);
		if (!ret.isEmpty())
			return ret;
		ret = runCommand("/opt/kde/bin/" + command, args);
		if (!ret.isEmpty())
			return ret;
	}
	
	return ret.stripWhiteSpace();
}


QString Wizard::kdeConfigValue(const QString& section, const QString& name, const QString& def)
{
	for ( QStringList::Iterator it = kdeSearchPaths.begin(); it != kdeSearchPaths.end(); ++it )
	{
		if (!QFile::exists((*it) + "/share/config/kdeglobals"))
			continue;
		
		QFile file((*it) + "/share/config/kdeglobals");
		if (!file.open( IO_ReadOnly ))
			continue;
		
		QTextStream stream( &file );
		QString line;
		QString sec;
		while ( !stream.atEnd() )
		{
			line = stream.readLine();
			if (line.startsWith("["))
			{
				sec = line.mid(1, line.length() - 2);
				continue;
			}
			if (sec != section)
				continue;
			QRegExp parser("([\\S]*)\\s*=\\s*([\\S]*)");
			if (parser.search(line) == -1)
				continue;
			if (parser.cap(1) == name)
				return parser.cap(2);
		}
		file.close();
	}
	return def;
}

QString Wizard::findIconPath(const QString& name)
{
	for ( QStringList::Iterator it = kdeSearchPaths.begin(); it != kdeSearchPaths.end(); ++it )
	{
		for ( QStringList::Iterator it2 = possibleIconPaths.begin(); it2 != possibleIconPaths.end(); ++it2 )
		{
			if (QFile::exists((*it) + (*it2).arg(name)))
				return (*it) + (*it2).arg(name);
		}
	}
	return QString::null;
}


