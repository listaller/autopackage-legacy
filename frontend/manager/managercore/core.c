/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <pthread.h> 

#include "core.h"
#include "frontend-interface.h"
#include "backend-autopackage.h"
#include "../config.h"

/* Stores a list of all allocated ApplicationInfo objects so they can be cleared up later */
struct ApplicationInfo** appInfoList = NULL;
int appInfoCount = 0;


/* Used by the backends to create an ApplicationInfo object and add it to the list
 * All allocated variables will be assigned to the created struct which takes over ownership of
 * them. They should be free'd when the rest of the struct is free'd */
struct ApplicationInfo* createAppInfo(void (*callback)(char*, void (*)(int)), char* name, char* comment, char* icon, int root,
				      char* packageName, int size, char* timestamp, AutoPackageAppType type)
{
	// Create the struct
	struct ApplicationInfo* ret = (struct ApplicationInfo*) malloc(sizeof(struct ApplicationInfo));
	ret->name = name;
	ret->comment = comment;
	ret->icon = icon;
	ret->root = root;
	ret->packageName = packageName;
	ret->size = size;
	ret->timestamp = timestamp;
	ret->callback = callback;
	ret->type = type;
	// Add the struct to the list
	appInfoList = (struct ApplicationInfo**) realloc( appInfoList, sizeof(struct ApplicationInfo*) * (appInfoCount+1));
	appInfoList[appInfoCount] = ret;
	appInfoCount++;
	
	// Return it
	return ret;
}


void* findApplicationsThread(void* a)
{
	/* Decode the argument */
	void (*callback)(struct ApplicationInfo*) = (void (*)(struct ApplicationInfo*)) a;
	
	/* BACKENDS: Tell all backends to search */
#ifdef HAVE_AUTOPACKAGE
	autopackageSearch(callback);
#endif
	
	/* Tell the frontend we're done */
	callback(NULL);
	
	return NULL;
}

/* Called by the frontend.  This just runs the above function in a new thread */
void findApplications(void (*callback)(struct ApplicationInfo*))
{
	pthread_t t;
	pthread_create(&t, NULL, &findApplicationsThread, (void*) callback);
}


/* Since pthreads only lets us give one argument to the new thread, we package up the
   two we need to send in a struct, and pass that instead. */
struct UninstallPackageArgs
{
	struct ApplicationInfo* info;
	void (*callback)(int);
};

void* uninstallPackageThread(void* a)
{
	/* Decode the argument */
	struct UninstallPackageArgs* args = (struct UninstallPackageArgs*) a;
	
	/* The ApplicationInfo object contains a pointer to a function that will remove
	   that application.  Here we call that function */
	args->info->callback(args->info->packageName, args->callback);
	
	free(args);
	
	return NULL;
}

/* Called by the frontend.  This just runs the above function in a new thread. */
void uninstallPackage(struct ApplicationInfo* info, void (*callback)(int))
{
	struct UninstallPackageArgs* a = (struct UninstallPackageArgs*) malloc(sizeof(struct UninstallPackageArgs));
	a->callback = callback;
	a->info = info;
	
	pthread_t t;
	pthread_create(&t, NULL, &uninstallPackageThread, (void*) a);
}

void cleanup()
{
	/* Frees memory allocated in the ApplicationInfo list */
	int i;
	for (i=0 ; i<appInfoCount ; i++)
	{
		free(appInfoList[i]->name);
		free(appInfoList[i]->icon);
		free(appInfoList[i]->packageName);
		free(appInfoList[i]->timestamp);
		free(appInfoList[i]);
	}
	free(appInfoList);
	
	/* BACKENDS: Let the individual backends cleanup */
#ifdef HAVE_AUTOPACKAGE
	autopackageCleanup();
#endif
}


