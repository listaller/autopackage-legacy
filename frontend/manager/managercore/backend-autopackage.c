/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "../config.h"
#ifdef HAVE_AUTOPACKAGE

#include <unistd.h> 
#include <sys/types.h>
#include <sys/wait.h> 
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <errno.h>
#include "core.h"

char* globalDbLocation;
char* userDbLocation;
char* locale;
char* locale1;
char* locale2;
static int logging = 0;

#define TRACE(s...) if (logging) { printf("autopackage-manager-backend: "); printf(s); }

/* Concatenates two strings and returns the newly allocated string which must be returned when no longer used.
 * If either s1 or s2 is NULL, this function returns NULL. */
char *ap_strconcat(const char *s1, const char *s2)
{
	char *new_str;
	if (!s1 || !s2)
		return NULL;
	new_str = calloc(strlen(s1) + strlen(s2) + 1, sizeof(char));
	strncpy (new_str, s1, strlen(s1));
	strncat (new_str, s2, strlen(s2));

	return new_str;
}

/* Locates the full path to the 'package' binary.
 * TODO: This should use binreloc */
static char *find_package_command()
{
	if (access ("/usr/bin/package", X_OK) == 0)
		return strdup ("/usr/bin/package");
	char *rv = ap_strconcat (getenv("HOME"), "/.local/bin/package");
	if (access (rv, X_OK) == 0)
		return rv;
	
	/* Pray that it is in $PATH */
	return strdup ("package");
}

void autopackageUninstall(char* packageName, void (*callback)(int))
{
	char command[1024];
	char *pkgcmd = find_package_command();
	snprintf(command, 1024, "__no_fd_close=1 %s remove %s", pkgcmd, packageName);
	free(pkgcmd);
	TRACE("Running uninstall command: %s\n", command);
	int ret = system(command);
	
	callback(WEXITSTATUS(ret));
}

/* This function executes the command given in the argument, and returns its stdout */
char* runCommand(char* command)
{
	TRACE("Running command %s through a pipe\n", command);
	FILE* p = popen(command, "r");
	if ((p == NULL) || (p < 0))
		return NULL;
	
	char* output = NULL;
	int len = 0;
	while (!feof(p))
	{
		char buffer[256];
		int n = fread(buffer, 1, 256, p);
		output = (char*)realloc(output, len + n);
		memcpy(output + len, &buffer, n);
		len += n;
	}
	pclose(p);
	
	output = (char*)realloc(output, len + 1);
	output[len] = '\0';
	
	return output;
}

// Returns 0 for success, 1 for failure
int parseDebianFile(const char* path, int root, const char* shortname, int size, char* timestamp, void (*callback)(struct ApplicationInfo*))
{
	FILE* desktopFileHandle = fopen(path, "r");

	TRACE ("parsing debian file for %s details\n", shortname);
	if (desktopFileHandle == NULL) {
		TRACE ("Failure: couldn't open debian file (%s): %s\n", path, strerror(errno));
		return 1;
	}
	char* desktopFile = NULL;
	int len = 0;
	while (!feof(desktopFileHandle))
	{
		char buffer[256];
		int n = fread(buffer, 1, 256, desktopFileHandle);
		desktopFile = (char*)realloc(desktopFile, len + n);
		memcpy(desktopFile + len, &buffer, n);
		len += n;
	}
	fclose(desktopFileHandle);
	
	char* startPos = strstr(desktopFile, "icon=\"");
	if (startPos == NULL)
		return 1;
	startPos += strlen("icon=\"");
	char* endPos = strstr(startPos, "\"");
	if (endPos == NULL)
		return 1;
	char* icon = (char*) malloc(endPos - startPos + 1);
	memcpy(icon, startPos, endPos - startPos);
	icon[endPos - startPos] = '\0';
	
	
	startPos = strstr(desktopFile, "title=\"");
	if (startPos == NULL)
		return 1;
	startPos += strlen("title=\"");
	endPos = strstr(startPos, "\"");
	if (endPos == NULL)
		return 1;
	char* title = (char*) malloc(endPos - startPos + 1);
	memcpy(title, startPos, endPos - startPos);
	title[endPos - startPos] = '\0';

	startPos = strstr(desktopFile, "longtitle=\"");
	if (startPos == NULL)
		return 1;
	startPos += strlen("longtitle=\"");
	endPos = strstr(startPos, "\"");
	if (endPos == NULL)
		return 1;
	char* description = (char*) malloc(endPos - startPos + 1);
	memcpy(description, startPos, endPos - startPos);
	description[endPos - startPos] = '\0';

	TRACE ("Debian file successfully parsed (name='%s', description='%s')\n", title, description);
	callback(createAppInfo(autopackageUninstall, title, description, icon, root, strdup(shortname), size, strdup(timestamp), APP_TYPE_DESKTOP));
	
	return 0;
}

int parseDesktop(const char* path, const char* shortname, int size, char* timestamp, void (*callback)(struct ApplicationInfo*))
{
	FILE* desktopFileHandle = fopen(path, "r");
	TRACE ("parsing desktop file for %s details\n", shortname);
	if (desktopFileHandle == NULL) {
		TRACE ("Failure: couldn't open desktop file (%s): %s\n", path, strerror(errno));
		return 1;
	}

	char* desktopFile = NULL;
	int len = 0;
	while (!feof(desktopFileHandle))
	{
		char buffer[256];
		int n = fread(buffer, 1, 256, desktopFileHandle);
		desktopFile = (char*)realloc(desktopFile, len + n);
		memcpy(desktopFile + len, &buffer, n);
		len += n;
	}
	fclose(desktopFileHandle);
	
	char* key = NULL;
	char* value = NULL;
	char* name = NULL;
	char* icon = NULL;
	char* comment = NULL;
	int lastMarker = 0;
	int i;
	
	for (i=0 ; i<len ; i++)
	{
		if ((desktopFile[i] == '=') && (key == NULL))
		{
			key = (char*)malloc(i - lastMarker + 1);
			memcpy(key, desktopFile + lastMarker, i - lastMarker);
			key[i - lastMarker] = '\0';
			lastMarker = i + 1;
		}
		if (((desktopFile[i] == '\n') || (i == (len-1))) && (value == NULL))
		{
			value = (char*)malloc(i - lastMarker + 1);
			memcpy(value, desktopFile + lastMarker, i - lastMarker);
			value[i - lastMarker] = '\0';
			lastMarker = i + 1;
			
			if (key != NULL)
			{
				// If it's not an application, skip the whole file
				if ((strcasecmp(key, "type") == 0) && (strcasecmp(value, "application") != 0))
				{
					free(key);
					free(value);
					free(name);
					free(icon);
					TRACE("parseDesktop(%s): Type is not set to application, skipping.\n", shortname);
					return 1;
				}
				if ((strcasecmp(key, "name") == 0) && (name == NULL))
					name = strdup(value); 
				else if ((strcasecmp(key, "icon") == 0) && (icon == NULL))
					icon = strdup(value);
				else if ((strcasecmp(key, "comment") == 0) && (comment == NULL))
					comment = strdup(value);
			}
			
			free(key); key = NULL;
			free(value); value = NULL;
		}
	}
	
	// Try opening it for writing so we can see if we need to be root to remove it
	int root = 0;
	desktopFileHandle = fopen(path, "r+");
	if (desktopFileHandle == NULL)
		root = 1;
	else
		fclose(desktopFileHandle);

	TRACE ("Desktop file successfully parsed (name='%s', description='%s')\n", name, comment);
	callback(createAppInfo(autopackageUninstall, name, comment, icon, root,
			       strdup(shortname), size, strdup (timestamp), APP_TYPE_DESKTOP));
	
	return 0;
}

/* Parses the package environment file for package details */
int parsePackageEnvironment (const char *path, const char *shortname, int size, const char *timestamp, void (*callback)(struct ApplicationInfo*))
{
	char *envfile = ap_strconcat (path, "/environment.en");
	char buf[256];
	FILE *f;

	TRACE ("Parsing package environment for %s details\n", shortname);
	if (!(f = fopen (envfile, "r"))) {
		TRACE ("Failure: couldn't open environment file (%s): %s\n", envfile, strerror(errno));
		free(envfile);
		return 1;
	}
	free (envfile);

	int root;
	char *name, *comment, *prefix = NULL;
	while (fgets (buf, 256, f))
	{
		/* Lines are in the form:
		 * export KEY="value" */
		char *key, *value;
		if (strncmp (buf, "export ", strlen("export ")) != 0)
			continue;
		key = buf + strlen("export ");
		value = strchr (key, '=');
		if (!value)
			continue;
		/* Strip the quote marks around the value: */
		value += 2;
		char *end;
		for (end = &(value[strlen(value) - 1]); end >= value; end--) {
			
			if (*end == ' ' || *end == '\n' || *end == '"' || *end == '\r')
				*end = '\0';
			else
				break;
		}
		
		if (strncmp (key, "DISPLAYNAME", strlen("DISPLAYNAME")) == 0) 
			name = strdup (value);
		else if (strncmp (key, "SUMMARY", strlen ("SUMMARY")) == 0)
			comment = strdup (value);
		else if (strncmp (key, "PREFIX", strlen ("PREFIX")) == 0)
			prefix = strdup (value);
	}
	/* If we're allowed to write to $PREFIX, then we don't need root to uninstall */
	if (prefix && access (prefix, W_OK | X_OK) == 0)
		root = 0;
	else
		root = 1;
	fclose(f);
	TRACE ("Package environment file successfully parsed (name='%s', description='%s')\n", name, comment);
	callback(createAppInfo(autopackageUninstall, name, comment, strdup ("autopackage-installer.png"), root,
			       strdup(shortname), size, strdup (timestamp), APP_TYPE_SYSTEM));
	
}

void readFileList(const char* path, const char* shortname, int size, char* timestamp, void (*callback)(struct ApplicationInfo*))
{
	char* fileListPath = ap_strconcat (path, "/files");

	FILE* fileListHandle = fopen(fileListPath, "r");

        if (!fileListHandle)
        {
                printf("autopackage-manager: warning: %s is not a valid path, corrupt database?\n", fileListPath);
                free(fileListPath);
                return;
        }
        
	char* fileList = NULL;
	int len = 0;
	while (!feof(fileListHandle))
	{
		char buffer[256];
		int n = fread(buffer, 1, 256, fileListHandle);
		fileList = (char*)realloc(fileList, len + n);
		memcpy(fileList + len, &buffer, n);
		len += n;
	}
	fclose(fileListHandle);
	free(fileListPath);

	char* debianUser = ap_strconcat (getenv("HOME"), "/.menu");
	
	char* line;
	int lastMarker = 0;
	int i;
	int isParsed = 0;
	for (i=0 ; i<len ; i++)
	{
		if ((fileList[i] == '\n') || (i == (len-1)))
		{
			line = (char*)malloc(i - lastMarker + 1);
			memcpy(line, fileList + lastMarker, i - lastMarker);
			line[i - lastMarker] = '\0';
			lastMarker = i + 1;
			if ((strstr(line, ".desktop") != NULL) && ((strstr(line, "applnk") != NULL) || (strstr(line, "applications") != NULL)))
			{
				if (parseDesktop(line, shortname, size, timestamp, callback) == 0) {
					isParsed = 1;
					break;
				}
				else
					TRACE("Failed to parse desktop file\n");
			}
			if (strstr(line, "/usr/lib/menu") != 0) // We have a (global) debian menu
			{
				if (parseDebianFile(line, 1, shortname, size, timestamp, callback) == 0) {
					isParsed = 1;
					break;
				}
				else
					TRACE("Failed to parse debian file (system dir)\n");
			}
			if (strstr(line, debianUser) != 0) // We have a (user) debian menu
			{
				if (parseDebianFile(line, 0, shortname, size, timestamp, callback) == 0) {
					isParsed = 1;
					break;
				}
				else
					TRACE("Failed to parse debian file (user dir)\n");
			}
		}
	}
	if (!isParsed)
		parsePackageEnvironment(path, shortname, size, timestamp, callback);
	free(fileList);
	free (debianUser);
}

void autopackageSearch(void (*callback)(struct ApplicationInfo*))
{
	char command[1024];
	char *pkgcmd = find_package_command();
	snprintf(command, 1024, "AUTOPACKAGE_MANAGER_FORMAT=true __no_fd_close=1 %s list", pkgcmd);
	free(pkgcmd);
	/* This one-liner lists the installed packages in the format:
	   path|size|timestamp|shortname */
	char* packages = runCommand(command);
	int len = strlen(packages);
	int lastMarker = 0;
	char* path = NULL;
	char* size = NULL;
	char* timestamp = NULL;
	char* shortname = NULL;
	int i;

	if (getenv("AUTOPACKAGE_MANAGER_BACKEND_DEBUG") && *getenv("AUTOPACKAGE_MANAGER_BACKEND_DEBUG"))
		logging = 1;
	
	for (i=0 ; i<=len ; i++)
	{
		if ((packages[i] == '|') && (path == 0))
		{
			path = (char*) malloc(i - lastMarker + 1);
			memcpy(path, packages + lastMarker, i - lastMarker);
			path[i - lastMarker] = '\0';
			lastMarker = i + 1;
		}
		else if ((packages[i] == '|') && (size == 0))
		{
			size = (char*) malloc(i - lastMarker + 1);
			memcpy(size, packages + lastMarker, i - lastMarker);
			size[i - lastMarker] = '\0';
			lastMarker = i + 1;
		}
		else if ((packages[i] == '|') && (timestamp == 0))
		{
			timestamp = (char*) malloc(i - lastMarker + 1);
			memcpy(timestamp, packages + lastMarker, i - lastMarker);
			timestamp[i - lastMarker] = '\0';
			lastMarker = i + 1;
		}
		else if ((packages[i] == '\n') && (shortname == NULL))
		{
			shortname = (char*) malloc(i - lastMarker + 1);
			memcpy(shortname, packages + lastMarker, i - lastMarker);
			shortname[i - lastMarker] = '\0';
			lastMarker = i + 1;
			if (strcmp(shortname, "autopackage-gtk") == 0 && strlen(shortname) == strlen("autopackage-gtk"))
				/* Special case this as a "system application"; no nead to read its filelist */
				parsePackageEnvironment(path, shortname, atoi(size), timestamp, callback);
			else
				readFileList(path, shortname, atoi(size), timestamp, callback);
			
			free(path); path = NULL;
			free(shortname); shortname = NULL;
			free(size); size = NULL;
			free(timestamp); timestamp = NULL;
		}
	}
	free(packages);
}

void autopackageCleanup()
{

}

#endif

