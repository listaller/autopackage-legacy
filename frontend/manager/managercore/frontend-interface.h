/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FRONTEND_INTERFACE_H
#define FRONTEND_INTERFACE_H

#ifdef __cplusplus
extern "C"
{
#endif

typedef enum {
	APP_TYPE_DESKTOP,
	APP_TYPE_SYSTEM
} AutoPackageAppType;
	
/* This struct is returned in a callback from findApplications.  It contains
   all you need to know about an application. */
typedef struct ApplicationInfo
{
	/* The application's friendly name - read from the .desktop file */
	char* name;

	/* The application's comment - typically a description of the app */
	char* comment;
	
	/* The application's icon - read from the .desktop file
	   You need to find the full path to this icon yourself */
	char* icon;
	
	/* Set to 1 if the application needs to be removed by root */
	int root;
	
	/* The name of the package this application belongs to */
	char* packageName;

	/* The type of the application - desktop app or system app */
	AutoPackageAppType type;
	
	/* An internal thing.  Don't call this. */
	void (*callback)(char*, void (*)(int));
	
	/* The size of the package, in bytes */
	int size;
	
	/* The time/date the package was installed (UTC)
	   Format:  yyyy-mm-ddThh:mm:ss+0000
	   Example: 2004-08-17T22:36:47+0100 */
	char* timestamp;
} ApplicationInfo;


/* Call this when your frontend is starting up.  It will find all available
   desktop files using all the backends it knows.
   The function returns immediately.

   When a desktop file is found, the callback function is run.
   Example callback function prototype:
   void myCallback(struct ApplicationInfo* info);
   
   When the search is done, the callback will be run with info = NULL */
void findApplications(void (*callback)(struct ApplicationInfo*));


/* Call this to remove a package
   The function returns immediately.

   info:        the ApplicationInfo struct given to you in the callback
                from findApplications
   callback:    called when the removal is complete.  the argument to
                this callback is an error code. */
void uninstallPackage(struct ApplicationInfo* info, void (*callback)(int));


/* Frees all data used internally.
   Call this before your application closes */
void cleanup();

#ifdef __cplusplus
}
#endif

#endif
