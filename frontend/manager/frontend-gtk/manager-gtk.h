#ifndef _MANAGER_GTK_H_
#define _MANAGER_GTK_H_

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <libintl.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "../managercore/frontend-interface.h"

/* Forget what you kids learned at college - global variables rocks! </sarcasm> */
extern int logging;
extern GladeXML *xml;
extern ApplicationInfo *selected;
extern guint pulse_progress_id;
/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#endif

#define W(x) glade_xml_get_widget (xml, x)
#define TRACE(s...) if (logging) { printf("autopackage-manager-gtk: "); printf(s); }

/* To handle error codes from uninstall */
enum ErrorSource {
	ErrSrcPackage	= 0x00010000,
	ErrSrcAutosu	= 0x00020000,
	ErrSrcThis	= 0x00030000,
	ErrSrcMask	= 0xFFFF0000
};

enum UninstallError {
	AutosuNotFound = 1,
	AutosuNotExecutable = 2,
	GeneralError = 3,
	
};
enum
{
    COL_PIXBUF,
    COL_NAME,
    COL_INFO,
    COL_DETAILS,
    NUM_COLS
};

void pkglst_selection_changed_cb (GtkTreeSelection *selection,  gpointer data);
gboolean pulse_progress (GtkWidget *progressbar);
void add_to_list (ApplicationInfo *info);
void clear_error(GError **error);

#endif /* _MANAGER_GTK_H_ */
