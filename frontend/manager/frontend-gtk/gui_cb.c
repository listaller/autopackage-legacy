#include "../managercore/frontend-interface.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "manager-gtk.h"

static gboolean uninstall_completed_callback(gpointer _errorcode);
static void enable_disable_gui(gboolean enabled);
static void show_uninstall_error_message (gint errorcode);

/* Read data printed by autosu */
static gboolean iochannel_data_available_cb (GIOChannel *src, GIOCondition cond, gpointer data)
{
	if (cond & (G_IO_IN | G_IO_PRI))
	{
		g_print ("Condition: 0x%04X (_IN or _PRI)\n", cond);
		GIOStatus status;
		gchar buf[255];
		GError *err = NULL;
		gsize bytes_read = 0;
		do
		{
			status = g_io_channel_read_chars (src, buf, sizeof(buf), &bytes_read, &err);
			switch (status)
			{
				case G_IO_STATUS_EOF:
				case G_IO_STATUS_NORMAL:
					if (g_ascii_isdigit(*buf))
					{
						// Assume this is the exit code of the child
						gint exitcode = atoi(buf);
						uninstall_completed_callback(GINT_TO_POINTER(WEXITSTATUS(ErrSrcPackage | exitcode)));
						return FALSE;
					}
					break;
					
				case G_IO_STATUS_ERROR:
					g_error ("Got G_IO_STATUS_ERROR while reading from autosu-gtk: %s", err->message);
					clear_error (&err);
					g_io_channel_shutdown(src, FALSE, NULL);
					uninstall_completed_callback(GINT_TO_POINTER(ErrSrcThis | GeneralError));
					return FALSE;
			}		
		} while (status != G_IO_STATUS_AGAIN);
	}
	else if (cond & G_IO_HUP)
	{
		/* This is OK. It just means autosu has terminated */
		g_io_channel_shutdown (src, FALSE, NULL);
		TRACE ("Got Hangup on iowatch. Shutting down iochannel\n", cond);
		return FALSE;
	}
	else
	{
		/* Woaa.. something funky happen. Better shut down and bail out! */
		g_print ("Unhandled condition: 0x%04X... aborting watch\n", cond);
		uninstall_completed_callback(GINT_TO_POINTER(ErrSrcThis | GeneralError));
		return FALSE;
	}
	return TRUE;
}

/* Periodically poll to see if autosu has exited yet. (We can't use g_child_add_watch() since its GLib 2.4) */
static gboolean autosu_child_exited_cb (gpointer data)
{
	GPid pid = GPOINTER_TO_INT(data);
	int status, rv;
	if ((rv = waitpid (pid, &status, WNOHANG)) > 0)
	{
		TRACE("Autosu has exited\n");
		if (!WIFEXITED(status))
		{
			g_warning ("Autosu did not terminate normally. Status=0x%X", status);
			uninstall_completed_callback (GINT_TO_POINTER ( ErrSrcAutosu | 666 ) );
		}
		else
		{
			uninstall_completed_callback (GINT_TO_POINTER ( ErrSrcAutosu | WEXITSTATUS(status)) );
		}
		return FALSE;
	}
	else if (rv < 0)
	{
		g_warning ("Failed to call waitpid on autosu (PID=%d): %s", pid, g_strerror(errno));
		return FALSE;
	}
	return TRUE;
}

/* autosu will print the exitcode of its child to fd 5 so lets redirect it to
 * stdout so that we can easily capture it */
static void autosu_child_setup_cb (gpointer data)
{
	if (dup2(1,5) == -1)
	{
		perror("dup2(1,5) failed for child setup function: ");
	}
}

gchar *find_autosu(void)
{
	gchar *home = g_strdup_printf("%s/.local/libexec/autopackage/autosu", g_get_home_dir());
	gchar *autosu = NULL;
	if (g_file_test("/usr/libexec/autopackage/autosu-gtk", G_FILE_TEST_IS_REGULAR))
		autosu = g_strdup("/usr/libexec/autopackage/autosu-gtk");
	else if (g_file_test(home, G_FILE_TEST_IS_REGULAR))
		autosu = g_strdup(home);
	g_free (home);

	return autosu;
}

static void uninstall_package_as_root (void)
{
	gchar *autosu = find_autosu();
	gboolean success;
	TRACE("Using autosu (%s) to run 'package remove'\n", autosu);
	if (!autosu)
	{
		uninstall_completed_callback(GINT_TO_POINTER(ErrSrcThis | AutosuNotFound));
		return;
	}
	
	GError *err = NULL;
	gint child_exitstatus;
	gchar *argv[8];
	argv[0] = autosu;
	argv[1] = "--root-only";  /* Don't show 'No Password' button */
	argv[2] = "-m";	      /* Adjust title+description accordingly */
	argv[3] = "manage";
	argv[4] = "package";
	argv[5] = "remove";
	argv[6] = selected->packageName;
	argv[7] = NULL;
	gint child_stdout;
	GPid child_pid;
	
	success = g_spawn_async_with_pipes(NULL, argv, NULL, G_SPAWN_CHILD_INHERITS_STDIN | G_SPAWN_DO_NOT_REAP_CHILD,
					   autosu_child_setup_cb, NULL, &child_pid,
					   NULL /* stdin */, &child_stdout, NULL /* stderr */,
					   &err);
	if (!success)
     	{
		uninstall_completed_callback(GINT_TO_POINTER( ErrSrcThis | AutosuNotExecutable ));
		g_warning ("Failed to launch autosu: %s", err->message);
		clear_error (&err);
		return;
	}
	TRACE("Autosu successfully spawned (pid %u)\n", child_pid);
	/* In GLib 2.4 there's a nice child watcher integrated into the mainloop, but since we
	 * we want to be 2.0 compatible, we have to poll with waitpid() instead :-( */
	g_timeout_add (200, autosu_child_exited_cb, GINT_TO_POINTER(child_pid));
	
	GIOChannel *ioc = g_io_channel_unix_new (child_stdout);
	// Let it run and we'll get called by glib as soon as there's data for us to read (or something else happens)
	g_io_add_watch (ioc, G_IO_IN | G_IO_HUP | G_IO_PRI | G_IO_ERR , iochannel_data_available_cb, NULL);
}

// TODO: Parse the error code and show message to user
static gboolean uninstall_completed_callback(gpointer _errorcode)
{
    int errorcode = GPOINTER_TO_INT(_errorcode);
    GtkWidget *view = W("packagelist");
    GtkListStore *store = GTK_LIST_STORE ( gtk_tree_view_get_model(GTK_TREE_VIEW(view)) );
    TRACE("uninstall completed, errorcode=0x%08x (%d) (value=%d)\n", errorcode, errorcode, errorcode & ~ErrSrcMask);

    if ((errorcode & ~ErrSrcMask) == 0)
    {
        GtkTreeSelection *sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
        GtkTreeIter iter;
        GtkTreeModel *model;

        gtk_tree_selection_get_selected(sel, &model, &iter);
        GdkPixbuf *pixbuf;
        gtk_tree_model_get(model, &iter, COL_PIXBUF, &pixbuf, -1);
        if (pixbuf) gdk_pixbuf_unref(pixbuf);
        gtk_list_store_remove(store, &iter);

        /* remove from internal list */
	GList *pkg_list = g_object_get_data (G_OBJECT(W("packagelist")), "pkglist");
	g_object_set_data (G_OBJECT(W("packagelist")), "pkglist", g_list_remove(pkg_list, selected));

        /* select the first item */
        GtkTreePath *path = gtk_tree_path_new_from_string("0");
        gtk_tree_view_set_cursor(GTK_TREE_VIEW(view), path, NULL, FALSE);
        gtk_tree_path_free(path);
    }
    else
	    show_uninstall_error_message(errorcode);
    
    gtk_label_set_text(GTK_LABEL(W("status")),
	(const gchar *) _("Select the application you want to uninstall and click \"Remove\"."));
    
    enable_disable_gui(TRUE);
    gtk_widget_grab_focus(view);
    gtk_timeout_remove(pulse_progress_id);
    gtk_widget_hide(W("removing"));
    gtk_widget_show(W("appfilter"));
    return FALSE;
}

/* Marchall the call to the main thread through an idle function
 * (g_idle_add is thread safe) */
static void uninstall_completed_callback_marshaller (int errorcode)
{
    g_idle_add (uninstall_completed_callback, GINT_TO_POINTER(ErrSrcPackage | errorcode));
}

/* Called when the 'Remove' buttons is clicked */
void remove_clicked_cb(GtkWidget *button, gpointer data)
{
	g_assert( selected );
	TRACE("remove clicked for %s\n", selected->name);
	
	GtkLabel *status = GTK_LABEL(W("status"));
	
	char *text = g_strdup_printf((const gchar *) _("Removing %s..."), selected->name);
	gtk_label_set_text(status, text);
	
	enable_disable_gui(FALSE);
	
	gtk_widget_show(W("removing"));
	gtk_widget_hide(W("appfilter"));
	// clearlooks can send us into an infinite loop with this line so hide it instead
	pulse_progress_id = gtk_timeout_add(50, (GSourceFunc) pulse_progress, W("removing"));
	
	
	if (selected->root && geteuid() != 0)
	{
		TRACE("Need elevated privileges to remove selected package!\n");
		uninstall_package_as_root();
	}
	else
		uninstallPackage(selected, uninstall_completed_callback_marshaller);
}

/* Called by GTK+ when the app list filter combo is changed */
void filter_changed_cb (GtkOptionMenu *optionmenu, gpointer data)
{
	GList *node = g_object_get_data (G_OBJECT(W("packagelist")), "pkglist");
	AutoPackageAppType filter = gtk_option_menu_get_history (GTK_OPTION_MENU (W("appfilter")));
	TRACE ("Filter changed: %s\n", filter == APP_TYPE_DESKTOP ? "APP_TYPE_DESKTOP" : "APP_TYPE_SYSTEM");

	gtk_list_store_clear (GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (W("packagelist")))));

	while (node) {
		ApplicationInfo *i = node->data;
		switch (filter) {
			case APP_TYPE_DESKTOP:
				if (i->type == APP_TYPE_DESKTOP)
					add_to_list(i);
				break;
				
			case APP_TYPE_SYSTEM:
				add_to_list(i);
				break;
		}
	
		node = node->next;
	}
}

// Called every time the selected app changes
static void update_selection(ApplicationInfo *info)
{
	if (info == NULL)
	{
		TRACE("user has nothing selected\n ");
		gtk_widget_hide (W("rootwarning"));
		gtk_widget_set_sensitive(W("remove"), FALSE);
	}
	else
	{
		TRACE("user selected %s, %s\n", info->name, info->root ? "root install" : "user install");
		if (info->root && geteuid() != 0)
			gtk_widget_show(W("rootwarning"));
		else
			gtk_widget_hide(W("rootwarning"));
		
		gtk_widget_set_sensitive(W("remove"), TRUE);
	}
	selected = info;
}

/* Called by GTK every time something new is selected (or unselected) in the list of packages */
void pkglst_selection_changed_cb (GtkTreeSelection *selection,  gpointer data)
{
    GtkTreeIter iter;
    GtkTreeModel *model;

    if (gtk_tree_selection_get_selected(selection, &model, &iter))
    {
	    ApplicationInfo *info;
	    
	    gtk_tree_model_get(model, &iter, COL_INFO, &info, -1);
	    update_selection(info);
    }
    else
	    update_selection(NULL);
}

static void enable_disable_gui(gboolean enabled)
{
    gtk_widget_set_sensitive(W("packagelist"), enabled);
    gtk_widget_set_sensitive(W("remove"), enabled);
    gtk_widget_set_sensitive(W("close"), enabled);
    gtk_widget_set_sensitive(W("appfilter"), enabled);
}

static void show_uninstall_error_message (gint errorcode)
{
	guint src = errorcode & ErrSrcMask;
	guint val = errorcode & ~ErrSrcMask;
	g_return_if_fail (val != 0);
	GString *s = g_string_new ("");
	g_string_append_printf (s, "<span weight=\"bold\" size=\"larger\">%s</span>\n\n%s",
				_("Uninstallation Failed"), _("There was an error trying to uninstall the selected package. "));
	switch (src)
	{
		case ErrSrcPackage:
			switch (val)
			{
				case 6:
					g_string_append (s, _("Removing this package would break one or more other packages."));
					break;
				default:
					/* Nothing fancy to report at this point (TODO: Find out what other exit codes means. */
					break;
			}
			break;
			
		case ErrSrcAutosu:
			switch (val)
			{
				case 11:
					/* User clicked cancel */
					g_string_free (s, TRUE);
					return;
				case 12:
					/* User clicked 'No Password'. Should not happen since we don't have that button enabled */
					g_assert_not_reached();
					break;
				case 14:
					/* User typed incorrect password 3 times */
					g_string_append (s, _("You entered the wrong password too many times"));
					break;
				case 13:
					/* Fall through */
				case 15:
					/* su exited abnormally or a system error ocurred */
					g_string_append(s, _("Failed to acquire administrator permissions."));
					break;
				default:
					/* Nothing further to report */
					break;
			}
			break;
			
		case ErrSrcThis:
			switch (val)
			{
				case AutosuNotFound:
					g_string_append(s, _("The program used to switch to root (autosu-gtk) could not be found. "
							     "Start 'autopackage-manager-gtk' as root directly to uninstall this application."));
					break;
				case AutosuNotExecutable:
					g_string_append(s, _("The program used to switch to root (autosu-gtk) could not be launched. "
							     "Make sure autopackage is correctly installed."));
					break;
				case GeneralError:
				default:
					/* Nothing good to report, so let's just shut up */
					break;
			}
			break;
	}
	GtkWidget *dlg = gtk_message_dialog_new_with_markup (NULL, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							     GTK_MESSAGE_ERROR,
							     GTK_BUTTONS_OK,
							     s->str);
	gtk_dialog_run(GTK_DIALOG(dlg));
	gtk_widget_destroy(dlg);
	g_string_free (s, TRUE);
}
