/***************************************************************************
 *   Copyright (C) 2004-2005 by Mike Hearn <mike@plan99.net>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* TODO List:
 *
 * - Merge it with the GTKFE
 * - Assumes either /usr or ~/.local install but autopackage is in theory itself relocatable
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>

#include <gtk/gtk.h>
#include <gmodule.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define ENABLE_BINRELOC

#include "prefix.h"
#include "manager-gtk.h"



int logging = 0;
GladeXML *xml;
gchar *selfpath = NULL;
gchar *prefix = NULL;
ApplicationInfo *selected;

guint pulse_progress_id = 0;

/* This determines what apps to show in the list (see frontend-interface.h for details) */
//AutoPackageAppType filter = APP_TYPE_DESKTOP;

/* To simplify pointer comparisons */
#define gpointer_diff(a,b) (gint) (GPOINTER_TO_INT(a) - GPOINTER_TO_INT(b))

/* string_to_utf8: This function will do its best trying to convert the
 *		   input string to UTF-8. It will convert as many bytes as
 *		   possible, appending partial conversion with the
 *		   string 'Invalid Unicode'
 *		   If 'str' is null, the empty string ("") is returned!
 *
 * Returns: the converted string. This must always be free()'d
 *	    since this function never returns NULL.
 */
gchar *string_to_utf8 (const gchar *str)
{
	gchar *retval, *tmp;
	GError *err = NULL;
	gsize bytes_read, bytes_written;
	gboolean ends_with_newline = FALSE;
	gint valid_utf8bytes = 0;
	
	g_return_val_if_fail (str, g_strdup(""));

	if (str[strlen (str) - 1] == '\n')
		ends_with_newline = TRUE;
	if (g_utf8_validate (str, -1, (const gchar**) &tmp))
		return g_strdup (str);

	valid_utf8bytes = gpointer_diff (tmp,str) - 1;
	
	retval = g_locale_to_utf8 (str, -1, &bytes_read, &bytes_written, &err);
	if (err)
	{
		g_free (retval);
		clear_error(&err);
		if (bytes_read > 0)
			valid_utf8bytes = bytes_read;
		
		tmp = g_locale_to_utf8 (str, valid_utf8bytes, NULL, NULL, &err);
		if (G_UNLIKELY(err)) {
			/* This should not even happen */
			TRACE ("Couldn't even convert %d first bytes to UTF-8: %s", valid_utf8bytes, err->message);
			g_error_free (err);
			return g_strdup ("");
		}
		/* Failed character encoding conversion, return partial string */
		retval = g_strdup_printf ("%s (%s)%c", tmp, _("Invalid Unicode"), ends_with_newline ? '\n' : '\0');
		g_free (tmp);
	}

	return retval;
}

static char *get_data_file(const char *name)
{
    char *file;

    file = g_build_filename (prefix, "share/autopackage-manager", name, NULL);
    if (!g_file_test (file, G_FILE_TEST_IS_REGULAR))
    {
        char *dir = g_path_get_dirname(selfpath);
        g_free (file);
        file = g_strdup_printf("%s/%s", dir, name);
        g_free (dir);
    }
    return file;
}


void clear_error(GError **error)
{
    if (*error)
    {
        g_error_free(*error);
        *error = NULL;
    }
}


static GdkPixbuf *lookup_icon(char *name)
{
    GError *error = NULL;
    GModule *mod = NULL;
    GdkPixbuf *pixbuf = NULL;
    char *tmpname = g_strdup(name);
    char *ptr = tmpname + strlen(tmpname);

    TRACE("name is %s\n", tmpname);

    if (g_path_is_absolute(tmpname))
    {
        pixbuf = gdk_pixbuf_new_from_file(tmpname, &error);
    }
    else
    {
        
        /* chop off the extension, if any */
        while (ptr > tmpname)
        {
            if (*ptr == '.')
            {
                *ptr = '\0';
                break;
            }
            ptr--;
        }
    }
    
    TRACE("looking up %s\n", tmpname);

    /* Use GtkIconTheme when available */
    if (!pixbuf)
        mod = g_module_open(NULL, 0);
    if (mod)
    {
        gpointer (*get_icon_theme) (void) = NULL;
        gboolean (*has_icon) (gpointer icon_theme, const gchar *icon_name) = NULL;
        GdkPixbuf* (*load_icon) (gpointer icon_theme,
                                 const gchar *icon_name,
                                 gint size,
                                 gint flags,
                                 GError **error);

        if (g_module_symbol (mod, "gtk_icon_theme_get_default", (gpointer *) &get_icon_theme)
         && g_module_symbol (mod, "gtk_icon_theme_has_icon", (gpointer *) &has_icon)
         && g_module_symbol (mod, "gtk_icon_theme_load_icon", (gpointer *) &load_icon))
        {
            gpointer icon_theme = get_icon_theme();
            if (has_icon(icon_theme, tmpname))
            {
		TRACE("Icon=%s (1)\n", tmpname);
                clear_error(&error);
                pixbuf = load_icon(icon_theme,
                                   tmpname,
                                   48,
                                   0,
                                   &error);
            } else
                TRACE("gtk_icon_theme_has_icon() failed\n");
        } else
            TRACE("No gtk_icon_theme\n");
        g_module_close(mod);
    }

    if (!pixbuf)
    {
        const char *home = g_get_home_dir();
        char **search = g_new0(char *, 4);
        char *dir;
        int i = 0;

        search[0] = g_strdup_printf("%s/.kde/share/icons", home);
        search[1] = g_strdup_printf("%s/.icons/gnome/48x48/apps", home);
        search[2] = g_strdup_printf("/usr/share/icons");
        search[3] = g_strdup_printf("/usr/share/pixmaps");

        while ((dir = search[i]))
        {
            char *file = g_strdup_printf("%s/%s.png", search[i], tmpname);
            if (g_file_test(file, G_FILE_TEST_IS_REGULAR))
            {
		TRACE("Icon=%s (2)\n", file);
                clear_error(&error);
                pixbuf = gdk_pixbuf_new_from_file(file, &error);
                g_free(file);
                break;
            }
            g_free(file);
            i++;
        }
        g_strfreev(search);
    }

    if (!pixbuf)
    {
        char *file = get_data_file("kpackage48.png");
        clear_error(&error);
        pixbuf = gdk_pixbuf_new_from_file(file, &error);
        g_free(file);
	TRACE("Icon=default icon (3)\n");
    }

    if (pixbuf && (gdk_pixbuf_get_width(pixbuf) > 48 || gdk_pixbuf_get_height(pixbuf) > 48))
    {
        GdkPixbuf *tmp = gdk_pixbuf_scale_simple(pixbuf,
              48,
              48 * (gdk_pixbuf_get_width(pixbuf) / gdk_pixbuf_get_height(pixbuf)),
              GDK_INTERP_BILINEAR);
        g_object_unref(G_OBJECT(pixbuf));
        pixbuf = tmp;
    }

    free(tmpname);
    
    if (!pixbuf)
    {
        if (error)
        {
            g_warning("couldn't load icon: %s", error->message);
            g_error_free(error);
        } else
            g_warning("couldn't load icon\n");
        return NULL;
    }

    return pixbuf;
}

static gchar *nice_date(char *timestamp)
{
	gchar **components, **date, **stime;
	GDate *gd;
	struct tm tm;
	time_t packageTime, now;

	components = g_strsplit(timestamp, "T", 2);
	date = g_strsplit(components[0], "-", 3);
	stime = g_strsplit(components[1], ":", 3);
	if (strlen(stime[2]) > 2)
		stime[2][2] = '\0';

	gd = g_date_new();
	g_date_set_dmy(gd, atoi(date[2]), atoi(date[1]), atoi(date[0]));
	g_date_to_struct_tm(gd, &tm);
	tm.tm_hour = atoi(stime[0]);
	tm.tm_min = atoi(stime[1]);
	tm.tm_sec = atoi(stime[2]);
	packageTime = mktime(&tm);
	now = time(NULL);
	g_date_free(gd);
	g_strfreev(date);
	g_strfreev(stime);
	g_strfreev(components);

	time_t diff = now - packageTime;
	
	if (diff < 0)
		/* Translators: This is used as a time when package was installed */
		return g_strdup(_("The Future"));

	else if (diff < 60) 
		return g_strdup_printf (ngettext ("%d second ago", "%d seconds ago", diff), diff);
	else if (diff < 60 * 60) {
		gint val = diff/60;
		return g_strdup_printf(ngettext ("%d minute ago", "%d minutes ago", val), val);
	} else if (diff < 60 * 60 * 24) {
		gint val = diff / (60*60);
		return g_strdup_printf(ngettext ("%d hour ago", "%d hours ago", val), val);
	} else {
		/* FIXME: Add cases for months too */
		gint val = diff / (60*60*24);
		return g_strdup_printf (ngettext ("%d day ago", "%d days ago", val), val);
	}
}

void add_to_list (ApplicationInfo *info)
{
	GdkPixbuf *pixbuf = lookup_icon(info->icon);
	gchar *details, *size, *date;
	gchar *namestr;
	GtkListStore *store = GTK_LIST_STORE ( gtk_tree_view_get_model(GTK_TREE_VIEW(W("packagelist"))) );
	
	/* FIXME: These should use MiB and KiB instead */
	if (info->size < 1024)
		size = g_strdup_printf(ngettext ("%d byte", "%d bytes", info->size), info->size);
	else if (info->size < 1024 * 1024)
		size = g_strdup_printf(_("%.2f KB"), info->size / 1024.0);
	else if (info->size < 1024 * 1024 * 1024)
		size = g_strdup_printf(_("%.2f MB"), info->size / 1024.0 / 1024.0);
	
	date = nice_date(info->timestamp);
	details = g_strdup_printf(_("Size: %s\nInstalled: %s"), size, date);
	g_free(date);
	g_free(size);
	gchar *name = string_to_utf8 (info->name);
	gchar *comment = NULL;
	if (info->comment)
		comment = string_to_utf8 (info->comment);
	else
		comment = g_strdup (_("No description available"));
	if (*name == '\0') {
		g_free(name);
		/* We need to show *something* at least */
		name = g_strdup(_("Unknown Application"));
	}
	
	namestr = g_strdup_printf ("<b>%s</b>\n<span size=\"smaller\">%s</span>", name, comment);
	
	GtkTreeIter iter;
	gtk_list_store_append(store, &iter);
	gtk_list_store_set(store, &iter,
			   COL_NAME, namestr,
			   COL_INFO, info,
			   COL_PIXBUF, pixbuf,
			   COL_DETAILS, details,
			   -1);
	g_free (namestr);
	if (!selected)
	{
		/* select the first item */
		GtkWidget *view = W("packagelist");
		GtkTreePath *path = gtk_tree_path_new_from_string("0");
		gtk_tree_view_set_cursor(GTK_TREE_VIEW(view), path, NULL, FALSE);
		gtk_tree_path_free(path);
		gtk_widget_grab_focus(view);
	}
}

static gboolean application_found(gpointer ptr)
{
    ApplicationInfo *info = (ApplicationInfo *) ptr;
    AutoPackageAppType filter = gtk_option_menu_get_history (GTK_OPTION_MENU (W("appfilter")));

    TRACE("application_found: %p\n", ptr);
   
    if (!info)
    {
        TRACE("search completed\n");
        GtkLabel *label = GTK_LABEL(W("status"));
        gtk_label_set_text(label,
		(const gchar *) _("Select the application you want to uninstall and click \"Remove\"."));
	gtk_widget_show (W("appfilter"));
	gtk_notebook_set_current_page(GTK_NOTEBOOK(W("notebook1")), 1);
        return FALSE;
    }
    GList *pkg_list = g_object_get_data (G_OBJECT(W("packagelist")), "pkglist");
    g_object_set_data (G_OBJECT(W("packagelist")), "pkglist", g_list_append (pkg_list, info));
    
    if (info->type == APP_TYPE_DESKTOP) {
	    TRACE("Desktop application found: %s\n", info->name);
    } else {
	    TRACE("System application found: %s\n", info->name);
	    if (filter == APP_TYPE_DESKTOP)
		    return FALSE; // ???
    }
	 
    add_to_list (info);
    
    gtk_timeout_remove(pulse_progress_id);
    gtk_widget_set_sensitive(W("remove"), TRUE);

    gtk_main_iteration();
    
    return FALSE;
}

/* Runs in a separate thread, so marshal it back to the main thread
 * (g_idle_add is thread safe) */
static void application_found_callback(ApplicationInfo *info)
{
    TRACE("info=%p\n", info);
    g_idle_add_full(G_PRIORITY_HIGH_IDLE, application_found, info, NULL);
}


gboolean pulse_progress (GtkWidget *progressbar)
{
	gtk_progress_bar_pulse(GTK_PROGRESS_BAR(progressbar));
	return TRUE;
}


static gboolean post_gui_init(gpointer ptr)
{
    TRACE("post gui init\n");

    GtkListStore *store = gtk_list_store_new (NUM_COLS, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_POINTER, G_TYPE_STRING);
    
    GtkCellRenderer *textrenderer = gtk_cell_renderer_text_new();
    GtkCellRenderer *iconrenderer = gtk_cell_renderer_pixbuf_new();
    GtkCellRenderer *detailsrenderer = gtk_cell_renderer_text_new();
    
    GtkTreeView *view = GTK_TREE_VIEW(W("packagelist"));
    g_assert (view);


    /* the expand property is GTK 2.4+ only */
    GObjectClass *tc = g_type_class_ref(GTK_TYPE_TREE_VIEW_COLUMN);
    gboolean expando = (g_object_class_find_property(tc, "expand") != NULL);
    g_type_class_unref(tc);
    
    double align = expando ? 0.0 : 1.0;
    
    g_object_set(G_OBJECT(detailsrenderer),
	"foreground", "DarkGray",
	"scale", PANGO_SCALE_SMALL,
	"xalign", align,
	NULL);

    gtk_tree_view_insert_column_with_attributes(view, -1, _("Icon"),
						iconrenderer, "pixbuf", COL_PIXBUF, NULL);

    if (expando)
    {
        GtkTreeViewColumn* textcolumn = NULL;
        textcolumn = gtk_tree_view_column_new_with_attributes(_("Name"), textrenderer, "markup", COL_NAME, NULL);

        g_object_set(G_OBJECT(textcolumn),
                     "expand", TRUE,
                     NULL);
        gtk_tree_view_insert_column(view, textcolumn, -1);
    }
    else
    {
        gtk_tree_view_insert_column_with_attributes(view, -1, _("Name"), textrenderer, "markup", COL_NAME, NULL);
    }
    gtk_tree_view_insert_column_with_attributes(view, -1, _("Details"),
            detailsrenderer, "text", COL_DETAILS, NULL);
    gtk_tree_view_set_model(view, GTK_TREE_MODEL(store));
    g_object_unref(store);      /* destroy model with view */

    findApplications(&application_found_callback);
    GtkTreeSelection *select = gtk_tree_view_get_selection(view);
    gtk_tree_selection_set_mode(select, GTK_SELECTION_SINGLE);
    g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(pkglst_selection_changed_cb), NULL);

    g_signal_connect(G_OBJECT(W("window1")), "delete_event", G_CALLBACK(gtk_main_quit), NULL);

    return FALSE;
}

static void init_binreloc()
{
	GError *err = NULL;

	if (!gbr_init(&err))
	{
		g_print ("WARNING: Couldn't initialize the binary relocation support (binreloc): %s\n", err->message);
		g_print ("WARNING: Falling back to hard-coded default prefix (/usr)\n");
	}
	selfpath = gbr_find_exe("/usr/bin/autopackage-manager-gtk");
	prefix = gbr_find_prefix("/usr");
}

int main(int argc, char *argv[])
{
    char *xml_file;
    
    if (getenv("AUTOPACKAGE_MANAGER_GTK_DEBUG") && *getenv("AUTOPACKAGE_MANAGER_GTK_DEBUG"))
        logging = 1;

    TRACE("starting up\n");
    init_binreloc();
    if (!getenv("DISPLAY") || *getenv("DISPLAY") == '\0')
	    g_print("WARNING: DISPLAY is unset. This will probably not work!\n");
    else
	    TRACE("Will connect to DISPLAY=%s\n", getenv("DISPLAY"));
    g_thread_init(NULL);
    
#ifdef ENABLE_NLS
    {
	    gchar *localedir = g_build_filename(prefix, "share", "locale", NULL);
	    TRACE("Loading locale files from '%s'\n", localedir);
	    bindtextdomain (GETTEXT_PACKAGE, localedir);
	    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	    textdomain (GETTEXT_PACKAGE);
	    g_free (localedir);
    }
#endif
    
    if (!gtk_init_check(&argc, &argv))
    {
	    g_print("ERROR: Failed to initialize GTK+. Check that your X-server is running and accepting connections from this user (%s)\n", g_get_user_name());
	    return -1;
    }
    
    /* set up the GUI */
    xml_file = g_build_filename(prefix, "share/autopackage-manager/manager-gtk.glade", NULL);
    TRACE("Loading GUI from file %s...\n", xml_file);
    if (!g_file_test (xml_file, G_FILE_TEST_IS_REGULAR))
    {
	    char *dir = g_path_get_dirname(selfpath);
	    xml_file = g_strdup_printf("%s/manager-gtk.glade", dir);
	    g_free (dir);
	    TRACE("Standard GUI file not found, trying '%s' instead\n", xml_file);
    }
    
    xml = glade_xml_new(xml_file, NULL, NULL);
    g_return_val_if_fail(xml != NULL, -1);

    g_free (xml_file);
    xml_file = NULL;

    GdkColor *white = &gtk_widget_get_style(W("topbox"))->white;
    gtk_widget_modify_bg(W("topbox"), GTK_STATE_NORMAL, white);

    char *icon = get_data_file("kpackage48.png");
    gtk_window_set_icon_from_file(GTK_WINDOW(W("window1")), icon, NULL);
    g_free(icon);

    glade_xml_signal_autoconnect(xml);

    gtk_widget_hide(W("rootwarning"));
    
    /* the 1 here skips the initial pulsing progress bar */
    gtk_notebook_set_current_page(GTK_NOTEBOOK(W("notebook1")), 0);

    if (argv[1] && strcmp(argv[1], "--rootmode") == 0)
        gtk_widget_show(W("rootMode"));

    gtk_widget_show(W("searching"));
    g_idle_add(post_gui_init, NULL);
    pulse_progress_id = gtk_timeout_add(20, (GSourceFunc) pulse_progress, W("searching"));
    gtk_progress_bar_set_pulse_step(GTK_PROGRESS_BAR(W("searching")), 0.01);
    
    TRACE("entering main loop\n");
    gtk_main();
    
    TRACE("shutting down\n");
    cleanup();
    g_free (prefix);
    g_free (selfpath);

    return 0;
}

