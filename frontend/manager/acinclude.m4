# Check for Qt compiler flags, linker flags, and binary packages
AC_DEFUN([gw_CHECK_QT],
[
AC_REQUIRE([AC_PROG_CXX])
AC_REQUIRE([AC_PATH_X])

AC_MSG_CHECKING([Qt])
AC_ARG_WITH([qtdir], [  --with-qtdir=DIR        Qt installation directory [default=$QTDIR]], QTDIR=$withval)
# Check that QTDIR is defined or that --with-qtdir given
QT_SEARCH="$QTDIR /usr/lib/qt31 /usr/local/qt31 /usr/lib/qt3 /usr/local/qt3 /usr/lib/qt2 /usr/local/qt2 /usr/lib/qt /usr/local/qt"
QTDIR=""
for i in $QT_SEARCH; do
    if test -f $i/include/qglobal.h -a x$QTDIR = x; then QTDIR=$i; fi
done


if test x"$QTDIR" = x ; then
    if test -f /usr/include/qt3/qglobal.h ; then
        QTDIR=/usr
        QTINCDIR=/usr/include/qt3
        QTLIBDIR=/usr/lib
    fi
else
    QTINCDIR=$QTDIR/include
    QTLIBDIR=$QTDIR/lib
fi

HAS_QT=0
if test x"$QTINCDIR" = x ; then
    AC_MSG_RESULT([Not found])
else
    AC_MSG_RESULT([$QTINCDIR $QTLIBDIR])
    
    # Change backslashes in QTINCDIR to forward slashes to prevent escaping
    # problems later on in the build process, mainly for Cygwin build
    # environment using MSVC as the compiler
    # TODO: Use sed instead of perl
    QTINCDIR=`echo $QTINCDIR | perl -p -e 's/\\\\/\\//g'`
    QTLIBDIR=`echo $QTLIBDIR | perl -p -e 's/\\\\/\\//g'`
    
    # Figure out which version of Qt we are using
    AC_MSG_CHECKING([Qt version])
    QT_VER=`grep 'define.*QT_VERSION_STR\W' $QTINCDIR/qglobal.h | perl -p -e 's/\D//g'`
    
    case "${QT_VER}" in
        3*)
            QT_MAJOR="3"
            QT_MINOR=`expr $QT_VER / 10 % 10`
            AC_MSG_RESULT([$QT_VER ($QT_MAJOR.$QT_MINOR)])
        ;;
        *)
            AC_MSG_RESULT([*** Don't know how to handle this Qt major version])
        ;;
    esac

    if test "$QT_VER" -lt 330; then
        AC_MSG_RESULT([**** QT must be at least version 3.3])
        QTDIR=
    else
        USE_QT=1
    fi
fi

if test "$USE_QT" = 1; then
    MOC=$QTDIR/bin/moc
    UIC=$QTDIR/bin/uic
    
    # Calculate Qt include path
    QT_CXXFLAGS="-I$QTINCDIR"
    
    QT_IS_EMBEDDED="no"
    # On unix, figure out if we're doing a static or dynamic link
    case "${host}" in
        *-cygwin)
	    AC_DEFINE_UNQUOTED(WIN32, "", Defined if on Win32 platform)
            if test -f "$QTLIBDIR/qt.lib" ; then
                QT_LIB="qt.lib"
                QT_IS_STATIC="yes"
                QT_IS_MT="no"
            elif test -f "$QTLIBDIR/qt-mt.lib" ; then
                QT_LIB="qt-mt.lib" 
                QT_IS_STATIC="yes"
                QT_IS_MT="yes"
            elif test -f "$QTLIBDIR/qt$QT_VER.lib" ; then
                QT_LIB="qt$QT_VER.lib"
                QT_IS_STATIC="no"
                QT_IS_MT="no"
            elif test -f "$QTLIBDIR/qt-mt$QT_VER.lib" ; then
                QT_LIB="qt-mt$QT_VER.lib"
                QT_IS_STATIC="no"
                QT_IS_MT="yes"
            fi
            ;;
    
        *)
            QT_IS_STATIC=`ls $QTLIBDIR/libqt*.a 2> /dev/null`
            if test "x$QT_IS_STATIC" = x; then
                QT_IS_STATIC="no"
            else
                QT_IS_STATIC="yes"
            fi
            if test x$QT_IS_STATIC = xno ; then
                QT_IS_DYNAMIC=`ls $QTLIBDIR/libqt*.so 2> /dev/null` 
                if test "x$QT_IS_DYNAMIC" = x;  then
                    AC_MSG_ERROR([*** Couldn't find any Qt libraries])
                fi
            fi
    
            if test "x`ls $QTLIBDIR/libqt-mt.* 2> /dev/null`" != x ; then
                QT_LIB="-lqt-mt"
                QT_IS_MT="yes"
            elif test "x`ls $QTLIBDIR/libqt.* 2> /dev/null`" != x ; then
                QT_LIB="-lqt"
                QT_IS_MT="no"
            elif test "x`ls $QTLIBDIR/libqte.* 2> /dev/null`" != x ; then
                QT_LIB="-lqte"
                QT_IS_MT="no"
                QT_IS_EMBEDDED="yes"
            elif test "x`ls $QTLIBDIR/libqte-mt.* 2> /dev/null`" != x ; then
                QT_LIB="-lqte-mt"
                QT_IS_MT="yes"
                QT_IS_EMBEDDED="yes"
            fi
            ;;
    esac
    AC_MSG_CHECKING([if Qt is static])
    AC_MSG_RESULT([$QT_IS_STATIC])
    AC_MSG_CHECKING([if Qt is multithreaded])
    AC_MSG_RESULT([$QT_IS_MT])
    AC_MSG_CHECKING([if Qt is embedded])
    AC_MSG_RESULT([$QT_IS_EMBEDDED])

    QT_GUILINK=""
    QASSISTANTCLIENT_LDADD="-lqassistantclient"
    case "${host}" in
        *irix*)
            QT_LIBS="$QT_LIB"
            if test $QT_IS_STATIC = yes ; then
                QT_LIBS="$QT_LIBS -L$x_libraries -lXext -lX11 -lm -lSM -lICE"
            fi
            ;;
    
        *linux*)
            QT_LIBS="$QT_LIB"
            if test $QT_IS_STATIC = yes && test $QT_IS_EMBEDDED = no; then
                QT_LIBS="$QT_LIBS -L$x_libraries -lXext -lX11 -lm -lSM -lICE -ldl -ljpeg"
            fi
            ;;
    
    
        *osf*) 
            # Digital Unix (aka DGUX aka Tru64)
            QT_LIBS="$QT_LIB"
            if test $QT_IS_STATIC = yes ; then
                QT_LIBS="$QT_LIBS -L$x_libraries -lXext -lX11 -lm -lSM -lICE"
            fi
            ;;
    
        *solaris*)
            QT_LIBS="$QT_LIB"
            if test $QT_IS_STATIC = yes ; then
                QT_LIBS="$QT_LIBS -L$x_libraries -lXext -lX11 -lm -lSM -lICE -lresolv -lsocket -lnsl"
            fi
            ;;


        *win*)
            # linker flag to suppress console when linking a GUI app on Win32
            QT_GUILINK="/subsystem:windows"

	    if test $QT_MAJOR = "3" ; then
	        if test $QT_IS_MT = yes ; then
        	    QT_LIBS="/nodefaultlib:libcmt"
                else
                	QT_LIBS="/nodefaultlib:libc"
                fi
            fi

            if test $QT_IS_STATIC = yes ; then
                QT_LIBS="$QT_LIBS $QT_LIB kernel32.lib user32.lib gdi32.lib comdlg32.lib ole32.lib shell32.lib imm32.lib advapi32.lib wsock32.lib winspool.lib winmm.lib netapi32.lib"
                if test $QT_MAJOR = "3" ; then
                    QT_LIBS="$QT_LIBS qtmain.lib"
                fi
            else
                QT_LIBS="$QT_LIBS $QT_LIB"        
                if test $QT_MAJOR = "3" ; then
                    QT_CXXFLAGS="$QT_CXXFLAGS -DQT_DLL"
                    QT_LIBS="$QT_LIBS qtmain.lib qui.lib user32.lib netapi32.lib"
                fi
            fi
            QASSISTANTCLIENT_LDADD="qassistantclient.lib"
            ;;

    esac


    if test x"$QT_IS_EMBEDDED" = "xyes" ; then
            QT_CXXFLAGS="-DQWS $QT_CXXFLAGS"
    fi

    if test x"$QT_IS_MT" = "xyes" ; then
            QT_CXXFLAGS="$QT_CXXFLAGS -D_REENTRANT -DQT_THREAD_SUPPORT"
    fi

    QT_LDADD="-L$QTLIBDIR $QT_LIBS"

    AC_MSG_CHECKING([QT_CXXFLAGS])
    AC_MSG_RESULT([$QT_CXXFLAGS])
    AC_MSG_CHECKING([QT_LDADD])
    AC_MSG_RESULT([$QT_LDADD])

    AC_SUBST(QT_CXXFLAGS)
    AC_SUBST(QT_LDADD)
    AC_SUBST(QT_GUILINK)
    AC_SUBST(QASSISTANTCLIENT_LDADD)
    AC_SUBST(MOC)
    AC_SUBST(UIC)
fi

])

# Check for binary relocation support.
# Written by Hongli Lai
# http://autopackage.org/

AC_DEFUN([AM_BINRELOC],
[
	AC_ARG_ENABLE(binreloc,
		[  --enable-binreloc       compile with binary relocation support
                          (default=enable when available)],
		enable_binreloc=$enableval,enable_binreloc=auto)

	BINRELOC_CFLAGS=
	BINRELOC_LIBS=
	if test "x$enable_binreloc" = "xauto"; then
		AC_CHECK_FILE([/proc/self/maps])
		AC_CACHE_CHECK([whether everything is installed to the same prefix],
			       [br_cv_valid_prefixes], [
                               # datarootdir variables was introduced with autoconf-2.60
				if test "$bindir" = '${exec_prefix}/bin' -a "$sbindir" = '${exec_prefix}/sbin' -a \
                                       \( "$datadir" = '${prefix}/share' -o \( "$datadir" = '${datarootdir}' -a "$datarootdir" = '${prefix}/share' \) \) -a \
                                       "$libdir" = '${exec_prefix}/lib' -a \
					"$libexecdir" = '${exec_prefix}/libexec' -a "$sysconfdir" = '${prefix}/etc'
				then
					br_cv_valid_prefixes=yes
				else
					br_cv_valid_prefixes=no
				fi
				])
	fi
	AC_CACHE_CHECK([whether binary relocation support should be enabled],
		       [br_cv_binreloc],
		       [if test "x$enable_binreloc" = "xyes"; then
		       	       br_cv_binreloc=yes
		       elif test "x$enable_binreloc" = "xauto"; then
			       if test "x$br_cv_valid_prefixes" = "xyes" -a \
			       	       "x$ac_cv_file__proc_self_maps" = "xyes"; then
				       br_cv_binreloc=yes
			       else
				       br_cv_binreloc=no
			       fi
		       else
			       br_cv_binreloc=no
		       fi])

	if test "x$br_cv_binreloc" = "xyes"; then
		BINRELOC_CFLAGS="-DENABLE_BINRELOC"
		AC_DEFINE(ENABLE_BINRELOC,,[Use binary relocation?])
	fi
	AC_SUBST(BINRELOC_CFLAGS)
	AC_SUBST(BINRELOC_LIBS)
])
