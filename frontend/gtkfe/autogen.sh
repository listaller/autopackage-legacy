#!/bin/bash
srcdir=`dirname "$0"`
test -z "$srcdir" && srcdir=.

PKG_NAME="gtkfe"

(test -f "$srcdir/configure.in" \
  && test -f "$srcdir/frontend/main.c") || {
	echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
	echo " top-level $PKG_NAME directory"
	exit 1
}


if ! gtkDir=`pkg-config --variable=prefix gtk+-2.0 2>&-`; then
	if gtkDir=`command -v gtk-demo 2>&-`; then
		gtkDir=$(dirname "`dirname \"$gtkDir\"`")
	else
		gtkDir=/usr
	fi
fi

ggettextize() {
	if ! test -f po/Makefile.in.in; then
		echo glib-gettextize -c
		glib-gettextize -c
	fi
}


if test -d /usr/local/share/aclocal; then
	searchDir="/usr/local/share/aclocal"
elif test -d /usr/share/aclocal; then
	searchDir="/usr/share/aclocal"
fi

if [[ "$gtkDir/share/aclocal"=="$searchDir" ]] || "$searchDir"=="" ]]; then
	echo aclocal -I "$gtkDir/share/aclocal" &&
	aclocal -I "$gtkDir/share/aclocal"
else
	echo aclocal -I "$gtkDir/share/aclocal" -I "$searchDir" &&
	aclocal -I "$gtkDir/share/aclocal" -I "$searchDir"
fi

ggettextize &&
echo "intltoolize --copy --force --automake" && intltoolize --copy --force --automake &&
echo "autoconf" && autoconf &&
echo "automake --gnu -a -c" && automake --gnu -a -c &&
./configure "$@"
