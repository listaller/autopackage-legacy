/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *                2003, 2005 Mike Hearn <mike@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "processor.h"
#include "protocol.h"
#include "utils.h"
#include "main.h"
#include "receiver.h"
#include "logview.h"
#include "eggstatusicon.h"
#include "gul-ellipsizing-label.h"
#include "common-strings.h"


#define W(x) glade_xml_get_widget (xml, x)

#define PROTOCOL_VERSION "7"

static gboolean helloInitialized = FALSE;
static gboolean failed = FALSE;
static guint sessionDepth = 1;
static gboolean installStarted = FALSE;
static gboolean progressStarted = FALSE;
static guint maxPackages = 0;
static guint packagesInstalled = 0;
static GList *interactionList = NULL;
static gboolean summaryShown = FALSE;

#define COMPLETED_PROBLEM 0
#define COMPLETED_INSTALL 1
#define COMPLETED_UNINSTALL 2
#define COMPLETED_VERIFY 3
#define COMPLETED_UNKNOWN 4
static gboolean presented = FALSE;
static void present_completed (int what);


static gboolean
process_hello (gchar *data)
{
	gchar *first, *tmp;

	/* Initial handshake for every package */

	tmp = strchr (data, '\n');
	first = g_strndup (data, tmp - data);

	if (my_strcmp (first + 6, PROTOCOL_VERSION)) {
		/* Don't increase the session depth if this is the first HELLO. */
		if (helloInitialized)
			sessionDepth++;

		protocol_send ("LETS GO");
		g_free (first);
		helloInitialized = TRUE;

		return TRUE;

	} else {
		protocol_send ("UHOH VERSION_MISMATCH");
		show_error (W("main"),
			    "Protocol version mismatch, expected %d but got %d ... check your autopackage installation is correct",
			    PROTOCOL_VERSION, first + 6);
		g_free (first);
		return FALSE;
	}
}


static gboolean
process_terminate ()
{
	sessionDepth--;
	if (sessionDepth > 0)
		return TRUE;

	/* The backend terminated. */

	receiver_stop ();

	pulse_stop ("globalProgress");
	if (failed)
		present_completed (COMPLETED_PROBLEM);
	else if (!presented)
		/* Shut down */
		gtk_main_quit();

	finished = TRUE;
	return TRUE;
}


static gboolean
process_identify (gchar *data)
{
	GList *lines;
	gchar *displayName;
	guint id;
	gchar *context;
	gchar *msg;

	/* We are current processing a package */

	lines       = strlist_new (data);
	displayName = strlist_get (lines, 1);
	id          = atoi (strlist_get (lines, 2));
	context     = strlist_get (lines, 3);

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);

	gtk_label_set_text (GTK_LABEL (W("name")), displayName);
	if (my_strcmp (context, "prep"))
	{
		GList *children;
		
		msg = g_strconcat ("<span weight=\"bold\" size=\"x-large\">", _APKG(intl_APKG_FE_PREPARING), "</span>", NULL);
				
		/* Preparing a package (checking dependancies) */
		gtk_label_set_markup (GTK_LABEL (W("title")), msg);
		g_free (msg);

		gtk_widget_show (W("prepareImage"));
		gtk_widget_hide (W("installImage"));
		gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);

		children = gtk_container_get_children (GTK_CONTAINER (
				GTK_LOG_VIEW (W("logview"))->vbox
			));
		if (g_list_first (children))
			gtk_log_view_add_separator (GTK_LOG_VIEW (W("logview")));
		g_list_free (children);
		/* Window title */
		msg = g_strconcat (_APKG(intl_APKG_FE_PREPARING), " %s", NULL);
		set_title (msg, displayName);
		g_free (msg);
		
		//if (!failed) gtk_log_view_clear (GTK_LOG_VIEW (W("logview")));

	}
	else if (my_strcmp (context, "install"))
	{
		/* Installing a package */
		gchar *msg = g_strdup_printf("<span weight=\"bold\" size=\"x-large\">%s:</span>", _("Currently installing"));
		gtk_label_set_markup (GTK_LABEL (W("title")), msg);
		g_free (msg);
		
		gtk_widget_hide (W("prepareImage"));
		gtk_widget_show (W("installImage"));

		packagesInstalled = id;
		gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
		gtk_log_view_clear (GTK_LOG_VIEW (W("logview")));

		if (maxPackages > 1)
		{
			gchar *title_fmt = g_strconcat (_APKG(intl_APKG_FE_INSTALLING), " %s (",
							_APKG(intl_APKG_FE_PACKAGE_PROGRESS), ")", NULL);
			set_title (title_fmt,
				   displayName, id, maxPackages,
				   ((gdouble) id / (gdouble) (maxPackages + 1)) * 100.0);
			g_free (title_fmt);
			//msg = g_strdup_printf (_("Package %d of %d"), id, maxPackages);
			msg = g_strdup_printf (_APKG(intl_APKG_FE_PACKAGE_PROGRESS), id, maxPackages);
			gtk_progress_bar_set_text (GTK_PROGRESS_BAR (W("globalProgress")), msg);
			g_free (msg);
			gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (W("globalProgress")),
				(gdouble) id / (gdouble) (maxPackages + 1));
		} else {
			gchar *tmp = g_strconcat (_APKG(intl_APKG_FE_INSTALLING), " ", displayName, NULL);
			set_title (tmp);
			g_free (tmp);
		}
	}
	else if (my_strcmp (context, "verify"))
	{
		/* verifying a package */
		gchar *msg = g_strconcat ("<span weight=\"bold\" size=\"x-large\">", _APKG(intl_APKG_FE_VERIFYING), "</span>", NULL);
		//char *msg = g_strdup_printf("<span weight=\"bold\" size=\"x-large\">%s:</span>", _("Currently verifying"));
		gtk_label_set_markup (GTK_LABEL (W("title")), msg);
		g_free(msg);
		gtk_widget_hide (W("prepareImage"));
		gtk_widget_show (W("installImage"));

		gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
		gtk_log_view_clear (GTK_LOG_VIEW (W("logview")));

		if (maxPackages > 1)
		{
			/* Window title */
			gchar *title_fmt = g_strconcat (_APKG(intl_APKG_FE_VERIFYING), " %s (",
							_APKG(intl_APKG_FE_PACKAGE_PROGRESS), ")", NULL);
			set_title (title_fmt,
				displayName, id, maxPackages,
				((gdouble) id / (gdouble) (maxPackages + 1)) * 100.0);

			msg = g_strdup_printf (_APKG(intl_APKG_FE_PACKAGE_PROGRESS), id, maxPackages);
			gtk_progress_bar_set_text (GTK_PROGRESS_BAR (W("globalProgress")), msg);
			g_free (msg);
			gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (W("globalProgress")),
				(gdouble) id / (gdouble) (maxPackages + 1));
		} else
		{
			msg = g_strconcat (_APKG(intl_APKG_FE_VERIFYING), " ", displayName, NULL);
			set_title (msg);
			g_free (msg);
		}
		
	}

	strlist_free (lines);
	return TRUE;
}


static gboolean process_series (gchar *data)
{
	gchar *max;

	/* Get maximum number of packages */

	max = strchr (data, '\n') + 1;
	stripNewline (max);
	maxPackages = atoi (max);

	if (maxPackages > 1)
	{
		gchar *msg;

		gtk_widget_show (W("globalProgress"));
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (W("globalProgress")), 0.0);
		/* Need to use ngettext here since singular form may be used in some languages even though n > 1 */
		msg = g_strdup_printf (_NAPKG("Preparing %d package...", "Preparing %d packages...", maxPackages),
				       maxPackages);
		//msg = g_strdup_printf (_("Preparing %d packages..."), maxPackages);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (W("globalProgress")), msg);
		g_free (msg);
	}

	installStarted = TRUE;
	return TRUE;
}


static gboolean process_status (gchar *data)
{
	gchar *status;

	status = strchr (data, '\n') + 1;
	stripNewline (status);
	if (strlen (status) == 0)
		return TRUE;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), status);
	return TRUE;
}


static gboolean
process_test (gchar *data)
{
	gchar *name, *str;

	name = strchr (data, '\n') + 1;
	stripNewline (name);
	if (strlen (name) == 0)
		return TRUE;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	str = g_strdup_printf (_APKG(intl_APKG_FE_TESTING), name);
	gtk_log_view_check_begin (GTK_LOG_VIEW (W("logview")), str);
	g_free (str);
	return TRUE;
}

static gboolean
process_searching(gchar *data)
{
	gtk_log_view_check_searching (GTK_LOG_VIEW(W("logview")));
	return TRUE;
}


static gboolean
process_progressbar (gchar *data)
{
	GList *lines;
	gdouble max, current;
	gchar *label;

	lines   = strlist_new (data);
	max     = (gdouble) atoi (strlist_get (lines, 1));
	current = (gdouble) atoi (strlist_get (lines, 2));
	label   = (gchar *) strlist_get (lines, 3);

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);

	if (max == current) {
		/* The progress bar has reached it's end */
		if (progressStarted) gtk_log_view_set_progress (GTK_LOG_VIEW (W("logview")), 1.0);
		progressStarted = FALSE;

	} else {
		if (!progressStarted)
			gtk_log_view_add_progress (GTK_LOG_VIEW (W("logview")), label);
		gtk_log_view_set_progress (GTK_LOG_VIEW (W("logview")), current / max);
		progressStarted = TRUE;
	}

	strlist_free (lines);
	return TRUE;
}


static gboolean
process_fail (gchar *data)
{
	gchar *msg;

	msg = strchr (data, '\n') + 1;
	failed = TRUE;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	gtk_log_view_add_fail (GTK_LOG_VIEW (W("logview")), msg);

	return TRUE;
}


static gboolean
process_passtest (gchar *data)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	gtk_log_view_check_pass (GTK_LOG_VIEW (W("logview")));
	return TRUE;
}


static gboolean
process_failtest (gchar *data)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	gtk_log_view_check_fail (GTK_LOG_VIEW (W("logview")));
	return TRUE;
}


static gboolean
process_recommendtest (gchar *data)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	gtk_log_view_check_recommended (GTK_LOG_VIEW (W("logview")));
	return TRUE;
}


static gboolean
process_download_progress (gchar *data)
{
	GList *lines;
	gchar *rate, *progress;
	gchar *markup, *progress_text;
	GtkProgressBar *progressbar;
	int iprogress;

	lines    = strlist_new (data);
	rate     = strlist_get (lines, 1);
	progress = strlist_get (lines, 2);
	/* char *time = g_list_nth_data(lines, 3); */
	
	markup = g_strdup_printf ("<span foreground=\"gray\">%s%s</span>", rate, _("kb/s"));
	
	gtk_label_set_markup (GTK_LABEL (W("speed_label")), markup);
	g_free(markup);

	progressbar = GTK_PROGRESS_BAR (W("download_progress"));

	iprogress = atoi (progress);
	gtk_progress_bar_set_fraction (progressbar, (double) iprogress / 100);

	progress_text = g_strdup_printf (_("Download %d%% complete"), iprogress);
	gtk_progress_bar_set_text (progressbar, progress_text);
	g_free (progress_text);

	strlist_free (lines);

	return TRUE;
}


static gboolean
process_download_start (gchar *data)
{
	GList *lines = strlist_new (data);
	char  *url   = strlist_get (lines, 1);
	char  *description = strlist_get (lines, 2);

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 2);

	g_assert (url);
	g_assert (description);

	gul_ellipsizing_label_set_text (GUL_ELLIPSIZING_LABEL (W("url_label")), url);
	gtk_label_set_text (GTK_LABEL (W("speed_label")), "");

	strlist_free (lines);

	return TRUE;
}


static gboolean
process_download_prepare (gchar *data)
{
	GList *lines = strlist_new (data);
	char *state = strlist_get (lines, 1);
	char *markup, *label;

	if (my_strcmp(state, "CONNECTING"))
		label = _APKG(intl_APKG_FE_CONNECTING);
	else if (my_strcmp(state, "NEGOTIATING"))
		label = _APKG(intl_APKG_FE_NEGOTIATING);

	markup = g_strdup_printf("%s%s%s", "<span foreground='grey'>", label, "</span>");
	
	gtk_label_set_markup(GTK_LABEL(W("speed_label")), markup);
	
	g_free (markup);
	strlist_free (lines);
	return TRUE;
}


static gboolean
process_download_finish (gchar *data)
{
	/* FIXME no-op for now */
	return TRUE;
}


static gboolean
process_display_summary (gchar *data)
{
	GList *lines = strlist_new (data);
	char *optype;
	char *result;
	char *shortname;

	int i;

	optype    = strlist_get (lines, 1);
	result    = strlist_get (lines, 2);
	shortname = strlist_get (lines, 3);

	/* TODO: make this not suck */
	/* in particular, these strings are not good for translators */
	if (my_strcmp (optype, "install"))
	{
		int ok_packagecount;
		int menu_count;
		int recommend_count;

		present_completed (COMPLETED_INSTALL);

		ok_packagecount = atoi ((char *) g_list_nth_data (lines, 4));
		menu_count = atoi ((char *) g_list_nth_data (lines, 4 + ok_packagecount + 1));
		recommend_count = atoi ((char *) g_list_nth_data (lines, 4 + ok_packagecount + menu_count + 4));

		if (ok_packagecount > 0)
		{
			gchar *tmp, *msg;
			#include "installed.csource"
			GdkPixbuf *pixbuf;

/* 			msg = g_strdup (_NAPKG(intl_APKG_FE_INSTALL_SUCCESS_SINGULAR, */
/* 					       intl_APKG_FE_INSTALL_SUCCESS_PLURAL, */
/* 					       ok_packagecount)); */
			if (ok_packagecount > 1)
				msg = _APKG(intl_APKG_FE_INSTALL_SUCCESS_PLURAL);
			else
				msg = _APKG(intl_APKG_FE_INSTALL_SUCCESS_SINGULAR);
			
			gtk_log_view_clear (GTK_LOG_VIEW (W("logview")));

			pixbuf = gdk_pixbuf_new_from_inline (sizeof (installed_data),
				installed_data, FALSE, NULL);
			gtk_log_view_add_image_head (GTK_LOG_VIEW (W("logview")), pixbuf, msg);
			g_object_unref (pixbuf);
			//gtk_log_view_add_status_bold (GTK_LOG_VIEW (W("logview")), msg);

			for (i = 0; i < ok_packagecount; i++)
		        {
				gchar *expanded_form;

				expanded_form = g_strdup_printf (" • %s", (gchar *) g_list_nth_data (lines, 5 + i));
				gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), expanded_form);
				g_free (expanded_form);
			}
		}

		if (menu_count > 0)
 		{
 			gchar *middle, *msg;
 			#include "menus.csource"
			GdkPixbuf *pixbuf;

/* 			msg = g_strdup (_NAPKG(intl_APKG_FE_MENU_ENTRY_SINGULAR, */
/* 					      intl_APKG_FE_MENU_ENTRY_PLURAL, */
/* 					      menu_count)); */
			if (menu_count > 1)
				msg = g_strdup (_APKG(intl_APKG_FE_MENU_ENTRY_PLURAL));
			else
				msg = g_strdup (_APKG(intl_APKG_FE_MENU_ENTRY_SINGULAR));

			if (ok_packagecount > 0) gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), "");

			pixbuf = gdk_pixbuf_new_from_inline (sizeof (menus_data),
				menus_data, FALSE, NULL);
			gtk_log_view_add_image_head (GTK_LOG_VIEW (W("logview")), pixbuf, msg);
			g_object_unref (pixbuf);
			g_free (msg);

			for (i = 4 + ok_packagecount + 2; i <= 4 + ok_packagecount + 1 + menu_count; i++)
			{
				char *menu_name, *categories;

				menu_name = desktop_file_get_name ((gchar *) g_list_nth_data (lines, i));
				categories = desktop_file_get_categories ((gchar *) g_list_nth_data (lines, i));
				if (menu_name)
				{
					gchar *expanded_form;

					if (categories)
						expanded_form = g_strdup_printf (" • %s (%s)", menu_name, categories);
					else
						expanded_form = g_strdup_printf (" • %s", menu_name);
					gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), expanded_form);
					g_free (expanded_form);
					g_free (menu_name);
				}
			}
		}

		if (recommend_count > 0)
		{
			gchar *msg;
			GdkPixbuf *pixbuf;
			#include "tip.csource"

			if (ok_packagecount > 0) gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), "");

			msg = g_strdup_printf (_NAPKG (intl_APKG_FE_FAILED_RECOMMENDS_SINGULAR,
						intl_APKG_FE_FAILED_RECOMMENDS_PLURAL,
						recommend_count), recommend_count);
/* 			if (recommend_count > 1) */
/* 				msg = g_strdup (_APKG(intl_APKG_FE_FAILED_RECOMMENDS_PLURAL)); */
/* 			else */
/* 				msg = g_strdup (_APKG(intl_APKG_FE_FAILED_RECOMMENDS_SINGULAR)); */

			pixbuf = gdk_pixbuf_new_from_inline (sizeof (tip_data),
				tip_data, FALSE, NULL);
			gtk_log_view_add_image_head (GTK_LOG_VIEW (W("logview")), pixbuf, msg);
			g_object_unref (pixbuf);
			g_free (msg);

			for (i = 4 + ok_packagecount + menu_count + 5; i <= 4 + ok_packagecount + menu_count + recommend_count + 4; i++)
			{
				msg = g_strdup_printf (" • %s",
					(char*) g_list_nth_data (lines, i));
				gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), msg);
				g_free (msg);
			}
		}

		if (my_strcmp (result, "failure")) {
			/* Failure adds an extra bit to the end of data, namely, the packages that failed.
			   therefore we can display packages that succeeded too. */
			char *failed_count_s = (char*) g_list_nth_data (lines, 4 + ok_packagecount + 1 + menu_count);
			gchar *msg;
			int failed_count = atoi (failed_count_s);

/* 			msg = g_strdup (_NAPKG(intl_APKG_FE_FAILED_INSTALL_PACKAGES_SINGULAR, */
/* 					       intl_APKG_FE_FAILED_INSTALL_PACKAGES_PLURAL, */
/* 					       ok_packagecount)); */
			if (ok_packagecount > 1)
				msg = _APKG(intl_APKG_FE_FAILED_INSTALL_PACKAGES_PLURAL);
			else
				msg = _APKG(intl_APKG_FE_FAILED_INSTALL_PACKAGES_SINGULAR);
			
			gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), msg);

			for (i = 5 + ok_packagecount + 1 + menu_count + 1; i <= 5 + ok_packagecount + 1 + menu_count + 1 + failed_count; i++)
			{
				gchar *expanded_form = g_strdup_printf (" • %s", (gchar *) g_list_nth_data (lines, i));
				gtk_log_view_add_status (GTK_LOG_VIEW(W("logview")), expanded_form);
				g_free (expanded_form);
			}
		} else {
			gchar *msg;
			msg = g_strdup(_("You can remove this package by using the \"Manage 3rd party software\" program, "
					 "in the System Tools menu."));
			gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), "");
			gtk_log_view_add_status (GTK_LOG_VIEW (W("logview")), msg);
			g_free (msg);
		}
	}
	else if (my_strcmp (optype, "uninstall"))
		printf ("FIXME\n");
	else if (my_strcmp (optype, "verify"))
	{
		if (my_strcmp (result, "success"))
			present_completed (COMPLETED_VERIFY);
		else
			present_completed (COMPLETED_PROBLEM);
	}
	
	summaryShown = TRUE;
	
	strlist_free (lines);
	return TRUE;
}


static gboolean animation_initialized = FALSE;
static gboolean kill_uninstall_animation = FALSE;
static char *uninstall_msg = NULL;

static GdkPixbuf *arrow = NULL;
static GdkPixbuf *buf = NULL;
static GdkPixbuf *bin = NULL;
static int width;
static int height;
static int arrow_width;
static int arrow_height;
static int bin_width;

static PangoLayout *label1, *label2;
static int label1_height;
static GdkGC *gc;

/* this could probably be simplified */
static void swoop_animation (double *scalex, double *scaley, int *left, int *top, int *tmp, int *alpha)
{
	*scalex = *scaley = 1.0 + (0.2 * (1.0 - (double) *tmp / (double) arrow_width));

	const int swoop_length = 20;
	const double swoop_start = 1.1;
	
	*left = arrow_width - swoop_length;
	if (*scalex <= swoop_start)
	{
		*left += swoop_length -
			 (int) (cos((0.5 * M_PI) * (swoop_start - *scalex) * 10) * swoop_length);
	}
	*top = *top - (int) ((((double) arrow_height * *scaley) - (double) arrow_height) / 2.0);
	*alpha = (int) (255.0 * (double) *tmp / (double) arrow_width);
}

static gboolean init_animation(GtkWidget *data)
{
	char *tmp;

	/* load the pictures */
	tmp = lookup_file("arrow.png");
	arrow = gdk_pixbuf_new_from_file(tmp, NULL);
	free(tmp);
	
	if (!arrow) return FALSE;
	
	GtkAllocation rect;
	rect = data->allocation;
	buf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8,
			     rect.width, rect.height);
	if (!buf) return FALSE;
	
	tmp = lookup_file("uninstall.png");
	bin = gdk_pixbuf_new_from_file(tmp, NULL);
	free(tmp);
	
	if (!bin) return FALSE;
	
	/* init text rendering */
	gc = gdk_gc_new(data->window);
	label1 = gtk_widget_create_pango_layout(data, NULL);
	pango_layout_set_markup(label1,
				uninstall_msg,
				-1);
	pango_layout_get_pixel_size(label1, NULL, &label1_height);
	
	label2 = gtk_widget_create_pango_layout(data, NULL);
        tmp = g_strdup_printf("<span color='darkgrey'>%s</span>", _APKG(intl_APKG_FE_PLEASE_WAIT));
	pango_layout_set_markup(label2,	tmp, -1);
        g_free(tmp);
	
	/* some convenience variables we'll need in the animation */
	width = gdk_pixbuf_get_width(buf);
	height = gdk_pixbuf_get_height(buf);
	arrow_width = gdk_pixbuf_get_width(arrow);
	arrow_height = gdk_pixbuf_get_height(arrow);
	bin_width = gdk_pixbuf_get_width(bin);
        
        return TRUE;
}

/* this code really needs to be cleaned up :) -m */
static gboolean draw_arrows_animation (GtkWidget *data)
{
	if (kill_uninstall_animation) return FALSE;
		
	if (!animation_initialized)
	{
		if (!init_animation(data))
		{
                        printf("animation failed to initialize\n");
			kill_uninstall_animation = TRUE;
			return FALSE;
		}
		animation_initialized = TRUE;
	}

	/* tracks the left edge of each arrow: distance from right edge of window */
	static unsigned int x = 0;

	/* next frame ... */
	x -= 2;

	/* blank out the double buffer for this frame */
	gdk_pixbuf_fill(buf, 0);

	/* if we've done a full iteration of the animation, reset */
	if (x <= 0)
	{
		/* the x2 here cuts down on the number of arrow */
		x = 2 * arrow_width;
	}

	/* paint the dustbin picture */
	gdk_pixbuf_copy_area(bin, 0, 0,
			     gdk_pixbuf_get_width(bin),
			     gdk_pixbuf_get_height(bin),
			     buf, 0, 0);


        /* force it down the ugly but fast codepath for now .... will have to be rewritten to use cairo/gl */
	if (gtk_minor_version <= 2) {
		/* GTK 2.2 seems to have a bug in gdk_pixbuf_composite which causes graphic corruption,
		 * so use a different animation. */
		static int sx = -9999;
		int x, w;

		if (sx == -9999)
			/* Initialize sx */
			sx = -gdk_pixbuf_get_width (arrow);
		x = gdk_pixbuf_get_width (bin) + sx;

		if (sx >= 0) {
			w = gdk_pixbuf_get_width (arrow);
			if (x + w > width)
				w = width - x;
			gdk_pixbuf_copy_area (arrow, 0, 0,
					      w, gdk_pixbuf_get_height (arrow),
					      buf,
					      x, 100);
		} else {
			gdk_pixbuf_copy_area (arrow,
					      -sx, 0,
					      gdk_pixbuf_get_width (arrow) + sx,
					      gdk_pixbuf_get_height (arrow),
					      buf,
					      gdk_pixbuf_get_width (bin), 100);
		}

		sx += 5;
		if (x > width)
			sx = -gdk_pixbuf_get_width (arrow);

	}
        else
        {
		/* tmp tracks the left edge of the current arrow */
		int tmp = width - x;
		int skip = 0;
		gboolean alternator = FALSE;
		while (tmp >= -arrow_width && !skip)
		{
			int alpha = 255;
			int left;
			int top;
			double scalex = 1.0;
			double scaley = 1.0;
			
			tmp -= arrow_width;
			
			left = tmp;
			top  = 128 - arrow_height;

			/* do the initial part of the animation, where the arrow swoops down */
			if (tmp < arrow_width)
			{
				swoop_animation(&scalex, &scaley, &left, &top, &tmp, &alpha);
				skip = 1;
			}
			else
			{
				if (tmp > width - arrow_width)
				{
					alpha = 0;
				}
				else if (tmp > width - (2*arrow_width))
				{
					double opacity = ((double)(tmp - (width - (2*arrow_width)))
							  / (double)(arrow_width));
					alpha = (int) (255.0 * (1.0 - opacity));
				}
			}
			
			/* this stops us packing the arrows too tightly */
			if ((alternator = !alternator))
				continue;
			
			gdk_pixbuf_composite(arrow, buf, 0, 0,
					     width, height,
					     left, top,
					     scalex, scaley,
					     GDK_INTERP_NEAREST, alpha);
		}
	}

	/* now copy the backbuffer to the window */
	GdkRectangle rect;
	gdk_window_get_size(data->window, &rect.width, &rect.height);
	rect.x = rect.y = 0;
	gdk_window_begin_paint_rect(data->window, &rect);
	gdk_window_clear(data->window);
	gdk_pixbuf_render_to_drawable(buf, data->window, NULL,
				      0, 0, 0, 0, width, height,
				      GDK_RGB_DITHER_NONE, 0, 0);
	/* and draw the text */
	gdk_draw_layout(data->window, gc, 170, 0, label1);
	gdk_draw_layout(data->window, gc, 170, label1_height + 5, label2);
	
	gdk_window_end_paint(data->window);

        /* HACK! stop the animation here as it takes too much cpu time (so we draw the first frame) */
	return FALSE;
}

gboolean process_uninstalling (gchar *data)
{
	GList *lines = strlist_new (data);

	if (uninstall_msg) g_free(uninstall_msg);

	kill_uninstall_animation = FALSE;
        
        char *what = "autopackage";
	if (my_strcmp ("rpm", strlist_get (lines, 3))) what = "RPM";
        uninstall_msg = g_strdup_printf(_APKG(intl_APKG_FE_REMOVING), what, g_list_nth_data(lines, 2));
        
        char *tmp = g_strdup_printf(_(intl_UNINSTALLING), g_list_nth_data(lines, 2));
        set_title(tmp);
	char *msg = g_strdup_printf("<span weight=\"bold\" size=\"x-large\">%s</span>", tmp);
	gtk_label_set_markup (GTK_LABEL (W("title")), msg);
	g_free(msg);
        g_free(tmp);
        
	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 3);

	/* It seems that the DestroyNotify doesn't get called upon exit, but this is either a bug
	 * in GTK+ or libglade... */
	g_object_set_data_full (G_OBJECT(W("uninstall-drawing-area")), "packagename",
				g_strdup (g_list_nth_data(lines, 1)),
				(GDestroyNotify) g_free);
        
	/* poor mans animation */
	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE, 50, (GSourceFunc) draw_arrows_animation, W("uninstall-drawing-area"), NULL);

	strlist_free (lines);
	
	return TRUE;
}

gboolean process_uninstalling_done (gchar *data)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	kill_uninstall_animation = TRUE;
	return TRUE;	
}

gboolean process_uninstalling_fail (gchar *data)
{
	kill_uninstall_animation = TRUE;
	
	process_fail (data);
	return TRUE;	
}


gboolean
process (gchar *data)
{
	gboolean result = TRUE;

	if (startsWith (data, "HELLO "))
		return process_hello (data);

	if (startsWith (data, "TERMINATE\n"))
		result = process_terminate ();

	else if (startsWith (data, "IDENTIFY\n"))
		result = process_identify (data);

	else if (startsWith (data, "SERIES\n"))
		result = process_series (data);

	else if (startsWith (data, "STATUS\n"))
		result = process_status (data);

	else if (startsWith (data, "TEST\n"))
		result = process_test (data);

	else if (startsWith (data, "PROGRESSBAR\n"))
		result = process_progressbar (data);

	else if (startsWith (data, "FAIL\n"))
		result = process_fail (data);

	else if (startsWith (data, "FAILTEST\n"))
		result = process_failtest (data);

	else if (startsWith (data, "PASSTEST\n"))
		result = process_passtest (data);

	else if (startsWith (data, "RECOMMEND\n"))
		result = process_recommendtest (data);

	else if (startsWith (data, "DISPLAY-SUMMARY\n"))
		result = process_display_summary (data);

	else if (startsWith (data, "DOWNLOAD-START\n"))
		result = process_download_start (data);
	
	else if (startsWith (data, "DOWNLOAD-PREPARE\n"))
		result = process_download_prepare (data);

	else if (startsWith (data, "DOWNLOAD-PROGRESS\n"))
		result = process_download_progress (data);

	else if (startsWith (data, "DOWNLOAD-FINISH\n"))
		result = process_download_finish (data);

	else if (startsWith (data, "SEARCHING\n"))
		result = process_searching (data);

	else if (startsWith (data, "UNINSTALLING\n"))
		result = process_uninstalling (data);

	else if (startsWith (data, "UNINSTALL-DONE\n"))
		result = process_uninstalling_done (data);

	else if (startsWith (data, "UNINSTALL-FAIL\n"))
	        result = process_uninstalling_fail (data);

	else {
		show_error (W("install"),
			_("The backend sent an unknown command:\n%s"),
			data);
		exit (1);
	}

	protocol_send ("OK");
	return result;
}


static void raise_window ()
{
        gtk_window_present(GTK_WINDOW(W("install")));
}


/* Present an "installation completed" screen */
static void present_completed (int what)
{
	gchar *msg, *tmp;

	presented = TRUE;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (W("notebook1")), 1);
	pulse_stop ("globalProgress");
	gtk_widget_hide (W("globalProgress"));

	if (what == COMPLETED_PROBLEM)
		gtk_image_set_from_file (GTK_IMAGE (W("installImage")), lookup_file ("help.png"));
	else if (what == COMPLETED_INSTALL || what == COMPLETED_VERIFY || what == COMPLETED_UNKNOWN)
		gtk_image_set_from_file (GTK_IMAGE (W("installImage")), lookup_file ("package-ok.png"));
	gtk_widget_hide (W("prepareImage"));
	gtk_widget_show (W("installImage"));

	gtk_label_set_text (GTK_LABEL (W("name")), "");

	/* FIXME-Post 1.0: These should be common for all frontends */
	if (what == COMPLETED_PROBLEM)
		tmp = _APKG(intl_APKG_FE_FINISHED);
	else if (what == COMPLETED_INSTALL)
		tmp = _APKG(intl_APKG_FE_COMPLETED_INSTALL);
	else if (what == COMPLETED_VERIFY)
		tmp = _APKG(intl_APKG_FE_COMPLETED_VERIFY);
	else if (what == COMPLETED_UNKNOWN)
		tmp = _APKG(intl_APKG_FE_FINISHED);
	
	msg = g_strdup_printf("<span weight=\"bold\" size=\"x-large\">%s</span>", tmp);
	gtk_label_set_markup (GTK_LABEL (W("title")), msg);
	g_free (msg);

	gtk_window_set_title (GTK_WINDOW (W("install")), tmp);

	gtk_widget_hide (W("hide"));
	gtk_widget_show (W("close"));
	gtk_button_set_label (GTK_BUTTON (W("close")), GTK_STOCK_OK);
	gtk_button_set_use_stock (GTK_BUTTON (W("close")), TRUE);
	gtk_widget_grab_default (W("close"));
	gtk_widget_grab_focus (W("close"));

	tmp = lookup_file ("package-open.png");
	if (!statusIcon) statusIcon = egg_status_icon_new();

	/* We use %d notation even if it is singular because there are languages that use singular
	 * for n != 1 */

	msg = g_strdup_printf (ngettext("Installation of %d package is complete",
					"Installation of %d packages is complete", maxPackages),
			       maxPackages);
			       
/* 	if (maxPackages == 1) */
/* 		msg = g_strdup_printf (_("Installation of the requested package is complete")); */
/* 	else */
/* 		msg = g_strdup_printf (_("Installation of %d packages is complete"), maxPackages); */
		       
	egg_status_icon_set_tooltip(statusIcon, msg, NULL);
	egg_status_icon_set_is_blinking (statusIcon, TRUE);
	egg_status_icon_set_from_file (statusIcon, tmp);
	g_signal_connect (G_OBJECT(statusIcon), "activate", G_CALLBACK (raise_window), NULL);

	g_free (tmp);
	g_free (msg);	
}

