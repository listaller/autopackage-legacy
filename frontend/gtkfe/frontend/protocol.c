/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _PROTOCOL_C_
#define _PROTOCOL_C_

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#include "protocol.h"
#include "main.h"
#include "utils.h"


gboolean
protocol_send (gchar *format, ...)
{
	va_list ap;
	gchar *msg;
	FILE *fifo;
	gboolean result;

	fifo = fopen (fifoName, "w");
	if (!fifo)
		return FALSE;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);

	trace (2, "Protocol - sending: %s", msg);

	result = (fprintf (fifo, "%s\n", msg) > strlen (msg));
	fclose (fifo);
	trace (3, "Protocol - sent.");
	g_free (msg);
	return result;
}


GList *
protocol_recv_lines ()
{
	GList *lines = NULL;
	gchar line[1024];
	FILE *fifo;

	fifo = fopen (fifoName, "r");
	if (!fifo)
		return NULL;

	while (!feof (fifo) && fgets (line, sizeof (line), fifo)) {
		stripNewline (line);
		lines = g_list_append (lines, g_strdup (line));
	}
	fclose (fifo);
	return lines;
}


gchar *
protocol_recv ()
{
	GString *str;
	gchar line[1024], *data;
	FILE *fifo;

	fifo = fopen (fifoName, "r");
	if (!fifo)
		return NULL;

	str = g_string_new ("");
	while (!feof (fifo)) {
		if (fgets (line, sizeof (line), fifo))
			g_string_append (str, line);
	}
	fclose (fifo);

	data = str->str;
	g_string_free (str, FALSE);
	return data;
}


gchar *
protocol_recv_first ()
{
	gchar *line;
	FILE *fifo;

	fifo = fopen (fifoName, "r");
	if (!fifo)
		return NULL;
	line = g_new0 (gchar, 1024);
	if (!line) {
		fclose (fifo);
		return NULL;
	}

	if (fgets (line, 1024, fifo)) {
		stripNewline (line);
		return line;
	} else
		return NULL;
}


#endif /* _PROTOCOL_C_ */
