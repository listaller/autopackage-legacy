/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>

#include "binreloc.h"
#include "utils.h"
#include "protocol.h"
#include "receiver.h"
#include "processor.h"
#include "logview.h"
#include "eggstatusicon.h"
#include "gul-ellipsizing-label.h"


#define W(x) glade_xml_get_widget (xml, x)

gchar *exe_dir, *data_dir, *locale_dir;

gchar *fifoName = NULL;			/* Filename of the FIFO */
GladeXML *xml = NULL;			/* The main Glade GUI */
gint exitCode = 0;
gboolean silentMode = FALSE;
gboolean finished = FALSE;
EggStatusIcon *statusIcon = NULL;


static gboolean
show (EggStatusIcon *icon, gpointer user_data)
{
	silentMode = FALSE;

	gtk_widget_show (W("install"));
	g_object_unref (statusIcon);
	statusIcon = NULL;

	return FALSE;
}


gboolean
hide ()
{
	char *s = lookup_file ("package-closed.png");

	silentMode = TRUE;

	statusIcon = egg_status_icon_new_from_file (s);
	egg_status_icon_set_tooltip (EGG_STATUS_ICON (statusIcon), _("Preparing packages..."), NULL);
	g_signal_connect (G_OBJECT (statusIcon), "activate", G_CALLBACK (show), NULL);

	gtk_widget_hide (W("install"));
	g_free(s);

	return FALSE;
}


gboolean
delete_install_window ()
{
	/* The user tried to close the window. Only close if the installation has finished. */
	if (finished) {
		gtk_main_quit ();
		return FALSE;
	} else
		return TRUE;
}


/* Read data from the receiver thread and pass them to the data processor */
static gboolean
read_from_fifo (gpointer d)
{
	gchar *data;
	gboolean result;

	data = receiver_get ();
	if (!data) {
		/* No data yet. Do nothing. */
		usleep (10000);
		return TRUE;
	}

	result = process (data);
	g_free (data);
	if (!result)
		exit (1);

	receiver_resume ();
	return TRUE;
}


static gboolean
do_pulse (GtkWidget *progress)
{
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (progress));
	return TRUE;
}


/* Start a pulsing progress bar */
void
pulse_start (gchar *widgetName)
{
	GObject *progress;
	guint id;

	progress = (GObject *) W(widgetName);
	id = (guint) GPOINTER_TO_INT (g_object_get_data (progress, "PULSEID"));
	if (id == 0) {
		id = gtk_timeout_add (20, (GtkFunction) do_pulse, progress);
		g_object_set_data (progress, "PULSEID", GINT_TO_POINTER ((gint) id));
	}
}


/* Stop a pulsing progress bar */
void
pulse_stop (gchar *widgetName)
{
	GObject *progress;
	guint id;

	progress = (GObject *) W(widgetName);
	id = (guint) GPOINTER_TO_INT (g_object_get_data (progress, "PULSEID"));
	if (id != 0) {
		g_source_remove (id);
		g_object_set_data (progress, "PULSEID", NULL);
	}
}


/* Is a progress bar pulsing? */
gboolean
pulsing (gchar *widgetName)
{
	GObject *progress;

	progress = (GObject *) W(widgetName);
	return ((guint) GPOINTER_TO_INT (g_object_get_data (progress, "PULSEID")) != 0);
}


/* Set the window title and tray icon tooltip */
void
set_title (gchar *format, ...)
{
	gchar *msg;
	va_list ap;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);

	gtk_window_set_title (GTK_WINDOW (W("install")), msg);
	if (silentMode)
		egg_status_icon_set_tooltip (statusIcon, msg, NULL);
	g_free (msg);
}


static void
exit_func ()
{
	receiver_stop ();
}


static void
initialize ()
{
	GdkColor *white;
	GError *error = NULL;

	/* Setup the GUI */
	xml = load_glade ("frontend.glade");
	white = &gtk_widget_get_style (W("titleBox"))->white;
	gtk_widget_modify_bg (W("titleBox"), GTK_STATE_NORMAL, white);
	/* gtk_window_set_default_size (GTK_WINDOW (W("install")), 600, 350); */
	gtk_widget_show (W("logview"));

	gtk_label_set_selectable (GTK_LABEL (W("url_label")), TRUE);

	if (g_getenv ("AUTOPACKAGE_GTK_SILENT") && g_getenv ("AUTOPACKAGE_GTK_SILENT")[0] == '1')
		hide ();
	else
                gtk_widget_show (W("install")); 

	/* Listen for input from the FIFO */
	if (!g_thread_supported ())
		g_thread_init (NULL);
	if (!receiver_start (&error)) {
		show_error (NULL, _("System error: unable to start the receiver thread.\n%s"),
			error->message);
		g_error_free (error);
		exit (1);
	}
	g_atexit (exit_func);

	/* Watch for input */
	g_idle_add (read_from_fifo, NULL);
}


GtkWidget *
glade_gul_ellipsizing_label_new (gchar *name, gchar *str1)
{
	GtkWidget *widget;
	widget = gul_ellipsizing_label_new (str1);
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_widget_show (widget);
	return widget;
}


int
main (int argc, char *argv[])
{
	GError *error = NULL;
	struct stat buf;

	void *volatile p = &gtk_log_view_new;
        
	if (!gbr_init (&error)) {
		g_warning ("Cannot initialize BinReloc: %s\n", error->message);
		g_error_free (error);
	}
	exe_dir = gbr_find_exe_dir (NULL);
	data_dir = gbr_find_data_dir (DATADIR);
	locale_dir = gbr_find_locale_dir (LOCALEDIR);

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, locale_dir);
	bindtextdomain ("autopackage", locale_dir);
	bind_textdomain_codeset ("autopackage", "UTF-8");
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif

	gtk_init (&argc, &argv);
	if (g_getenv ("AUTOPACKAGE_GTKFE_XID") && g_getenv ("AUTOPACKAGE_GTKFE_ATOM")) {
		/* Tell the launcher to exit as soon as GTK+ is initialized */
		GdkEvent event;
		GdkNativeWindow win;

		win = (GdkNativeWindow) atoi (g_getenv ("AUTOPACKAGE_GTKFE_XID"));
		event.type = GDK_CLIENT_EVENT;
		event.client.type = GDK_CLIENT_EVENT;
		event.client.window = gdk_window_foreign_new (win);
		event.client.send_event = TRUE;
		event.client.message_type = gdk_atom_intern (g_getenv ("AUTOPACKAGE_GTKFE_ATOM"), FALSE);
		event.client.data_format = 8;
		strcpy (event.client.data.b, "DONE");
		gdk_event_send_client_message (&event, win);
		while (gtk_events_pending ())
			gtk_main_iteration ();
	}


	/* Sanity-check arguments */
	if (argc != 2) {
		g_printerr (
			_("This program is used internally by Autopackage"
			" and should not be launched directly. "
			"Use autopackage-launcher-gtk instead.\n"));
		return 1;
	}

	if (stat (argv[1], &buf) != 0) {
		g_printerr ("ERROR: file %s not found.\n", argv[1]);
		return 1;

	} else if (!S_ISFIFO (buf.st_mode)) {
		g_printerr ("ERROR: %s is not a FIFO!\n", argv[1]);
		return 1;
	}

	fifoName = argv[1];
	initialize ();
	gtk_main ();

	g_free (exe_dir);
	g_free (data_dir);
	g_free (locale_dir);

	return exitCode;
}
