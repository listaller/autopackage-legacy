/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <gtk/gtk.h>
#include "utils.h"
#include "main.h"
#include "desktop-file.h"


static void
replace_all (gchar **str, gchar *from, gchar *to)
{
	GString *newstr;
	gchar *found;

	g_return_if_fail (str != NULL);
	g_return_if_fail (from != NULL);
	g_return_if_fail (to != NULL);

	newstr = g_string_new (*str);
	found = strstr (newstr->str, from);
	while (found != NULL) {
		gint pos;

		pos = GPOINTER_TO_INT (found) - GPOINTER_TO_INT (newstr->str);
		g_string_erase (newstr, pos, strlen (from));
		g_string_insert (newstr, pos, to);
		found = GINT_TO_POINTER (GPOINTER_TO_INT (found) + strlen (to));
		found = strstr (found, from);
	}

	g_free (*str);
	*str = newstr->str;
	g_string_free (newstr, FALSE);
}


void
show_error (GtkWidget *parent, gchar *format, ...)
{
	GtkWidget *dialog;
	va_list ap;
	gchar *msg;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);

	dialog = gtk_message_dialog_new (GTK_WINDOW (parent),
		GTK_DIALOG_MODAL,
		GTK_MESSAGE_ERROR,
		GTK_BUTTONS_OK,
		msg);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	g_free (msg);
}


GladeXML *
load_glade (gchar *filename)
{
	GladeXML *xml;
	gchar *file;
	GList *searchPaths = NULL, *tmp;
	gboolean found = FALSE;

	if (exe_dir)
		searchPaths = g_list_append (searchPaths,
			g_strdup_printf ("%s/../glade", exe_dir));
	searchPaths = g_list_append (searchPaths,
		g_strdup_printf ("%s/autopackage-gtk/glade", data_dir));


	for (tmp = searchPaths; tmp; tmp = tmp->next) {
		file = g_strdup_printf ("%s/%s", (gchar *) tmp->data, filename);
		if (g_file_test (file, G_FILE_TEST_EXISTS)) {
			found = TRUE;
			break;
		}
		g_free (file);
	}

	for (tmp = searchPaths; tmp; tmp = tmp->next)
		g_free (tmp->data);
	g_list_free (searchPaths);

	if (!found)
		goto err;

	xml = glade_xml_new (file, NULL, GETTEXT_PACKAGE);
	if (!xml)
		goto err;
	glade_xml_signal_autoconnect (xml);
	return xml;


	err:
	show_error (NULL,
		_("Unable to initialize the user interface file \"%s\". This frontend program must be terminated.\n\n"
		"Possible causes of this error:\n"
		"- This frontend program is improperly installed.\n"
		"- The installation is somehow damaged after it is installed.\n\n"
		"Please try reinstalling this program (autopackage-gtk).\n"
		"If that doesn't help, submit a bugreport to the Autopackage authors:\n"
		"http://autopackage.org/"),
		filename);
	exit (1);
}


gchar *
lookup_file (gchar *basename)
{
	GladeXML *xml;
	gchar *file;
	GList *searchPaths = NULL, *tmp;
	gboolean found = FALSE;

	if (exe_dir)
		searchPaths = g_list_append (searchPaths,
			g_strdup_printf ("%s/../glade", exe_dir));
	searchPaths = g_list_append (searchPaths,
		g_strdup_printf ("%s/autopackage-gtk/glade", data_dir));


	for (tmp = searchPaths; tmp; tmp = tmp->next) {
		file = g_strdup_printf ("%s/%s", (gchar *) tmp->data, basename);
		if (g_file_test (file, G_FILE_TEST_EXISTS)) {
			found = TRUE;
			break;
		}
		g_free (file);
	}

	for (tmp = searchPaths; tmp; tmp = tmp->next)
		g_free (tmp->data);
	g_list_free (searchPaths);

	if (found)
		return file;

	err:
	show_error (NULL,
		_("Unable to load data file \"%s\". This frontend program must be terminated.\n\n"
		"Possible causes of this error:\n"
		"- This frontend program is improperly installed.\n"
		"- The installation is somehow damaged after it is installed.\n\n"
		"Please try reinstalling this program (autopackage-gtk).\n"
		"If that doesn't help, submit a bugreport to the Autopackage authors:\n"
		"http://autopackage.org/"),
		basename);
	exit (1);
}


/* Split a string using newline as delimiter. Put the result in a GList. */
GList *
strlist_new (gchar *str)
{
	GList *lines = NULL;

	do {
		gchar *end, *line;

		end = strchr (str, '\n');
		if (end) {
			line = g_strndup (str, end - str);
			stripNewline (line);
			lines = g_list_append (lines, line);
			str = end + 1;
		} else {
			lines = g_list_append (lines, g_strdup (str));
			break;
		}
	} while (1);

	if (strlen (((gchar *) g_list_last (lines)->data)) == 0)
		lines = g_list_remove_link (lines, g_list_last (lines));

	return lines;
}

gchar *
strlist_get (GList *list, int index)
{
	return (gchar *) g_list_nth_data (list, index);
}

void
strlist_free (GList *list)
{
	GList *tmp;

	if (!list)
		return;
	for (tmp = list; tmp; tmp = tmp->next)
		if (tmp->data) g_free (tmp->data);
	g_list_free (list);
}


gchar *
desktop_file_get_categories (gchar *filename)
{
	GnomeDesktopFile *desktop;
	GError *error = NULL;

	desktop = gnome_desktop_file_load (filename, &error);
	if (desktop) {
		char *categories = NULL;

		gnome_desktop_file_get_string (desktop, "Desktop Entry", "Categories", &categories);
		if (categories) {
			replace_all (&categories, "Application;", "");
			replace_all (&categories, "KDE;", "");
			replace_all (&categories, "Qt;", "");
			replace_all (&categories, "Core;", "");
			replace_all (&categories, "GTK;", "");
			replace_all (&categories, "GNOME;", "");
			replace_all (&categories, "X-Red-Hat-Base;", "");
			if (g_str_has_suffix (categories, ";"))
				categories[strlen (categories) - 1] = '\0';
			replace_all (&categories, ";", ", ");
		}
		return categories;
	}
	return NULL;
}


gchar *
desktop_file_get_name (gchar *filename)
{
	GnomeDesktopFile *desktop;
	GError *error = NULL;

	desktop = gnome_desktop_file_load (filename, &error);
	if (desktop) {
		char *name, *generic_name, *label = NULL;

		gnome_desktop_file_get_string (desktop, "Desktop Entry", "Name", &name);
		gnome_desktop_file_get_string (desktop, "Desktop Entry", "GenericName", &generic_name);

		if (name && *name) {
			if (generic_name && *generic_name) {
				if (!my_strcmp (name, generic_name)) {
					label = g_strdup_printf ("%s %s", name, generic_name);
					g_free (generic_name);
					g_free (name);
				} else {
					g_free (generic_name);
					label = name;
				}
			} else
				label = name;
		} else if (generic_name)
			label = generic_name;

		return label;
	}
	return NULL;
}


void
trace (gint level, gchar *format, ...)
{
	va_list ap;
	gchar *msg;
	const gchar *debuglevel;

	debuglevel = g_getenv ("GTKFE_DEBUGLEVEL");
	if (!debuglevel || !*debuglevel)
		return;

	if (atoi (debuglevel) >= level) {
		va_start (ap, format);
		msg = g_strdup_vprintf (format, ap);
		va_end (ap);

		g_print ("%s\n", msg);
		g_free (msg);
	}
}
