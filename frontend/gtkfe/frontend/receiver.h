/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _RECEIVER_H_
#define _RECEIVER_H_

#include <glib.h>

/* The receiver is a thread that reads input from the autopackage FIFO
 * and stores the result in a queue. The main thread can then retrieve the
 * data from the queue. We do this because opening a FIFO will block until
 * the FIFO has been opened by another process.
 */

typedef struct {
	GMutex *data_mutex;
	gchar *data;

	GMutex *resume_mutex;
	GCond *resume;

	GMutex *stop_mutex;
	gboolean stop;

	GThread *thread;
} Receiver;


/* The global Receiver object. */
extern Receiver *receiver;


gboolean receiver_start (GError **error);
void     receiver_stop  (void);

/* Retrieve the first message from the FIFO and pause the receiver. */
gchar   *receiver_get    (void);
/* After we've processed the message, we call this function to resume the receiver. */
void     receiver_resume (void);

#endif /* _RECEIVER_H_ */
