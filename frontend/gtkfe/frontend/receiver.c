/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _RECEIVER_C_
#define _RECEIVER_C_

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include "receiver.h"
#include "main.h"
#include "protocol.h"
#include "utils.h"


Receiver *receiver = NULL;
GQuark receiver_error_domain;


static gpointer
read_data (gpointer d)
{
	while (1) {
		gchar *data;
		gboolean stop;

		/* Check whether it's time to shutdown. */
		g_mutex_lock (receiver->stop_mutex);
		stop = receiver->stop;
		g_mutex_unlock (receiver->stop_mutex);
		if (stop) {
			trace (1, "Receiver thread - stopping.");
			return NULL;
		}

		/* Receive message from the FIFO. */
		trace (3, "Receiver thread - receiving.");
		data = protocol_recv ();
		trace (2, "Receiver thread - got data: (\n%s)", data);

		g_mutex_lock (receiver->data_mutex);
		receiver->data = data;
		g_mutex_unlock (receiver->data_mutex);

		/* Wait until the main thread has processed this message. */
		g_mutex_lock (receiver->resume_mutex);
		g_cond_wait (receiver->resume, receiver->resume_mutex);
		g_mutex_unlock (receiver->resume_mutex);
	}
	return NULL;
}


gboolean
receiver_start (GError **error)
{
	GIOChannel *io;
	int p[2];

	receiver = g_new (Receiver, 1);
	receiver->data_mutex = g_mutex_new ();
	receiver->data = NULL;
	receiver->resume_mutex = g_mutex_new ();
	receiver->resume = g_cond_new ();
	receiver->stop_mutex = g_mutex_new ();
	receiver->stop = FALSE;
	receiver->thread = g_thread_create (read_data, NULL, TRUE, error);

	if (receiver->thread == NULL) {
		g_mutex_free (receiver->data_mutex);
		g_mutex_free (receiver->resume_mutex);
		g_cond_free (receiver->resume);
		g_mutex_free (receiver->stop_mutex);
		g_free (receiver);
		return FALSE;
	} else
		return TRUE;
}


void
receiver_stop ()
{
	if (!receiver)
		return;

	trace (1, "Receiver - stopping.");

	/* Tell the receiver thread to shut down. */
	g_mutex_lock (receiver->stop_mutex);
	receiver->stop = TRUE;
	g_mutex_unlock (receiver->stop_mutex);
	receiver_resume ();
	g_thread_join (receiver->thread);

	/* Free resources. */
	g_mutex_free (receiver->data_mutex);
	g_free (receiver->data);
	g_mutex_free (receiver->resume_mutex);
	g_cond_free (receiver->resume);
	g_mutex_free (receiver->stop_mutex);
	g_free (receiver);
	receiver = NULL;

	/* Touch and remove the FIFO to prevent the backend from freezing */
/*	fd = open (fifoName, O_RDONLY | O_NONBLOCK | O_NOCTTY,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	fd2 = open (fifoName, O_WRONLY | O_NONBLOCK | O_NOCTTY,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	remove (fifoName);
	if (fd != -1) close (fd);
	if (fd2 != -1) close (fd2); */
}


gchar *
receiver_get ()
{
	gchar *data;

	if (!receiver)
		return;

	g_mutex_lock (receiver->data_mutex);
	data = receiver->data;
	receiver->data = NULL;
	g_mutex_unlock (receiver->data_mutex);
	return data;
}


void
receiver_resume ()
{
	if (!receiver)
		return;

	g_mutex_lock (receiver->resume_mutex);
	g_cond_signal (receiver->resume);
	g_mutex_unlock (receiver->resume_mutex);
}


#endif /* _RECEIVER_C_ */
