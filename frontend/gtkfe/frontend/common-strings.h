/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef _COMMON_STRINGS_H_
#define _COMMON_STRINGS_H_

#include <glib.h>

static const gchar *intl_APKG_FE_COMPLETED_FAILED="The operation failed to complete";
static const gchar *intl_APKG_FE_COMPLETED_INSTALL="Installation completed";
static const gchar *intl_APKG_FE_COMPLETED_VERIFY="Verification completed";
static const gchar *intl_APKG_FE_CONNECTING="Connecting, please wait";
static const gchar *intl_APKG_FE_DOWNLOAD_BANNER="Downloading %s";
static const gchar *intl_APKG_FE_DOWNLOAD_INITIALIZING="Initializing";
static const gchar *intl_APKG_FE_FAILED_INSTALL_PACKAGES_PLURAL="The following packages failed to install correctly:";
static const gchar *intl_APKG_FE_FAILED_INSTALL_PACKAGES_SINGULAR="The following package failed to install correctly:";
static const gchar *intl_APKG_FE_FAILED_RECOMMENDS_PLURAL="%d software packages could be installed to enhance this package:";
static const gchar *intl_APKG_FE_FAILED_RECOMMENDS_SINGULAR="%d software package could be installed to enhance this package:";
static const gchar *intl_APKG_FE_FINISHED="Finished";
static const gchar *intl_APKG_FE_INSTALLING="Installing package:";
static const gchar *intl_APKG_FE_INSTALL_SUCCESS_PLURAL="The following packages were successfully installed:";
static const gchar *intl_APKG_FE_INSTALL_SUCCESS_SINGULAR="The following package was successfully installed:";
static const gchar *intl_APKG_FE_MENU_ENTRY_PLURAL="The following menu entries are now available:";
static const gchar *intl_APKG_FE_MENU_ENTRY_SINGULAR="The following menu entry is now available:";
static const gchar *intl_APKG_FE_NEGOTIATING="Negotiating";
static const gchar *intl_APKG_FE_PACKAGE_PROGRESS="package %d of %d";
static const gchar *intl_APKG_FE_PLEASE_WAIT="This may take a moment, please wait ...";
static const gchar *intl_APKG_FE_PREPARING="Preparing package:";
static const gchar *intl_APKG_FE_PROTOCOL_MISMATCH="Protocol version mismatched (expected %s, but got %s).nPlease ensure your installation of autopackage is correct.";
static const gchar *intl_APKG_FE_RECOMMENDED="recommended";
static const gchar *intl_APKG_FE_REMOVE_COMMAND="Remove this package by running %s from the command line.";
static const gchar *intl_APKG_FE_REMOVING="%s is removing '%s'";
static const gchar *intl_APKG_FE_SEARCHING="searching";
static const gchar *intl_APKG_FE_TESTING="Checking for %s ...";
static const gchar *intl_APKG_FE_TOTALSIZE="This installation used %s of disk space.";
static const gchar *intl_APKG_FE_VERIFY_BAD="Verify failed: one or more components are broken or not present.";
static const gchar *intl_APKG_FE_VERIFYING="Verifying package:";
static const gchar *intl_APKG_FE_VERIFY_OK="Verify successful: all required components for this package are present.";
static const gchar *intl_UNINSTALLING="Removing %s ...";

#endif /* _COMMON_STRING_H_ */
