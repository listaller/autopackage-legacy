/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _LOG_VIEW_H_
#define _LOG_VIEW_H_

#include <gtk/gtk.h>

#define GTK_TYPE_LOG_VIEW		(gtk_log_view_get_type ())
#define GTK_LOG_VIEW(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_LOG_VIEW, GtkLogView))
#define GTK_LOG_VIEW_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_LOG_VIEW, GtkLogViewClass))
#define GTK_IS_LOG_VIEW(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_LOG_VIEW))
#define GTK_IS_LOG_VIEW_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_LOG_VIEW))
#define GTK_LOG_VIEW_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_LOG_VIEW, GtkLogViewClass))


typedef struct
{
	GtkScrolledWindow parent;

	/* private */
	GtkWidget *viewport;
	GtkWidget *vbox;
	GtkWidget *popup;
	gboolean scroll;
        GdkPixbuf *busy;
        void *busy_anim;
} GtkLogView;


typedef struct
{
	GtkScrolledWindowClass parent_class;
} GtkLogViewClass;


GType gtk_log_view_get_type (void);
GtkWidget *gtk_log_view_new (void);

gchar *gtk_log_view_get_text (GtkLogView *view);
void gtk_log_view_clear (GtkLogView *view);

void gtk_log_view_prepare_scroll (GtkLogView *view);
void gtk_log_view_scroll (GtkLogView *view);

void gtk_log_view_check_begin (GtkLogView *view, gchar *text);
void gtk_log_view_check_pass (GtkLogView *view);
void gtk_log_view_check_fail (GtkLogView *view);
void gtk_log_view_check_recommended (GtkLogView *view);
void gtk_log_view_check_searching (GtkLogView *view);

void gtk_log_view_add_separator (GtkLogView *view);
void gtk_log_view_add_status (GtkLogView *view, gchar *text);
void gtk_log_view_add_status_bold (GtkLogView *view, gchar *text);
void gtk_log_view_add_fail (GtkLogView *view, gchar *text);
void gtk_log_view_add_head (GtkLogView *view, gchar *text);
void gtk_log_view_add_image (GtkLogView *view, GdkPixbuf *pixbuf);
void gtk_log_view_add_image_head (GtkLogView *view, GdkPixbuf *pixbuf, gchar *text);

void gtk_log_view_add_progress (GtkLogView *view, gchar *text);
void gtk_log_view_set_progress (GtkLogView *view, gdouble progress);


#endif /* _LOG_VIEW_H_ */
