/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _INTERACTIONS_C_
#define _INTERACTIONS_C_

#include "interactions.h"
#include "utils.h"


/* Add an interaction to the list */
GList *
interaction_add (GList *list, gchar *name, gchar *message, gchar *defaultValue, InteractType type)
{
	Interaction *ia;

	g_return_val_if_fail (name != NULL, list);
	g_return_val_if_fail (message != NULL, list);
	g_return_val_if_fail (defaultValue != NULL, list);

	ia = (Interaction *) g_new0 (Interaction, 1);
	ia->name = g_strdup (name);
	ia->message = g_strdup (message);
	ia->defaultValue = g_strdup (defaultValue);
	ia->type = type;
	ia->value = g_strdup ("");
	return g_list_append (list, ia);
}


/* Create a GUI and handle all interactions in a list */
void
interaction_execute (GList *list)
{
	
}


const gchar *
interaction_get (GList *list, gchar *name)
{
	for (; list; list = list->next)
	{
		Interaction *ia = (Interaction *) list->data;
		if (my_strcmp (name, ia->name))
			return ia->value;
	}
	return "";
}


void
interaction_free (GList *list)
{
	GList *tmp;

	if (!tmp)
		return;

	for (tmp = list; tmp; tmp = tmp->next)
	{
		Interaction *ia = (Interaction *) tmp->data;
		g_free (ia->name);
		g_free (ia->message);
		g_free (ia->defaultValue);
		if (ia->value)
			g_free (ia->value);
		g_free (ia);
	}
	g_list_free (list);
}


#endif /* _INTERACTIONS_C_ */
