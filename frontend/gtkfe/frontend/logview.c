/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include "common-strings.h"
#include "logview.h"
#include "utils.h"


typedef struct
{
	GtkWidget *box;
	GtkWidget *image;
	GtkWidget *alignment;
	GtkWidget *text;
} GtkLogEntry;

typedef struct
{
	GtkWidget *box;
	GtkWidget *image;
	GtkWidget *alignment;
	GtkWidget *text;
} GtkImageLogEntry;

typedef struct
{
	GtkWidget *hbox;
	GtkWidget *text;
	GtkWidget *progress;
} GtkLogProgressEntry;


/* Evil hack to change the GtkTextView's mouse cursor! */
struct _GtkTextWindow
{
	GtkTextWindowType type;
	GtkWidget *widget;
	GdkWindow *window;
	GdkWindow *bin_window;
};


/******* Animation stuff *******/

typedef struct _AnimateImageFromPixbufData
{
	GtkImage *image;
	GdkPixbuf *pixbuf;
	GdkPixbuf *scratch;
	int frame_width;
	int cur_frame, total_frames;
	gboolean terminate;	/* End next frame? */
} AnimateImageFromPixbufData;


static gboolean
animate_image_from_pixbuf_iterate (gpointer user_data)
{
	AnimateImageFromPixbufData *data = user_data;

	if (data->terminate)
	{
		g_object_unref (data->scratch);
		g_object_unref (data->pixbuf);
		g_object_unref (data->image);
		g_free (data);
		return FALSE;
	}
  
	gdk_pixbuf_copy_area (data->pixbuf, data->cur_frame * data->frame_width, 0,
			data->frame_width, data->frame_width, data->scratch, 0, 0);
	gtk_image_set_from_pixbuf (data->image, data->scratch);

	data->cur_frame++;
	if (data->cur_frame >= data->total_frames)
		data->cur_frame = 0;	/* Loop the animation */
	return TRUE;
}


/* Image is the GtkImage you wish to animate.
 * pixbuf stores the frames in a horizontal strip. Each frame must be a square.
 * delay is the delay between frames.
 *
 * Returns the animations struct, the animation should be terminated with animate_image_end()
 */

static AnimateImageFromPixbufData *
animate_image_from_pixbuf (GtkImage *image, GdkPixbuf *pixbuf, int delay)
{
	AnimateImageFromPixbufData *data = g_new0 (AnimateImageFromPixbufData, 1);
  
	data->image = image;
	data->pixbuf = pixbuf;
	data->frame_width = gdk_pixbuf_get_height(data->pixbuf);
	data->cur_frame = 0;
	data->total_frames = (int) (gdk_pixbuf_get_width(pixbuf) / data->frame_width);
	data->terminate = FALSE;

	data->scratch = gdk_pixbuf_new (GDK_COLORSPACE_RGB, TRUE, 8, data->frame_width, data->frame_width);
	gdk_pixbuf_copy_area (data->pixbuf, 0, 0, data->frame_width, data->frame_width, data->scratch, 0, 0);
	gtk_image_set_from_pixbuf (image, data->scratch);

	g_object_ref (image);
	g_object_ref (pixbuf);

	g_timeout_add (delay, animate_image_from_pixbuf_iterate, data);
	return data;
}


static void
animate_image_end (AnimateImageFromPixbufData *data)
{
	data->terminate = TRUE;
}


/********************************/


static void
clear_internal (GtkLogView *view, gboolean destroy_children)
{
	GList *children, *list;

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	for (list = children; list; list = list->next)
	{
		GObject *obj;
		gpointer entry, progress;

		obj = G_OBJECT (list->data);
		entry = g_object_get_data (obj, "ENTRY");
		progress = g_object_get_data (obj, "PROGRESS");
		if (entry) g_free (entry);
		if (progress) g_free (progress);
		if (destroy_children) gtk_widget_destroy (GTK_WIDGET (obj));
	}
	g_list_free (children);
}


static gboolean
on_destroy (GtkLogView *view)
{
	clear_internal (view, FALSE);
	return FALSE;
}


static gboolean
do_scroll (GtkLogView *view)
{
	GtkAdjustment *adj;

	adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (view));

        int value = gtk_minor_version <= 2 ? adj->upper - adj->page_size : adj->upper;
	gtk_adjustment_set_value (adj, value);
	return FALSE;
}


static gboolean
button_press_event (GtkWidget *widget, GdkEventButton *event, GtkLogView *view)
{
	if (event->button == 3)
	{
		gtk_menu_popup (GTK_MENU (view->popup), NULL, NULL, NULL, NULL,
			event->button, event->time);
		g_signal_stop_emission_by_name (widget, "button_press_event");
		return TRUE;
	}
	return FALSE;
}


static void
on_copy (GtkWidget *widget, GtkLogView *view)
{
	gchar *text;

	text = gtk_log_view_get_text (view);
	gtk_clipboard_set_text (gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
		text, -1);
	g_free (text);
}


static void
add_line (GtkTextBuffer *buf)
{
	GtkTextIter start, end;

	if (gtk_text_buffer_get_line_count (buf) == 1)
	{
		gchar *tmp;
		
		gtk_text_buffer_get_start_iter (buf, &start);
		gtk_text_buffer_get_end_iter (buf, &end);
		tmp = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
		if (strlen (tmp) > 0)
			gtk_text_buffer_insert (buf, &end, "\n", -1);
		g_free (tmp);
	} else
	{
		gtk_text_buffer_get_end_iter (buf, &end);
		gtk_text_buffer_insert (buf, &end, "\n", -1);
	}
}


static void
gtk_log_view_init (GtkLogView *view)
{
	GtkWidget *item;
	char *s;
	GError *error = NULL;
	
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (view), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (view), GTK_SHADOW_NONE);

	gtk_scrolled_window_set_hadjustment (GTK_SCROLLED_WINDOW (view), NULL);
	gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW (view), NULL);
	g_signal_connect (view, "destroy", G_CALLBACK (on_destroy), NULL);

	view->viewport = gtk_viewport_new (
		gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (view)),
		gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (view)));
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (view->viewport), GTK_SHADOW_NONE);
	gtk_container_add (GTK_CONTAINER (view), view->viewport);
	gtk_widget_show (view->viewport);

	view->vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (view->viewport), view->vbox);
	gtk_widget_show (view->vbox);

	gtk_widget_set_size_request (GTK_WIDGET (view), 400, 170);

/* FIXME: keep this or not? */
if (!my_strcmp (g_getenv ("GRAY"), "1")) {
gtk_viewport_set_shadow_type (GTK_VIEWPORT (view->viewport), GTK_SHADOW_IN);
gtk_widget_modify_bg (view->viewport, GTK_STATE_NORMAL, &gtk_widget_get_style (view->viewport)->white);
gtk_container_set_border_width (GTK_CONTAINER (view->vbox), 3);
}

	view->popup = gtk_menu_new ();
	item = gtk_menu_item_new_with_mnemonic (_("_Copy All Text"));
	g_signal_connect (item, "activate", G_CALLBACK (on_copy), view);
	gtk_menu_shell_append (GTK_MENU_SHELL (view->popup), item);
	gtk_widget_show_all (view->popup);
	g_signal_connect (view->viewport, "button_press_event",
		G_CALLBACK (button_press_event), view);

	/* load busy animation, we lose some encapsulation here (file path) */
	s = lookup_file ("hourglass.png");
	view->busy = gdk_pixbuf_new_from_file(s, &error);
	g_free(s);
}


GType
gtk_log_view_get_type ()
{
	static GType log_view_type = 0;

	if (!log_view_type)
	{
		static const GTypeInfo log_view_info =
		{
			sizeof (GtkLogViewClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			NULL,		/* class init */
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (GtkLogView),
			0,		/* n_preallocs */
			(GInstanceInitFunc) gtk_log_view_init,
		};

		log_view_type = g_type_register_static (GTK_TYPE_SCROLLED_WINDOW,
			"GtkLogView", &log_view_info, 0);
	}

	return log_view_type;
}


GtkWidget *
gtk_log_view_new ()
{
	return gtk_widget_new (GTK_TYPE_LOG_VIEW,
		"hadjustment", NULL,
		"vadjustment", NULL,
		NULL);
}


gchar *
gtk_log_view_get_text (GtkLogView *view)
{
	GList *children, *list;
	GString *str;
	gchar *result;

	g_return_val_if_fail (view != NULL, NULL);
	g_return_val_if_fail (GTK_IS_LOG_VIEW (view), NULL);

	str = g_string_new ("");
	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	for (list = children; list; list = list->next)
	{
		GObject *obj;
		GtkLogEntry *entry;
		GtkImageLogEntry *image_entry;
		GtkLogProgressEntry *progress;

		obj = G_OBJECT (list->data);
		entry = (GtkLogEntry *) g_object_get_data (obj, "ENTRY");
		image_entry = (GtkImageLogEntry *) g_object_get_data (obj, "IMAGEENTRY");

		progress = (GtkLogProgressEntry *) g_object_get_data (obj, "PROGRESS");
		if (entry)
		{
			GtkTextBuffer *buf;
			GtkTextIter start, end;
			gchar *text;

			buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));
			gtk_text_buffer_get_start_iter (buf, &start);
			gtk_text_buffer_get_end_iter (buf, &end);
			text = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
			g_string_append_printf (str, "%s\n", text);
			g_free (text);
		} else if (image_entry)
		{
			GtkTextBuffer *buf;
			GtkTextIter start, end;
			gchar *text;

			buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (image_entry->text));
			gtk_text_buffer_get_start_iter (buf, &start);
			gtk_text_buffer_get_end_iter (buf, &end);
			text = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
			g_string_append_printf (str, "%s\n", text);
			g_free (text);
		} else if (progress)
		{
			g_string_append_printf (str, "%s [%.0f%%]\n",
				(gchar *) gtk_label_get_text (GTK_LABEL (progress->text)),
				gtk_progress_bar_get_fraction (GTK_PROGRESS_BAR (progress->progress)) * 100.0);
		} else if (GTK_IS_SEPARATOR (obj))
			g_string_append (str, "-------------------------------\n");
		else if (GTK_IS_TEXT_VIEW (obj))
		{
			GtkTextBuffer *buf;
			GtkTextIter start, end;
			gchar *text;

			buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (obj));
			gtk_text_buffer_get_start_iter (buf, &start);
			gtk_text_buffer_get_end_iter (buf, &end);
			text = gtk_text_buffer_get_text (buf, &start, &end, FALSE);
			g_string_append_printf (str, "%s\n", text);
			g_free (text);
		}
	}
	g_list_free (children);

	result = str->str;
	g_string_free (str, FALSE);
	return result;
}


void
gtk_log_view_clear (GtkLogView *view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));
	clear_internal (view, TRUE);
}


void
gtk_log_view_prepare_scroll (GtkLogView *view)
{
	GtkAdjustment *adj;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (view));
	view->scroll = adj->value >= adj->upper - adj->page_size;
}


void
gtk_log_view_scroll (GtkLogView *view)
{
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

        if (gtk_minor_version <= 2)
        {
                /* this works around a bug in earlier versions of GTK+,
                 * which would puke if you tried to scroll to the end
                 */
                if (view->scroll)
                        gtk_idle_add ((GtkFunction) do_scroll, view);
        }
        else
        {
                do_scroll (view);
        }
}


static gboolean
text_motion_event (GtkWidget *text, GdkEventMotion *event)
{
	GTK_TEXT_VIEW (text)->mouse_cursor_obscured = FALSE;
	return FALSE;
}


static gboolean
text_enter_event (GtkWidget *text)
{
	gdk_window_set_cursor (GTK_TEXT_VIEW (text)->text_window->bin_window, NULL);
	return FALSE;
}


static void
text_view_realize (GtkWidget *textView, GtkWidget *viewport)
{
	gtk_widget_modify_base (GTK_WIDGET (textView), GTK_STATE_NORMAL,
		&(gtk_widget_get_style (viewport)->bg[GTK_STATE_NORMAL]));
}


static GtkWidget *
new_text_view (GtkLogView *view)
{
	GtkWidget *textView;
	GtkTextBuffer *buf;

	textView  = gtk_text_view_new ();
	GTK_TEXT_VIEW (textView)->mouse_cursor_obscured = FALSE;
	gtk_text_view_set_editable (GTK_TEXT_VIEW (textView), FALSE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (textView), GTK_WRAP_WORD);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (textView), FALSE);

	/* Make the textview background consistant */
	g_signal_connect (textView, "realize", G_CALLBACK (text_view_realize), view->viewport);

	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));
	gtk_box_pack_start (GTK_BOX (view->vbox), textView, FALSE, TRUE, 0);
	g_signal_connect (textView, "button_press_event",
		G_CALLBACK (button_press_event), view);

	/* Hacks to change the mouse cursor */
	gtk_widget_add_events (textView, GDK_ENTER_NOTIFY_MASK);
	g_signal_connect (textView, "motion-notify-event",
		G_CALLBACK (text_motion_event), NULL);
	g_signal_connect (textView, "enter-notify-event",
		G_CALLBACK (text_enter_event), NULL);

	gtk_widget_show (textView);
	return textView;
}


static GtkWidget *
get_last_text_view (GtkLogView *view)
{
	GList *children, *last;
	GtkWidget *textView;

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	last = g_list_last (children);
	if (last && last->data && GTK_IS_TEXT_VIEW (last->data))
		textView = GTK_WIDGET (last->data);
	else if (last && last->data && g_object_get_data (G_OBJECT (last->data), "IMAGEENTRY"))
		textView = ((GtkImageLogEntry *) g_object_get_data (G_OBJECT (last->data), "IMAGEENTRY"))->text;
	else
		textView = new_text_view (view);

	g_list_free (children);
	return textView;
}


void
gtk_log_view_check_begin (GtkLogView *view, gchar *text)
{
	GtkLogEntry *entry;
	GtkTextBuffer *buf;
	gchar *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	entry = (GtkLogEntry *) g_new0 (GtkLogEntry, 1);

	entry->box = gtk_hbox_new (FALSE, 4);
	gtk_container_set_border_width (GTK_CONTAINER (entry->box), 1);
	gtk_box_pack_start (GTK_BOX (view->vbox), entry->box, FALSE, FALSE, 0);
	
	entry->image = gtk_image_new ();
	gtk_widget_set_size_request (entry->image, 22, 22);
	gtk_box_pack_start (GTK_BOX (entry->box), entry->image, FALSE, FALSE, 0);
	view->busy_anim = animate_image_from_pixbuf (GTK_IMAGE (entry->image), view->busy, 20);
	
	
	entry->alignment = gtk_alignment_new (0.0, 0.5, 1.0, 0.0);
	gtk_box_pack_start (GTK_BOX (entry->box), entry->alignment, TRUE, TRUE, 0);

	entry->text = gtk_text_view_new ();
	GTK_TEXT_VIEW (entry->text)->mouse_cursor_obscured = FALSE;
	gtk_text_view_set_editable (GTK_TEXT_VIEW (entry->text), FALSE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (entry->text), GTK_WRAP_WORD);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (entry->text), FALSE);

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);

	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));
	gtk_text_buffer_set_text (buf, utf8txt, -1);
	gtk_container_add (GTK_CONTAINER (entry->alignment), entry->text);
	g_signal_connect (entry->text, "button_press_event",
		G_CALLBACK (button_press_event), view);

	/* Hacks to change the mouse cursor */
	gtk_widget_add_events (entry->text, GDK_ENTER_NOTIFY_MASK);
	g_signal_connect (entry->text, "motion-notify-event",
		G_CALLBACK (text_motion_event), NULL);
	g_signal_connect (entry->text, "enter-notify-event",
		G_CALLBACK (text_enter_event), NULL);

	gtk_widget_show_all (entry->box);
	g_object_set_data (G_OBJECT (entry->box), "ENTRY", entry);

	gtk_log_view_scroll (view);

	g_free (utf8txt);
}


void
gtk_log_view_check_pass (GtkLogView *view)
{
	GList *children;
	GtkLogEntry *entry;
	GtkWidget *box;
	GtkTextBuffer *buf;
	GtkTextIter end;
	GtkTextTag *tag;
	GtkTextMark *mark;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	box = (GtkWidget *) g_list_last (children)->data;
	g_list_free (children);

	entry = (GtkLogEntry *) g_object_get_data (G_OBJECT (box), "ENTRY");
	animate_image_end(view->busy_anim);
	gtk_image_set_from_stock (GTK_IMAGE (entry->image), GTK_STOCK_APPLY, GTK_ICON_SIZE_BUTTON);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));

	gtk_text_buffer_get_end_iter (buf, &end);

	mark = gtk_text_buffer_get_mark (buf, "searching-mark");
	if (mark != NULL) {
		GtkTextIter searching;
		gtk_text_buffer_get_iter_at_mark (buf, &searching, mark);
		gtk_text_buffer_delete (buf, &searching, &end);
		gtk_text_buffer_delete_mark (buf, mark);
	} else {
		gtk_text_buffer_insert (buf, &end, " ", 1);
		gtk_text_buffer_get_end_iter (buf, &end);
	}
	
	tag = gtk_text_buffer_create_tag (buf, "passed",
		"foreground", "#008200",
		"weight", PANGO_WEIGHT_BOLD,
		NULL);
	gtk_text_buffer_insert_with_tags (buf, &end, _("OK"), -1, tag, NULL);
}

void
gtk_log_view_check_searching (GtkLogView *view)
{
	GList *children;
	GtkLogEntry *entry;
	GtkWidget *box;
	GtkTextBuffer *buf;
	GtkTextIter end;
	GtkTextTag *tag;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	box = (GtkWidget *) g_list_last (children)->data;
	g_list_free (children);

	entry = (GtkLogEntry *) g_object_get_data (G_OBJECT (box), "ENTRY");
	g_assert(entry);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));
	g_assert(buf);

	gtk_text_buffer_get_end_iter (buf, &end);
	gtk_text_buffer_insert (buf, &end, " ", 1);
	gtk_text_buffer_get_end_iter (buf, &end);
	gtk_text_buffer_create_mark (buf, "searching-mark", &end,  TRUE);
	tag = gtk_text_buffer_create_tag (buf, "searching",
		"foreground", "blue",
		"weight", PANGO_WEIGHT_BOLD,
		NULL);
	gtk_text_buffer_insert_with_tags (buf, &end, _APKG(intl_APKG_FE_SEARCHING), -1, tag, NULL);
}

void
gtk_log_view_check_fail (GtkLogView *view)
{
	GList *children;
	GtkLogEntry *entry;
	GtkWidget *box;
	GtkTextBuffer *buf;
	GtkTextIter end;
	GtkTextTag *tag;
	GtkTextMark *mark;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	box = (GtkWidget *) g_list_last (children)->data;
	g_list_free (children);
	g_assert(box != NULL);

	entry = (GtkLogEntry *) g_object_get_data (G_OBJECT (box), "ENTRY");
	g_assert(entry != NULL);
	
	animate_image_end(view->busy_anim);
	gtk_image_set_from_stock (GTK_IMAGE (entry->image), GTK_STOCK_CANCEL, GTK_ICON_SIZE_BUTTON);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));

	gtk_text_buffer_get_end_iter (buf, &end);
	mark = gtk_text_buffer_get_mark (buf, "searching-mark");
	if (mark != NULL) {
		GtkTextIter searching;
		gtk_text_buffer_get_iter_at_mark (buf, &searching, mark);
		gtk_text_buffer_delete (buf, &searching, &end);
		gtk_text_buffer_delete_mark (buf, mark);
	} else {
		gtk_text_buffer_insert (buf, &end, " ", 1);
		gtk_text_buffer_get_end_iter (buf, &end);
	}

	tag = gtk_text_buffer_create_tag (buf, "failed",
		"foreground", "#C90000",
		"weight", PANGO_WEIGHT_BOLD,
		NULL);
	gtk_text_buffer_insert_with_tags (buf, &end, _("failed"), -1, tag, NULL);
}


void
gtk_log_view_check_recommended (GtkLogView *view)
{
	GList *children;
	GtkLogEntry *entry;
	GtkWidget *box;
	GtkTextBuffer *buf;
	GtkTextIter end;
	GtkTextTag *tag;
        GtkTextMark *mark;
        
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	box = (GtkWidget *) g_list_last (children)->data;
	g_list_free (children);

	entry = (GtkLogEntry *) g_object_get_data (G_OBJECT (box), "ENTRY");
	animate_image_end(view->busy_anim);
	gtk_image_set_from_stock (GTK_IMAGE (entry->image), GTK_STOCK_CANCEL, GTK_ICON_SIZE_BUTTON);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));

	gtk_text_buffer_get_end_iter (buf, &end);
        mark = gtk_text_buffer_get_mark (buf, "searching-mark");
        if (mark != NULL) {
		GtkTextIter searching;
		gtk_text_buffer_get_iter_at_mark (buf, &searching, mark);
		gtk_text_buffer_delete (buf, &searching, &end);
		gtk_text_buffer_delete_mark (buf, mark);
                gtk_text_buffer_get_end_iter (buf, &end);
	} else {
		gtk_text_buffer_insert (buf, &end, " ", 1);
		gtk_text_buffer_get_end_iter (buf, &end);
	}

	tag = gtk_text_buffer_create_tag (buf, "recommended",
		"foreground", "blue",
		"weight", PANGO_WEIGHT_BOLD,
		NULL);
	gtk_text_buffer_insert_with_tags (buf, &end, _APKG(intl_APKG_FE_RECOMMENDED), -1, tag, NULL);
}


void
gtk_log_view_add_separator (GtkLogView *view)
{
	GtkWidget *sep;
	

	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	sep = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (view->vbox), sep, FALSE, TRUE, 12);
	gtk_widget_show (sep);

	gtk_log_view_scroll (view);
}


void
gtk_log_view_add_status (GtkLogView *view, gchar *text)
{
	GtkWidget *textView;
	GtkTextBuffer *buf;
	GtkTextIter end;
	gchar *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	textView = get_last_text_view (view);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);
	add_line (buf);
	gtk_text_buffer_get_end_iter (buf, &end);
	gtk_text_buffer_insert (buf, &end, utf8txt, -1);

	gtk_log_view_scroll (view);
	
	g_free (utf8txt);
}


void
gtk_log_view_add_status_bold (GtkLogView *view, gchar *text)
{
	GtkWidget *textView;
	GtkTextBuffer *buf;
	GtkTextIter end;
	gchar *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	textView = get_last_text_view (view);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);

	add_line (buf);
	gtk_text_buffer_get_end_iter (buf, &end);

	if (!gtk_text_tag_table_lookup (gtk_text_buffer_get_tag_table (buf), "bold"))
		gtk_text_buffer_create_tag (buf, "bold",
			"weight", PANGO_WEIGHT_BOLD,
			NULL);
	gtk_text_buffer_insert_with_tags_by_name (buf, &end, utf8txt, -1, "bold", NULL);

	gtk_log_view_scroll (view);
	
	g_free (utf8txt);
}


void
gtk_log_view_add_fail (GtkLogView *view, gchar *text)
{
	GtkWidget *textView;
	GtkTextBuffer *buf;
	GtkTextIter end;
	char *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	textView = get_last_text_view (view);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));
	if (!gtk_text_tag_table_lookup (gtk_text_buffer_get_tag_table (buf), "fail"))
		gtk_text_buffer_create_tag (buf, "fail",
			"foreground", "#C90000",
			"weight", PANGO_WEIGHT_BOLD,
			NULL);

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);

	add_line (buf);
	gtk_text_buffer_get_end_iter (buf, &end);
	gtk_text_buffer_insert_with_tags_by_name (buf, &end, _("Error: "), -1, "fail", NULL);
	gtk_text_buffer_get_end_iter (buf, &end);
	gtk_text_buffer_insert (buf, &end, utf8txt, -1);

	gtk_log_view_scroll (view);
	
	g_free (utf8txt);
}


void
gtk_log_view_add_head (GtkLogView *view, gchar *text)
{
	GtkWidget *textView;
	GtkTextBuffer *buf;
	GtkTextIter end;
	char *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	textView = get_last_text_view (view);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));
	if (!gtk_text_tag_table_lookup (gtk_text_buffer_get_tag_table (buf), "head"))
		gtk_text_buffer_create_tag (buf, "head",
			"weight", PANGO_WEIGHT_BOLD,
			NULL);

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);

	add_line (buf);
	gtk_text_buffer_get_end_iter (buf, &end);
	gtk_text_buffer_insert_with_tags_by_name (buf, &end, utf8txt, -1, "head", NULL);

	gtk_log_view_scroll (view);

	g_free (utf8txt);
}


void
gtk_log_view_add_image (GtkLogView *view, GdkPixbuf *pixbuf)
{
	GtkWidget *textView;
	GtkTextBuffer *buf;
	GtkTextIter end;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	textView = get_last_text_view (view);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textView));

	add_line (buf);
	gtk_text_buffer_get_end_iter (buf, &end);

	gtk_text_buffer_insert_pixbuf (GTK_TEXT_BUFFER (buf), &end, pixbuf);

	gtk_log_view_scroll (view);
}


void
gtk_log_view_add_image_head (GtkLogView *view, GdkPixbuf *pixbuf, gchar *text)
{
	GtkLogEntry *entry;
	GtkTextBuffer *buf;
	GtkTextIter start;
	gchar *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	entry = (GtkLogEntry *) g_new0 (GtkImageLogEntry, 1);

	entry->box = gtk_hbox_new (FALSE, 4);
	gtk_container_set_border_width (GTK_CONTAINER (entry->box), 1);
	gtk_box_pack_start (GTK_BOX (view->vbox), entry->box, FALSE, FALSE, 0);

	entry->alignment = gtk_alignment_new (0.0, 0.5, 1.0, 0.0);
	gtk_box_pack_start (GTK_BOX (entry->box), entry->alignment, TRUE, TRUE, 0);

	entry->image = gtk_image_new_from_pixbuf (pixbuf);
	gtk_box_pack_start (GTK_BOX (entry->box), entry->image, FALSE, FALSE, 0);

	entry->text = gtk_text_view_new ();
	GTK_TEXT_VIEW (entry->text)->mouse_cursor_obscured = FALSE;
	gtk_text_view_set_editable (GTK_TEXT_VIEW (entry->text), FALSE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (entry->text), GTK_WRAP_WORD);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (entry->text), FALSE);

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);

	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry->text));
	gtk_text_buffer_create_tag (buf, "head",
			"weight", PANGO_WEIGHT_BOLD,
			NULL);
	gtk_text_buffer_get_start_iter (buf, &start);
	gtk_text_buffer_insert_with_tags_by_name (buf, &start, utf8txt, -1, "head", NULL);
	gtk_container_add (GTK_CONTAINER (entry->alignment), entry->text);
	g_signal_connect (entry->text, "button_press_event",
		G_CALLBACK (button_press_event), view);

	/* Hacks to change the mouse cursor */
	gtk_widget_add_events (entry->text, GDK_ENTER_NOTIFY_MASK);
	g_signal_connect (entry->text, "motion-notify-event",
		G_CALLBACK (text_motion_event), NULL);
	g_signal_connect (entry->text, "enter-notify-event",
		G_CALLBACK (text_enter_event), NULL);

	gtk_widget_show_all (entry->box);
	g_object_set_data (G_OBJECT (entry->box), "IMAGEENTRY", entry);

	gtk_log_view_scroll (view);

	g_free (utf8txt);
}


void
gtk_log_view_add_progress (GtkLogView *view, gchar *text)
{
	GtkLogProgressEntry *entry;
	GtkWidget *pad;
	gchar *msg, *utf8txt;
	
	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	gtk_log_view_prepare_scroll (view);

	if (!g_utf8_validate (text, -1, NULL))
		utf8txt = g_locale_to_utf8 (text, -1, NULL, NULL, NULL);
	else
		utf8txt = g_strdup(text);

	entry = (GtkLogProgressEntry *) g_new0 (GtkLogProgressEntry, 1);

	entry->hbox = gtk_hbox_new (FALSE, 4);
	gtk_container_set_border_width (GTK_CONTAINER (entry->hbox), 1);
	msg = g_strdup_printf ("%s:", utf8txt);
	entry->text = gtk_label_new (msg);
	gtk_box_pack_start (GTK_BOX (entry->hbox), entry->text, FALSE, FALSE, 0);
	g_free (msg);

	entry->progress = gtk_progress_bar_new ();
	gtk_box_pack_start (GTK_BOX (entry->hbox), entry->progress, TRUE, TRUE, 0);

	pad = gtk_label_new ("");
	gtk_widget_set_size_request (pad, 3, -1);
	gtk_box_pack_start (GTK_BOX (entry->hbox), pad, FALSE, TRUE, 0);

	gtk_box_pack_start (GTK_BOX (view->vbox), entry->hbox, FALSE, FALSE, 0);
	g_object_set_data (G_OBJECT (entry->hbox), "PROGRESS", entry);
	gtk_widget_show_all (entry->hbox);

	gtk_log_view_scroll (view);
}


void
gtk_log_view_set_progress (GtkLogView *view, gdouble progress)
{
	GList *children;
	GObject *last;
	GtkLogProgressEntry *entry;
	gchar *msg;

	g_return_if_fail (view != NULL);
	g_return_if_fail (GTK_IS_LOG_VIEW (view));

	children = gtk_container_get_children (GTK_CONTAINER (view->vbox));
	g_return_if_fail (children != NULL);
	
	last = (GObject *) g_list_last (children)->data;
	entry = (GtkLogProgressEntry *) g_object_get_data (last, "PROGRESS");
	g_list_free (children);

	g_return_if_fail (entry != NULL);
	
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (entry->progress), progress);
	msg = g_strdup_printf ("%.0f%%", progress * 100);
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (entry->progress), msg);
	g_free (msg);
}
