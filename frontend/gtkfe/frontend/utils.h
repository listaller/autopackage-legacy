/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <string.h>

/*
 * Standard gettext macros.
 */
#ifdef ENABLE_NLS
#  include <libintl.h>
#  undef _
#  define _(String) dgettext (GETTEXT_PACKAGE, String)
#  ifdef gettext_noop
#    define N_(String) gettext_noop (String)
#  else
#    define N_(String) (String)
#  endif
#  define _APKG(String) dgettext ("autopackage", String)
#  define _NAPKG(Singular,Plural,Count) dngettext("autopackage", Singular, Plural, Count)
#else
#  define textdomain(String) (String)
#  define gettext(String) (String)
#  define dgettext(Domain,Message) (Message)
#  define dcgettext(Domain,Message,Type) (Message)
#  define bindtextdomain(Domain,Directory) (Domain)
#  define _(String) (String)
#  define N_(String) (String)
#  define _APKG(String) (String)
#  define _NAPKG(Singular,Plural,Count) (Singular)
#endif



#define my_strcmp(x,y) (x && y && strcmp (x, y) == 0)
#define startsWith(x,y) (x && y && strncmp (x, y, strlen (y)) == 0)

#define stripNewline(str) if (strlen (str) > 0 && str[strlen (str) - 1] == '\n') str[strlen (str) - 1] = 0

void show_error (GtkWidget *parent, gchar *format, ...);
GladeXML *load_glade (gchar *filename);
gchar *lookup_file (gchar *basename);

GList *strlist_new  (gchar *str);
gchar *strlist_get  (GList *list, int index);
void   strlist_free (GList *list);

gchar *desktop_file_get_categories (gchar *filename);
gchar *desktop_file_get_name (gchar *filename);


void trace (gint level, gchar *format, ...);

#endif /* UTILS_H_ */
