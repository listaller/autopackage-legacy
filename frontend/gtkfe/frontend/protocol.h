/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include <glib.h>

/* Convenience functions for reading and writing to the autopackage frontend FIFO.
 * You should not use the protocol_recv* functions directly; use Receiver instead.
 */

/* Send a message to the FIFO. */
gboolean protocol_send (gchar *format, ...);

/* Receive all data from the FIFO and return the result as a GList.
 * Each item contains a line, without the newline character. */
GList *protocol_recv_lines (void);

/* Read all data from the FIFO but return the data as a big string. */
gchar *protocol_recv (void);

/* Read only one line from the FIFO. */
gchar *protocol_recv_first (void);


#endif /* _PROTOCOL_H_ */
