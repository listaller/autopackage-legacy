/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _INTERACTIONS_H_
#define _INTERACTIONS_H_

#include <glib.h>

/* Interaction handling */


typedef enum
{
	INTERACT_STRING,
	INTERACT_PATH
} InteractType;


typedef struct
{
	gchar *name;
	gchar *message;
	gchar *defaultValue;
	InteractType type;

	/* When interaction has finished, the value will be assigned to this variable */
	gchar *value;
} Interaction;


GList *interaction_add (GList *list, gchar *name, gchar *message,
			gchar *defaultValue, InteractType type);
void interaction_execute (GList *list);
const gchar *interaction_get (GList *list, gchar *name);
void interaction_free (GList *list);


#endif /* _INTERACTIONS_H_ */
