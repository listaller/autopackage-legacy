/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _MAIN_H_
#define _MAIN_H_

#include <glib.h>
#include <stdio.h>
#include <glade/glade.h>
#include <eggstatusicon.h>


extern gchar *exe_dir, *data_dir, *locale_dir;

extern gchar *fifoName;
extern GladeXML *xml;
extern gboolean silentMode;
extern gint exitCode;
extern gboolean finished;
extern EggStatusIcon *statusIcon;


gboolean cancel (void);
void pulse_start (gchar *widgetName);
void pulse_stop (gchar *widgetName);
gboolean pulsing (gchar *widgetName);

void set_title (gchar *format, ...);


#endif /* _MAIN_H_ */
