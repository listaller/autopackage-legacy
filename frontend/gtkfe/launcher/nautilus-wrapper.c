/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Because it's impossible to override Nautilus's shell script MIME type detection,
   we associate this program to text/x-sh. It checks whether argv[1] is an Autopackage.
   If it is, launch the Autopackage launcher.
   If it isn't, launch it in Nautilus.

   UPDATE: this program isn't necessary anymore as of GNOME 2.6, which uses the Freedesktop.org
   MIME spec.
 */

#include <glib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <gtk/gtk.h>

#include "package.h"
#include "binreloc.h"


int
main (int argc, char *argv[])
{
	PackageInfo *info;
	char *startup_id;

	if (!argv[1] || !g_file_test (argv[1], G_FILE_TEST_EXISTS))
		return 1;

	startup_id = getenv ("DESKTOP_STARTUP_ID");
	gtk_init (&argc, &argv);
	gbr_init (NULL);
	info = package_info_load (argv[1]);
	if (info) {
		package_info_free (info);
		setenv ("DESKTOP_STARTUP_ID", startup_id, TRUE);
		execlp ("autopackage-launcher-gtk", "autopackage-launcher-gtk", argv[1], NULL);
		_exit (1);

	} else {
		void *gdk;

		setenv ("DESKTOP_STARTUP_ID", startup_id, TRUE);
		gdk = dlopen (NULL, RTLD_LAZY);
		if (gdk) {
			void (*gdk_notify_startup_complete)() = dlsym (gdk,
				"gdk_notify_startup_complete");
			if (gdk_notify_startup_complete)
				gdk_notify_startup_complete ();
			dlclose (gdk);
		}
		setenv ("DESKTOP_STARTUP_ID", NULL, TRUE);
		execlp ("nautilus", "nautilus", argv[1], NULL);
		_exit (1);
	}
	return 0;
}
