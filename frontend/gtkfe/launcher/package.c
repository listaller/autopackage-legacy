/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _PACKAGE_C_
#define _PACKAGE_C_

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "package.h"
#include "binreloc.h"


#define stripNewline(str) if (strlen (str) > 0 && str[strlen (str) - 1] == '\n') str[strlen (str) - 1] = 0


static void
show_error (gchar *format, ...)
{
	GtkWidget *dialog;
	va_list ap;
	gchar *msg;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);

	dialog = gtk_message_dialog_new (NULL,
		GTK_DIALOG_MODAL,
		GTK_MESSAGE_ERROR,
		GTK_BUTTONS_OK,
		msg);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	g_free (msg);
}


PackageInfo *
package_info_load (gchar *filename)
{
	FILE *f;
	gchar buf[1024];
	int fd[2], status;
	pid_t pid;
	GString *description;
	PackageInfo *info;

	g_return_val_if_fail (filename != NULL, NULL);
	if (!g_file_test (filename, G_FILE_TEST_EXISTS))
		return NULL;

	f = fopen (filename, "r");
	if (!f)
		return NULL;

	/* Autopackages are bash scripts so check for interpreter line */
	if (!fgets (buf, sizeof (buf), f) || strcmp (buf, "#!/bin/bash\n") != 0) {
		fclose (f);
		return NULL;
	}
	fclose (f);

	pipe (fd);
	pid = fork ();
	switch (pid)
	{
	case 0: { /* child */
		gchar *extractor, *dir;

		extractor = NULL;
		dir = gbr_find_exe_dir (NULL);
		if (dir) {
			extractor = g_strdup_printf ("%s/extractinfo", extractor);
			g_free (dir);
			if (!g_file_test (extractor, G_FILE_TEST_EXISTS)) {
				g_free (extractor);
				extractor = NULL;
			}
		}

		if (!extractor) {
			dir = gbr_find_libexec_dir (LIBEXECDIR);
			extractor = g_strdup_printf ("%s/autopackage-gtk/extractinfo", dir);
			g_free (dir);
		}

		close (fd[0]);
		dup2 (fd[1], 1);
		execl ("/bin/bash", "/bin/bash", extractor, filename, NULL);
		_exit (1);
		break;
	}

	case -1: /* error */
		show_error ("System error: unable to fork a process.");
		return NULL;

	default: /* parent */
		close (fd[1]);
		info = (PackageInfo *) g_new0 (PackageInfo, 1);
		info->filename = g_strdup (filename);

		f = fdopen (fd[0], "r");
		while (!feof (f)) {
			gchar **array;

			if (!fgets (buf, sizeof (buf), f)) continue;
			stripNewline (buf);

			array = g_strsplit (buf, "=", 2);
			if (!array[0])
			{
				g_strfreev (array);
				break;
			}

			if (strcmp (array[0], "SHORTNAME") == 0)
				info->shortName = g_strdup (array[1]);
			else if (strcmp (array[0], "DISPLAYNAME") == 0)
				info->displayName = g_strdup (array[1]);
			else if (strcmp (array[0], "ROOTNAME") == 0)
				info->rootName = g_strdup (array[1]);
			else if (strcmp (array[0], "SOFTWAREVERSION") == 0)
				info->softwareVersion = g_strdup (array[1]);
			else if (strcmp (array[0], "SUMMARY") == 0)
				info->summary = g_strdup (array[1]);

			g_strfreev (array);
		}

		description = g_string_new ("");
		while (!feof (f)) {
			if (!fgets (buf, sizeof (buf), f)) continue;
			g_string_append (description, buf);
		}
		info->description = description->str;
		g_string_free (description, FALSE);

		fclose (f);
		waitpid (pid, &status, 0);

		if (!WIFEXITED (status) || WEXITSTATUS (status) != 0) {
			package_info_free (info);
			return NULL;
		}
	}

	if (!info->rootName) {
		package_info_free (info);
		return NULL;
	}

	return info;
}


void
package_info_free (PackageInfo *info)
{
	g_return_if_fail (info != NULL);

	g_free (info->shortName);
	g_free (info->displayName);
	g_free (info->rootName);
	g_free (info->softwareVersion);
	g_free (info->summary);
	g_free (info->description);
	g_free (info);
}

#endif /* _PACKAGE_C_ */
