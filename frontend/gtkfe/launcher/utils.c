/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003  Hongli Lai <h.lai@chello.nl>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdlib.h>
#include <gtk/gtk.h>
#include "utils.h"


extern gchar *exe_dir, *bin_dir, *data_dir;


void
show_error (GtkWidget *parent, gchar *format, ...)
{
	GtkWidget *dialog;
	va_list ap;
	gchar *msg;

	va_start (ap, format);
	msg = g_strdup_vprintf (format, ap);
	va_end (ap);

	dialog = gtk_message_dialog_new (NULL,
		GTK_DIALOG_MODAL,
		GTK_MESSAGE_ERROR,
		GTK_BUTTONS_OK,
		msg);
	if (parent)
		gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	g_free (msg);
}


GladeXML *
load_glade (gchar *filename)
{
	GladeXML *xml;
	gchar *file;
	GList *searchPaths = NULL, *tmp;
	gboolean found = FALSE;

	if (exe_dir)
		searchPaths = g_list_append (searchPaths,
			g_strdup_printf ("%s/../glade", exe_dir));
	searchPaths = g_list_append (searchPaths,
		g_strdup_printf ("%s/autopackage-gtk/glade", data_dir));


	for (tmp = searchPaths; tmp; tmp = tmp->next) {
		file = g_strdup_printf ("%s/%s", (gchar *) tmp->data, filename);
		if (g_file_test (file, G_FILE_TEST_EXISTS)) {
			found = TRUE;
			break;
		}
		g_free (file);
	}

	for (tmp = searchPaths; tmp; tmp = tmp->next)
		g_free (tmp->data);
	g_list_free (searchPaths);

	if (!found)
		goto err;

	xml = glade_xml_new (file, NULL, GETTEXT_PACKAGE);
	if (!xml)
		goto err;
	glade_xml_signal_autoconnect (xml);
	return xml;


	err:
	show_error (NULL,
		_("Unable to initialize the user interface file \"%s\". This frontend program must be terminated.\n\n"
		"Possible causes of this error:\n"
		"- This frontend program is improperly installed.\n"
		"- The installation is somehow damaged after it is installed.\n\n"
		"Please try reinstalling this program (autopackage-gtk).\n"
		"If that doesn't help, submit a bugreport to the Autopackage authors:\n"
		"http://autopackage.org/"),
		filename);
	exit (1);
}


void
reverse_container_children (GtkContainer *container)
{
	GList *children;
	GList *list;

	children = g_list_copy (gtk_container_get_children (container));
	for (list = children; list != NULL; list = list->next) {
		GtkWidget *widget;

		widget = (GtkWidget *) list->data;
		g_object_ref (G_OBJECT (widget));
		gtk_container_remove (container, widget);
	}

	children = g_list_reverse (children);
	for (list = children; list != NULL; list = list->next) {
		GtkWidget *widget;

		widget = (GtkWidget *) list->data;
		gtk_container_add (container, widget);

		if (G_TYPE_FROM_INSTANCE (widget) == GTK_TYPE_HBUTTON_BOX) {
			GtkButtonBox *box = GTK_BUTTON_BOX (widget);

			if (gtk_button_box_get_layout (box) == GTK_BUTTONBOX_START)
				gtk_button_box_set_layout (box, GTK_BUTTONBOX_END);
			else if (gtk_button_box_get_layout (box) == GTK_BUTTONBOX_END)
				gtk_button_box_set_layout (box, GTK_BUTTONBOX_START);
		}
	}
}


gchar *
get_frontend_filename ()
{
	gchar *file;
	GList *searchPaths = NULL, *tmp;
	gboolean found = FALSE;

	if (exe_dir)
		searchPaths = g_list_append (searchPaths,
			g_strdup_printf ("%s/../frontend", exe_dir));
	searchPaths = g_list_append (searchPaths,
		g_strdup (bin_dir));


	for (tmp = searchPaths; tmp; tmp = tmp->next) {
		file = g_strdup_printf ("%s/autopackage-frontend-gtk", (gchar *) tmp->data);
		if (g_file_test (file, G_FILE_TEST_EXISTS)) {
			found = TRUE;
			break;
		}
		g_free (file);
	}

	for (tmp = searchPaths; tmp; tmp = tmp->next)
		g_free (tmp->data);
	g_list_free (searchPaths);

	if (found)
		return file;


	show_error (NULL,
		_("The internal frontend program \"autopackage-frontend-gtk\" was not found.\n\n"
		"Possible causes of this error:\n"
		"- This frontend program is improperly installed.\n"
		"- The installation is somehow damaged after it is installed.\n"
		"Please try reinstalling this program (autopackage-frontend-gtk).\n"
		"If that doesn't help, submit a bugreport to the Autopackage authors:\n"
		"http://autopackage.org/"));
	exit (1);
}


void
free_strlist (GList *list)
{
	GList *tmp;

	if (!list) return;
	for (tmp = list; tmp; tmp = tmp->next)
		if (tmp->data) g_free (tmp->data);
	g_list_free (list);
}
