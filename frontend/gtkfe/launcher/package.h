/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _PACKAGE_H_
#define _PACKAGE_H_

#include <glib.h>

typedef struct _PackageInfo {
	gchar *filename;

	/* Package metadata */
	gchar *shortName;
	gchar *displayName;
	gchar *rootName;
	gchar *softwareVersion;
	gchar *summary;

	gchar *description;
} PackageInfo;


PackageInfo *package_info_load (gchar *filename);
void         package_info_free (PackageInfo *info);


#endif /* _PACKAGE_H_ */
