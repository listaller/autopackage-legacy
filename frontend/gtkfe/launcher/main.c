/*  Autopackage GTK+ frontend
 *  Copyright (C) 2003, 2004, 2005  Hongli Lai <hongli@plan99.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <glade/glade.h>

#include "utils.h"
#include "package.h"
#include "binreloc.h"

#define W(x) glade_xml_get_widget (xml, x)
static GladeXML *xml = NULL;
static gchar *filename = NULL;
static guint check_child_id = 0, progress_step_id = 0;
static gulong check_client_event_signal = 0;
static gchar *myAtomName = NULL;
static pid_t pid = 0;

gchar *exe_dir, *bin_dir, *data_dir;


static gboolean
check_client_event (GtkWidget *win, GdkEventClient *event)
{
	gchar *name = gdk_atom_name (event->message_type);

	if (name && strcmp (name, myAtomName) == 0
	  && event->data_format == 8 && strcmp (event->data.b, "DONE") == 0)
	{
		/* The frontend poked us; we can rest in peace now */

		gtk_widget_hide (W("main"));
		gtk_main_quit ();
		while (gtk_events_pending ())
			gtk_main_iteration ();

		if (pid > 0) {
			int status;
			waitpid (pid, &status, 0);
			if (WIFEXITED (status))
				exit (WEXITSTATUS (status));
			else
				exit (1);
		} else
			exit (0);
	}
	return FALSE;
}


static void
initialize ()
{
	PackageInfo *info;
	GtkWidget *head;
	gboolean statistics = TRUE;

	if (!g_file_test (filename, G_FILE_TEST_EXISTS)) {
		show_error (NULL, _("The given file was not found:\n%s"),
			filename);
		exit (1);
	}

	info = package_info_load (filename);
	if (!info) {
		show_error (NULL, _("The given file does not appear to "
			"be a valid Autopackage package:\n%s"),
			filename);
		exit (1);
	}

	head = W("head");

	if (info->summary && *info->summary) {
		gtk_label_set_text (GTK_LABEL (head), g_strdup_printf (
			_("%s (%s)\n"
			"Version %s\n\n"
			"%s"),
			info->displayName, info->shortName, info->softwareVersion,
			info->summary));
	} else {
		gtk_label_set_text (GTK_LABEL (head), g_strdup_printf (
			_("%s (%s)\n"
			"Version %s"),
			info->displayName, info->shortName, info->softwareVersion));
	}


	if (g_getenv ("autopackage_anonymous_reporting"))
                statistics = !strcmp(g_getenv ("autopackage_anonymous_reporting"), "true");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (W("statistics")), statistics);


	/* Reverse the button order if we're not in GNOME */
	if (!g_getenv ("GNOME_DESKTOP_SESSION_ID") || !*g_getenv ("GNOME_DESKTOP_SESSION_ID")) {
		reverse_container_children (GTK_CONTAINER (W("hbuttonbox2")));
		gtk_widget_grab_default (W("install"));
		gtk_widget_grab_focus (W("install"));
	}

	/* Check for client event from the frontend */
	if (!myAtomName)
		myAtomName = g_strdup_printf ("ApkgGtkFeDone-%d", getpid ());
	check_client_event_signal = g_signal_connect (W("main"), "client_event",
		G_CALLBACK (check_client_event), NULL);

	gtk_window_set_title (GTK_WINDOW (W("main")),
                              g_strdup_printf (_("%s - Software installation"), info->displayName));
        /*gtk_widget_set_size_request(GTK_WIDGET (W("main")), 350, -1);*/
	gtk_widget_realize (W("main"));
	gtk_widget_show (W("main"));
}


gboolean
help ()
{
	GdkColor *white;
	GtkTextBuffer *buf;
	GladeXML *helpXML;
	gchar *version, *translators;
	GString *authors;
	#define H(x) glade_xml_get_widget (helpXML, x)

	helpXML = load_glade ("quickhelp.glade");
	gtk_window_set_transient_for (GTK_WINDOW (H("quickHelp")), GTK_WINDOW (W("main")));
	gtk_window_set_transient_for (GTK_WINDOW (H("aboutBox")), GTK_WINDOW (H("quickHelp")));
	gtk_window_set_type_hint (GTK_WINDOW (H("aboutBox")), GDK_WINDOW_TYPE_HINT_DIALOG);
	white = &gtk_widget_get_style (H("quickHelpEvent"))->white;
	gtk_widget_modify_bg (H("quickHelpEvent"), GTK_STATE_NORMAL, white);

	version = g_strdup_printf ("<span size=\"x-large\" weight=\"bold\">Autopackage %s</span>",
		VERSION);
	gtk_label_set_markup (GTK_LABEL (H("versionLabel")), version);
	g_free (version);

	gtk_label_set_markup (GTK_LABEL (H("webpageLabel")),
			      "<span color=\"blue\" underline=\"low\">http://autopackage.org/</span>");

	authors = g_string_new ("Mike Hearn (mike@plan99.net)\n"
				"Hongli Lai (h.lai@chello.nl)\n"
				"Jeremy Dreese (jdreese@bucknell.edu)\n"
				"Jacob Coby (jcobync@yahoo.com)\n"
				"Curtis Knight (knightcl@fastmail.fm)\n"
				"Filippos Papadopoulos (csst9923@cs.uoi.gr)\n"
                                "Dave Wickham (dave@aagames.co.uk)\n"
                                "David Eklund (deklund@fastmail.fm)\n"
                                "David Sansome (me@davidsansome.com)\n"
                                "Christian Hammond (chipx86@chipx86.com)\n"
                                "Tiago Cogumbreiro (cogumbreiro@linus.uac.pt)\n"
                                "Johan Ersvik (jersvik@fastmail.fm)\n"
                                "Tim Ringenbach (marv_sf@users.sourceforge.net)\n"
                                "Taj Morton (tajmorton@gmail.com)\n"
                                "Isak Savo (isak.savo@gmail.com)\n"
                                "Johan Ersvik (jersvik@fastmail.fm)\n"
                                "Matthias Braun (matze@braunis.de)\n"
                                "Fredrik Tolf (fredrik@dolda2000.com)\n"
                                "Vincent Béron (vberon@mecano.gme.usherb.ca)\n"
                                "Peter Lund (firefly@diku.dk)\n"
                                "Francesco Montorsi (f18m_cpp217828@yahoo.it)\n");

	/* This should be translated into the names and email of the translator(s)
	   of the current language. Separate multiple entries with a newline character (\n) */
	translators = g_strdup (_("translator_credits"));
	if (g_str_equal (translators, "translator_credits") == FALSE) {
		authors = g_string_append_c (authors, '\n');
		authors = g_string_append (authors, _("Translated by:\n"));
		authors = g_string_append (authors, translators);
	}
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (H("authorsTextView")));
	gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buf), authors->str, authors->len);

	g_string_free (authors, TRUE);
	g_free (translators);
	
	gtk_widget_show (H("quickHelp"));
	g_object_unref (helpXML);
	return FALSE;
}


static gboolean
progress_step (gpointer data)
{
	GtkWidget *progress;

	progress = W("progress");
	gtk_progress_bar_pulse (GTK_PROGRESS_BAR (progress));
	return TRUE;
}


static gboolean
check_child (gpointer data)
{
	pid_t pid = GPOINTER_TO_INT (data);
	int status, result;

	result = waitpid (pid, &status, WNOHANG);
	if (result == -1) {
		/* Error? */
		g_source_remove (progress_step_id);
		g_source_remove (check_child_id);
		g_signal_handler_disconnect (W("main"), check_client_event_signal);

		kill (pid, SIGTERM);
		show_error (W("main"), _("An unknown error has occured."));
		waitpid (pid, &status, 0);
		exit (1);

	} else if (result == pid) {
		/* Child exited */
		g_source_remove (progress_step_id);
		g_source_remove (check_child_id);
		g_signal_handler_disconnect (W("main"), check_client_event_signal);

		if (WIFEXITED (status)) 	{
			/* Normal exit */
			if (WEXITSTATUS (status) == 0) {
				GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (W("main")),
					GTK_DIALOG_MODAL,
					GTK_MESSAGE_INFO,
					GTK_BUTTONS_OK,
					_("Installation successful."));
				gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
				gtk_dialog_run (GTK_DIALOG (dialog));

				exit (0);

			} else if (WEXITSTATUS (status) == 1)
				show_error (W("main"),
					_("Unable to extract the package's contents."));

			else if (WEXITSTATUS (status) == 2)
				show_error (W("main"),
					_("Integrity check failed. This package is damaged.\n"
					"- If you have obtained it via the Internet, please download it again.\n"
					"- Otherwise, or if this problem keeps occuring, contact this package's "
					"packager/vendor."));

			else if (WEXITSTATUS (status) == 13)
				/* User pressed Cancel in autosu */
				exit (0);

			else
				show_error (W("main"),
					_("An unknown error has occured (exit status %d)."),
					WEXITSTATUS (status));

			exit (1);

		} else if (WIFSIGNALED (status)) {
			/* Child crashed? */
			show_error (W("main"),
				_("The installation has failed because the package "
				"has encountered a system error.\n"
				"(Process %d caught signal %d (%s))"),
				pid, WTERMSIG (status),
				g_strsignal (WTERMSIG (status)));
			exit (1);
		}
	}
	return TRUE;
}


void
install_click ()
{
	/* Launch the package using 'package install'.
	   Create a timeout for the progress bar pulse and
	   another one for checking the child's exit status
	   and whether the frontend has been launched yet. */

	gchar *frontend = get_frontend_filename ();
	gboolean statistics = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (W("statistics")));
	gchar *package_path;

	pid = fork ();
	switch (pid) {
	case 0:
		/* Child */
		setenv ("AUTOPACKAGE_GTKFE_ATOM", myAtomName, TRUE);
		setenv ("AUTOPACKAGE_GTKFE_XID",
			g_strdup_printf ("%d", (int) GDK_WINDOW_XWINDOW (W("main")->window)),
			TRUE);
		setenv ("AUTOPACKAGE_FRONTEND", frontend, TRUE);
		setenv ("_autopackage_stats_override", statistics ? "true" : "false", TRUE);
		if (exe_dir)
		{
			package_path = g_build_path ("/", exe_dir, "package", NULL);
			if (package_path)
			{
				execlp (package_path, package_path, "install", filename, NULL);
				g_free (package_path);
			}
		}
		/* 1 = "Could not extract package contents" */
		_exit (1);

	case -1:
		show_error (W("main"),
			_("There is too little free memory to launch this package. "
			"Please close unnecessary programs and try again."));
		break;

	default:
		gtk_widget_set_sensitive (W("install"), FALSE);
		gtk_widget_set_sensitive (W("button2"), FALSE);
		gtk_widget_set_sensitive (W("statistics"), FALSE);
		progress_step_id = gtk_timeout_add (10, progress_step, NULL);
                gtk_progress_bar_set_pulse_step(GTK_PROGRESS_BAR(W("progress")), 0.005);
		check_child_id = gtk_timeout_add (100, check_child, GINT_TO_POINTER (pid));
		break;
	}
}


gboolean
cancel ()
{
	if (check_child_id) {
		int status;

		/* The installation is still running. Kill the child. */
		kill (pid, SIGTERM);
		waitpid (pid, &status, 0);
	}

	gtk_main_quit ();
	return TRUE;
}


int
main (int argc, char *argv[])
{
	gchar *locale_dir;

	gbr_init (NULL);
	exe_dir    = gbr_find_exe_dir  (NULL);
	bin_dir    = gbr_find_bin_dir  (BINDIR);
	data_dir   = gbr_find_data_dir (DATADIR);
	locale_dir = gbr_find_locale_dir (LOCALEDIR);
#ifdef ENABLE_NLS
	/* Not *really* neccessary, since we're using dcgettext, but it provides a nice fallback */
	bindtextdomain (GETTEXT_PACKAGE, locale_dir);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	g_free (locale_dir);
    
	gtk_init (&argc, &argv);


	/* Parse arguments */
	if (argv[1]) {
		if (strcmp (argv[1], "--version") == 0 || strcmp (argv[1], "-V") == 0) {
			g_print (_("Autopackage GTK+ frontend version %s\n"), VERSION);
			return 0;
		}

		xml = load_glade ("launcher.glade");
		filename = argv[1];
		initialize ();
		gtk_main ();
		return 0;

	} else {
		g_print (_("Usage: autopackage-launcher-gtk <FILENAME>\n"));
		return 1;
	}
}
