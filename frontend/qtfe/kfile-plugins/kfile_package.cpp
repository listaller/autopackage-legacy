#include "kfile_package.h"


#include <kgenericfactory.h>
#include <kstandarddirs.h>
#include <kwordwrap.h> 
#include <qapplication.h>
#include <qregexp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/wait.h> 
#include <unistd.h>

typedef KGenericFactory<PackagePlugin> packageFactory;
K_EXPORT_COMPONENT_FACTORY(kfile_package, packageFactory( "kfile_package" ))


PackagePlugin::PackagePlugin(QObject *parent, const char *name, const QStringList &args)
: KFilePlugin(parent, name, args)
{
	KFileMimeTypeInfo* info = addMimeTypeInfo( "application/x-autopackage" );
	
	KFileMimeTypeInfo::GroupInfo* group = addGroupInfo(info, "packageInfo", i18n("Autopackage information"));
	KFileMimeTypeInfo::ItemInfo* item = addItemInfo(group, "Name", i18n("Name"), QVariant::String);
	item = addItemInfo(group, "Version", i18n("Version"), QVariant::String);
	item = addItemInfo(group, "Summary", i18n("Summary"), QVariant::String);
	setAttributes(item, KFileMimeTypeInfo::MultiLine);
	
	group = addGroupInfo(info, "technical", i18n("Technical details"));
	item = addItemInfo(group, "Rootname", i18n("Root name"), QVariant::String);
	item = addItemInfo(group, "Shortname", i18n("Short name"), QVariant::String);
}
       

bool PackagePlugin::readInfo( KFileMetaInfo& info, uint /*what*/)
{
	int fd[2], status;
	pid_t pid;
	char buf[1024];
	FILE *f;
	QString fileData;
	
	if (info.path().isNull())  // We can only deal with local files
		return false;
	
	QString helperPath = locate("data", "autopackage-frontend-qt/extractinfo");
	if (helperPath.isNull())  // Helper script not found
		return false;
	
	pipe (fd);
	pid = fork ();
	switch (pid)
	{
	case 0: // child
		close (fd[0]);
		dup2 (fd[1], 1);
		execl ("/bin/bash", "/bin/bash", helperPath.latin1(), info.path().latin1(), NULL);
		_exit (1);
		break;

	case -1: // error
		return false;

	default: // parent
		close (fd[1]);

		f = fdopen (fd[0], "r");
		while (!feof (f))
		{
			if (!fgets (buf, sizeof (buf), f)) continue;
			fileData += buf;
		}
		
		fclose (f);
		waitpid (pid, &status, 0);
	}
	
	QStringList lines = QStringList::split('\n', fileData);
	QString displayName, rootName, shortName, softwareVersion, summary, description;
	bool des = false;
	for ( QStringList::Iterator it = lines.begin(); it != lines.end(); ++it )
	{
		if (des)
		{
			description += (*it);
			continue;
		}
		
		QRegExp re("([A-Z]+)=(.*)");
		if (re.search(*it) == -1)
		{
			des = true;
			continue;
		}
		if (re.cap(1) == "DISPLAYNAME")
			displayName = re.cap(2);
		else if (re.cap(1) == "ROOTNAME")
			rootName = re.cap(2);
		else if (re.cap(1) == "SHORTNAME")
			shortName = re.cap(2);
		else if (re.cap(1) == "SOFTWAREVERSION")
			softwareVersion = re.cap(2);
		else if (re.cap(1) == "SUMMARY")
			summary = re.cap(2);
	}
	
	QRect r(0, 0, 300, -1);
	QFontMetrics m = QApplication::fontMetrics();
	KWordWrap* summaryW = KWordWrap::formatText(m, r, 0, summary, -1);
	
	KFileMetaInfoGroup group = appendGroup(info, "packageInfo");
	appendItem(group, "Name", displayName);
	appendItem(group, "Version", softwareVersion);
	appendItem(group, "Summary", summaryW->wrappedString());
	
	group = appendGroup(info, "technical");
	appendItem(group, "Shortname", shortName);
	appendItem(group, "Rootname", rootName);
	
	delete summaryW;
	
	return true;
}

