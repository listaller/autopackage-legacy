#ifndef __KFILE_PACKAGE_H__
#define __KFILE_PACKAGE_H__

#include <kfilemetainfo.h>


class PackagePlugin: public KFilePlugin
{
	Q_OBJECT
	
public:
	PackagePlugin( QObject *parent, const char *name, const QStringList& args );	
	virtual bool readInfo( KFileMetaInfo& info, uint what);
};



#endif
