[Meta]
ShortName: autopackage-qt
SoftwareVersion: 1.4.2
DisplayName: Autopackage Qt Graphical User Interface
RootName: @autopackage.org/autopackage-qt:$SOFTWAREVERSION
Summary: A graphical (QT) frontend for installing Autopackage packages.
Packager: David Sansome <me@davidsansome.com>
Maintainer: David Sansome <me@davidsansome.com>
License: GNU General Public License Version 2
PackageDesktop: autopackage-frontend-qt.desktop
AutopackageTarget: 1.4
URL: http://autopackage.org/
Repository: http://ftp.sunsite.dk/projects/autopackage/packages/autopackage-gtk.xml
PackageFileName: $SHORTNAME-$SOFTWAREVERSION.package

[Description]
Provides a Qt based frontend for installing autopackages.


[BuildPrepare]
make clean || true
if [[ "$APKG_BUILD_SKIP_CONFIGURE" != "1" ]]; then
	if locateCommand apgcc; then
		CC=apgcc CXX=apg++ ./configure || exit 1
	else
		./configure || exit 1
	fi
fi
if [[ "$APKG_BUILD_SKIP_MAKE" != "1" ]]; then
	export APBUILD_BOGUS_DEPS='X11 Xext pthread m libXrender'
	make || exit 1
	if [ -e ./kfile-plugins/libkfile_package.so ]; then
		mkdir -p $build_root/kfile-plugins
		cp ./kfile-plugins/libkfile_package.so $build_root/kfile-plugins/kfile_package.so
		
		cp ./kfile-plugins/kfile_package.desktop ./kfile-plugins/kfile_package.la \
		./kfile-plugins/extractinfo $build_root/kfile-plugins/
	fi
fi
cp -v bin/autopackage-frontend-qt* po/*.gmp src/*.desktop "$build_root"

# build qt manager
cd ../manager/

make clean || true
./configure --disable-frontend-gtk --enable-backend-autopackage \
--enable-frontend-qt CC=apgcc CXX=apg++
rm -f frontend-qt/autopackage-manager-qt
make

# add qt manager files
cd frontend-qt
cp autopackage-manager-qt "$build_root"
cp autopackage-manager-qt.desktop "$build_root"

if [[ "$(uname -m)" == "x86_64" ]]; then
	if [[ -e "$source_dir/x86/$SHORTNAME-$SOFTWAREVERSION.package" ]]; then
		export HYBRID_PACKAGE="$source_dir/x86/$SHORTNAME-$SOFTWAREVERSION.package"
	else
		red; outn "WARNING: "; normal;
		out "You're building on a 64 bit machine but $source_dir/x86/$SHORTNAME-$SOFTWAREVERSION.package was not found. It's needed to create a Hybrid package.";
	fi
fi

[BuildUnprepare]
# Nothing to do here

[Imports]
echo '*' | import


[Prepare]
require @trolltech.com/qt 3.1
require @autopackage.org/autopackage-gtk 1.2


[Install]
installExe autopackage-frontend-qt autopackage-manager-qt

copyFile de.gmp "$PREFIX/share/locale/de/LC_MESSAGES/autopackage-qt.mo"
copyFile es.gmp "$PREFIX/share/locale/es/LC_MESSAGES/autopackage-qt.mo"

if [ -e kfile-plugins/kfile_package.so ]; then
	# $PREFIX is already a KDE dir, becase _PLATFOROM
	# sets PREFIX when it detects a KDE app/lib in payload
	# so, no need to updateEnv KDEDIRS
	copyFiles kfile-plugins/kfile_package.la kfile-plugins/kfile_package.so "$PREFIX/lib/kde3"
	copyFiles kfile-plugins/kfile_package.desktop "$PREFIX/share/services"
	copyFiles kfile-plugins/extractinfo "$PREFIX/share/apps/autopackage-frontend-qt"
fi

installMimeDesktop autopackage-frontend-qt.desktop
installMenuItem "System Tools" autopackage-manager-qt.desktop


[Uninstall]
uninstallFromLog

