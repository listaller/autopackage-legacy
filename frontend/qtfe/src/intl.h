#include <qstring.h>
// Wrappers around gettext that return QStrings

QString gtr(const char* id);
QString ngtr(const char* singularId, const char* pluralId, int n);
