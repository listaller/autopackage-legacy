/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qlistview.h>
#include <qprogressbar.h>


class StatusItem : public QListViewItem
{
public:
	StatusItem(QListView* parent);
	void paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align);
	void paintFocus(QPainter* p, const QColorGroup& cg, const QRect& r);
	void setup();
	int width(const QFontMetrics& fm, const QListView* lv, int c) const;
	int rtti() const { return 1002; }
	
public slots:
	void showProgressBar(bool showProgressBar);
	void setTotalSteps(int totalSteps);
	void setProgress(int progress);
	void setColor(QColor c);
	
private:
	QProgressBar* bar;
	bool showBar;
	QColor colour;
};




class HeaderItem : public QListViewItem
{
public:
	HeaderItem(QListView* parent);
	void paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align);
	void paintFocus(QPainter* p, const QColorGroup& cg, const QRect& r);
	void setup();
	int width(const QFontMetrics& fm, const QListView* lv, int c) const;
	int rtti() const { return 1003; }
};

