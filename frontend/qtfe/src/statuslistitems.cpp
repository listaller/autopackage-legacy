/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "statuslistitems.h"
#include <qpainter.h>
#include <qpixmap.h>
#include <qfont.h>
#include <qapplication.h>

#define COLUMN1WIDTH 150

StatusItem::StatusItem(QListView* parent)
 : QListViewItem(parent, parent->lastItem())
{
	widthChanged(1);
	bar = new QProgressBar(0);
	bar->hide();
	bar->resize(COLUMN1WIDTH, height());
	showBar = false;
}


void StatusItem::paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align)
{
	p->fillRect(0, 0, width, height(), cg.base());
	
	if (column != 1)
	{
		QListViewItem::paintCell(p, cg, column, width, align);
		return;
	}
	
	if (showBar)
	{
		QPixmap barPixmap = QPixmap::grabWidget(bar, 0, 0, COLUMN1WIDTH, height());
		p->drawPixmap(0, 0, barPixmap);
	}
	else
	{
		int pixmapWidth = (pixmap(1) == NULL) ? 0 : pixmap(1)->width();
		if (pixmapWidth > 0)
			p->drawPixmap(listView()->itemMargin(), 0, *pixmap(1));
		QFont font = p->font();
		font.setBold(true);
		p->setFont(font);
		p->setPen(colour);
		p->drawText(pixmapWidth + listView()->itemMargin()*2, listView()->itemMargin(), width, QFontMetrics(font).height(), Qt::AlignLeft, text(1));
	}
}

void StatusItem::paintFocus(QPainter* p, const QColorGroup& cg, const QRect& r)
{
	(void) p, (void) cg, (void) r;
}

int StatusItem::width(const QFontMetrics& fm, const QListView* lv, int c) const
{
	(void) fm, (void) lv;

	return (c == 0) ? listView()->viewport()->width() - COLUMN1WIDTH : COLUMN1WIDTH;
}

void StatusItem::setup()
{
	setHeight(16);
}

void StatusItem::setTotalSteps(int totalSteps)
{
	bar->setTotalSteps(totalSteps);
	repaint();
}

void StatusItem::setProgress(int progress)
{
	bar->setProgress(progress);
	repaint();
}

void StatusItem::setColor(QColor c)
{
	colour = c;
}

void StatusItem::showProgressBar(bool showProgressBar)
{
	showBar = showProgressBar;
	repaint();
}






HeaderItem::HeaderItem(QListView* parent)
 : QListViewItem(parent, parent->lastItem())
{
}


void HeaderItem::paintCell(QPainter* p, const QColorGroup& cg, int column, int width, int align)
{
	(void) align;

	p->fillRect(0, 0, width, height(), cg.base());
	if (column != 0)
		return;
	
	QFont boldFont = p->font();
	boldFont.setBold(true);
	p->setFont(boldFont);
	p->drawText(listView()->itemMargin(), listView()->itemMargin(), width, QFontMetrics(boldFont).height(), Qt::AlignLeft, text(0));
	
	int textWidth = QFontMetrics(boldFont).width(text(0));
	p->fillRect(0, height() - 4 - listView()->itemMargin(), textWidth-10, 4, cg.highlight());
	
	QColor ca = cg.highlight();
	QColor cb = cg.base();
	// Taken from KPixmapEffect::gradient
	int rDiff, gDiff, bDiff;
	int rca, gca, bca /*, rcb, gcb, bcb*/;
	
	rDiff = (/*rcb = */ cb.red())   - (rca = ca.red());
	gDiff = (/*gcb = */ cb.green()) - (gca = ca.green());
	bDiff = (/*bcb = */ cb.blue())  - (bca = ca.blue());
	
	register int rl = rca << 16;
	register int gl = gca << 16;
	register int bl = bca << 16;
	
	int rcdelta = ((1<<16) / 20) * rDiff;
	int gcdelta = ((1<<16) / 20) * gDiff;
	int bcdelta = ((1<<16) / 20) * bDiff;
	for( int x = textWidth-10; x < textWidth+10; x++)
	{
		rl += rcdelta;
		gl += gcdelta;
		bl += bcdelta;
	
		p->setPen(QColor(rl>>16, gl>>16, bl>>16));
		p->drawLine(x, height() - 4 - listView()->itemMargin(), x, height() - listView()->itemMargin() - 1);
	}
}

void HeaderItem::paintFocus(QPainter* p, const QColorGroup& cg, const QRect& r)
{
	(void) p, (void) cg, (void) r;
}

int HeaderItem::width(const QFontMetrics& fm, const QListView* lv, int c) const
{
	(void) fm, (void) lv;

	return (c == 0) ? listView()->viewport()->width() - COLUMN1WIDTH : COLUMN1WIDTH;
}

void HeaderItem::setup()
{
	setHeight(qApp->fontMetrics().height() + listView()->itemMargin()*3 + 4);
}

