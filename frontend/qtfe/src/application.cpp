#include "application.h"

Application::Application(int argc, char* argv[])
 : QApplication(argc, argv)
{
	hideAtom = XInternAtom(qt_xdisplay(), "QTFE_HIDE", False);
}

bool Application::x11EventFilter(XEvent* ev)
{
	if ((ev->xclient.message_type == hideAtom) && (ev->xclient.type == ClientMessage))
	{
		mainWidget()->hide();
		return true;
	}
	return false;
}

