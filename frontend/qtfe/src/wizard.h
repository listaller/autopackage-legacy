/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef WIZARD_H
#define WIZARD_H

#include "wizardbase.h"
#include "autopackagepipe.h"
#include "statuslistitems.h"
#include <qlistbox.h>
#include <qprocess.h>

/*struct Interaction
{
	QString type;
	QString name;
	QString defaultVal;
	QString label;
};*/

class MenuItem : public QListBoxText
{
public:
	MenuItem(QListBox* listbox, QString name, QString p)
	 : QListBoxText(listbox, name) {path = p;}
	
	QString path;
};

class Wizard : public WizardBase
{
	Q_OBJECT

public:
	Wizard(QString pipePath, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
	~Wizard();
	
	void customEvent(QCustomEvent* event);

public slots:
	virtual void previousPressed();
	virtual void nextPressed();
	virtual void cancelPressed();
	virtual void shortcutPressed();
	virtual void kdeDirResult();
	
	virtual void reject() {}

private slots:
	virtual void incProgress();
	
private:
	QString env(QString key);
	void parseDesktopFile(QString path);
	void mkdirRecursive(QString dir);
	
	AutopackagePipe* pipe;
	bool donePrepareHeader;
	StatusItem* lastStatusItem;
	
	bool failed;
	bool finished;
	int sectionCount;
	
	QPixmap failIcon;
	QPixmap passIcon;
	QPixmap recommendedIcon;
	QPixmap searchingIcon;
	
	QProcess* kdeDirProc;
	QString kdeDir;
	
	QTimer* busyTimer;
	QString currentName;
	
	//QValueList<struct Interaction> interactionQueue;
};

#endif

