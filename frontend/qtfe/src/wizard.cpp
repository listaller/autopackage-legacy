/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "wizard.h"
#include "../data/data.h"
#include <stdlib.h>
#include <qlabel.h>
#include <qtextedit.h>
#include <qwidgetstack.h>
#include <qmessagebox.h>
#include <qpushbutton.h>
#include <qheader.h>
#include <qfiledialog.h>
#include <qinputdialog.h>
#include <qlistbox.h>
#include <qapplication.h>
#include <qtextbrowser.h>
#include <qtoolbutton.h>
#include <qtimer.h>
#include <libintl.h>

#include <unistd.h>
#include <sys/types.h>

#include <X11/X.h> 
#include <X11/Xlib.h>

#include "intl.h"

Wizard::Wizard(QString pipePath, QWidget* parent, const char* name, bool modal, WFlags fl)
: WizardBase(parent,name, modal,fl)
{
	shortcutButton->setText(gtr("Create desktop shortcut"));
	descriptionLabel->setText(gtr("<b>Description:</b>"));
	nextLabel->setText(gtr("Please click \"Next\" to continue"));
	pleaseWaitLabel->setText(gtr("Please wait while the software is installed..."));
	nextButton->setText(gtr("Next >"));
	cancelButton->setText(gtr("Cancel"));
	
	donePrepareHeader = false;
	sectionCount = 0;
	failed = false;
	finished = false;
	
	passIcon.convertFromImage(qembed_findImage("pass"));
	failIcon.convertFromImage(qembed_findImage("fail"));
	recommendedIcon.convertFromImage(qembed_findImage("recommended"));
	searchingIcon.convertFromImage(qembed_findImage("searching"));
	QPixmap icon = qembed_findImage("kpackage");
	appIcon->setPixmap(icon);
	
	setCaption(gtr("Installing %1").arg(env("DISPLAYNAME")));
	appNameLabel->setText("<b>" + env("DISPLAYNAME") + "</b>");
	summaryLabel->setText(env("SUMMARY"));
	
	/* 1.0 packages will have DESCRIPTION. In 1.2 DESCRIPTION was obsoleted. */
	if (env("DESCRIPTION").isEmpty()) {
		// some packages have a period at the end of SUMMARY but they really shouldn't *shrug*
		if (env("SUMMARY").right(1)==".")
			appDescriptionBox->setText(env("SUMMARY"));
		else
			appDescriptionBox->setText(env("SUMMARY")+".");
	}
	else
		appDescriptionBox->setText(env("DESCRIPTION"));
	
	listView->setSorting(-1);
	listView->header()->hide();
	listView->setHScrollBarMode(QScrollView::AlwaysOff);
	listView->setVScrollBarMode(QScrollView::AlwaysOn);
	listView->setSelectionMode(QListView::NoSelection);
	
	pipe = new AutopackagePipe(pipePath, this);
	
	busyTimer = new QTimer( parent );
	connect(busyTimer, SIGNAL(timeout()), this, SLOT(incProgress()));
	
	kdeDirProc = new QProcess(this);
	connect(kdeDirProc, SIGNAL(readyReadStdout()), SLOT(kdeDirResult()));
	kdeDirProc->addArgument("kde-config");
	kdeDirProc->addArgument("--prefix");
	if (kdeDirProc->start() == FALSE)
	{
		// kde-config wasn't found in $PATH, so let's look for it
		QStringList probableLocations;
		probableLocations.append("/opt/kde3");
		probableLocations.append("/opt/kde");
		probableLocations.append("/usr");
		probableLocations.append("/usr/local");
		
		for ( QStringList::Iterator it = probableLocations.begin(); it != probableLocations.end(); ++it )
		{
			if (QFile::exists((*it) + "/bin/kde-config"))
			{
				kdeDir = (*it);
				break;
			}
		}
	}
	
	show();
	
	//skip the intro-screen if we have nothing to show
	if(env("DISPLAYNAME").isEmpty())
	{
		installStack->raiseWidget(1);
		cancelButton->hide();
		nextButton->setEnabled(false);
		pipe->start();
	}
	
	// Tell the launcher to hide itself
	if (getenv("AUTOPACKAGE_QTFE_XID") != NULL)
	{
		Window w = QString(getenv("AUTOPACKAGE_QTFE_XID")).toInt();
		Atom a = QString(getenv("AUTOPACKAGE_QTFE_ATOM")).toInt();
		XEvent ev;
		ev.xclient.type = ClientMessage;
		ev.xclient.display = qt_xdisplay();
		ev.xclient.window = w;
		ev.xclient.message_type = a;
		ev.xclient.format = 32;
		XSendEvent(qt_xdisplay(), w, False, 0L, &ev);
	}
}

Wizard::~Wizard()
{
}

void Wizard::kdeDirResult()
{
	while (kdeDirProc->canReadLineStdout())
		kdeDir += kdeDirProc->readLineStdout();
	
}

void Wizard::previousPressed()
{
	int currentId = installStack->id(installStack->visibleWidget());
	
	installStack->raiseWidget(--currentId);
}

void Wizard::nextPressed()
{
	if(finished)
	{
		pipe->writeLine("OK");
		accept();
	}
	
	int currentId = installStack->id(installStack->visibleWidget());
	installStack->raiseWidget(++currentId);
	
	switch (currentId)
	{
	case 1:
		pipe->start();
		cancelButton->hide();
		nextButton->setEnabled(false);
		break;
	case 2:
		//display the summary page
		nextButton->setText(gtr("Finish"));
		nextButton->setEnabled(true);
		finished = true;
		break;
	}
}

void Wizard::cancelPressed()
{
	pipe->cancelled = true;
	pipe->start();
	//wait before closing the frontend to give the backend time to receive
	// the UHOH
	QTimer* closeDelayTimer = new QTimer(this);
	closeDelayTimer->start(50);
	connect(closeDelayTimer,SIGNAL(timeout()),this,SLOT(accept()));
}

void Wizard::shortcutPressed()
{
	MenuItem* mi = (MenuItem*) menuList->selectedItem();
	
	QFileInfo info(mi->path);
	if (!info.exists())
		return;
	
	QString destDir;
	if (!kdeDir.isEmpty())
	{
		if (!QFileInfo(kdeDir, ".").isWritable())
			destDir = QDir::homeDirPath() + "/Desktop";
		else
		{
			destDir = kdeDir + "/share/apps/kdesktop/Desktop";
			if (!QDir(destDir).exists())
				QMessageBox::warning(this, "Shortcut created", "You may need to restart KDE for the shortcut to appear on the desktop.", QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		}
	}
	else
		destDir = QDir::homeDirPath() + "/Desktop";
	
	if (!QDir(destDir).exists())
		mkdirRecursive(destDir);
	
	QFile source(mi->path);
	QFile dest(destDir + "/" + info.fileName());
	source.open(IO_ReadOnly);
	dest.open(IO_WriteOnly | IO_Truncate);
	QDataStream destStream(&dest);
	QByteArray data = source.readAll();
	destStream.writeRawBytes(data.data(), data.size());
	source.close();
	dest.close();
}

void Wizard::mkdirRecursive(QString dir)
{
	QStringList pathComponents = QStringList::split("/", dir);
	QString path;
	QDir d;
	for ( QStringList::Iterator it = pathComponents.begin(); it != pathComponents.end(); ++it )
	{
		path += "/" + (*it);
		d.setPath(path);
		if (!d.exists())
			d.mkdir(path);
	}
}

QString Wizard::env(QString key)
{
	char* value = getenv(key.latin1());
	if (value == NULL)
		return QString::null;
	return QString(value);
}

void Wizard::customEvent(QCustomEvent* event)
{
	if (event->type() != 2000)
	{
		WizardBase::customEvent(event);
		return;
	}
	
	AutopackageEvent* e = (AutopackageEvent*) event;
	switch (e->apType)
	{
		case AutopackageEvent::BadProtocol:
			QMessageBox::critical(this, gtr("Protocol mismatch"), gtr("The versions of Autopackage and this frontend do not match.  Try reinstalling Autopackage."), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
			accept();
			break;
		case AutopackageEvent::Hello:
			sectionCount++;
			break;
		case AutopackageEvent::Identify:
		{
			currentName = e->name;
			if (e->context == "prep")
			{
				if (donePrepareHeader)
					break;
				HeaderItem* item = new HeaderItem(listView);
				item->setText(0, gtr("Preparing packages for installation..."));
				listView->ensureItemVisible(item);
				donePrepareHeader = true;
			}
			else if (e->context == "install")
			{
				HeaderItem* item = new HeaderItem(listView);
				item->setText(0, gtr("Installing %1").arg(e->name));
				listView->ensureItemVisible(item);
			}
			else if (e->context == "verify")
			{
				setCaption(gtr("Verifying %1").arg(currentName));
				summaryLabel->setText(gtr("Verifying %1").arg(currentName));
				HeaderItem* item = new HeaderItem(listView);
				item->setText(0, gtr("Verifying %1").arg(e->name));
				listView->ensureItemVisible(item);
			}
			break;
		}
		case AutopackageEvent::Test:
			switch (e->testType)
			{
			case 0:
				lastStatusItem = new StatusItem(listView);
				lastStatusItem->setText(0, gtr("Checking %1").arg(e->name));
				break;
			case 1:
				lastStatusItem->setText(1, gtr("Passed"));
				lastStatusItem->setPixmap(1, passIcon);
				break;
			case 2:
				lastStatusItem->setText(1, gtr("Failed"));
				lastStatusItem->setColor(QColor(255, 0, 0));
				lastStatusItem->setPixmap(1, failIcon);
				break;
			case 3:
				lastStatusItem->setText(1, gtr("Recommended"));
				lastStatusItem->setPixmap(1, recommendedIcon);
				break;
			case 4:
				lastStatusItem->setText(1, gtr("Searching..."));
				lastStatusItem->setPixmap(1, searchingIcon);
				break;
			}
			listView->ensureItemVisible(lastStatusItem);
			break;
		case AutopackageEvent::StatusLine:
			lastStatusItem = new StatusItem(listView);
			lastStatusItem->setText(0, e->name);
			listView->ensureItemVisible(lastStatusItem);
			break;
		case AutopackageEvent::Fail:
			lastStatusItem = new StatusItem(listView);
			lastStatusItem->setText(0, e->name);
			lastStatusItem->setText(1, gtr("Failed"));
			lastStatusItem->setColor(QColor(255, 0, 0));
			lastStatusItem->setPixmap(1, failIcon);
			listView->ensureItemVisible(lastStatusItem);
			failed = true;
			break;
		case AutopackageEvent::ProgressBar:
			if (lastStatusItem->text(0) != e->name)
			{
				lastStatusItem = new StatusItem(listView);
				lastStatusItem->setTotalSteps(e->max);
				lastStatusItem->setText(0, e->name);
				lastStatusItem->showProgressBar(true);
				listView->ensureItemVisible(lastStatusItem);
			}
			lastStatusItem->setProgress(e->pos);
			break;
		case AutopackageEvent::Terminate:
			sectionCount--;
			if (sectionCount == 0)
			{
				finished = true;
				if (failed)
					pleaseWaitLabel->setText(gtr("Uninstallation failed"));
				nextButton->setEnabled(true);
				nextButton->setText(gtr("Finish"));
				nextButton->setFocus();	
			}
			break;
		case AutopackageEvent::Download:
			switch (e->downloadType)
			{
			case 0:
				lastStatusItem = new StatusItem(listView);
				lastStatusItem->setTotalSteps(100);
				lastStatusItem->setText(0, gtr("Downloading %1").arg(e->url));
				listView->ensureItemVisible(lastStatusItem);
				break;
			case 1:
				lastStatusItem->setText(1, gtr("Connecting..."));
				break;
			case 2:
				lastStatusItem->setText(1, gtr("Error"));
				lastStatusItem->setColor(QColor(255, 0, 0));
				lastStatusItem->setPixmap(1, failIcon);
				lastStatusItem = new StatusItem(listView);
				lastStatusItem->setText(0, e->reason);
				break;
			case 3:
				if (e->pos == -1)
					lastStatusItem->setText(1, gtr("Downloading..."));
				else
				{
					lastStatusItem->showProgressBar(true);
					lastStatusItem->setProgress(e->pos);
				}
				break;
			case 4:
				lastStatusItem->showProgressBar(false);
				if (e->result.lower() == "failure")
				{
					lastStatusItem->setText(1, gtr("Failed"));
					lastStatusItem->setColor(QColor(255, 0, 0));
					lastStatusItem->setPixmap(1, failIcon);
				}
				else
				{
					lastStatusItem->setText(1, gtr("Done"));
					lastStatusItem->setPixmap(1, passIcon);
				}
				break;
			}
			break;
		case AutopackageEvent::Uninstall:
			pleaseWaitLabel->setText(gtr("Please wait while the software is removed..."));
			switch (e->uninstallType)
			{
				HeaderItem* item;
				case 0:
					cancelButton->hide();
					item = new HeaderItem(listView);
					item->setText(0, gtr("Uninstalling %1").arg(e->name));
					listView->ensureItemVisible(item);
					lastStatusItem = new StatusItem(listView);
					lastStatusItem->setTotalSteps(0);
					lastStatusItem->showProgressBar(true);
					lastStatusItem->setText(0, gtr("Uninstalling %1").arg(e->name));
					busyTimer->start(25);
					listView->ensureItemVisible(lastStatusItem);
					break;
				case 1:
					busyTimer->stop();
					lastStatusItem->showProgressBar(false);
					lastStatusItem->setText(1, gtr("Done"));
					lastStatusItem->setPixmap(1, passIcon);
					break;
				case 2:
					finished = true;
					busyTimer->stop();
					lastStatusItem->showProgressBar(false);
					lastStatusItem->setText(1, gtr("Failed"));
					lastStatusItem->setColor(QColor(255, 0, 0));
					lastStatusItem->setPixmap(1, failIcon);
					lastStatusItem = new StatusItem(listView);
					lastStatusItem->setText(0, e->reason);
					listView->ensureItemVisible(lastStatusItem);
					break;
			}
			break;
		case AutopackageEvent::Summary:
			listView->setMaximumSize(listView->width(), listView->height());
			pleaseWaitLabel->setText(gtr("Installation complete!"));
			HeaderItem* item = new HeaderItem(listView);
			item->setText(0, gtr("Installation complete!"));
			StatusItem* sitem = new StatusItem(listView);
			sitem->setText(0, gtr("Click \"next\" to continue"));
			listView->ensureItemVisible(sitem);
			nextButton->setEnabled(true);
			nextButton->setFocus();
			
			if (e->opType == "install")
			{
				QString labelText;
				if (e->packages.count() > 0)
					labelText = ngtr("%1 package was installed successfully.",
					                 "%1 packages were installed successfully.",
				                         e->packages.count()).arg(e->packages.count());
				
				if (e->failedPackages.count() > 0)
					labelText += ngtr("  %1 package failed to install.",
					                  "  %1 packages failed to install.",
				                          e->failedPackages.count()).arg(e->failedPackages.count());
				
				labelText = "<p><b>" + labelText + "</b></p>";
				labelText += "<p>" + gtr("The installation size was: <b>%1 KB</b>").arg(QString::number(e->size/1024)) + "<br>";
				labelText += gtr("You can remove this package by using the \"Manage 3rd party software\" program, in the System Tools menu.") + "</p>";
				
				if (e->suggestions.count() > 0)
				{
					labelText += "<p>" + ngtr("The following software could be installed to enhance this package:", "The following software could be installed to enhance this package:", e->suggestions.count()) + "<br><ul>";
					for ( QStringList::Iterator it = e->suggestions.begin(); it != e->suggestions.end(); ++it )
					{
						labelText += "<li>" + (*it);
					}
					labelText += "</ul>";
				}
				
				summaryBox->setText(labelText);
				
				if ((e->menuItems.count() == 0) || (getuid()==0) )
				{
					menuLabel->hide();
					menuList->hide();
					shortcutButton->hide();
				}
				else
				{
					menuLabel->setText(ngtr("The following menu entry is now available:",
					                        "The following menu entries are now available:",
					                        e->menuItems.count()));
					
					for ( QStringList::Iterator it = e->menuItems.begin(); it != e->menuItems.end(); ++it )
						parseDesktopFile(*it);
					
					if (menuList->count() == 0)
					{
						menuLabel->hide();
						menuList->hide();
						shortcutButton->hide();
					}
					else
						menuList->setCurrentItem(menuList->firstItem());
				}
			}
			else if(e->opType == "verify")
			{
				menuLabel->hide();
				menuList->hide();
				shortcutButton->hide();
				if(e->result == "success")
				{
					summaryBox->setText(gtr("The installation of the %1 package is intact.").arg(currentName));
				}
				else if(e->result == "failed")
				{
					summaryBox->setText(gtr("The installation of the %1 package is damaged. Try re-installing it.").arg(currentName));
				}
			}
			break;
	}
}

void Wizard::parseDesktopFile(QString path)
{
	QFile file(path);
	file.open(IO_ReadOnly);
	QTextStream stream(&file);
	
	bool inDesktopSection = false;
	QString name;
	QString iconName;
	while (1)
	{
		QString line = stream.readLine();
		if (line.isNull()) // EOF
			break;
		
		if (line.startsWith("["))
		{
			if (inDesktopSection)
				break;
			inDesktopSection = true;
			continue;
		}
		if (!inDesktopSection)
			continue;
		if (line.startsWith("Name="))
		{
			name = line.right(line.length() - 5);
			break;
		}
	}
	
	file.close();
	
	if (name.isEmpty())
		return;
	
	new MenuItem(menuList, name, path);
}

void Wizard::incProgress()
{
	static int progress = 0;
	lastStatusItem->setProgress( progress++ );
}


