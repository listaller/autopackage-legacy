#include "launcher.h"
#include "application.h"
#include "../data/data.h"
#include <qpixmap.h>
#include <qlabel.h>
#include <qmessagebox.h>
#include <iostream>

Launcher::Launcher(QString path, QWidget* parent, const char* name, bool modal, WFlags fl)
: LauncherBase(parent,name, modal,fl)
{
	QPixmap icon = qembed_findImage("kpackage");
	appIcon->setPixmap(icon);
	show();
	
	setenv("AUTOPACKAGE_FRONTEND", "autopackage-frontend-qt", true);
	setenv("AUTOPACKAGE_QTFE_XID", QString::number(winId()).latin1(), true);
	setenv("AUTOPACKAGE_QTFE_ATOM", QString::number(((Application*)qApp)->hideAtom).latin1(), true);
	
	proc = new QProcess(this);
	connect(proc, SIGNAL(processExited()), SLOT(processExited()));
	connect(proc, SIGNAL(readyReadStdout()), SLOT(readyReadStdout()));
	connect(proc, SIGNAL(readyReadStderr()), SLOT(readyReadStderr()));
	proc->addArgument("package");
	proc->addArgument("install");
	proc->addArgument(path);
	if (!proc->start())
	{
		QMessageBox::critical(this, tr("Error"), tr("An error occured launching the package file in bash"), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		deleteLater();
	}
}

Launcher::~Launcher()
{
	proc->tryTerminate();
}

void Launcher::processExited()
{
	if (!proc->normalExit())
	{
		QMessageBox::critical(this, tr("Error"), tr("The installer crashed, or was terminated"), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		deleteLater();
		return;
	}
	
	switch (proc->exitStatus())
	{
	case 0:
		// Normal exit
		break;
	case 1:
		QMessageBox::critical(this, tr("Error"), tr("Unable to extract the package's contents"), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		break;
	case 2:
		QMessageBox::critical(this, tr("Error"), tr("<p>Integrity check failed. This package is damaged.</p><ul><li>If you have obtained it via the Internet, please download it again.</li><li>Otherwise, or if this problem keeps occuring, contact this package's packager/vendor</li></ul>"), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		break;
	case 13:
		// User cancelled in autosu
		break;
	default:
		QMessageBox::critical(this, tr("Error"), tr("An unknown error occured (%1)").arg(QString::number(proc->exitStatus())), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		break;
	}
	deleteLater();
}

void Launcher::readyReadStdout()
{
	while(proc->canReadLineStdout())
		printf("%s\n", proc->readLineStdout().latin1());
}

void Launcher::readyReadStderr()
{
	while(proc->canReadLineStderr())
		printf("%s\n", proc->readLineStderr().latin1());
}

