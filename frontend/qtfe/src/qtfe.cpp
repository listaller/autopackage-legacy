
/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libintl.h>

#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <qapplication.h>
#include <qmessagebox.h>
#include <qfile.h>
#include <qtextcodec.h>
#include "prefix.h"

#include "wizard.h"
#include "launcher.h"
#include "application.h"


int main(int argc, char *argv[])
{
	Application app(argc, argv);
	
	QTextCodec *codec = QTextCodec::codecForName("utf8");
	QTextCodec::setCodecForCStrings(codec);
	setlocale (LC_ALL, "");
	bindtextdomain ("autopackage-qt", LOCALEDIR);
	bind_textdomain_codeset ("autopackage-qt", "UTF-8");
	textdomain ("autopackage-qt");
	
	if (argc <= 1)
	{
		QMessageBox::critical(NULL, "Error", "This program is a frontend to autopackage, and should not be run by itself.<br>To install a package, run \"package install &lt;package-file&gt;\".", QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		return 1;
	}
	if (!QFile::exists(argv[1]))
	{
		QMessageBox::critical(NULL, "Error", QString("The file <b>%1</b> does not exist").arg(QString(argv[1])), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
		return 1;
	}
	
	QWidget* w;
	
	QFileInfo info(argv[1]);
	if (info.isFile())
		w = (QWidget*) new Launcher(info.absFilePath(), NULL);
	else
		w = (QWidget*) new Wizard(info.absFilePath(), NULL);
	
	app.setMainWidget(w);
	return app.exec();
}
