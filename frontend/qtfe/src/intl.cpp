#include <libintl.h>
#include <locale.h>
#include <qstring.h>
#include "intl.h"

// Wrappers around gettext that return QStrings

QString gtr(const char* id)
{
	return QString(gettext(id));
}

QString ngtr(const char* singularId, const char* pluralId, int n)
{
	return QString(ngettext(singularId, pluralId, n));
}
