/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "autopackagepipe.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <qapplication.h>
#include <qdatetime.h>

AutopackagePipe::AutopackagePipe(QString pP, QObject* p)
 : QThread()
{
	pipePath = pP;
	parent = p;
	
	cancelled = false;
}


AutopackagePipe::~AutopackagePipe()
{
}

void AutopackagePipe::run()
{
	int fp;
	char readbuf[100];
	while(1)
	{
		QCString thisRead;
		fp = open(pipePath.latin1(), O_RDONLY);
		while(1)
		{
			int ret = read(fp, readbuf, 100);
			if (ret == 0) // End of file
				break;
			if (ret == -1) // Error
			{
				printf("FIFO read() error\n");
				close(fp);
				return;
			}
			QCString retString(readbuf, ret+1);
			thisRead += retString;
		}
		close(fp);
		//printf("Data: %s\n", (const char*) thisRead);
		
		data += QStringList::split('\n', thisRead, true);
		
		parseData();
		//msleep(100);
	}
}

void AutopackagePipe::writeLine(QString line)
{
	writeLock.lock();
	QString returnedLine = line + "\n";
	int fp = open(pipePath.latin1(), O_WRONLY);
	write(fp, returnedLine.latin1(), returnedLine.length());
	close(fp);
	writeLock.unlock();
}

void AutopackagePipe::parseData()
{
	while (!data.empty())
	{
		QString firstLine = data.first();
		if (firstLine.startsWith("HELLO "))
		{
			bool ok;
			int version = data.first().right(data.first().length() - 6).toInt(&ok);
			if (!ok || (version != 7))
			{
				AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::BadProtocol);
				QApplication::postEvent(parent, event);
				writeLine("UHOH VERSION_MISMATCH");
			}
			else if(cancelled)
			{
				writeLine("UHOH USER_CANCELLED");
			}
			else
			{
				AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Hello);
				QApplication::postEvent(parent, event);
				writeLine("LETS GO");
			}
			//printf("HELLO received with version %d\n",version);
			data.pop_front();
		}
		else if (firstLine == "IDENTIFY")
		{
			data.pop_front();
			QString displayName = data.first();
			data.pop_front();
			bool ok;
			int seriesId = data.first().toInt(&ok);
			QString context;
			if (!ok)
			{
				// Perhaps the series ID got missed out, and this is the "context" line
				if (data.count() > 1)
					data.pop_front();
				seriesId = 0;
			}
			else
				data.pop_front();
				
			context = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Identify);
			event->name = displayName;
			event->context = context;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "TEST")
		{
			data.pop_front();
			QString name = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Test);
			event->testType = 0;
			event->name = name;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "PASSTEST")
		{
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Test);
			event->testType = 1;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "FAILTEST")
		{
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Test);
			event->testType = 2;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "RECOMMEND")
		{
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Test);
			event->testType = 3;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "SEARCHING")
		{
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Test);
			event->testType = 4;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "TERMINATE")
		{
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Terminate);
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "SERIES")
		{
			data.pop_front();
			data.pop_front();
			writeLine("OK");
		}
		else if (firstLine == "STATUS")
		{
			QString line;
			data.pop_front();
			if (data.count() > 0)
			{
				line = data.first();
				data.pop_front();
			}
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::StatusLine);
			event->name = line;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "FAIL")
		{
			data.pop_front();
			QString line = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Fail);
			event->name = line;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "PROGRESSBAR")
		{
			data.pop_front();
			int max = data.first().toInt();
			data.pop_front();
			int pos = data.first().toInt();
			data.pop_front();
			QString label = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::ProgressBar);
			event->max = max;
			event->pos = pos;
			event->name = label;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "UNINSTALLING")
		{
			data.pop_front();
			QString name = data.first();
			data.pop_front();
			QString shortname = data.first();
			data.pop_front();
			QString packageManager = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Uninstall);
			event->uninstallType = 0;
			event->name = name;
			event->shortname = shortname;
			event->packageManager = packageManager;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "UNINSTALL-DONE")
		{
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Uninstall);
			event->uninstallType = 1;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "UNINSTALL-FAIL")
		{
			data.pop_front();
			QString reason = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Uninstall);
			event->uninstallType = 2;
			event->reason = reason;
			QApplication::postEvent(parent, event );
		}
		else if (firstLine == "DISPLAY-SUMMARY")
		{
			data.pop_front();
			QString opType = data.first();
			data.pop_front();
			QString result = data.first();
			data.pop_front();
			QString removeLine = data.first();
			data.pop_front();
			QStringList packages;
			QStringList menuItems;
			QStringList failedPackages;
			long size = 0;
			QStringList suggestions;
			if (opType == "install")
			{
				int packageCount = data.first().toInt();
				data.pop_front();
				for ( ; packageCount > 0 ; --packageCount )
				{
					packages.append(data.first());
					data.pop_front();
				}
				int menuCount = data.first().toInt();
				data.pop_front();
				for ( ; menuCount > 0 ; --menuCount )
				{
					menuItems.append(data.first());
					data.pop_front();
				}
				int sizeCount = data.first().toInt();
				data.pop_front();
				for ( ; sizeCount > 0 ; --sizeCount )
				{
					size += data.first().toInt();
					data.pop_front();
				}
				int suggestionsCount = data.first().toInt();
				data.pop_front();
				for ( ; suggestionsCount > 0 ; --suggestionsCount )
				{
					suggestions.append(data.first());
					data.pop_front();
				}
			}
			if (((opType == "install") || (opType == "verify")) && (result == "failure"))
			{
				int failedCount = data.first().toInt();
				data.pop_front();
				for ( ; failedCount > 0 ; --failedCount )
				{
					failedPackages.append(data.first());
					data.pop_front();
				}
			}
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Summary);
			event->opType = opType;
			event->result = result;
			event->packages = packages;
			event->menuItems = menuItems;
			event->failedPackages = failedPackages;
			event->suggestions = suggestions;
			event->size = size;
			event->removeLine = removeLine;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "DOWNLOAD-START")
		{
			data.pop_front();
			QString url = data.first();
			data.pop_front();
			QString description = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Download);
			event->downloadType = 0;
			event->url = url;
			event->name = description;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "DOWNLOAD-PREPARE")
		{
			data.pop_front();
			QString stage = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Download);
			event->downloadType = 1;
			event->stage = stage;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "DOWNLOAD-START-FAILED")
		{
			data.pop_front();
			QString reason = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Download);
			event->downloadType = 2;
			event->reason = reason;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "DOWNLOAD-PROGRESS")
		{
			data.pop_front();
			QString rate = data.first();
			data.pop_front();
			bool ok;
			int progress = data.first().toInt(&ok);
			if (!ok) progress = -1;
			data.pop_front();
			QString time = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Download);
			event->downloadType = 3;
			event->pos = progress;
			QApplication::postEvent(parent, event);
		}
		else if (firstLine == "DOWNLOAD-FINISH")
		{
			data.pop_front();
			QString result = data.first();
			data.pop_front();
			writeLine("OK");
			
			AutopackageEvent* event = new AutopackageEvent(AutopackageEvent::Download);
			event->downloadType = 4;
			event->result = result;
			QApplication::postEvent(parent, event);
		}
		else
		{
			data.pop_front();
		}
	}
}

