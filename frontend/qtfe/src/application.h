#ifndef APPLICATION_H
#define APPLICATION_H

#include <qapplication.h>
#include <X11/X.h> 
#include <X11/Xlib.h> 

class Application : public QApplication
{
public:
	Application(int argc, char* argv[]);
	
	bool x11EventFilter(XEvent* ev);
	
	Atom hideAtom;
};

#endif
