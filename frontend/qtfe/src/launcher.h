#ifndef LAUNCHER_H
#define LAUNCHER_H

#include "launcherbase.h"
#include <qprocess.h>

class Launcher : public LauncherBase
{
	Q_OBJECT

public:
	Launcher(QString path, QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
	~Launcher();
	
public slots:
	void processExited();
	void readyReadStdout();
	void readyReadStderr();
	
private:
	QProcess* proc;
};

#endif

