/***************************************************************************
 *   Copyright (C) 2004 by David Sansome                                   *
 *   me@davidsansome.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef AUTOPACKAGEPIPE_H
#define AUTOPACKAGEPIPE_H

#include <qobject.h>
#include <qthread.h>
#include <qstringlist.h>
#include <qmutex.h>
#include <qvariant.h>

class AutopackageEvent : public QCustomEvent
{
public:
	enum ApType
	{
		BadProtocol = 2000,
		Hello,
		Identify,
		Test,
		StatusLine,
		Fail,
		ProgressBar,
		Summary,
		Uninstall,
		Download,
		Terminate
	} apType;
	
	AutopackageEvent(enum ApType at)
	 : QCustomEvent( 2000 ), apType( at) {}
	~AutopackageEvent() {}
	
	QString name;
	QString context;
	int testType;
	int max;
	int pos;
	QString opType;
	QString result;
	QStringList packages;
	QStringList menuItems;
	QStringList failedPackages;
	QStringList suggestions;
	long size;
	QString removeLine;
	int downloadType;
	QString url;
	QString stage;
	QString reason;
	QString defaultVal;
	QString label;
	QString interactType;
	QString shortname;
	QString packageManager;
	int uninstallType;
};


#ifndef QT_THREAD_SUPPORT
#error Qt is not compiled with threading support
#endif



class AutopackagePipe : public QThread
{
public:
	AutopackagePipe(QString pipePath, QObject *parent);
	~AutopackagePipe();

	void run();
	void writeLine(QString line);
	
	bool cancelled;
	
private:
	void parseData();
	
	QMutex writeLock;
	QString pipePath;
	QObject* parent;
	
	QStringList data;
};

#endif
