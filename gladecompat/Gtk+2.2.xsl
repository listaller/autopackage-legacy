<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Keep the header info -->
<xsl:output omit-xml-declaration="no" />
<xsl:output encoding="UTF-8" standalone="no"/>
<xsl:output doctype-system="http://glade.gnome.org/glade-2.0.dtd" />

<!-- match elements and attributes to omit with empty templates -->
<!-- Gtk+ 2.4 -->
<xsl:template match="//widget/property[@name='decorated']" />
<xsl:template match="//widget/property[@name='gravity']" />
<xsl:template match="//widget/property[@name='visible_window']" />
<xsl:template match="//widget/property[@name='above_child']" />
<xsl:template match="//widget/property[@name='overwrite']" />
<xsl:template match="//widget/property[@name='accepts_tab']" />
<xsl:template match="//widget/property[@name='fixed_height_mode']" />

<!-- The property exists in 2.0, but the getter/setter is >= 2.4 -->
<xsl:template match="//widget[@class='GtkButton']/property[@name='focus_on_click']" />

<xsl:template match="//widget/property[@name='top_padding']" />
<xsl:template match="//widget/property[@name='bottom_padding']" />
<xsl:template match="//widget/property[@name='left_padding']" />
<xsl:template match="//widget/property[@name='right_padding']" />

<!-- Gtk+ 2.6 -->
<!-- The following *property* exists in 2.0 according to devhelp, but the getter/setter is >= 2.6 -->
<xsl:template match="//widget[@class='GtkComboBox']/property[@name='focus_on_click']" />
<xsl:template match="//widget/property[@name='show_hidden']" />

<xsl:template match="//widget/property[@name='ellipsize']" />
<xsl:template match="//widget/property[@name='width_chars']" />
<xsl:template match="//widget/property[@name='single_line_mode']" />
<xsl:template match="//widget/property[@name='angle']" />
<xsl:template match="//widget/property[@name='focus_on_map']" />
<xsl:template match="//widget[@class='GtkComboBox']/property[@name='add_tearoffs']" />
<xsl:template match="//widget/property[@name='hover_selection']" />
<xsl:template match="//widget/property[@name='hover_expand']" />


<!-- Gtk+ 2.8 -->
<xsl:template match="//widget/property[@name='urgency_hint']" />
<xsl:template match="//widget/property[@name='do_overwrite_confirmation']" />
<xsl:template match="//widget/property[@name='wrap_license']" />
<xsl:template match="//widget[@class='GtkIconView']/property[@name='reorderable']" />
<xsl:template match="//widget[@class='GtkMenuBar']/property[@name='child_pack_direction']" />
<xsl:template match="//widget[@class='GtkMenuBar']/property[@name='pack_direction']" />


<!-- copy all remaining nodes into the output document -->
<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>

