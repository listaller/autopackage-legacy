/*  Autopackage Glade Compatibility Fixer
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef GLOBALS_H
#define GLOBALS_H
#include <glade/glade.h>
extern GladeXML *xml;
#define W(name) glade_xml_get_widget(xml, name)
#define POINTER_DIFF(p1,p2) (GPOINTER_TO_INT(p1) - GPOINTER_TO_INT(p2))

#define trace(s...) G_STMT_START \
 { if (g_getenv("AUTOPACKAGE_DEBUG")) { g_print ("DBG: "); g_print(s); } } \
 G_STMT_END

enum {
	COL_ICON,
	COL_FILENAME,
	COL_DISPLAYNAME,
	NUM_COLUMNS
};
extern gchar *datadir;
#endif
