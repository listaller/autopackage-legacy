/*  Autopackage Glade Compatibility Fixer
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <string.h>
#include <glade/glade.h>
#include <gtk/gtk.h>
#include "globals.h"
#include "binreloc.h"

void treeFiles_row_deleted_cb (GtkTreeModel *model, GtkTreePath *path, gpointer user_data);
void treeFiles_row_inserted_cb (GtkTreeModel *model, GtkTreePath *path_arg, GtkTreeIter *iter_arg, gpointer user_data);

GladeXML *xml = NULL;
gchar *datadir = NULL;

void init_gui (void)
{
	/* Set up dependency list view (no list filling is done here though) */
	GtkListStore *model;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeView *tree;

	tree = GTK_TREE_VIEW (W("treeFiles"));
	model = gtk_list_store_new (NUM_COLUMNS, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_STRING);
   
	gtk_tree_view_set_model (GTK_TREE_VIEW (tree), GTK_TREE_MODEL (model));
	g_signal_connect (G_OBJECT(model), "row-inserted",
			  G_CALLBACK (treeFiles_row_inserted_cb), NULL);
	g_signal_connect (G_OBJECT(model), "row-deleted",
			  G_CALLBACK (treeFiles_row_deleted_cb), NULL);
	
	g_object_unref (G_OBJECT (model));

	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (tree)),
				     GTK_SELECTION_SINGLE);
   
	renderer = gtk_cell_renderer_pixbuf_new();
	column = gtk_tree_view_column_new_with_attributes ("Icon", renderer,
							   "pixbuf", COL_ICON,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes ("Glade File", renderer,
							   "text", COL_DISPLAYNAME,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);

	/* Setup drag'n'drop */
	
	static GtkTargetEntry targets[] =	{
		{ "STRING",        0, 0 },
		{ "text/plain",    0, 1 },
		{ "text/uri-list", 0, 2 },
	};
	
	gtk_drag_dest_set (GTK_WIDGET(tree),
			   GTK_DEST_DEFAULT_ALL,
			   targets,
			   3,
			   GDK_ACTION_COPY | GDK_ACTION_MOVE | GDK_ACTION_LINK);

	/* Populate the option menu */
	GDir *dir = g_dir_open (datadir, 0, NULL);
	if (!dir)
		return;
	const gchar *entry;
	GtkWidget *menu = gtk_menu_new ();
	while (entry = g_dir_read_name(dir))
	{
		const gchar *ext = strrchr(entry, '.');
		if (!ext)
			continue;
		if (g_str_equal (ext, ".xsl"))
		{
			gchar *fname = g_strndup (entry,
						  POINTER_DIFF(ext, entry));
			trace ("Appending %s to menu\n", fname);
			GtkWidget *item = gtk_menu_item_new_with_label (fname);
			g_object_set_data (G_OBJECT(item), "filename",
					   (gpointer) g_build_filename (datadir, entry, NULL));
			gtk_widget_show (item);
			gtk_menu_shell_append (GTK_MENU_SHELL(menu), item);
			g_free (fname);
		}
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (W("mnuGtkVersion")), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (W("mnuGtkVersion")), 0);
}

static gboolean show_dep_error_dialog (gpointer data)
{
	GtkWidget *dlg = gtk_message_dialog_new
		(GTK_WINDOW(W("main_window")),
		 GTK_DIALOG_DESTROY_WITH_PARENT,
		 GTK_MESSAGE_ERROR,
		 GTK_BUTTONS_OK,
		 "The program 'xsltproc' couldn't be found. "
		 "Make sure it is installed correctly before you run "
		 "this program.");
	gtk_dialog_run (GTK_DIALOG(dlg));
	gtk_main_quit();
	return FALSE;
}

int main (int argc, char *argv[])
{
	GError *err = NULL;
	gchar *gladefile;
	gchar *curdir = g_get_current_dir ();
	if (!gbr_init(&err)) {
		g_print ("Warning: Couldn't initialize BinReloc: %s\n", err->message);
		g_error_free (err);
		err = NULL;
	}
	
	datadir = g_build_filename (gbr_find_data_dir(PACKAGE_DATA_DIR), PACKAGE, NULL);
	trace ("App datadir = %s\n", datadir);
	gtk_init (&argc, &argv);

	gladefile = g_build_filename (gbr_find_exe_dir (curdir), PACKAGE ".glade", NULL);

	/* A bit hackish, but it allows the program to run with all its files in the same directory,
	 * i.e. no need to install... */
	if (!g_file_test (gladefile, G_FILE_TEST_EXISTS))
		gladefile = g_build_filename (datadir, PACKAGE ".glade");
	else
	{
		/* Ok, we're running in "not-installed" mode.. re-set datadir to bindir */
		g_free (datadir);
		datadir = gbr_find_exe_dir (curdir);
	}
	trace ("Running with datadir=%s\n", datadir);
	xml = glade_xml_new (gladefile, NULL, NULL);
	g_free (gladefile);
	g_free (curdir);

	gchar *prog = g_find_program_in_path ("xsltproc");
	
	glade_xml_signal_autoconnect (xml);
	init_gui();

	if (!prog)
		g_idle_add ((GSourceFunc) show_dep_error_dialog, NULL);
	else
		gtk_widget_show (W("main_window"));
	gtk_main ();
	g_free (prog);
	g_free (datadir);
	return 0;
}

