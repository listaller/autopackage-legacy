/*  Autopackage Glade Compatibility Fixer
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glade/glade.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "globals.h"

/* Represents a file that failed to be loaded */
typedef struct {
	gchar *filename;
	gchar *reason;
} failed_file_t;

static void free_failed_file_entry (failed_file_t *entry)
{
	g_free (entry->filename);
	g_free (entry->reason);
}

static void show_failed_to_add_dialog (const GList *fnames)
{
	g_return_if_fail (fnames != NULL);
	GString *s = g_string_new ("");
	gint n_files = 0;
	
	while (fnames) {
		failed_file_t *entry = (failed_file_t *) fnames->data;
		
		gchar *fname = g_path_get_basename (entry->filename);
		n_files++;
		g_string_append_printf (s, "\n\342\200\242 <b>%s</b> <i>(%s)</i>", fname, entry->reason);
		fnames = fnames->next;
		g_free (fname);
	}
	GtkWidget *dlg = gtk_message_dialog_new (GTK_WINDOW(W("main_window")),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_CLOSE,
						 /* Translation nightmare, but this wont be translatable anyway */
						 "Couldn't add file%s", n_files > 1 ? "s" : "");
	if (n_files > 1) 
		gtk_message_dialog_format_secondary_markup (GTK_MESSAGE_DIALOG(dlg),
							  "The following %d files was not added to the list:"
							  "%s", n_files, s->str);
	else
		gtk_message_dialog_format_secondary_markup (GTK_MESSAGE_DIALOG(dlg),
							  "The following file was not added to the list:"
							  "%s", s->str);
	gtk_dialog_run (GTK_DIALOG(dlg));
	gtk_widget_destroy (dlg);
	g_string_free (s, TRUE);
}

/* Returns an shortened version of the filename in UTF8 format */
static gchar *make_path_shorter (const gchar *filename)
{
	gchar *tmp = NULL;
	gchar *rv = NULL;
	/* FIXME: This should really be UTF8 safe, since 'filename' could already be in that encoding  */
	if (strncmp (filename, g_get_home_dir(), strlen(g_get_home_dir())) == 0) {
		GString *s = g_string_new ("");	
		g_string_append (s, "~");
		filename += strlen (g_get_home_dir());
		g_string_append (s, filename);
		tmp = s->str;
		g_string_free (s, FALSE);
	}
	else
		tmp = g_strdup (filename);

	rv = g_filename_to_utf8 (tmp, -1, NULL, NULL, NULL);
	g_free (tmp);
	return rv;
}

/* Checks if the supplied file name is already in the list of files */
static gboolean already_in_filelist (const gchar *filename)
{
	static GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	gboolean valid;
	if (!model)
		model = gtk_tree_view_get_model (GTK_TREE_VIEW (W("treeFiles")));
	valid = gtk_tree_model_get_iter_first (model, &iter);
	while (valid)
	{
		gchar *item;
		gtk_tree_model_get (model, &iter,
				    COL_FILENAME, &item, -1);
		if (g_str_equal (item, filename))
		{
			g_free (item);
			return TRUE;
		}
		g_free (item);
		valid = gtk_tree_model_iter_next (model, &iter);
	}

	return FALSE;
}

static GdkPixbuf *get_icon (void)
{
	static GdkPixbuf *image = NULL;
	
	if (!image) {
		gchar *imgname = g_build_filename (datadir, "pixmaps",
						   "gnome-mime-application-x-glade.png", NULL);
		image = gdk_pixbuf_new_from_file_at_size (imgname, 24, 24, NULL);
		g_free (imgname);
	}
	return image;
}

static gboolean add_to_filelist (const gchar *filename)
{
	static GtkListStore *store = NULL;
	GtkTreeIter iter;
	const gchar *ext = strrchr (filename, '.');
	if (!ext || !g_str_equal (ext, ".glade")) {
		return FALSE;
	}
	if (!store)
		store = (GtkListStore*) gtk_tree_view_get_model (GTK_TREE_VIEW (W("treeFiles")));
	gtk_list_store_append (store, &iter);
	gchar *displayname = make_path_shorter (filename);
	// FIXME: find out why displayname isn't shown
	gtk_list_store_set (store, &iter,
			    COL_ICON, get_icon(),
			    COL_DISPLAYNAME, displayname,
			    COL_FILENAME, filename,
			    -1);
	g_free (displayname);
	//gtk_widget_set_sensitive (W("btnConvert"), TRUE);
	return TRUE;
}

void add_filename_from_fileselector (GtkWidget *btn, gpointer data)
{
	GtkWidget *file_selector = GTK_WIDGET (data);
	gchar **selected_filenames;
	gint i = 0;
	GList *failed_files = NULL;
	
	selected_filenames = gtk_file_selection_get_selections (GTK_FILE_SELECTION (file_selector));
	g_return_if_fail (selected_filenames != NULL);
	while (selected_filenames[i])
	{
		gchar *filename = selected_filenames[i];
		if (already_in_filelist (filename))
		{
			failed_file_t *entry = g_new (failed_file_t, 1);
			entry->filename = g_strdup (filename);
			entry->reason = g_strdup ("Already in list");
			failed_files = g_list_append (failed_files, entry);
		}
		else if (!add_to_filelist (filename))
		{
			failed_file_t *entry = g_new (failed_file_t, 1);
			entry->filename = g_strdup (filename);
			entry->reason = g_strdup ("Invalid file type");
			failed_files = g_list_append (failed_files, entry);
		}
		i++;
		
	}
	if (failed_files)
		show_failed_to_add_dialog(failed_files);
	g_list_foreach (failed_files, (GFunc) free_failed_file_entry, NULL);
	g_list_free (failed_files);

}

void btnAdd_clicked_cb (GtkWidget *btn, gpointer data)
{
	GtkWidget *file_selector;

	file_selector = gtk_file_selection_new ("Select Glade files");
	gtk_file_selection_set_select_multiple (GTK_FILE_SELECTION (file_selector), TRUE);
	gtk_file_selection_complete (GTK_FILE_SELECTION(file_selector), "*.glade");
	g_signal_connect (GTK_FILE_SELECTION (file_selector)->ok_button,
			  "clicked",
			  G_CALLBACK (add_filename_from_fileselector),
			  file_selector);
	g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->ok_button,
				  "clicked",
				  G_CALLBACK (gtk_widget_destroy), 
				  file_selector);
	
	g_signal_connect_swapped (GTK_FILE_SELECTION (file_selector)->cancel_button,
				  "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  file_selector);

	gtk_widget_show (file_selector);
}

static gchar *get_selected_stylesheet (void)
{
	GtkWidget *menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (W("mnuGtkVersion")));
	GtkWidget *item = gtk_menu_get_active (GTK_MENU(menu));

	return (gchar *) g_object_get_data (G_OBJECT(item), "filename");
}

/* Handler for the button click event ("Convert" button).
 * Takes care of the actual conversion by calling xsltproc */
void btnConvert_clicked_cb (GtkWidget *btn, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *filename;
	gboolean valid, overwrite;
	gchar *stylesheet;
	GString *errstr = g_string_new ("");
	model = gtk_tree_view_get_model (GTK_TREE_VIEW (W("treeFiles")));
	valid = gtk_tree_model_get_iter_first (model, &iter);

	stylesheet = get_selected_stylesheet ();
	overwrite = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (W("chkOverwrite")));
	while (valid)
	{
		gchar *newfname, *cmd, *stdout, *stderr;
		gint exitstatus = 0;
		GError *err = NULL;
		gtk_tree_model_get (model, &iter, 
				    COL_FILENAME, &filename,
				    -1);

			  
		if (overwrite)
			newfname = g_strdup (filename);
		else
		{
			gchar *gtkversion = g_path_get_basename (stylesheet);
			gchar *path = g_path_get_dirname (filename);
			gchar *basename = g_path_get_basename (filename);
			if (strrchr(gtkversion, '.') != NULL)
				*(strrchr(gtkversion, '.')) = '\0';
			if (strrchr(basename, '.') != NULL)
				*(strrchr(basename, '.')) = '\0';
			newfname = g_strdup_printf ("%s/%s-%s.glade", path, basename,  gtkversion);
			
			g_free (gtkversion);
			g_free (path);
			g_free (basename);
		}
		cmd = g_strdup_printf ("xsltproc -o '%s' --novalid '%s' '%s'", newfname, stylesheet, filename);
		trace ("Running command: %s\n", cmd);
		g_spawn_command_line_sync (cmd, &stdout, &stderr, &exitstatus, &err);
		if (err || WEXITSTATUS(exitstatus) != 0)
		{
			gchar *fname = g_path_get_basename (filename);
			g_string_append_printf (errstr, "%s: %s (exitstatus %d)\n",
						fname, (err ? err->message : stderr),
						WEXITSTATUS(exitstatus));
			g_free (fname);
			if (err)
				g_error_free(err);
			err = NULL;
			valid = gtk_tree_model_iter_next (model, &iter);
		}
		else
		{
			valid = gtk_list_store_remove (GTK_LIST_STORE(model), &iter);
		}
		g_free (cmd);
		g_free (filename);
		g_free (newfname);
	}
	if (errstr->len > 0)
	{
		/* Not all files could be converted; keep the program alive */
		GtkWidget *dlg = W("convertErrorDialog");
		GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW(W("txtError")));
		gtk_text_buffer_set_text (buf, errstr->str, -1);
		gtk_dialog_run (GTK_DIALOG(dlg));
		g_string_free (errstr, TRUE);
	}
	else
	{
		/* We're done, shut down */
		g_string_free (errstr, TRUE);
		gtk_main_quit();
	}
}

/* Drag'n'drop */
void treeFiles_drag_data_received_cb (GtkWidget *widget, GdkDragContext *dc, gint x, gint y,
				      GtkSelectionData *selection_data, guint info, guint t, gpointer data)
{
	gchar **files = g_strsplit ((gchar*) selection_data->data,
				    "\r\n", -1);
	GList *failed_files = NULL;
	gint i = 0;
	if (!files)
		return;

	while (files[i])
	{
		gchar *unix_fname = g_filename_from_uri (files[i], NULL, NULL);
		if (unix_fname == NULL) {
			i++;
			continue;
		}
		if (already_in_filelist (unix_fname))
		{
			failed_file_t *entry = g_new (failed_file_t, 1);
			entry->filename = g_strdup (unix_fname);
			entry->reason = g_strdup ("Already in list");
			failed_files = g_list_append (failed_files, entry);
		}
		else if (!add_to_filelist (unix_fname))
		{
			failed_file_t *entry = g_new (failed_file_t, 1);
			entry->filename = g_strdup (unix_fname);
			entry->reason = g_strdup ("Invalid file type");
			failed_files = g_list_append (failed_files, entry);
		}

		i++;
	}
	/* Tell X/GTK+ that we've received the drop */
	gtk_drag_finish (dc, TRUE, FALSE, t);
	
	if (failed_files){
		show_failed_to_add_dialog (failed_files);
	}
	g_list_foreach (failed_files, (GFunc) g_free, NULL);
	g_list_free (failed_files);
}

void treeFiles_row_deleted_cb (GtkTreeModel *model, GtkTreePath *path, gpointer user_data)
{
	GtkTreeIter iter;
	GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (W("treeFiles")));
	gboolean empty = !gtk_tree_model_get_iter_first (model, &iter);

	trace ("Row deleted, list is%s empty\n", empty ? "" : " not");
	gtk_widget_set_sensitive (W("btnConvert"), empty);

	g_return_if_fail (sel != NULL);

	if (!empty)
		gtk_tree_selection_select_path (sel, path);
}
void treeFiles_row_inserted_cb (GtkTreeModel *model, GtkTreePath *path_arg, GtkTreeIter *iter_arg, gpointer user_data)
{
	trace ("Row inserted\n");
	gtk_widget_set_sensitive (W("btnConvert"), TRUE);
}

static void remove_selected_file (void)
{
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	GtkTreeModel *model;
	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (W("treeFiles")));

	g_return_if_fail (sel != NULL);

	if (gtk_tree_selection_get_selected (sel, &model, &iter))
		gtk_list_store_remove (GTK_LIST_STORE(model), &iter);
}

gboolean treeFiles_key_press_cb (GtkWidget *w, GdkEventKey *ev, gpointer data)
{
	switch (ev->keyval)
	{
		case GDK_Delete:
			remove_selected_file();
			return TRUE;
	}
	return FALSE;
}

static gint get_number_of_files (void)
{
	return gtk_tree_model_iter_n_children (gtk_tree_view_get_model (GTK_TREE_VIEW (W("treeFiles"))),
					       NULL);
}

gboolean mainwindow_key_press_cb (GtkWidget *w, GdkEventKey *ev, gpointer data)
{
	GtkWidget *dlg;
	gint filecount, response;
	trace ("Mainwin keypress: %d\n", ev->keyval);
	switch (ev->keyval)
	{
		case GDK_Escape:
			filecount = get_number_of_files ();
			if (filecount == 0)
				gtk_main_quit();
			else
			{
				dlg = gtk_message_dialog_new (GTK_WINDOW(W("main_window")),
							      GTK_DIALOG_DESTROY_WITH_PARENT,
							      GTK_MESSAGE_QUESTION,
							      GTK_BUTTONS_YES_NO,
							      "Really Quit");
				gtk_message_dialog_format_secondary_markup (GTK_MESSAGE_DIALOG(dlg),
									    "If you quit now, the %d selected file%s won't be converted.\n"
									    /* FIXME: Change this to use ngettext if we want to translate the app */
									    "Are you sure you want to quit?", filecount, filecount == 1 ? "" : "s");
				response = gtk_dialog_run (GTK_DIALOG(dlg));
				gtk_widget_destroy (dlg);
				if (response == GTK_RESPONSE_YES) 
					gtk_main_quit();
			}
			return TRUE;
	}
	
	return FALSE;
}

/** Popup menu **/

/* GTK+'s popup menu system is retarded in oh so many ways.. Damnit! I shouldn't have to bother with
 * this crap!
 *
 * See: http://mail.gnome.org/archives/gtk-devel-list/2006-February/msg00168.html */
static void popup_menu_position (GtkMenu *menu, gint *x, gint *y, gboolean *push_in, gpointer user_data)
{
	GtkWidget *tree = W("treeFiles");
	GtkWidget *mwin = W("main_window");
	gint main_x, main_y;
	if (!tree || !mwin) 
		return;
	gtk_window_get_position (GTK_WINDOW(mwin), &main_x, &main_y);
	/* This will at least make sure the damn menu is within the tree view! */
	*x = main_x + tree->allocation.x + 50;
	*y = main_y + tree->allocation.y + 50;
	*push_in = TRUE;
}

static void show_popup (GtkMenuPositionFunc func, gint button, guint32 ev_time)
{
	gtk_menu_popup (GTK_MENU (W("popup")), NULL, NULL, func, NULL, button, ev_time);
}

void popup_remove_cb (GtkMenuItem *item, gpointer data)
{
	remove_selected_file();
}

void popup_add_cb (GtkMenuItem *item, gpointer data)
{
	btnAdd_clicked_cb (NULL, NULL);
}

gboolean treeFiles_popup_cb (GtkWidget *widget, gpointer data)
{
	show_popup (popup_menu_position, 0, gtk_get_current_event_time());
	return TRUE;
}

gboolean treeFiles_mousebutton_cb (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	GdkEventButton *event_button;

	if (event->type == GDK_BUTTON_PRESS)
	{
		event_button = (GdkEventButton *) event;
		if (event_button->button == 3)
		{
			show_popup (NULL, event_button->button, event->time);
			return TRUE;
		}
	}
	return FALSE;

}
