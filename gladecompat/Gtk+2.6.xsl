<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Keep the header info -->
<xsl:output omit-xml-declaration="no" />
<xsl:output encoding="UTF-8" standalone="no"/>
<xsl:output doctype-system="http://glade.gnome.org/glade-2.0.dtd" />

<!-- match elements and attributes to omit with empty templates -->
<!-- Gtk+ 2.8 -->
<xsl:template match="//widget/property[@name='urgency_hint']" />
<xsl:template match="//widget/property[@name='do_overwrite_confirmation']" />
<xsl:template match="//widget/property[@name='wrap_license']" />
<xsl:template match="//widget[@class='GtkIconView']/property[@name='reorderable']" />
<xsl:template match="//widget[@class='GtkMenuBar']/property[@name='child_pack_direction']" />
<xsl:template match="//widget[@class='GtkMenuBar']/property[@name='pack_direction']" />

<!-- copy all remaining nodes into the output document -->
<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>

