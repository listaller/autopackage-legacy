/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Global variables and macros. */
#ifndef GLOBALS_H
#define GLOBALS_H
#include <glade/glade.h>

/* Available from glib 2.8 and gcc 4.x*/
#ifndef G_GNUC_NULL_TERMINATED
# if     __GNUC__ >= 4
#  define G_GNUC_NULL_TERMINATED __attribute__((__sentinel__))
# else
#  define G_GNUC_NULL_TERMINATED
# endif
#endif

#ifndef G_GNUC_MALLOC
/* Available from glib 2.6 (which we depend on, so this isn't strictly needed, but lets keep it in case we lower the requirement) */
# define  G_GNUC_MALLOC
#endif

extern GladeXML *xml;
#define W(name) glade_xml_get_widget(xml, name)

extern gboolean enable_debug;
extern GdkColor red;
extern GdkColor black;

extern gchar *datadir;
extern gchar *pixmapdir;

#define trace(s...) G_STMT_START { if (enable_debug) { g_print ("DBG: "); g_print (s);} } G_STMT_END

#define AUTOPACKAGE_MKAPSPEC_ERROR g_quark_from_static_string ("AUTOPACKAGE_ERRORS")

typedef enum
{
	APKG_ERROR_COMMAND_NOT_FOUND,
	APKG_ERROR_PERMISSION_DENIED,
	APKG_ERROR_ENCODING,
	APKG_ERROR_FAILED
} AutopackageError;

#endif
