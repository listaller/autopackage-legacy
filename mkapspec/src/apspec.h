/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef APSPEC_H
#define APSPEC_H

gchar *create_apspec (void);
gboolean read_apspec (const gchar *filename, GError **err);
gchar *find_existing_apspec(const gchar *directory);

typedef struct
{
	gchar *shortname;
	gchar *rootname;
	gchar *version;
	gboolean optional;
} DependencyInfo;

DependencyInfo *dependency_info_create (const gchar *shortname, 
									   const gchar *rootname,
									   const gchar *version,
									   gboolean optional);
void dependency_info_free(DependencyInfo *dinfo);

#endif /* APSPEC_H */
