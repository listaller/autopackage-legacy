/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* GUI setup and handling code for the additional dialogs in the program.
 * Basically anything that isn't the main window */
#include <string.h>
#include <gtk/gtk.h>

#include "globals.h"
#include "utils.h"


GtkTextTag *tag_comment, *tag_keyword, *tag_section;
GtkTextTag *tag_default = NULL; /* No additional formatting (e.g. let gtk theme handle that) */

void init_preview_dialog (void)
{
	static gboolean done = FALSE;
	PangoFontDescription *font_desc;
	GtkTextBuffer *text;
	GtkWidget *view;

	view = W("txt_preview");
	text = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
	gtk_text_buffer_set_text (text, "", -1);
	
	if (done)
		return;
	done = TRUE;
	/* Monospace font! */
	font_desc = pango_font_description_new ();
	pango_font_description_set_family_static (font_desc, "monospace");
	gtk_widget_modify_font (view, font_desc);
	pango_font_description_free (font_desc);


	/* Syntax Highlighting */
	tag_comment = gtk_text_buffer_create_tag (text, "info",
						  "foreground", "blue", NULL);
	tag_keyword = gtk_text_buffer_create_tag (text, "warning",
						  "foreground", "#2e9344",
						  "weight", PANGO_WEIGHT_BOLD, NULL);
	tag_section =  gtk_text_buffer_create_tag (text, "debug",
						   "foreground", "#899f22", NULL);
	
}

void init_custom_prepscript_dialog (void)
{
	static gboolean done = FALSE;
	PangoFontDescription *font_desc;
	GtkWidget *view;
	if (done)
		return;
	done = TRUE;
	view = W("txt_edit_prepare_script");
	/* Monospace font! */
	font_desc = pango_font_description_new ();
	pango_font_description_set_family_static (font_desc, "monospace");
	gtk_widget_modify_font (view, font_desc);
	pango_font_description_free (font_desc);
}
/* Returns the first non-whitespace character in an
 * ascii string. If no characters are found (or only white-
 * spaces) then ' ' is returned. */
static gchar get_first_token (const gchar *line)
{
	gint i;
	
	for (i = 0; line[i] != '\0'; i++)
	{
		if (!g_ascii_isspace (line[i]))
			return line[i];
	}
	return ' ';
}

/* Tries to determine if a line is a keyword, such as
 * RootName: @something...
 *
 * It is not bullet proof, but it will ignore things like
 * "this is description: the program is good". It doesn't get
 * any better unless we add lexical capabilities... */
static gboolean is_keyword (const gchar *text)
{
	gint i;

	i = 0;
	
	/* Ignore leading whitespaces */
	while (g_ascii_isspace (text[i++]) && text[i]);
	
	for (; text[i] != '\0'; i++)
	{
		if (g_ascii_isspace(text[i]))
			return FALSE;
		if (text[i] == ':')
			return TRUE;
	}

	return FALSE;
}

/* Appends a line to the text buffer. If len is positive, only the first 'len' characters
 * will be appended. If 'newline' is true, an additional newline will be added. 'tag' should
 * point to a valid tag to use for text style. */
static void append_line (const gchar *line, GtkTextTag *tag, gint len, gboolean newline)
{
	static GtkTextView *view = NULL;
	static GtkTextBuffer *buf = NULL;
	static GtkTextIter iter;
	
	if (!buf)
	{
		view = GTK_TEXT_VIEW (W("txt_preview"));
		buf = gtk_text_view_get_buffer (view);
	}
	gtk_text_buffer_get_end_iter (buf, &iter);
	if (tag)
		gtk_text_buffer_insert_with_tags (buf, &iter, line, len, tag, NULL);
	else
		gtk_text_buffer_insert (buf, &iter, line, len);
	if (newline)
		gtk_text_buffer_insert (buf, &iter, "\n", 1);
}

/* Adds a text to the preview window and also applies syntax highlighting */
void fill_preview_dialog (const gchar *text)
{
       	gchar **lines, *ptr;
	gint i, len;
	
	lines = g_strsplit (text, "\n", -1);
	for (i = 0; lines[i] != NULL; i++)
	{
		switch (get_first_token (lines[i]))
		{
			case '#':
				append_line (lines[i], tag_comment, -1, TRUE);
				break;

			case '[':
				if (strchr(lines[i], ']'))
					append_line (lines[i], tag_section, -1, TRUE);
				else
					append_line (lines[i], tag_default, -1, TRUE);
				break;

			case ' ':
				/* This means that the entire line is whitespace only */
				append_line ("\n", tag_default, 1, FALSE);
				
			default:
				if (is_keyword (lines[i]))
				{
					len = POINTER_DIFF(strchr (lines[i], ':'), lines[i]);
					append_line (lines[i], tag_keyword, len + 1, FALSE);
					ptr = lines[i] + len + 1;
					append_line (ptr, tag_default, -1, TRUE);
				}
				else
					append_line (lines[i], tag_default, -1,TRUE);
				break;
		}
	}
}
