/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Callback functions for the dialogs (anything that isn't the main window).
 * Also static helper functions for the callbacks. */
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <gtk/gtk.h>
#include <string.h>
#include "globals.h"
#include "utils.h"
#include "mainwin.h"

/* Timeout function that counts down 5 seconds before
 * closing the output dialog. */
gboolean autoclose_cmd_output_cb (gpointer data)
{
	GtkWidget *dialog = GTK_WIDGET (data);
	static gint timeout = 5;
	gchar *txt;

	if (timeout) {
		timeout--;
		txt = g_strdup_printf ("All done, closing in %d seconds", timeout + 1);
		gtk_label_set_text (GTK_LABEL (W("lbl_running_cmd")), txt);
		g_free (txt);
		return TRUE;
	}
	
	gtk_widget_hide (dialog);
	return FALSE;
}

/* Run a series of commands through popen(3) and prints the output to the output dialog.
 * Arguments:
 *	cmds: An array of gchar*'s to execute.
 *	directory: The directory where the commands should be run. It will become the cwd
 *		   for the child process. */
static void run_commands (GPtrArray *cmds, const gchar *directory)
{
	gint i;
	gboolean success = TRUE;
	GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (W("txt_cmd_output")));
	GtkTextIter iter;
	GtkTextMark *mark;
	GtkWidget *lbl = W("lbl_running_cmd");


	gtk_text_buffer_set_text (buf, "", -1);
	gtk_text_buffer_get_start_iter (buf, &iter);
	mark = gtk_text_buffer_create_mark (buf, "last_line", &iter, FALSE);
	
	g_return_if_fail (cmds != NULL);

	gtk_window_set_transient_for (GTK_WINDOW(W("dlg_cmd_output")), GTK_WINDOW(W("main_window")));

	gtk_window_present (GTK_WINDOW(W("dlg_cmd_output")));
	while (g_main_context_pending(NULL))
		g_main_context_iteration (NULL, FALSE);
	for (i = 0; i < cmds->len; i++)
	{
		FILE *p;
		gint exitstatus;
		gchar *cmd;
		gchar output[1024];

		/* Run it in the correct directory, and give us the output from stderr */
		cmd = g_strdup_printf ("cd \"%s\" && %s 2>&1", directory, (gchar *) g_ptr_array_index (cmds, i));

		if (! (p = popen (cmd, "r")))
		{
			g_warning ("Couldn't run popen(%s, \"r\"): %s", cmd, g_strerror (errno));
			g_free(cmd);
			continue;
		}
		g_free(cmd);
		gtk_label_set_text (GTK_LABEL(lbl), (gchar *) g_ptr_array_index (cmds, i));
		while (fgets(output, 1024, p))
		{
			/* FIXME: UTF-8 Validate!! */
			gtk_text_buffer_insert (buf, &iter, output, -1);
			gtk_text_buffer_move_mark (buf, mark, &iter);
					
			gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (W("txt_cmd_output")), mark, 
						      0, TRUE, 0, 1);
			while (g_main_context_pending(NULL))
				g_main_context_iteration (NULL, FALSE);
		}
		exitstatus = pclose (p);
		if (exitstatus == -1) 
			g_warning ("Error during pclose: %s", g_strerror (errno));
		if (exitstatus != 0)
		{
			gtk_widget_show (W("hbox_cmd_error"));
			gtk_widget_hide (W("btn_cmd_cancel"));
			gtk_widget_show (W("btn_cmd_close"));
			success = FALSE;
			break;
		}
	}
	gtk_label_set_text (GTK_LABEL (lbl), "All done");
	gtk_widget_hide (W("btn_cmd_cancel"));
	gtk_widget_show (W("btn_cmd_close"));
	if (success)
		g_timeout_add (1000, (GSourceFunc) autoclose_cmd_output_cb, W("dlg_cmd_output"));
}

void start_script_generation_cb (GtkWidget *btn, gpointer user_data)
{
	GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (W("txt_install_cmd")));
	GtkTextIter start,end;
	gchar *txt;
	gchar *destdir;
	gchar **cmds;
	gint i;
	GPtrArray *real_commands;
	
	gtk_text_buffer_get_start_iter (buf, &start);
	gtk_text_buffer_get_end_iter (buf, &end);
	
	txt = gtk_text_buffer_get_text (buf, &start, &end, FALSE);

	/* Parse the commands from the text box and create a GPtrArray containing each command. */
	cmds = g_strsplit (txt, "\n", -1);
	g_free (txt);
	destdir = get_unique_tempdir();
	real_commands = g_ptr_array_new ();
	for (i = 0; cmds[i] != NULL; i++)
	{
		gchar *pos;
		GString *s = g_string_new ("");
		if ((pos = strstr (cmds[i], "$DIR")))
		{
			g_string_append_len (s, cmds[i], POINTER_DIFF(pos, cmds[i]));
			g_string_append (s, destdir);
			g_string_append (s, pos + 4);
		}
		else
			g_string_append (s, cmds[i]);
		g_ptr_array_add (real_commands, s->str);
		g_string_free (s, FALSE);
	}
	g_strfreev (cmds);

	gtk_widget_hide (W("dlg_scriptgen"));
	run_commands (real_commands, get_project_directory());
	
	g_ptr_array_foreach (real_commands, (GFunc) g_free, NULL);
	g_ptr_array_free (real_commands, TRUE);

	/* Analyze the install root and create an install script from its contents */
	gchar *install_script = create_install_script (destdir, get_shortname());
	set_install_script (install_script);
	g_free (install_script);
	/* Clean up after us */
	if (!g_str_has_prefix (destdir, g_get_tmp_dir())) 
		g_warning ("Haxx0r warning: won't even try to remove %s", destdir);
	else
	{
		gchar *clean_cmd = g_strdup_printf ("rm -rf '%s'", destdir);
		g_spawn_command_line_async (clean_cmd, NULL);
		g_free (clean_cmd);
	}
		
	
}

gboolean hide_dialog_on_delete_cb (GtkWidget *dialog, GdkEvent  *event, gpointer   user_data)
{
	gtk_widget_hide (dialog);
	return TRUE;
}

void save_prepscript_clicked_cb (GtkWidget *btn, gpointer user_data)
{
	GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (W("txt_edit_prepare_script")));
	GtkTextIter start,end;
	gchar *txt;
	
	gtk_text_buffer_get_start_iter (buf, &start);
	gtk_text_buffer_get_end_iter (buf, &end);
	
	txt = gtk_text_buffer_get_text (buf, &start, &end, FALSE);

	g_object_set_data_full (G_OBJECT(W("main_window")), "[Prepare]",
							txt, g_free);
	gtk_widget_hide (W("dlg_edit_prepare_script"));
}

void cancel_prepscript_clicked_cb (GtkWidget *btn, gpointer user_data)
{
	gtk_widget_hide (W("dlg_edit_prepare_script"));
}
