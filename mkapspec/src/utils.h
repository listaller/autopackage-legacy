/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef UTILS_H
#define UTILS_H
#include <glib.h>


typedef struct _SkeletonInfo SkeletonInfo;

struct _SkeletonInfo
{
	gchar *rootname;
	gchar *displayname;
	gchar *shortname;
	gchar *notes;
};

gboolean validate_shortname (const gchar *txt);
gboolean validate_rootname (const gchar *txt);
G_GNUC_MALLOC gchar *get_autopackage_skeleton_dir (void);

void skeleton_info_free (SkeletonInfo *info);
SkeletonInfo *get_skeleton_info (const gchar *filename);

gchar *get_unique_tempdir(void);
gboolean create_directory (const gchar *directory, GError **err);

gchar *create_install_script (const gchar *destdir, const gchar *shortname);
G_GNUC_NULL_TERMINATED gboolean is_regular (const gchar *first_item , ...);
G_GNUC_NULL_TERMINATED gboolean is_dir (const gchar *first_item , ...);

void backup_file (const gchar *filename);
//G_GNUC_MALLOC gchar *get_file_encoding (const gchar *filename);
/* Useful in conjuction with for instance strchr or strstr, to find out the length
 * of the sub string */
#define POINTER_DIFF(larger,smaller) (GPOINTER_TO_INT(larger) - GPOINTER_TO_INT(smaller))

#endif
