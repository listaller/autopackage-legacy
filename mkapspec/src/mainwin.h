/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#ifndef MAINWIN_H
#define MAINWIN_H
#include <string.h>
void init_mainwin (void);
/* Adds a dependency, with default values for version and optional */
void add_dependency (const gchar *shortname, const gchar *rootname);
void add_dependency_full (const gchar *shortname, const gchar *rootname, const gchar *version, gboolean optional);

void populate_page_0 (void);
void populate_page_1 (void);
void populate_page_2 (void);
void populate_page_3 (void);
void populate_page_4 (void);
void populate_page_5 (void);

void set_install_script (const gchar *script);
G_CONST_RETURN gchar *get_shortname (void);

G_CONST_RETURN gchar *get_project_directory (void);
void set_project_directory (const gchar *newdir);

gboolean get_project_using_autotools (void);
gboolean find_text_in_list_view (GtkTreeModel *model, GtkTreeIter *iter, 
								 const gchar *needle, gint col);

GtkListStore *get_deps_list_store(void);
#define CONVERT_PARENT_ITER(_filter, _child, _iter) \
	(gtk_tree_model_filter_convert_iter_to_child_iter (\
	GTK_TREE_MODEL_FILTER (_filter), _child, _iter))

#define CONVERT_CHILD_ITER(_filter,_child,_iter)\
	(gtk_tree_model_filter_convert_child_iter_to_iter (\
	GTK_TREE_MODEL_FILTER (_filter), _child, _iter))

enum {
	DEP_COL_SHORTNAME,
	DEP_COL_DISPLAYNAME,
	DEP_COL_ROOTNAME,
	DEP_COL_NOTES,
	DEP_COL_VISIBLE,
	NUM_DEP_COLS
};
enum {
	SDEP_COL_NAME,
	SDEP_COL_VERSION,
	SDEP_COL_RECOMMENDED,
	SDEP_COL_ROOTNAME,
	NUM_SDEP_COLS
};

/* Evaluates to TRUE if the specified GtkEntry has no text in it */
#define ENTRY_IS_EMPTY(e) (strlen(gtk_entry_get_text(GTK_ENTRY(e))) == 0)

#endif
