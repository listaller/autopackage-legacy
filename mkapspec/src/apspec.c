/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* APSpec file parsing functions */
#include <gtk/gtk.h>
#include <string.h>
#include "globals.h"
#include "utils.h"
#include "mainwin.h"
#include "apspec.h"

void free_extra_metakey_list(GList *lst)
{
	GList *node = lst;
	gchar **item;
	trace ("Freeing list of extra (localized) meta keys\n");
	while (node)
	{
		item = (gchar **) node->data;
		g_free (item[0]);
		g_free (item[1]);
		g_free (item);
		node = node->next;
	}
	g_list_free (lst);
}
static gchar *textview_get_all_text (GtkWidget *textview)
{
	GtkTextBuffer *buf;
	GtkTextIter start, end;

	g_return_val_if_fail (textview != NULL, NULL);
	buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
	gtk_text_buffer_get_start_iter (buf, &start);
	gtk_text_buffer_get_end_iter   (buf, &end);

	return gtk_text_buffer_get_text (buf, &start, &end, FALSE);
}

/* Parses an Autopackage rootname into its components: domain, shortname and version.
 * If either of these is NULL, their value will be ignored (so it's safe to use this
 * function to just extract e.g. the domain part.
 *
 * If this function returns TRUE, it is safe to call g_free() on any of the out variables.
 * Do NOT attempt to free the out variables if this function returns FALSE!.
 *
 * Returns: TRUE if the rootname was successfully parsed - i.e., at least domain and shortname could be extracted.
 * 		   	Otherwise, it returns FALSE.
 */
static gboolean 
parse_rootname (const gchar *rootname, gchar **domain, gchar **shortname, gchar **version)
{
	gchar **arr = NULL;
	gboolean rv = TRUE;
	g_return_val_if_fail (rootname != NULL, FALSE);
	
	trace("Parsing rootname (%s) and set will set domain=%c, shortname=%c, version=%c\n", rootname,
		  domain ? 'Y' : 'N', shortname ? 'Y' : 'N', version ? 'Y' : 'N');
	/* NULL-ify so that they can safely be passed to g_free() */
	if (domain)		*domain = NULL;
	if (shortname) 	*shortname = NULL;
	if (version)	*version = NULL;
	
	arr = g_strsplit (rootname, "/", -1);
	if (arr[0]) {
		if (domain)
			*domain = g_strdup (arr[0]);
	}
	else
		return FALSE;
	
	if (arr[1])
	{
		gint i = 0;
		gchar *last = NULL;
		while (arr[++i])
			last = arr[i];
		
		gchar **arr2 = g_strsplit(last, ":", 2);
		if (arr2[0]) {
			if (shortname)
				*shortname = g_strdup(arr2[0]);
			if (arr2[1] && version)
				*version = g_strdup (arr2[1]);
		}
		else
			rv = FALSE;
		g_strfreev (arr2);
	}		
	g_strfreev (arr);
	if (!rv) {
		if (domain) 	g_free (*domain);
		if (shortname)	g_free (*shortname);
		if (version)	g_free (*version);
	}
	return rv;
}

/* Returns the selected text from a combo box. This function is used as
 * a replacement for the gtk_combo_box_get_active_text() that's available
 * from GTK+ 2.6 and above.
 *
 * The returned string needs to be free'd afterwards */
G_GNUC_MALLOC static gchar *combobox_get_active_text (GtkWidget *combo)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *rv = NULL;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
	gtk_combo_box_get_active_iter (GTK_COMBO_BOX (combo), &iter);
	gtk_tree_model_get (model, &iter, 0, &rv, -1);

	return rv;
}

static gboolean combobox_set_active_text (GtkWidget *combo, const gchar *text, gboolean add_if_not_found)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *tmp;
	
	model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
	gboolean valid = gtk_tree_model_get_iter_first(model, &iter);
	while (valid)
	{
		gtk_tree_model_get (model, &iter, 0, &tmp, -1);
		if (g_str_equal (tmp, text)) {
			gtk_combo_box_set_active_iter (GTK_COMBO_BOX (combo), &iter);
			return TRUE;
		}
		valid = gtk_tree_model_iter_next (model, &iter);
	}
	if (add_if_not_found) {
		gtk_list_store_append (GTK_LIST_STORE(model), &iter);
		gtk_list_store_set(GTK_LIST_STORE(model), &iter, 
						   0, text, 
						   -1);
		return TRUE;
	}
	return FALSE;
}

static gboolean set_dependencies (gchar **preparelines)
{
	int i;
	GList *deps = NULL;
	
	g_return_val_if_fail(preparelines != NULL, TRUE);
	
	for (i = 0; preparelines[i] != NULL; i++)
	{
		if (*(preparelines[i]) == '\0')
			continue;
		
		gchar **arr = g_strsplit_set (preparelines[i], " \t", -1);
		if (!arr || !arr[0] || !arr[1] || !arr[2])
		{
			g_warning ("Failed to parse existing dependency: %s is not a valid prepare line. Probably a custom shell script", preparelines[i]);
			g_strfreev(arr);
			return FALSE;
		}
		gboolean optional = FALSE;
		gchar *rootname, *shortname, *version;
		if (g_str_equal (arr[0], "recommend"))
			optional = TRUE;
		else if (g_str_equal (arr[0], "require"))
			optional = FALSE;
		else
		{
			trace ("Command %s is not require nor recommend. This must be a custom shell script\n", arr[0]);
			g_strfreev(arr);
			return FALSE;
		}
		rootname = arr[1];
		version = arr[2];
		for (gint i = 0; rootname[i] != '\0'; i++) {
			if (rootname[i] == '"' || rootname[i] == '\'') /* TODO: handle \ escape? */
				rootname[i] = ' ';
		}
		g_strstrip(rootname);
		if (!parse_rootname(rootname, NULL, &shortname, NULL))
		{
			trace ("Failed to parse \"%s\" as a rootname. Probably a custom shell script", rootname);
			g_strfreev(arr);
			return FALSE;
		}
		deps = g_list_prepend (deps, dependency_info_create(shortname, rootname, version, optional));
		g_free (shortname);
		g_strfreev (arr);
	}
	/* Now we are sure there were no parse errors, let's add the deps to the treeview */
	GList *node = g_list_first(deps);
	while (node)
	{
		DependencyInfo *d = (DependencyInfo*) node->data;
		add_dependency_full(d->shortname, d->rootname, d->version, d->optional);
		dependency_info_free(d);
		node = node->next;
	}
	g_list_free(deps);
	return TRUE;
}

/* Gets the selected dependencies and returns them in a format suitable for
 * the apspec-format. */
static gchar *get_dependencies (void)
{
	GtkTreeModel *store;
	GtkTreeIter iter;
	GString *rv;
	gchar *tmp;

	store = gtk_tree_view_get_model (GTK_TREE_VIEW (W("tree_selected_deps")));
	if (gtk_tree_model_get_iter_first (store, &iter) == FALSE)
		return g_strdup ("");
	rv = g_string_new ("");
	do
	{
		gchar *root, *version;
		gboolean recommend;
		gtk_tree_model_get (store, &iter,
				    SDEP_COL_ROOTNAME, &root,
				    SDEP_COL_VERSION, &version,
				    SDEP_COL_RECOMMENDED, &recommend,
				    -1);
		if (recommend)
			g_string_append (rv, "recommend ");
		else
			g_string_append (rv, "require ");
		if (g_str_equal(version, "<Enter Version!>"))
			*version = '\0';
		g_string_append_printf (rv, "'%s' %s\n", root, version);
		g_free (root);
		g_free (version);
	} while (gtk_tree_model_iter_next (GTK_TREE_MODEL (store), &iter));

	/* If needed, append the custom shell script */
	tmp = g_object_get_data (G_OBJECT(W("main_window")), "[Prepare]");
	if (tmp)
		g_string_append (rv, tmp);
	
	return g_string_free (rv, FALSE);
}

G_GNUC_MALLOC static gchar *get_object_data_key_for_group (const gchar *group)
{
	g_return_val_if_fail (group != NULL, NULL);
	
	if (*group == '\0')
		return NULL;
	
	return g_strstrip(g_strdup (group));
}

static GtkWidget *get_textfield_for_group (const gchar *group)
{
	g_return_val_if_fail (group != NULL, NULL);
	
	if (*group == '\0')
		return NULL;
	if (g_str_equal (group, "[Install]"))
		return W("txt_install");
	if (g_str_equal (group, "[Description]"))
		return W("txt_description");
	
	return NULL;
}

static GtkWidget *get_combobox_for_variable(const gchar *variable)
{
	g_return_val_if_fail (variable != NULL, NULL);
	if (*variable == '\0')
		return NULL;
	if (g_str_has_prefix (variable, "License:"))
		return W("cmb_license");
	if (g_str_has_prefix (variable, "Type:"))
		return W("cmb_type");
	
	return NULL;
}

/* Maps a apspec variable name to a GUI entry. If no entry is found, NULL
 * is returned */
static GtkWidget *get_entry_for_variable (const gchar *variable)
{
	g_return_val_if_fail (variable != NULL, NULL);
	
	if (*variable == '\0')
		return NULL;
	if (g_str_equal (variable, "ShortName"))
		return W("e_shortname");
	if (g_str_equal (variable, "SoftwareVersion"))
		return W("e_version");
	if (g_str_equal (variable, "DisplayName"))
		return W("e_displayname");
	if (g_str_equal (variable, "Summary"))
		return W("e_summary");
	if (g_str_equal (variable, "Maintainer"))
		return W("e_maintainer");
	if (g_str_equal (variable, "Packager"))
		return W("e_packager");
	
	return NULL;
}

/* Extracts the key and value from an APSpec line in the form:
 * Key: Value
 * If no ':' character was found, both key and value will be NULL. 
 *
 * It is always safe to call g_free() on both key and value after 
 * this function returns.
 * 
 * Returns: TRUE if key/value could be extracted (i.e., there's a colon in 'line), 
 * 			FALSE otherwise.
 */
static gboolean get_key_value_from_apspec_line (const gchar *line, gchar **key, gchar **value)
{
	gchar *ptr = NULL;
	
	g_return_val_if_fail (line != NULL, FALSE);
	
	if (key)
		*key = NULL;
	if (value)
		*value = NULL;
	
	if (*line == '[' || (ptr = strchr(line, ':')) == NULL)
		return FALSE;
	if (G_UNLIKELY(ptr == line))
	{
		/* ':' was the first character */
		if (key)
			*key = g_strdup ("");
		if (value)
			*value = g_strstrip(g_strdup(ptr + 1));
	}
	else
	{
		if (key)
			*key = g_strstrip(g_strndup (line, POINTER_DIFF(ptr, line)));
		if (value)
			*value = g_strstrip(g_strdup (ptr + 1));
	}
	
	return TRUE;
}

/* Returns a string containing all data belonging to the current group */
G_GNUC_MALLOC static gchar *
get_value_from_apspec_group (gchar **lines, gint *pos)
{
	GString *s = g_string_new("");
	while (lines[*pos] != NULL)
	{
		gchar *line = g_strchomp(lines[*pos]);
		if (*line == '[') {
			break;
		}
		g_string_append(s, line);
		g_string_append_c (s, '\n');
		(*pos)++;
	}
	/* Remove the last newline */
	if (s->len > 0)
		g_string_truncate(s, s->len - 1);
	
	return g_string_free(s, FALSE);
}

gboolean read_apspec (const gchar *filename, GError **err)
{
	gchar *tmp;
	gchar **lines;
	gint i = 0;
	gchar *key, *val;
	GPatternSpec *loc_key_pattern = NULL;
	gboolean notUTF8 = FALSE;
	trace("Reading existing apspec file: %s\n", filename);
	
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);
	
	if (!g_file_get_contents(filename, &tmp, NULL, err))
		return FALSE;
	loc_key_pattern = g_pattern_spec_new ("*[*]:*");
	lines = g_strsplit(tmp, "\n", -1);
	g_free (tmp);
	while (lines[i])
	{
		gchar *line = g_strstrip(lines[i]);
		i++;
		if (*line == '#' || *line == '\0' || g_str_equal (line, "[Meta]")) 
			continue;
		/* Not having the file UTF-8 encoded could be a problem, we will warn later */
		if (!g_utf8_validate(line, -1, NULL))
			notUTF8 = TRUE;
		
		GtkWidget *widget;
		if (get_key_value_from_apspec_line (line, &key, &val))
		{
			trace ("Got Key=%s, Value=%s\n", key, val);
			/* It's a Key:Value line */
			if ((widget = get_entry_for_variable (key)) != NULL)
			{
				/* It's an entry, put the value on this line into it */
				trace("%s is a variable for an entry\n", line);
				gtk_entry_set_text(GTK_ENTRY (widget), val);
			}
			else if ((widget = get_combobox_for_variable (key)) != NULL)
			{
				/* It's a combobox, select the text in that combo */
				trace ("%s is a variable for a combo box\n", line);
				combobox_set_active_text (widget, val, FALSE);
			}	
			else if (g_str_equal(key, "RootName"))
			{
				trace ("%s is the root name, extracting rootname + version\n", line);
				gchar *domain, *shortname, *version;
				if (parse_rootname (val, &domain, &shortname, &version))
				{
					tmp = g_strdup_printf ("%s/%s", domain, shortname);
					gtk_entry_set_text (GTK_ENTRY(W("e_rootname")), tmp);
					gtk_entry_set_text (GTK_ENTRY(W("e_version")), version);
					g_free (tmp);
					g_free (domain);
					g_free (shortname);
				}
				else
					/* Rootname looks invalid, but we'll put it out there anyway */
					gtk_entry_set_text (GTK_ENTRY(W("e_rootname")), val);
			}
			else 
			{
				/* A variable that has no text entry, just store the data so that we can write it back later */
				if (strchr(key, '[') != NULL) {
					trace ("Found localized key %s, storing as object data in extra list\n", key);
					GList *loc_list = g_object_steal_data (G_OBJECT(W("main_window")), "apspec_extra_meta");
					gchar **item = g_new (gchar *, 2);
					item[0] = g_strdup (key); 
					item[1] = g_strdup (val);
					gboolean add_to_gobj = FALSE;
					if (!loc_list)
						add_to_gobj = TRUE;
					loc_list = g_list_prepend (loc_list, item);
					g_object_set_data_full (G_OBJECT(W("main_window")), "apspec_extra_meta", 
											loc_list, (GDestroyNotify) free_extra_metakey_list);
				}
				else
				{
					trace ("%s is a variable that will be stored as object data (%s)\n", key, val);
					g_object_set_data_full (G_OBJECT(W("main_window")), key,
											g_strdup(val), g_free);
				}
			}
			g_free (key);
			g_free (val);
		}
		else if ((widget = get_textfield_for_group(line)) != NULL)
		{
			/* It's a text field for a group, further parsing needed */
			trace("%s is a group for a text field\n", line);
			val = get_value_from_apspec_group (lines, &i);
			GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (widget));
			gtk_text_buffer_set_text(buf, val, -1);
			g_free(val);
		}
		else if (g_str_equal (line, "[Prepare]"))
		{
			/* It's the dependencies */
			trace ("%s is the group of dependencies\n", line);
			val = get_value_from_apspec_group (lines, &i);
			gchar **arr = g_strsplit (val, "\n", -1);
			if (!set_dependencies (arr))
			{
				/* We have a custom shell script in the [Prepare] section.
				 * We can't parse that so let the user edit/view in plain text */
				gtk_widget_show (W("hbox_prepare_warning"));
				g_object_set_data_full (G_OBJECT(W("main_window")), 
										"[Prepare]", g_strdup(val), 
										g_free);
			}
			g_strfreev(arr);
		}
		else if (*line == '[')
		{
			/* A group that has no text field, just store the data so that we can write it back later */
			key = get_object_data_key_for_group(line);
			val = get_value_from_apspec_group (lines, &i);
			if (strchr(line, ':') != NULL) {
				/* We ignore localized groups */
				trace ("Ignoring %s since it's a localized group\n", line);
				g_free (val);
				
			}
			else
			{
				trace ("%s is a group that will be stored as object data\n", line);
				g_object_set_data_full(G_OBJECT(W("main_window")), line,
									   val, g_free);
			}
			/* 'val' has been taken over by glib now */
			g_free (key);
		}
		else
			g_printerr ("WRN: Don't know how to handle apspec line '%s'\n", line);

	}
	
	g_strfreev(lines);
	if (notUTF8) {
		g_set_error(err, AUTOPACKAGE_MKAPSPEC_ERROR,
					APKG_ERROR_ENCODING, "The apspec file is not UTF-8 encoded");
		trace("Existing apspec file parsed with warnings\n");
		return FALSE;
	}
	trace("Existing apspec file successfully parsed\n");
	return TRUE;
}

/* Appends a group to the specified GString. If there's object data attached to the 
 * main_window widget for the group, that data will be used as group contents.
 * If no data is available, the contents of 'default_text' will be used. */
static void append_group_conditional (GString *s, const gchar *group, const gchar *default_text)
{
	gchar *obj_data = g_object_get_data (G_OBJECT(W("main_window")), group);
	
	g_string_append (s, group);
	g_string_append_c (s, '\n');
	if (obj_data)
		g_string_append (s, obj_data);
	else if (default_text)
		g_string_append (s, default_text);
	g_string_append_c (s, '\n');
}

/* Appends a meta key to the GString. If data for said key exist on the main window
 * object, that data will be added, otherwise the default text will be added.
 * If both object data and default_text is NULL, nothing will be added! */
static void append_meta_conditional (GString *s, const gchar *key, const gchar *default_text)
{
	gchar *obj_data = g_object_get_data(G_OBJECT(W("main_window")), key);
	if (obj_data)
		g_string_append_printf (s, "%s: %s\n", key, obj_data);
	else if (default_text)
		g_string_append_printf (s, "%s: %s\n", key, default_text);
}

gchar *create_apspec (void)
{
	GString *s = g_string_sized_new (4096); /* Almost all .apspec files are < 4kb */
	const gchar *version;
	gchar *tmp;

	/* #define to save typing and ease reading a bit */
#define get_text(e) gtk_entry_get_text (GTK_ENTRY(W(e)))

	g_string_append (s, "# -*- shell-script -*-\n"
			 "# Generated by mkapspec " VERSION "\n"
			 "[Meta]\n");
	
	if (get_project_using_autotools())
		version = "@VERSION@";
	else
		version = get_text ("e_version");
	
	
	g_string_append_printf (s, "ShortName: %s\n", get_text ("e_shortname"));
	g_string_append_printf (s, "SoftwareVersion: %s\n", version);
	g_string_append_printf (s, "DisplayName: %s\n", get_text ("e_displayname"));
	g_string_append_printf (s, "RootName: %s:$SOFTWAREVERSION\n", get_text ("e_rootname"));
	g_string_append_printf (s, "Summary: %s\n", get_text ("e_summary"));
	g_string_append_printf (s, "Maintainer: %s\n", get_text ("e_maintainer"));
	g_string_append_printf (s, "Packager: %s\n", get_text ("e_packager"));
	
	append_meta_conditional (s, "PackageVersion", "1");
	append_meta_conditional (s, "CPUArchitectures", "x86");
	append_meta_conditional (s, "AutopackageTarget", "1.0");
	append_meta_conditional (s, "Repository", NULL);
	
	tmp = combobox_get_active_text (W("cmb_type"));
	g_string_append_printf (s, "Type: %s\n", tmp);
	g_free (tmp);
	
	tmp = combobox_get_active_text (W("cmb_license"));
	g_string_append_printf (s, "License: %s\n", tmp);
	g_free (tmp);
	
	GList *extra_meta_keys = g_object_get_data (G_OBJECT(W("main_window")), "apspec_extra_meta");
	while (extra_meta_keys)
	{
		gchar **item = (gchar**) extra_meta_keys->data;
		if (item)
			g_string_append_printf (s, "%s: %s\n", item[0], item[1]);
		extra_meta_keys = extra_meta_keys->next;
	}
	g_string_append_c (s, '\n');
	/** Done with [Meta] **/
	tmp = textview_get_all_text (W("txt_description"));
	g_strstrip(tmp);
	g_string_append (s, "[Description]\n");
	g_string_append (s, tmp);
	g_string_append (s, "\n\n");
	g_free (tmp);
	
	append_group_conditional (s, "[BuildPrepare]", 
			 "# If you're using autotools, the default should be enough.\n"
			 "# prepareBuild will set up apbuild and run configure for you. If you\n"
			 "# need to pass arguments to configure, just add them to prepareBuild:\n"
			 "# prepareBuild --enable-foo --disable-bar\n"
			 "prepareBuild\n\n");
	
	append_group_conditional(s, "[BuildUnprepare]", 
							"# If you're using prepareBuild above, there is no need to change this!\n"
			 				"unprepareBuild\n\n");
		 
	
	append_group_conditional (s, "[Globals]",
					"# Variables declared in this section will be available in all other sections\n\n");

	g_string_append (s, "[Prepare]\n");
	tmp = get_dependencies ();
	g_string_append (s, tmp);
	g_string_append_c (s, '\n');
	g_free (tmp);

	append_group_conditional (s, "[Imports]",
			 "# This command will tell makepackage what to include in the package.\n"
			 "# The selection comes from the files created by 'make install' or equivalent.\n"
			 "# Usually, you can leave this at the default\n"
			 "echo '*' | import\n\n");

	g_string_append (s, "[Install]\n");
	tmp = textview_get_all_text (W("txt_install"));
	g_strstrip (tmp);
	g_string_append (s, tmp);
	g_string_append (s, "\n\n");
	g_free (tmp);

	append_group_conditional (s, "[Uninstall]",
			 "# Leaving this at the default is safe unless you use custom commands in\n"
			 "# \"Install\" to create files. All autopackage API functions \n"
			 "# that installs files are logged.\n"
			 "uninstallFromLog\n");
#undef get_text

	return g_string_free (s, FALSE);
}

/* Tries to locate an existing apspec or apspec.in file in the provided 
 * directory.
 * If one is found, the full path to the file will be added.
 * If both an .apspec and an .apspec.in file is found, the latter will be returned.
 * If no file is found, NULL is returned.
 */
gchar *find_existing_apspec(const gchar *directory)
{
	GError *err = NULL;
	GDir *dir = NULL;
	const gchar *entry;
	gchar *rv = NULL;
	
	g_return_val_if_fail (directory != NULL, NULL);
	if (g_file_test(directory, G_FILE_TEST_IS_DIR) == FALSE)
		return NULL;
	dir = g_dir_open(directory, 0, &err);
	if (!dir) {
		g_warning ("Failed to search directory %s for existing apspec file: %s", directory, err->message);
		g_error_free(err);
		return NULL;
	}
	while ((entry = g_dir_read_name(dir)) != NULL)
	{
		if (!is_regular(directory, entry, NULL))
			continue;
		if (g_str_has_suffix(entry, ".apspec"))
			rv = g_build_filename (directory, entry, NULL);
		else if (g_str_has_suffix (entry, ".apspec.in"))
		{
			g_free(rv); /* g_free() handles NULL fine */
			rv = g_build_filename(directory, entry, NULL);
			break;
		}
	}
	g_dir_close(dir);
	return rv;
}

DependencyInfo *dependency_info_create (const gchar *shortname, 
									   const gchar *rootname,
									   const gchar *version,
									   gboolean optional)
{
	DependencyInfo *rv = g_new0 (DependencyInfo, 1);
	rv->shortname = g_strdup(shortname);
	rv->rootname = g_strdup(rootname);
	rv->version = g_strdup(version);
	rv->optional = optional;
	
	return rv;
}

void dependency_info_free(DependencyInfo *dinfo)
{
	if (!dinfo)
		return;
	g_free (dinfo->shortname);
	g_free (dinfo->rootname);
	g_free (dinfo->version);
	g_free (dinfo);
}
