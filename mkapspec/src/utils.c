/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Misc utility functions. Try to keep these more or less GUI indenpendent.
 * Rule of thumb: if you need to include gtk.h in this file, you're probably
 * placing your code in the wrong place :-) */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <glib.h>
#include "utils.h"
#include "globals.h"

void add_install_icon_theme (gpointer node, gpointer data)
{
	gchar *dir = (gchar*) node;
	GString *s = (GString *) data;

	g_string_append_printf (s, "installIcon share/icons/%s\n", dir);
	/* We're done with this now, free it! */
	g_free (node);
}

gboolean validate_shortname (const gchar *txt)
{
	g_return_val_if_fail (txt != NULL, FALSE);
	
	while (*txt)
	{
		if (!g_ascii_islower(*txt) && *txt != '-')
			return FALSE;
		txt++;
	}
	return TRUE;
}

gboolean validate_rootname (const gchar *txt)
{
	g_return_val_if_fail (txt != NULL, FALSE);
	
	if (*txt != '@')
		return FALSE;

	while (*txt)
	{
		if (g_ascii_isspace (*txt))
			return FALSE;
		if (g_ascii_isupper (*txt))
			return FALSE;
		txt++;
	}

	return TRUE;
}

static gchar *get_autopackage_config (void)
{
	gchar *config = NULL;
	
	if (g_file_test ("/etc/autopackage/config", G_FILE_TEST_EXISTS)) {
		config = g_strdup ("/etc/autopackage/config");
	}
	else
	{
		/* Try $HOME */
		gchar *xdg_home_env =g_getenv ("XDG_CONFIG_HOME")
			? g_build_filename (g_getenv ("XDG_CONFIG_HOME"), "autopackage", "config", NULL)
			: NULL;
		gchar *xdg_home_def = g_build_filename (g_get_home_dir (), ".config", "autopackage", "config", NULL);
		if (xdg_home_env && g_file_test (xdg_home_env, G_FILE_TEST_EXISTS)) {
			/* By ENVIRONMENT variable */
			config = xdg_home_env;
			g_free (xdg_home_def);
		}
		else if (g_file_test (xdg_home_def, G_FILE_TEST_EXISTS)) {
			/* By default $HOME config dir */
			config = xdg_home_def;
			g_free (xdg_home_env);
		}
	}
	
	return config;
}

G_GNUC_MALLOC gchar *get_autopackage_skeleton_dir (void)
{
	gchar *config = get_autopackage_config();
	gchar *apkg_datadir, *skeletondir;
	gchar *cmd;

	if (!config)
		return NULL;
	/* This could be solved in C-code, but it'll requre a couple of 100 lines of code. And we wouldn't
	 * even be sure that we got it right, since the config can contain any valid shell script code.
	 * Better to let bash handle it */
	cmd = g_strdup_printf ("bash -c 'source %s && echo $autopackage_share'", config);
	g_spawn_command_line_sync (cmd, &apkg_datadir, NULL, NULL, NULL);
	g_strchomp (apkg_datadir);
	skeletondir = g_build_filename (apkg_datadir, "skeletons", NULL);

	g_free (cmd);
	g_free (apkg_datadir);
	return skeletondir;
}

/* Frees all memory allocated for a SkeletonInfo struct. Always
 * use this when freeing SkeletonInfo in case there is a change
 * in its member fields. */
void skeleton_info_free (SkeletonInfo *info)
{
	if (!info)
		return;
	g_free (info->rootname);
	g_free (info->displayname);
	g_free (info->shortname);
	g_free (info);
}


gsize get_file_size(const gchar *filename)
{
	struct stat st;
	if (stat(filename, &st) != 0)
		return G_MAXUINT32;
	return st.st_size;
}
/* Returns the (relevant) information for a skeleton file. Full path needs to
 * be provided in the 'filename' argument.
 *
 * For details of what info is provided, see the SkeletonInfo struct in utils.h */
SkeletonInfo *get_skeleton_info (const gchar *filename)
{
	SkeletonInfo *i;
	gchar *contents, **arr;
	GError *err = NULL;
	gint x;

	if (filename == NULL || !g_file_test(filename, G_FILE_TEST_EXISTS))
		return NULL;
	if (get_file_size(filename) > 100 * 1024)
		/* No sane human being would create a skeleton of more than 100 KB... */
		return NULL;
	if (!g_file_get_contents (filename, &contents, NULL, &err))
	{
		g_print ("Couldn't get skeleton info for %s:\n%s\n", filename, err->message);
		return NULL;
	}
	i = g_new0 (SkeletonInfo, 1);
	arr = g_strsplit (contents, "\n", -1);
	g_free (contents);

	x = 0;
	for (x = 0; arr[x] != NULL; x++)
	{
		gchar *str = arr[x];
		if (g_ascii_strncasecmp (str, "[Notes]",
					 strlen ("[Notes]")) == 0) {
			/* Read everything up to we find a new [foo] section */
			GString *s = g_string_new ("");
			while (arr[++x] != NULL &&  *(arr[x]) != '[')
			{
				if (*arr[x] == '\0')
					g_string_append_c (s, '\n');
				else
				{
					g_string_append (s, arr[x]);
					g_string_append_c (s, ' ');
				}
			}
			i->notes = g_strchomp(s->str);
			g_string_free (s, FALSE);
		}
		else if (g_ascii_strncasecmp (str, "RootName:",
					      strlen("RootName:")) == 0) {
			i->rootname = g_strdup (&(str[9]));
			g_strstrip (i->rootname);
		}
		else if (g_ascii_strncasecmp (str, "DisplayName:",
					      strlen ("DisplayName:")) == 0) {
			i->displayname = g_strdup (&(str[12]));
			g_strstrip (i->displayname);
		}
		else if (g_ascii_strncasecmp (str, "ShortName:",
					      strlen("ShortName:")) == 0) {
			i->shortname = g_strdup (&(str[10]));
			g_strstrip (i->shortname);
		}
	}
	g_strfreev (arr);
	if (i->rootname && i->displayname && i->shortname && i->notes)
		return i;
	else
	{
		/* This was not a valid skeleton! */
		//g_print ("WRN: File %s does not seem to be a valid skeleton\n", filename);
		skeleton_info_free(i);
		return NULL;
	}
}

/* Generates a unique directory name. Note that it doesn't create the directory,
 * so it is theoretically possible for a race condition here if several mkapspec-
 * processes are running simultaniously. */
G_GNUC_MALLOC gchar *get_unique_tempdir (void)
{
	gchar *dir;
	
	while (TRUE)
	{
		dir = g_strdup_printf ("%s/mkapspec-%s-%d", g_get_tmp_dir(), g_get_user_name(),
				       g_random_int_range (100000, 999999));
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR))
			break;

		g_free (dir);
	}
	g_print ("unique directory: %s\n", dir);
	return dir;
}

/* Creates a directory and all missing directories needed. Behaves like
 * "mkdir -p" */
gboolean create_directory (const gchar *directory, GError **err)
{
	gchar **dirs;
	GString *s = g_string_sized_new (strlen (directory));
	gint i;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_print ("got directory: %s\n", directory);
	if (*directory != '/')
	{
		/* Dunno if it's really neccessary to check for, but
		 * what the heck.. */
		g_set_error (err,
			     AUTOPACKAGE_MKAPSPEC_ERROR,
			     APKG_ERROR_FAILED,
			     "Path is not absolute.");
		g_string_free (s, TRUE);
		return FALSE;
	}
	
	dirs = g_strsplit (directory, "/", -1);
	g_string_append_c (s, '/');
	for (i = 0; dirs[i]; i++)
	{
		g_string_append (s, dirs[i]);
		g_string_append_c (s, '/');
		if (g_file_test (s->str, G_FILE_TEST_IS_DIR))
			continue;
		if (mkdir (s->str, 0755) == -1)
		{
			perror ("mkdir");
			g_set_error (err,
				     AUTOPACKAGE_MKAPSPEC_ERROR,
				     APKG_ERROR_PERMISSION_DENIED,
				     "Couldn't create directory %s: %s",
				     s->str, g_strerror(errno));
			g_string_free (s, TRUE);
			g_strfreev (dirs);
			return FALSE;
		}
	}
	g_strfreev (dirs);
	g_string_free (s, TRUE);

	return TRUE;
}

/* Checks if a directory exists. The arguments will
 * be concatenated with a '/' separating them. The argument list
 * MUST be null-terminated.
 *
 * Example: is_dir ("/usr/share", "data", "myprogram", NULL)
 */
G_GNUC_NULL_TERMINATED gboolean is_dir (const gchar *first_item , ...)
{
	va_list ap;
	gchar *cur;
	GString *dir = NULL;
	gboolean rv = FALSE;

	if (*first_item == '/' || g_str_has_prefix (first_item, "./"))
		dir = g_string_new (first_item);
	else
	{
		dir = g_string_new ("./");
		g_string_append (dir, first_item);
	}
	g_string_append_c(dir, '/');
	
	va_start (ap, first_item);
	while ((cur = va_arg (ap, gchar*)))
	{
		g_string_append (dir, cur);
		g_string_append_c (dir, '/');
	}
	va_end (ap);
	if (g_file_test (dir->str, G_FILE_TEST_IS_DIR))
		rv = TRUE;
	g_string_free (dir, TRUE);
	
	return rv;
}

/* Checks if a file exists and is a regular type file. The arguments will
 * be concatenated with a '/' separating them. The argument list
 * MUST be null-terminated.
 *
 * Example: is_regular ("/usr/share", "data", "myprogram", "file.txt", NULL)
 */
G_GNUC_NULL_TERMINATED gboolean is_regular (const gchar *first_item , ...)
{
	va_list ap;
	gchar *cur;
	GString *dir = NULL; // g_string_new ("");
	gboolean rv = FALSE;

	if (*first_item == '/' || g_str_has_prefix (first_item, "./"))
		dir = g_string_new (first_item);
	else
	{
		dir = g_string_new ("./");
		g_string_append (dir, first_item);
	}
	g_string_append_c(dir, '/');
	
	va_start (ap, first_item);
	while ((cur = va_arg (ap, gchar*)))
	{
		g_string_append (dir, cur);
		g_string_append_c (dir, '/');
	}
	va_end (ap);
	g_string_truncate (dir, dir->len - 1);
	if (g_file_test (dir->str, G_FILE_TEST_IS_REGULAR))
		rv = TRUE;
	g_string_free (dir, TRUE);
	
	return rv;
}

/* Given a directory and a glob pattern, it returns TRUE of FALSE depending
 * on whether the glob matches any files in that directory.
 *
 * Example: eval_glob ("/dev", "hd?") => Will likely return TRUE */
gboolean eval_glob (const gchar *directory, const gchar *glob)
{
	GPatternSpec *ps;
	GDir *dir;
	const gchar *entry;
	gboolean rv = FALSE;

	trace ("Checking glob [%s] in directory [%s] : ", glob, directory);
	ps = g_pattern_spec_new (glob);
	dir = g_dir_open (directory, 0, NULL);
	if (!ps || !dir)
		return FALSE; /* Most likely, the dir didn't exist */

	while ((entry = g_dir_read_name(dir)))
	{
		if (g_pattern_match_string (ps, entry)){
			rv = TRUE;
			break;
		}
	}
	g_dir_close (dir);
	g_pattern_spec_free (ps);
	trace (" %s\n", rv ? "Match" : "No Match");
	
	return rv;
}

/* Returns a list of directories in the specified directory. */
G_GNUC_MALLOC GPtrArray *get_directories (const gchar *in_directory)
{
	GError *err = NULL;
	GDir *dir;
	GPtrArray *rv = g_ptr_array_new ();
	const gchar *entry;
	
	dir = g_dir_open (in_directory, 0, &err);
	if (err) {
		g_print ("Unable to open directory %s for reading: %s\n", in_directory, err->message);
		g_error_free (err);
		return rv;
	}
	while ((entry = g_dir_read_name (dir)))
	{
		if (is_dir (in_directory, entry, NULL))
			g_ptr_array_add (rv, g_strdup (entry));
	}
	g_dir_close (dir);

	return rv;
}

#include <stdlib.h>

/* This function examines the build root (destdir) and tries to match files in it
 * with the appropiate Autopackage API function.
 *
 * TODO: Provide a list of files that hasn't got matched */
gchar *create_install_script (const gchar *destdir, const gchar *shortname)
{
	GString *rv = g_string_new ("");
	gchar *tmp, *dir;
	tmp = g_strdup_printf ("find '%s'", destdir);
	system (tmp);
	g_free (tmp);

	/* Binaries */
	if (is_dir (destdir, "bin", NULL))
		g_string_append (rv, "installExe bin/*\n");

	/* ELF shared libraries */
	if (is_dir (destdir, "lib", NULL))
	{
		tmp = g_build_filename (destdir, "lib", NULL);
		if (eval_glob (tmp, "*.so.*"))
			g_string_append (rv, "installLib lib/*.so.*");
		g_free(tmp);
	}
	    
	/* Manual pages */
	/* FIXME: What about localized man pages? */
	if (is_dir (destdir, "man", NULL))
	{
		gchar *numstr;
		gint i;
		for (i = 1; i < 10; i++)
		{
			numstr = g_strdup_printf ("man%d", i);
			if (is_dir (destdir, "man", numstr, NULL))
				g_string_append_printf (rv, "installMan %d man/man%d/*\n", i, i);
			g_free (numstr);
		}
	}

	/* Program data */
	if (is_dir (destdir, "share", shortname, NULL))
		g_string_append_printf (rv, "installData share/%s\n", shortname);

	/* Icon themes */
	dir = g_build_filename (destdir, "share/icons", NULL);
	if (is_dir (dir, NULL)) {
		GPtrArray *dirs;
		/* Find eventual theme directories and 'installIcon' them */
		dirs = get_directories (dir);
		g_ptr_array_foreach (dirs, add_install_icon_theme, rv);
		g_ptr_array_free (dirs, TRUE);
	}
	g_free (dir);

	/* Typical .desktop-file icons */
	tmp = g_strconcat (shortname, ".*", NULL);
	dir = g_build_filename (destdir, "share/pixmaps",  NULL);
	if (eval_glob (dir, tmp))
		g_string_append_printf (rv, "installIcon share/pixmaps/%s\n", tmp);
	g_free (tmp);
	g_free (dir);
	
	/* Info */
	if (is_dir (destdir, "share/info", NULL))
		g_string_append (rv, "installInfo share/info/*\n");

	/* Translations */
	if (is_dir (destdir, "share/locale", NULL))
		g_string_append (rv, "installLocale share/locale/\n");

	/* GConf schemas */
	if (is_dir (destdir, "share/schemas", NULL))
		g_string_append (rv, "installGConfSchema share/schemas/*\n");

	/* Gnome app data (old-style) */
	tmp = g_strdup_printf ("%s.applications", shortname);
	if (is_regular (destdir, "share/application-registry", tmp, NULL)) 
		g_string_append_printf (rv, "installGnome2AppEntry share/application-registry/%s\n", tmp);
	g_free (tmp);

	/* Gnome mime data (old-style) */
	tmp = g_strdup_printf ("%s.keys", shortname);
	if (is_regular (destdir, "share/mime-info", tmp, NULL)) 
		g_string_append_printf (rv, "installGnome2Mime share/application-registry/%s\n", tmp);
	g_free (tmp);

	/* FD.o MIME*/
	tmp = g_strdup_printf ("%s.xml", shortname);
	if (is_regular (destdir, "share/mime/packages/", tmp, NULL))
		g_string_append_printf (rv, "installMime share/mime/packages/%s\n", tmp);
	g_free(tmp);

	/* Desktop File */
	tmp = g_strdup_printf ("%s.desktop", shortname);
	if (is_regular (destdir, "share/applications/", tmp, NULL)) {
		g_string_append (rv, "# You probably need to change the category\n");
		g_string_append_printf (rv, "installMenuItem \"Applications\" share/applications/%s\n", tmp);
	}
	g_free(tmp);

	tmp = rv->str;
	g_string_free (rv, FALSE);

	return tmp;
}

/* Backs up a file. 
 * FIXME: There's a possible race condition here */
void backup_file (const gchar *filename)
{
	gchar *backup_name;
	gint i = 0;

	if (!g_file_test (filename, G_FILE_TEST_IS_REGULAR))
		return;

	backup_name = g_strconcat (filename, ".bak", NULL);
	while (g_file_test (backup_name, G_FILE_TEST_EXISTS)) {
		g_free (backup_name);
		backup_name = g_strdup_printf ("%s.bak%d", filename, i++);
	}
	trace ("Backing up %s as %s\n", filename, backup_name);
	rename (filename, backup_name);
}

#if 0
G_GNUC_MALLOC gchar *get_file_encoding (const gchar *filename)
{
	gchar *output, *ptr, *rv;
	gchar *cmd;
	gint exitstatus;
	GError *err = NULL;
	g_return_val_if_fail (filename != NULL, NULL);
	
	cmd = g_strdup_printf ("file \"%s\"", filename);
	if (!g_spawn_command_line_sync(cmd, &output, NULL, &exitstatus, &err)) {
		trace ("Failed to call 'file' on apspec file %s: %s\n", filename, err->message);
		g_error_free(err);
		g_free (cmd);		
		return NULL;
	}
	g_free (cmd);		
	if (!WIFEXITED(exitstatus) || WEXITSTATUS(exitstatus) != 0) {
		trace ("'file' on apspec file %s exited abnormally (exitcode=%d)\n", 
			   filename, WEXITSTATUS(exitstatus));
		return FALSE;
	}
	g_strchomp(output);
	if (strlen(output) == 0) {
		trace ("No output from 'file' on apspec file %s\n", filename);
		return NULL;
	}
	/* Output should be "/path/to/file.apspec: ENCODING English text" */
	ptr = strchr (output, ':');
	if (!ptr) {
		trace ("Unable to determine encoding of apspec file based on file output.\n");
		return NULL;
	}
	/* Move forward in the string until we hit the encoding part */
	ptr++;	
	while (g_ascii_isspace(*ptr) && *ptr != '\0')
		ptr++;
	
	/* Find where the encoding ends, and put a NULL there to end the string */
	rv = g_strdup(ptr);
	ptr = rv;
	while (!g_ascii_isspace(*ptr) && *ptr != '\0')
		ptr++;
	*ptr = '\0';
	
	if (*rv == '\0')
	{
		trace ("No encoding could be determined from 'file' output [%s].\n", output);
		g_free (rv); 
		rv = NULL;
	}
	
	return rv;
}
#endif /* 0 */
