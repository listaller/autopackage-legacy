/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Main window setup/handling code. Basically everything that isn't a callback
 * or strictly related to a callback function. */
#include <gtk/gtk.h>
#include "globals.h"
#include "utils.h"
#include "mainwin.h"

void remove_dependency_cb (GtkWidget *btn, gpointer user_data);
void selected_dep_changed_cb (GtkTreeSelection *treeselection, gpointer user_data);
void tree_selected_deps_edited_cb (GtkCellRendererText *cell, const gchar *path_string,
				   const gchar *new_text, gpointer user_data);
void tree_selected_deps_toggled_cb (GtkCellRendererToggle *cell, gchar *path_str, gpointer data);
void validate_entry_cb (GtkEditable *editable, gpointer user_data);

static void append_skeleton (const SkeletonInfo const *i)
{
	static GtkListStore *store = NULL;
	GtkTreeIter iter;
	
	g_return_if_fail(i != NULL);
	
	if (!store)
		store = get_deps_list_store();
	
	gtk_list_store_insert_with_values(store, &iter, G_MAXINT, 
			    DEP_COL_SHORTNAME, i->shortname,
			    DEP_COL_DISPLAYNAME, i->displayname,
			    DEP_COL_ROOTNAME, i->rootname,
			    DEP_COL_NOTES, i->notes,
				DEP_COL_VISIBLE, TRUE,
			    -1);
}

static gboolean skeleton_already_added (const SkeletonInfo const *i)
{
	static GtkTreeModel *model = NULL;
	GtkTreeIter iter;

	g_return_val_if_fail(i != NULL, FALSE);
	g_return_val_if_fail(i->rootname != NULL && *(i->rootname) != '\0', FALSE);
	if (!model)
		model = gtk_tree_view_get_model (GTK_TREE_VIEW (W("tree_selected_deps")));
	
	return find_text_in_list_view (model, &iter, i->rootname, SDEP_COL_ROOTNAME);
}

static void display_error_in_listview (GtkWidget *tree, const char *message)
{
	static GtkListStore *store = NULL;
	GtkTreeIter iter;
	
	g_return_if_fail(GTK_IS_TREE_VIEW(tree));
	g_return_if_fail(message != NULL);
	
	if (!store)
		store = get_deps_list_store();
	gtk_list_store_clear(GTK_LIST_STORE(store));
	gtk_list_store_append (store, &iter);

	gtk_list_store_set (store, &iter,
			    DEP_COL_SHORTNAME, message,
			    -1);
	gtk_widget_set_sensitive(GTK_WIDGET(tree), FALSE);
}

/* Searches a tree model for a string in the specified column.
 * If the item was found, iter will point to that row and TRUE is returned.
 * If it isn't found, iter is invalid and FALSE is returned.
 * Complexity: O(n) where n is number of rows in the model */
gboolean find_text_in_list_view (GtkTreeModel *model, GtkTreeIter *iter, 
								 const gchar *needle, gint col)
{
	g_return_val_if_fail(model != NULL && iter != NULL, FALSE);
	
	gboolean valid = gtk_tree_model_get_iter_first(model, iter);	
	
	while (valid)
	{
		gchar *candidate;
		gtk_tree_model_get (model, iter,
						   col, &candidate,
						   -1);
		if (g_str_equal (candidate, needle))
		{
			g_free (candidate);
			return TRUE;
		}
		g_free (candidate);
		valid = gtk_tree_model_iter_next(model, iter);
	}
	
	return FALSE;
}

GtkListStore *get_deps_list_store(void)
{
	GtkTreeModel *mfilter = NULL;
	
	mfilter = gtk_tree_view_get_model (GTK_TREE_VIEW (W("tree_deps")));
	
	return GTK_LIST_STORE(gtk_tree_model_filter_get_model
						  (GTK_TREE_MODEL_FILTER (mfilter)));
}

void add_dependency (const gchar *shortname, const gchar *rootname)
{
	add_dependency_full (shortname, rootname, NULL, FALSE);
}

void add_dependency_full (const gchar *shortname, const gchar *rootname,
						  const gchar *version, gboolean optional)
{
	static GtkListStore *store = NULL;
	GtkTreeIter iter;

	trace ("Adding dependency on %s (%s) version %s\n", shortname, rootname, version);
	if (!store)
		store = (GtkListStore*) gtk_tree_view_get_model (GTK_TREE_VIEW (W("tree_selected_deps")));
	gtk_list_store_append (store, &iter);
	if (version == NULL)
		version = "<Enter Version!>";
	gtk_list_store_set (store, &iter,
			    SDEP_COL_NAME, shortname,
			    SDEP_COL_VERSION, version,
			    SDEP_COL_RECOMMENDED, optional,
			    SDEP_COL_ROOTNAME, rootname,
			    -1);
	gtk_widget_show (W("scroll_selected_deps"));
}

void init_mainwin (void)
{
	GdkColor *white;
	GtkNotebook *nb = GTK_NOTEBOOK (W("main_notebook"));
	gtk_notebook_set_show_tabs (nb, FALSE);
	
	white = &gtk_widget_get_style (W("title_box"))->white;
	gtk_widget_modify_bg (W("title_box"), GTK_STATE_NORMAL, white);

	gtk_label_set_text (GTK_LABEL (W("lbl_whatpage")), "Project Directory");

	gtk_widget_grab_focus (W("e_shortname"));

	g_signal_connect (G_OBJECT(W("e_shortname")), "changed",
			  G_CALLBACK(validate_entry_cb), W("img_shortname_warning"));
	g_signal_connect (G_OBJECT(W("e_rootname")), "changed",
			  G_CALLBACK(validate_entry_cb), W("img_rootname_warning"));

	/* Set up dependency list view (no list filling is done here though) */
	GtkListStore *list;
	GtkTreeModel *modelfilter;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeView *tree;
	GtkTreeSelection *sel;
	
	tree = GTK_TREE_VIEW (W("tree_deps"));
	list = gtk_list_store_new (NUM_DEP_COLS, 
							   G_TYPE_STRING, G_TYPE_STRING, 
							   G_TYPE_STRING, G_TYPE_STRING, 
							   G_TYPE_BOOLEAN);
   	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (W("tree_selected_deps")));
	
   	modelfilter = gtk_tree_model_filter_new (GTK_TREE_MODEL(list), NULL);	
	gtk_tree_model_filter_set_visible_column (
				GTK_TREE_MODEL_FILTER(modelfilter), DEP_COL_VISIBLE);
	
	g_signal_connect (G_OBJECT(sel), "changed",
   				G_CALLBACK(selected_dep_changed_cb), modelfilter);
	
	gtk_tree_view_set_model (GTK_TREE_VIEW (tree), GTK_TREE_MODEL (modelfilter));
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (list),
					      DEP_COL_SHORTNAME, GTK_SORT_ASCENDING);
	/* Ref no longer needed */
	g_object_unref (G_OBJECT (list));
	list = NULL;
	
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (tree)),
				     GTK_SELECTION_SINGLE);
   
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes ("Short Name", renderer,
							   "text", DEP_COL_SHORTNAME,
							   NULL);
	gtk_tree_view_column_set_sort_column_id (column, DEP_COL_SHORTNAME);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
       

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes ("Display Name", renderer,
							   "text", DEP_COL_DISPLAYNAME,
							   NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_column_set_sort_column_id (column, DEP_COL_DISPLAYNAME);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
	
/*	renderer = gtk_cell_renderer_toggle_new ();
	column = gtk_tree_view_column_new_with_attributes ("Visible?", renderer,
							   "active", DEP_COL_VISIBLE,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);*/
	
	gtk_tree_view_set_search_column (GTK_TREE_VIEW (tree), DEP_COL_SHORTNAME);

	/* Set up selected dependencies list view */
	tree = GTK_TREE_VIEW (W("tree_selected_deps"));
	list = gtk_list_store_new (NUM_SDEP_COLS, G_TYPE_STRING, G_TYPE_STRING, 
							   G_TYPE_BOOLEAN, G_TYPE_STRING);
   
	gtk_tree_view_set_model (GTK_TREE_VIEW (tree), GTK_TREE_MODEL (list));
	g_object_unref (G_OBJECT (list));
	list = NULL;
	gtk_tree_selection_set_mode (gtk_tree_view_get_selection (GTK_TREE_VIEW (tree)),
				     GTK_SELECTION_SINGLE);

	
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes ("Dependency", renderer,
							   "text", SDEP_COL_NAME,
							   NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
       
	renderer = gtk_cell_renderer_text_new();
	g_object_set (renderer,
		      "editable", TRUE,
		      NULL);
	g_signal_connect (renderer, "edited",
			  G_CALLBACK (tree_selected_deps_edited_cb), GINT_TO_POINTER(SDEP_COL_VERSION));
	column = gtk_tree_view_column_new_with_attributes ("Version", renderer,
							   "text", SDEP_COL_VERSION,
							   NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
	
	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (renderer, "toggled",
			  G_CALLBACK (tree_selected_deps_toggled_cb), GINT_TO_POINTER (SDEP_COL_RECOMMENDED));
	column = gtk_tree_view_column_new_with_attributes ("Dependency is optional", renderer,
							   "active", SDEP_COL_RECOMMENDED,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW(tree), column);
	
	populate_page_0();
}

/* populate_page_[0-9]: These functions tries to guess initial
 * values for the different fields. They will only do so the first
 * time the page is visited, so values will remain if the user presses
 * 'previous page' */

/* Project directory page */
void populate_page_0 (void)
{
	static gboolean done = FALSE;

	if (done)
		return;
	done = TRUE;

	gtk_entry_set_text (GTK_ENTRY(W("e_projectdir")), get_project_directory());
}

/* Software info page */
void populate_page_1 (void)
{
	static gboolean done = FALSE;
	
	if (done)
		return;
	done = TRUE;
	if (strlen(gtk_entry_get_text(GTK_ENTRY(W("e_shortname")))) == 0)
	{
		gchar *projname = g_path_get_basename (get_project_directory ());
		gchar *tmp = g_ascii_strdown (projname, -1);
		g_free (projname);
		
		gtk_entry_set_text (GTK_ENTRY(W("e_shortname")), tmp);
		g_free (tmp);
	}
}

/* Additional info page */
void populate_page_2 (void)
{
	GtkComboBox *license;
	static gboolean done = FALSE;
	
	if (done)
		return;
	done = TRUE;
	if (ENTRY_IS_EMPTY(W("e_rootname"))) {
		gchar *root = g_strdup_printf ("@your_namespace.org/%s", get_shortname());
		trace("Setting e_rootname to contain %s\n", root);
		gtk_entry_set_text (GTK_ENTRY (W("e_rootname")), root);
		g_free(root);
	}
	
	if (ENTRY_IS_EMPTY(W("e_maintainer"))) 
	{
		gchar *name = g_strdup_printf ("%s <email@here>", g_get_real_name ());
		gtk_entry_set_text (GTK_ENTRY (W("e_maintainer")), name);
		g_free(name);
	}
	if (ENTRY_IS_EMPTY(W("e_packager")))
		gtk_entry_set_text (GTK_ENTRY (W("e_packager")), gtk_entry_get_text(GTK_ENTRY(W("e_maintainer"))));

	if (get_project_using_autotools ()) {
		/* No need to specify version for autotools-based projects */
		gtk_widget_hide (W("lbl_whatversion"));
		gtk_widget_hide (W("e_version"));
	}
	else
	{
		if (ENTRY_IS_EMPTY(W("e_version")))
			gtk_entry_set_text (GTK_ENTRY(W("e_version")), "1.0");
	}
	license = GTK_COMBO_BOX (W("cmb_license"));
	/* Let's just hard code GNU GPL :-) */
	gtk_combo_box_set_active (license, 20);
	gtk_combo_box_set_active (GTK_COMBO_BOX (W("cmb_type")), 0);
}

/* Dependencies page */
void populate_page_3 (void)
{
	static gboolean done = FALSE;
	gchar *skeldir = get_autopackage_skeleton_dir ();

	if (done)
		return;
	done = TRUE;

	if (!g_file_test (skeldir, G_FILE_TEST_IS_DIR)) {
		g_warning ("Skeleton dir %s is not a directory!", skeldir);
		display_error_in_listview(W("tree_deps"), "Could not find autopackage skeleton dir.\nMake sure Autopackage is installed correctly");
		gtk_widget_set_sensitive(W("btn_add_dep"), FALSE);
		gtk_widget_set_sensitive(W("btn_del_dep"), FALSE);
		gtk_widget_set_sensitive(W("btn_dep_notes"), FALSE);
		
		g_free (skeldir);
		return;
	}
	GError *err = NULL;
	gchar *stdout = NULL, *cmd;
	cmd = g_strdup_printf ("find '%s' -type f -name 'skeleton.?'", skeldir);
	/* TODO: (maybe) If this turns out to be slow, we might want to run it
	 * async and parse output in parallel. That way we can give control back
	 * to the GUI at reasonable intervals */
	if (!g_spawn_command_line_sync (cmd, &stdout, NULL, NULL, &err))
	{
		g_warning ("Unable to run find: %s", err->message);
		g_error_free (err);
		err = NULL;
	}
	g_free (cmd);
	g_free (skeldir);
	if (!stdout)
		return;

	/* This should improve percieved responseness for slow computers */
	while (g_main_context_pending(NULL))
		g_main_context_iteration (NULL, FALSE);

	gchar **skeletons = g_strsplit (stdout, "\n", -1);
	gint i;
	for (i = 0; skeletons[i] != NULL; i++)
	{
		SkeletonInfo *info = get_skeleton_info (skeletons[i]);
		if (!info)
			continue;
		if (!skeleton_already_added(info))
			append_skeleton (info);
		else
			trace ("Skipping skeleton %s\n", info->rootname);
		skeleton_info_free (info);
	}
	GtkTreeModel *m = gtk_tree_view_get_model (GTK_TREE_VIEW(W("tree_deps")));
	gtk_tree_model_filter_refilter(GTK_TREE_MODEL_FILTER(m));
	g_free (stdout);
	g_strfreev (skeletons);
}

/* Install script page */
void populate_page_4 (void)
{
	static gboolean done = FALSE;

	if (done)
		return;
	done = TRUE;
}

/* All done page */
void populate_page_5 (void)
{
	static gboolean done = FALSE;
	gchar *default_file;
	const gchar *specname;
	if (done)
		return;
	done = TRUE;
	if (g_object_get_data(G_OBJECT(W("main_window")), "apspecfile") != NULL)
		/* We already have an apspec file */
		specname = g_basename(g_object_get_data(G_OBJECT(W("main_window")), "apspecfile"));
	else if (get_project_using_autotools())
		/* Integrate with the autotools build environment */
		specname = "default.apspec.in";
	else
		specname = "default.apspec";
	
	default_file = g_build_filename (get_project_directory(), "autopackage", specname, NULL);

	gtk_entry_set_text (GTK_ENTRY (W("e_outputfile")), default_file);
	
	if (g_file_test(default_file, G_FILE_TEST_IS_REGULAR)) {
		gtk_widget_set_sensitive (GTK_WIDGET (W("e_outputfile")), TRUE);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (W("chk_create_backup")), TRUE);
	}
	else
		gtk_widget_set_sensitive (GTK_WIDGET (W("e_outputfile")), FALSE);
	g_free (default_file);
}

void set_install_script (const gchar *script)
{
	GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (W("txt_install")));
	GtkTextIter iter;

	gtk_text_buffer_get_end_iter (buf, &iter);
	gtk_text_buffer_insert (buf, &iter, script, -1);
}

G_CONST_RETURN gchar *get_shortname (void)
{
	static gchar shortname[50];
	gchar *tmp;

	tmp = gtk_editable_get_chars (GTK_EDITABLE (W("e_shortname")), 0, -1);
	g_strlcpy (shortname, tmp, 50);
	g_free (tmp);

	return shortname;
}

G_CONST_RETURN gchar *get_project_directory (void)
{
	gchar *rv;
	rv = g_object_get_data(G_OBJECT(W("main_window")), "project_dir");

	g_assert (rv != NULL);
	return rv;
}

void set_project_directory (const gchar *newdir)
{
	g_object_set_data_full (G_OBJECT(W("main_window")), "project_dir", g_strdup (newdir), g_free);
}
			      
gboolean get_project_using_autotools (void)
{
	gchar *makefile_am = g_build_filename (get_project_directory(), "Makefile.am", NULL);
	gboolean rv = FALSE;

	if (g_file_test (makefile_am, G_FILE_TEST_EXISTS))
		rv = TRUE;

	g_free (makefile_am);
	return rv;
}
