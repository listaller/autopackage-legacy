/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Main file, keep this minimal. It's only used for starting everything up */
#include <unistd.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <time.h>
#include <stdlib.h>
#include "binreloc.h"
#include "globals.h"
#include "mainwin.h"


GladeXML *xml = NULL;
GdkColor red, black;
gboolean enable_debug = FALSE;


gchar *datadir = NULL;
gchar *pixmapdir = NULL;

/* Put all "general" initialization code in here. Note that
 * main window initialization code goes in init_main_window() (mainwin.c)
 * and dialog initialization code goes in init_<dialogname>() (dialogs.c) */
static void initialize (void)
{
	gdk_color_parse ("black", &black);
	gdk_color_parse ("red", &red);
}

/* Make sure we're not running as root or with root permissions */
static void sanity_check_permissions (void)
{
	GtkWidget *dialog = W("dlg_error");
	GtkWidget *label = W("lbl_errordialog_message");
	gchar *message = NULL;
	gboolean show = FALSE;
	uid_t uid, euid;

	uid = getuid();
	euid = geteuid();

	if (uid != euid) {
		message = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n"
					   "%s", "Running with too much priviliege",
					   "You are currently running this program with the SUID bit set, giving it "
					   "root permissions. The program will now abort.");
		show = TRUE;
	}
	if (uid == 0 || euid == 0)
	{
		message = g_strdup_printf ("<span weight=\"bold\" size=\"larger\">%s</span>\n\n"
					   "%s", "Running with too much priviliege",
					   "You are currently running this program as root which can be a sequrity risk. "
					   "Please restart this program as a user with less privilieges!");
		show = TRUE;
	}

	if (show) {
		gtk_label_set_markup (GTK_LABEL(label), message);
		gtk_window_set_transient_for (GTK_WINDOW(dialog), GTK_WINDOW(W("main_window")));
		gtk_window_present (GTK_WINDOW(dialog));
	}
	g_free (message);
}

/* Will be called before main() */
static void __attribute__((constructor)) locate_dirs () 
{
	gbr_init(NULL);
	datadir = gbr_find_data_dir(DATADIR);
	pixmapdir = g_build_filename (datadir, PACKAGE, NULL);
}

int main (int argc, char *argv[])
{
	GtkWidget *main_window;
	gchar *fname;
	g_random_set_seed (time(NULL));
	gtk_init (&argc, &argv);

	if (getenv ("AUTOPACKAGE_MKAPSPEC_DEBUG") && *(getenv("AUTOPACKAGE_MKAPSPEC_DEBUG")) != '0')
		enable_debug = TRUE;
	else
		enable_debug = FALSE;
	fname = g_build_filename (datadir, PACKAGE, "mkapspec.glade", NULL);
	xml = glade_xml_new (fname, NULL, NULL);
	g_free (fname);
	
	glade_xml_signal_autoconnect (xml);
	main_window = glade_xml_get_widget (xml, "main_window");
	set_project_directory (g_get_current_dir ());
	initialize ();
	init_mainwin();

	sanity_check_permissions ();
	
	gtk_widget_show (main_window);
	
	gtk_main ();
	return 0;
}
