/*  Autopackage Specification File Generator
 *  Copyright (C) 2006 Isak Savo <isak.savo@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Callback functions for the main window GUI and its widgets (including
 * any static callback helper functions). */
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <glade/glade.h>
#include "globals.h"
#include "utils.h"
#include "mainwin.h"
#include "dialogs.h"
#include "apspec.h"

gchar *finished_apspec = NULL;

static gint show_warning (const gchar *primary, const gchar *secondary)
{
	GtkWidget *dlg;

	dlg = gtk_message_dialog_new (GTK_WINDOW(W("main_window")), GTK_DIALOG_DESTROY_WITH_PARENT,
				      GTK_MESSAGE_WARNING, GTK_BUTTONS_OK,
				      primary);
	gtk_message_dialog_format_secondary_markup (GTK_MESSAGE_DIALOG(dlg), secondary);
	
	gint result = gtk_dialog_run (GTK_DIALOG(dlg));
	gtk_widget_destroy(dlg);
	return result;
}

static gint show_confirmation_close()
{
	GtkWidget *dlg;
	dlg = gtk_message_dialog_new (GTK_WINDOW(W("main_window")), GTK_DIALOG_DESTROY_WITH_PARENT,
				      GTK_MESSAGE_WARNING,
				      GTK_BUTTONS_NONE,
				      "Close this guide?");
	
	gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG(dlg),
						  "If you close this guide, all your changes will be lost. You must complete the "
						  "guide before your changes can be saved.\n"
						  "Are you sure you want to close this guide?");
	gtk_dialog_add_buttons (GTK_DIALOG(dlg), 
						"No, keep editing", GTK_RESPONSE_NO,
						"Yes, close guide", GTK_RESPONSE_YES,
						NULL);					
	gint result = gtk_dialog_run (GTK_DIALOG(dlg));
	gtk_widget_destroy(dlg);
	return result;
}

gboolean main_window_deleted_cb (GtkWidget *widget,
				 GdkEvent  *event,
				 gpointer   user_data)
{
	gint result = show_confirmation_close();

	if (result == GTK_RESPONSE_YES)
		return FALSE;
	else
		return TRUE;
}


void main_window_destroyed_cb (GtkObject *object, gpointer user_data)
{
	g_print ("Destroy cb\n");
	gtk_main_quit();
}

gboolean main_window_keypress_cb (GtkWidget *widget, GdkEventKey *ev, gpointer user_data)
{
	// Only consider ctrl, alt and shift as relevant modifiers
/* 	gboolean only_alt_mod = !(ev->state & (GDK_CONTROL_MASK | GDK_SHIFT_MASK)) && (ev->state & GDK_MOD1_MASK); */
/* 	//g_print ("Only ALT pressed: %s\n", only_alt_mod ? "Yes" : "No"); */
/* 	gboolean rv; */
	
	/* User pressed escape */
	/* FIXME: Add keyboard navigation */
/* 	if (!(ev->state & GDK_MODIFIER_MASK) && ev->keyval == GDK_Escape) */
/* 		g_signal_emit_by_name ((gpointer) W("main_window"), "delete-event", */
/* 				       W("main_window"), NULL, &rv); */
	/* Alt+arrow to navigate the wizard */
/* 	if (only_alt_mod) */
/* 	{ */
/* 		if (ev->keyval == GDK_leftarrow) */
/* 			g_signal_emit_by_name ((gpointer) W("btn_next"), "clicked", */
/* 					       W("btn_next"), NULL); */
/* 	} */
	/* Propagate the event */
	return FALSE;
}
enum {
	DirectionForward,
	DirectionBackward
};
gint direction = DirectionForward;
void next_page_cb (GtkWidget *btn, gpointer user_data)
{
	direction = DirectionForward;
	gtk_notebook_next_page (GTK_NOTEBOOK (W("main_notebook")));
}

void previous_page_cb (GtkWidget *btn, gpointer user_data)
{
	direction = DirectionBackward;
	gtk_notebook_prev_page (GTK_NOTEBOOK (W("main_notebook")));
}

void page_switched_cb (GtkNotebook *nb, GtkNotebookPage *page,
			      guint page_num, gpointer user_data)
{
	gchar *txt = g_strdup_printf ("<i>Page %d of %d</i>",
				      page_num, gtk_notebook_get_n_pages (nb) - 1);
	gtk_label_set_markup (GTK_LABEL (W("lbl_whatpage")), txt);
	g_free (txt);
	if (page_num <= 1)
		gtk_widget_set_sensitive (W("btn_prev"), FALSE);
	else
		gtk_widget_set_sensitive (W("btn_prev"), TRUE);

	if (page_num == gtk_notebook_get_n_pages (nb) - 1)
	{
		gtk_widget_set_sensitive (W("btn_next"), FALSE);
		gtk_widget_show (W("btn_save_and_exit"));
		gtk_widget_grab_default (W("btn_save_and_exit"));
	}
	else
	{
		gtk_widget_hide (W("btn_save_and_exit"));
		gtk_widget_set_sensitive (W("btn_next"), TRUE);
		gtk_widget_grab_default (W("btn_next"));
		/* If the user pressed back, we need to reset this and
		 * re-generate it later */
		g_free (finished_apspec);
		finished_apspec = NULL;
	}
	
	switch (page_num)
	{
		case 0:
			populate_page_0 ();
			break;
		
		case 1:
			{
				gchar *apspecfile = g_object_get_data(G_OBJECT(W("main_window")), "apspecfile");
				/* Only read .apspec if user clicked 'next' */
				if (direction == DirectionForward && apspecfile != NULL)
				{
					GError *err = NULL;
					if (!read_apspec(apspecfile, &err))
					{
						if (err->domain == AUTOPACKAGE_MKAPSPEC_ERROR &&
							err->code == APKG_ERROR_ENCODING) 
						{
							show_warning("Unsupported Encoding",
										 "The apspec file in the selected project directory is saved in an unsupported encoding. "
										 "This can lead to problems when saving the changes made in this program, and you are advised " 
										 "to open the file in a text editor and re-save it in UTF-8 encoding.");
						}
						else
						{
							gchar *msg = g_strdup_printf ("The apspec file in the project directory could not be opened.\n\n"
														  "The reported error was: %s", err->message);
							show_warning ("Failed to load file", msg);
							g_free (msg);
						}
						g_error_free (err);
					}
				}
						
				populate_page_1 ();
			}
			break;

		case 2:
			populate_page_2 ();
			break;

		case 3:
			populate_page_3 ();
			break;
			
		case 4:
			populate_page_4 ();
			break;
			
		case 5:
			populate_page_5 ();
			break;
		default:
			g_assert_not_reached ();
			break;
	}
}

void selected_dep_changed_cb (GtkTreeSelection *sel, gpointer user_data)
{
	GtkTreeIter iter;
	GtkTreeModel *model = GTK_TREE_MODEL(user_data);
	gboolean selected;
	g_return_if_fail (model != NULL);
	
	selected = gtk_tree_selection_get_selected (sel, &model, &iter);
	gtk_widget_set_sensitive(GTK_WIDGET(W("btn_del_dep")), selected);
}

void dependency_info_cb (GtkWidget *btn, gpointer user_data)
{
	GtkWidget *dlg;
	GtkTreeModel *model;
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	gchar *shortname;
	gchar *notes;
	
	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (W("tree_deps")));
 
	g_return_if_fail (sel != NULL);

	if (!gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		g_warning ("Nothing selected! Cannot add dep");
		return;
	}
	gtk_tree_model_get (model, &iter,
			    DEP_COL_SHORTNAME, &shortname,
			    DEP_COL_NOTES, &notes,
			    -1);
	dlg = W("dlg_skeleton_info");
	gchar *msg = g_strdup_printf ("<b>Skeleton notes for <i>%s</i>:</b>", shortname);
	gtk_label_set_markup (GTK_LABEL(W("lblSkeletonIntro")), msg);
	g_free (msg);
	if (!notes || *notes == '\0')
		msg = "<i>No notes available</i>";
	else
		msg = notes;
	gtk_label_set_markup (GTK_LABEL(W("lblNotes")), msg);

	g_free (shortname);
	g_free (notes); 
	gtk_widget_show (dlg);
}

void del_dependency_cb (GtkWidget *btn, gpointer user_data)
{
	GtkTreeModel *model;
	GtkTreeSelection *sel;
	GtkTreeIter iter;
	
	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (W("tree_selected_deps")));
	
	g_return_if_fail (sel != NULL);
	
	if (!gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		g_warning ("Nothing selected! Cannot remove dep");
		return;
	}
	gchar *rootname;
	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter,
					   SDEP_COL_ROOTNAME, &rootname, 
					   -1);
	gtk_list_store_remove(GTK_LIST_STORE (model), &iter);
	
	/* TODO: The GTK+ docs says this function is slow, but it doesn't tell the correct way to
	 * do it so we'll have to use it. The dataset is small and this is called infrequently anyway */
	if (!gtk_list_store_iter_is_valid (GTK_LIST_STORE(model), &iter))
	{
		gtk_tree_model_get_iter_first (GTK_TREE_MODEL(model), &iter);
		if (gtk_list_store_iter_is_valid (GTK_LIST_STORE(model), &iter))
			gtk_tree_selection_select_iter(sel, &iter);
	}
	else
		gtk_tree_selection_select_iter(sel, &iter);
	
	model = (GtkTreeModel*) get_deps_list_store();
	
	if (find_text_in_list_view (model, &iter, rootname, DEP_COL_ROOTNAME))
		gtk_list_store_set (GTK_LIST_STORE(model), &iter, 
							DEP_COL_VISIBLE, TRUE, 
							-1);
	else
		g_warning ("Couldn't re-show recently removed rootname '%s' in the dep list", rootname);
	
	g_free (rootname);
}

void add_dependency_cb (GtkWidget *btn, gpointer user_data)
{
	GtkTreeModel *model;
	GtkTreeSelection *sel;
	GtkTreeIter iter, child_iter;
	gchar *rootname;
	gchar *shortname;
	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (W("tree_deps")));

	g_return_if_fail (sel != NULL);

	if (!gtk_tree_selection_get_selected (sel, &model, &iter))
	{
		g_warning ("Nothing selected! Cannot add dep");
		return;
	}
	gtk_tree_model_get (model, &iter,
			    0, &shortname,
			    2, &rootname,
			    -1);
	
	GtkListStore *store = get_deps_list_store();
	CONVERT_PARENT_ITER(model, &child_iter, &iter);
	gtk_list_store_set(GTK_LIST_STORE(store), &child_iter, 
					   DEP_COL_VISIBLE, FALSE, 
					   -1);
	add_dependency (shortname, rootname);
	g_free (rootname);
	g_free (shortname);
}

void edit_prepare_section_cb (GtkWidget *btn, gpointer user_data)
{
	GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (W("txt_edit_prepare_script")));
	gchar *txt;
	
	init_custom_prepscript_dialog();
	
	txt = g_object_get_data (G_OBJECT(W("main_window")), "[Prepare]");
	if (txt == NULL)
		txt = "";
	gtk_text_buffer_set_text(buf,  txt, -1);
	gtk_widget_show (W("dlg_edit_prepare_script"));	
}

void deps_tree_dblclick_cb (GtkTreeView *treeview, GtkTreePath *arg1,
			    GtkTreeViewColumn *arg2, gpointer user_data)
{
	add_dependency_cb (W("btn_add_dep"), NULL);
}

gboolean tree_deps_keypress_cb (GtkWidget *w, GdkEventKey *ev, gpointer user_data)
{
	if (ev->keyval == GDK_Delete) {
		del_dependency_cb (W("btn_del_dep"), NULL);
		return TRUE; /* Don't propagate the event since we've handled it already */
	}
		
	return FALSE;
}

void tree_selected_deps_edited_cb (GtkCellRendererText *cell, const gchar *path_string,
				   const gchar *new_text, gpointer user_data)
{
	gint col = GPOINTER_TO_INT(user_data);
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (W("tree_selected_deps")));
	gchar *text;

	text = g_strdup (new_text);
	g_strstrip (text);
	if (*text != '\0' && !g_ascii_isdigit(*text))
		g_warning ("Version number [%s] doesn't start with a digit!!", text);
	/* FIXME: Maybe don't even set it if it is invalid? */
	gtk_tree_model_get_iter_from_string (model, &iter, path_string);
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    col, text,
			    -1);
	g_free (text);
}

void tree_selected_deps_toggled_cb (GtkCellRendererToggle *cell, gchar *path_string, gpointer user_data)
{
	gint col = GPOINTER_TO_INT(user_data);
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (W("tree_selected_deps")));
	gboolean checked;
	
	gtk_tree_model_get_iter_from_string (model, &iter, path_string);
	gtk_tree_model_get (model, &iter, col, &checked, -1);
	checked ^= 1;
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    col, checked,
			    -1);

}

void generate_script_cb (GtkWidget *btn, gpointer user_data)
{
	GtkWidget *dlg = W("dlg_scriptgen");

	gtk_widget_hide (W("hbox_cmd_error"));
	gtk_window_set_transient_for (GTK_WINDOW(dlg), GTK_WINDOW(W("main_window")));
	gtk_window_present (GTK_WINDOW(dlg));
}

void select_outputfile_cb (GtkWidget *btn, gpointer data)
{
	GtkWidget *fc;
	gchar *dir = gtk_editable_get_chars (GTK_EDITABLE(W("e_outputfile")), 0, -1);
	gchar *filename;
	
	/* GTK+ 2.4 */
	fc = gtk_file_chooser_dialog_new ("Select output filename",
					  GTK_WINDOW(W("main_window")),
					  GTK_FILE_CHOOSER_ACTION_SAVE,
					  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					  GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					  NULL);
	while (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		gchar *tmp = dir;
		dir = g_path_get_dirname (dir);
		g_free (tmp);
	}
	gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (fc), dir);
	if (gtk_dialog_run (GTK_DIALOG (fc)) == GTK_RESPONSE_ACCEPT)
	{
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (fc));
		g_free (dir);
		dir = g_path_get_basename (filename);
		set_project_directory (dir);
		gtk_widget_destroy (fc);
		gtk_entry_set_text (GTK_ENTRY(W("e_outputfile")), filename);
	}
	else
	{
		gtk_widget_destroy (fc);
		return;
	}
}

void view_outputfile_cb (GtkWidget *btn, gpointer data)
{
	if (finished_apspec == NULL)
		finished_apspec = create_apspec();
	
	init_preview_dialog ();
	fill_preview_dialog (finished_apspec);
	gtk_window_set_transient_for (GTK_WINDOW(W("dlg_preview_output")), GTK_WINDOW (W("main_window")));
	gtk_window_present (GTK_WINDOW(W("dlg_preview_output")));
}

void save_and_exit_cb (GtkWidget *btn, gpointer data)
{
	const gchar *filename;
	gchar *dir;
	GError *err = NULL;
	FILE *f;
	filename = gtk_entry_get_text (GTK_ENTRY(W("e_outputfile")));

	dir = g_path_get_dirname (filename);
	if (!create_directory (dir, &err))
	{
		gchar *msg = g_strconcat ("The file could not be saved in the directory you wanted. "
					  "The error reported was:\n<i>", err->message, "</i>", NULL);
		show_warning ("Couldn't save file", msg);
		return;
	}
	
	if (finished_apspec == NULL)
		finished_apspec = create_apspec();

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (W("chk_create_backup"))))
		backup_file (filename);
	if (!(f = fopen (filename, "w"))) {
		gchar *msg = g_strconcat ("The file could not be saved in the directory you wanted. "
					  "The error reported was:\n<i>", g_strerror(errno), "</i>", NULL);
		show_warning ("Couldn't save file", msg);
		return;
	}
	fputs (finished_apspec, f);
	fclose (f);

	gtk_main_quit();
}

void validate_entry_cb (GtkEditable *editable, gpointer user_data)
{
	gchar *txt = gtk_editable_get_chars (editable, 0, -1);
	const gchar *name = gtk_widget_get_name (GTK_WIDGET(editable));
	gboolean correct = TRUE;
	GtkWidget *img = GTK_WIDGET(user_data);

	g_assert (img != NULL);
	if (g_str_equal (name, "e_shortname"))
		correct = validate_shortname (txt);
	else if (g_str_equal (name, "e_rootname"))
		correct = validate_rootname (txt);

	trace ("Validating entry %s. Text: %s. (%s)\n", name, txt, correct ? "Ok" : "INVALID");
	if (!correct) {
		gtk_widget_modify_text (GTK_WIDGET(editable), GTK_STATE_NORMAL, &red);
		gtk_widget_show (img);
	} else {
		gtk_widget_modify_text (GTK_WIDGET(editable), GTK_STATE_NORMAL, &black);
		gtk_widget_hide (img);
	}
	g_free (txt);
}
		
void projectdir_changed_cb (GtkWidget *entry, gpointer user_data)
{
	const gchar *dir = gtk_entry_get_text (GTK_ENTRY(entry));

	trace ("projdir change_cb: %s :", dir);
	if (g_file_test(dir, G_FILE_TEST_IS_DIR))
	{
		set_project_directory (dir);
		gtk_widget_set_sensitive (W("btn_next"), TRUE);
		gchar *apspecdir = g_build_filename(dir, "autopackage", NULL);
		gchar *apspecfile = find_existing_apspec(apspecdir);
		if (apspecfile != NULL)
		{
			gchar *fmt = g_strconcat("<i>", "autopackage/", g_basename(apspecfile), "</i>", NULL);
			g_object_set_data_full (G_OBJECT(W("main_window")), "apspecfile", apspecfile, g_free);
			
			gtk_label_set_markup(GTK_LABEL(W("lbl_existing_apspec")), fmt);
			gtk_widget_show(W("hbox_existing_apspec"));
			g_free(fmt);
		}
		else
			gtk_widget_hide(W("hbox_existing_apspec"));
		trace ("OK\n");
	}
	else
	{
		gtk_widget_set_sensitive (W("btn_next"), FALSE);
		trace ("INVALID\n");
	}
}

void select_projdir_cb (GtkWidget *browse_button, gpointer data)
{
	GtkWidget *fc;
	gchar *dir;

	/* GTK+ 2.4 */
	fc = gtk_file_chooser_dialog_new ("Select project directory",
					  GTK_WINDOW(W("main_window")),
					  GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
					  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					  GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
					  NULL);
	gtk_window_set_transient_for (GTK_WINDOW(fc), GTK_WINDOW(W("main_window")));
	gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (fc), get_project_directory());

	if (gtk_dialog_run (GTK_DIALOG (fc)) == GTK_RESPONSE_ACCEPT)
	{
		dir = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (fc));
		gtk_entry_set_text (GTK_ENTRY(W("e_projectdir")), dir);
		g_free (dir);
	}

	gtk_widget_destroy (fc);
		
}
void e_outputfile_changed_cb (GtkWidget *entry, gpointer user_data)
{
	GtkWidget *btn = W("chk_create_backup");
	const gchar *text = gtk_entry_get_text (GTK_ENTRY(entry));
	
	g_return_if_fail (btn != NULL);
	
	gtk_widget_set_sensitive (btn, 
							  g_file_test (text, G_FILE_TEST_IS_REGULAR));
	gtk_widget_set_sensitive (W("btn_save_and_exit"), 
							  !g_file_test (text, G_FILE_TEST_IS_DIR));
	
}
