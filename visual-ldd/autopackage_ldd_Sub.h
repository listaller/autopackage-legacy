#ifndef AUTOPACKAGE_LDD_SUB_H
#define AUTOPACKAGE_LDD_SUB_H

#include <qlistview.h>
#include <qstatusbar.h>
#include <qfiledialog.h>
#include <qlabel.h>
#include <qstring.h>
#include <qdir.h>
#include <qfileinfo.h>
#include <qmessagebox.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include "myListViewItem.h"
#include "booleanListViewItem.h"




class autopackage_ldd_Sub : public autopackage_ldd
{
   Q_OBJECT
   
   std::vector <QString> vectorLdConfig;
   QLabel *statusLabel;
   
   public:
       autopackage_ldd_Sub( QWidget* parent = 0, const char* name = 0, WFlags fl = 0, QString argFilename = 0);
       ~autopackage_ldd_Sub();

   void resolver(booleanListViewItem* itemChild);
   QString findFullPath(QString soname);
   void loadFile(QString filename);
	void traverse(const QString& dirname);
   
	public slots:
       void fileOpen();
		 void aboutMenuItemSlot();
		 void aboutQtMenuItemSlot();
       void findChildSlot(QListViewItem *);          /* called when an expansion of a root is made */
       void deleteChildrenSlot(QListViewItem *);    /* called when a collapse of a root is made */
       void rightButtonPressed(QListViewItem *, const QPoint &, int );
       void expandAll();
       void collapseAll();
       void dumpTree();
       
   private:
       QString lastFileName;
};

#endif // AUTOPACKAGE_LDD_SUB_H
