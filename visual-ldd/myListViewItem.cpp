#include "myListViewItem.h"


myListViewItem::myListViewItem( QListView * parent,
                   QString s1,     QString s3  ,
                   QString s4  , QString s5  ,
                   QString s6  , QString s7  ,
                   QString s8  , QString s9   )
              :booleanListViewItem(parent, s1, s3)
{
   ;
}

myListViewItem::myListViewItem( QListViewItem * parent,
                   QString s1,     QString s3  ,
                   QString s4  , QString s5  ,
                   QString s6  , QString s7  ,
                   QString s8  , QString s9   )
              :booleanListViewItem(parent, s1, s3)
{
   ;
}


myListViewItem::myListViewItem(myListViewItem * parent )
               :booleanListViewItem(parent)
{
;
}
  
void myListViewItem::paintCell(QPainter * p, const QColorGroup & cg, int column, int width, int align)
{

/*Override QListViewItem::paintCell()
Create a new QColorGroup within and call the parent's method with
the new QColorGroup.
*/
    QColorGroup newCg = cg;
  /*  if (isSelected()) 
    {
        newCg.setColor(QColorGroup::HighlightedText, Qt::blue);
        newCg.setColor(QColorGroup::Highlight, Qt::red);
    }
*/    
    newCg.setColor(QColorGroup::Text, Qt::red);
    QListViewItem::paintCell(p,newCg,column,width,align);

}

