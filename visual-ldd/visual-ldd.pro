TEMPLATE	= app

CONFIG	+= qt thread warn_on debug

HEADERS	+= autopackage_ldd_Sub.h \
	read_elf.h \
	myListViewItem.h \
	booleanListViewItem.h

SOURCES	+= main.cpp \
	autopackage_ldd_Sub.cpp \
	read_elf.cpp \
	myListViewItem.cpp \
	booleanListViewItem.cpp

FORMS	= autopackage_ldd.ui

IMAGES	= images/about \
	images/editcopy \
	images/editcut \
	images/editpaste \
	images/filenew \
	images/fileopen \
	images/filesave \
	images/print \
	images/redo \
	images/searchfind \
	images/undo \
	images/exit

unix {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
}
