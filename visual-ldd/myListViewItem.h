#ifndef MY_LIST_VIEW_ITEM_H
#define MY_LIST_VIEW_ITEM_H

#include <qlistview.h>
#include <qstring.h>
#include <string>
#include <iostream>
#include "booleanListViewItem.h"


class myListViewItem : public booleanListViewItem
{
   //Q_OBJECT
   public:
     
  myListViewItem( QListView * parent,
		   QString,     QString,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null );
    
    myListViewItem( QListViewItem * parent,
		   QString,     QString,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null );
   
      myListViewItem(myListViewItem * parent );
      
      void paintCell ( QPainter * p, const QColorGroup & cg, int column, int width, int align );
};

#endif 

