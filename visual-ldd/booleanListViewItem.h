#ifndef BOOLEAN_LIST_VIEW_ITEM_H
#define BOOLEAN_LIST_VIEW_ITEM_H

#include <qlistview.h>
#include <qstring.h>
#include <string>
#include <iostream>


class booleanListViewItem : public QListViewItem
{
   
   
   
   public:
     
     bool beenExpanded;  
     
     booleanListViewItem( QListView * parent,
		   QString,     QString,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null );
    
    booleanListViewItem( QListViewItem * parent,
		   QString,     QString,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null,
		   QString = QString::null, QString = QString::null );
   
      booleanListViewItem(booleanListViewItem * parent );
      
};

#endif 

