#include "booleanListViewItem.h"


booleanListViewItem::booleanListViewItem( QListView * parent,
                   QString s1,     QString s3  ,
                   QString s4  , QString s5  ,
                   QString s6  , QString s7  ,
                   QString s8  , QString s9   )
              :QListViewItem(parent, s1, s3)
{
   beenExpanded = false;
}

booleanListViewItem::booleanListViewItem( QListViewItem * parent,
                   QString s1,     QString s3  ,
                   QString s4  , QString s5  ,
                   QString s6  , QString s7  ,
                   QString s8  , QString s9   )
              :QListViewItem(parent, s1, s3)
{
   beenExpanded = false;
}


booleanListViewItem::booleanListViewItem(booleanListViewItem * parent )
               :QListViewItem(parent)
{
   beenExpanded = false;
}
