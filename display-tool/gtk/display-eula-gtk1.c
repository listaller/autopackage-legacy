/*
 * EULA display program written in GTK+ 1.
 * Written by Hongli Lai and Taj Morton.
 * This software is public domain, do whatever you want with it.
 */
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>


GtkWidget *win;
GtkWidget *vbox;
GtkWidget *scroll;
GtkWidget *text;
GtkWidget *hbox;
GtkWidget *btnbox;
GtkWidget *button;


static void
exit_deny ()
{
	exit (1);
}


static void
exit_accept ()
{
	exit (0);
}


static void
load_license (const char *filename)
{
	FILE *f;
	char buf[1024 * 32];

	f = fopen (filename, "r");
	if (!f) {
		g_printerr ("Unable to open license file: %s\n", filename);
		exit (5);
	}

	gtk_text_freeze (GTK_TEXT (text));
	while (!feof (f)) {
		size_t bread;

		bread = fread (&buf, 1, sizeof (buf), f);
		gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, buf, bread);
	}
	gtk_text_thaw (GTK_TEXT (text));
	fclose (f);
}


int
main (int argc, char *argv[])
{
	if (argc < 3) {
		g_printerr ("Usage: %s <MODE> <EULA-FILENAME>\n", argv[0]);
		g_printerr ("Mode is:\n");
		g_printerr ("\t* --eula: Display I Agree/I Disagree\n");
		g_printerr ("\t* --notes: Display OK\n");
		return 127;
	}
	
	short unsigned int eulaMode;
	/* 0 == notes
	 * 1 == eula
	*/
	
	if (strcmp(argv[1],"--eula")==0) {
		eulaMode=1;
	}
	else if (strcmp(argv[1],"--notes")==0) {
		eulaMode=0;
	}
	else {
		g_printerr("Unknown MODE %s. Should be --eula or --notes\n", argv[1]);
		return 127;
	}

	/* Window */
	gtk_init (&argc, &argv);
	win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	
	if (eulaMode)
		gtk_window_set_title (GTK_WINDOW (win), "License Agreement");
	else
		gtk_window_set_title (GTK_WINDOW (win), "Release Notes");
	
	gtk_window_set_default_size (GTK_WINDOW (win), 525, 400);
	gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_CENTER);
	gtk_container_set_border_width (GTK_CONTAINER (win), 6);
	gtk_signal_connect (GTK_OBJECT (win), "delete_event",
		GTK_SIGNAL_FUNC (gtk_true), NULL);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (win), vbox);

	/* Text display */
	scroll = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);

	text = gtk_text_new (
		gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (scroll)),
		gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (scroll)));
	gtk_container_add (GTK_CONTAINER (scroll), text);
	load_license (argv[2]);

	/* Button boxes */
	hbox = gtk_hbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

	if (eulaMode) {
		/* Deny */
		btnbox = gtk_hbutton_box_new ();
		gtk_button_box_set_layout (GTK_BUTTON_BOX (btnbox), GTK_BUTTONBOX_START);
		gtk_box_pack_start (GTK_BOX (hbox), btnbox, TRUE, TRUE, 0);
	
		button = gtk_button_new_with_label ("Deny License");
		gtk_container_add (GTK_CONTAINER (btnbox), button);
		gtk_signal_connect (GTK_OBJECT (button), "clicked",
			GTK_SIGNAL_FUNC (exit_deny), NULL);
	}
	
	/* Accept */
	btnbox = gtk_hbutton_box_new ();
	if (eulaMode)
		gtk_button_box_set_layout (GTK_BUTTON_BOX (btnbox), GTK_BUTTONBOX_END);
	else
		gtk_button_box_set_layout (GTK_BUTTON_BOX (btnbox), GTK_BUTTONBOX_SPREAD);
	
	gtk_box_pack_start (GTK_BOX (hbox), btnbox, TRUE, TRUE, 0);

	if (eulaMode)
		button = gtk_button_new_with_label ("Accept License");
	else
		button = gtk_button_new_with_label ("OK");
	
	gtk_container_add (GTK_CONTAINER (btnbox), button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
		GTK_SIGNAL_FUNC (exit_accept), NULL);

	gtk_widget_grab_focus (text);
	gtk_widget_show_all (win);
	gtk_main ();
	return 0;
}
