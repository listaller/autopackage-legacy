/***************************************************************************
 *   Copyright (C) 2007 by Taj Morton                                      *
 *   tajmorton@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/*
  RETURN CODES:
  This app will return the following:
  * 0 if user accepted agreement
  * 1 if user did not accept agreement (or closed the window)
  * 5 if there was an error opening the file
*/

#include <qapplication.h>
#include "eulawindow.h"

void printUsage() {
	printf("display-eula-qt USAGE: display-eula-qt <MODE> <FILENAME>\n");
	printf("MODE is:\n");
	printf("\t* --eula: Display I Agree/I Disagree\n");
	printf("\t* --notes: Display OK\n");
}

int main(int argc, char *argv[]) {
	if (argc<3) {
		printUsage();
		return 127;
	}
	
	QApplication app(argc, argv);
	
	bool eulaMode;
	if (strcmp(app.argv()[1],"--eula")==0) {
		eulaMode=true;
	}
	else if (strcmp(app.argv()[1],"--notes")==0) {
		eulaMode=false;
	}
	else {
		printUsage();
		return 127;
	}
	
	EulaWindow window(eulaMode,0);
	if (window.loadFile(QString(argv[2])))
		window.show();
	else
		return 5;
	
	return app.exec();
}
