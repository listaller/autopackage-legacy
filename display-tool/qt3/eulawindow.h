/***************************************************************************
 *   Copyright (C) 2007 by Taj Morton                                      *
 *   tajmorton@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef HAVE_MAINWIN_H
#define HAVE_MAINWIN_H

#include <qwidget.h>
#include <qtextedit.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <qevent.h>

class EulaWindow : public QWidget {
	Q_OBJECT
	public:
		EulaWindow(const bool eulaMode, QWidget * parent = 0);
		~EulaWindow() { };
		
		bool loadFile(QString fileName);
	
	private:
		QTextEdit *textedit;
		QPushButton *yesButton, *noButton, *okButton;
		
		virtual void closeEvent(QCloseEvent * e);
	
	private slots:
		void yesButtonPressed();
		void noButtonPressed();
};

#endif
