/***************************************************************************
 *   Copyright (C) 2007 by Taj Morton                                      *
 *   tajmorton@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qapplication.h>
#include <qfile.h>
#include <qlayout.h>
#include <qmessagebox.h>
#include <qdesktopwidget.h>

#include "eulawindow.h"

EulaWindow::EulaWindow(const bool eulaMode, QWidget * parent) : QWidget(parent) {
	textedit = new QTextEdit(this);
	textedit->setReadOnly(true);
	
	if (eulaMode)
		setCaption("License Agreement");
	else
		setCaption("Release Notes");
	
	if (eulaMode) {
		yesButton = new QPushButton("Accept License", this);
		noButton = new QPushButton("Deny License", this);
		
		connect(yesButton, SIGNAL(clicked()), this, SLOT(yesButtonPressed()));
		connect(noButton, SIGNAL(clicked()), this, SLOT(noButtonPressed()));
	} else {
		okButton = new QPushButton("OK", this);
		connect(okButton, SIGNAL(clicked()), this, SLOT(yesButtonPressed()));
	}
	
	QVBoxLayout *mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(textedit);
	
	if (eulaMode) {
		QHBoxLayout *buttonLayout = new QHBoxLayout;
		buttonLayout->addWidget(noButton);
		buttonLayout->addWidget(yesButton);
		buttonLayout->setSpacing(4);
		mainLayout->addLayout(buttonLayout);
	}
	else
		mainLayout->addWidget(okButton);
	
	mainLayout->setSpacing(4);
	
	resize(525,400);
	

	int left=(QApplication::desktop()->width()-width())/2;
	int top=(QApplication::desktop()->height()-height())/2;
	move(left,top);
}

bool EulaWindow::loadFile(QString fileName) {
	QFile file(fileName);
	if (file.exists()) {
		if (file.open(IO_ReadOnly)) {
			textedit->setText(file.readAll());
		}
		else {
			QMessageBox::warning(this, caption(), "Could not open file ("+fileName+").", QMessageBox::Ok, QMessageBox::NoButton);
			return false;
		}
	}
	else {
		QMessageBox::warning(this, caption(), "File ("+fileName+") could not be found.", QMessageBox::Ok, QMessageBox::NoButton);
		return false;
	}
	return true;
}

void EulaWindow::yesButtonPressed() {
	QApplication::exit(0);
}

void EulaWindow::noButtonPressed() {
	QApplication::exit(1);
}

void EulaWindow::closeEvent(QCloseEvent * e) {
	QMessageBox::warning(this,caption(),"You must accept or deny this license.",QMessageBox::Ok, QMessageBox::NoButton);
	e->ignore();
}
